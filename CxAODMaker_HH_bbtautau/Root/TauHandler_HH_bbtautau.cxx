#include <iostream>

#include "CxAODMaker/TauHandler.h"
#include "CxAODMaker/EventInfoHandler.h"

#include "CxAODMaker_HH_bbtautau/TauHandler_HH_bbtautau.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"

#include "xAODTau/TauxAODHelpers.h"
#include "xAODTrigger/JetRoIContainer.h"

TauHandler_HH_bbtautau::TauHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent *event,
               EventInfoHandler & eventInfoHandler) :
  TauHandler (name, config, event, eventInfoHandler)
{
  using std::placeholders::_1; 
  m_selectFcns.clear(); 
  m_selectFcns.push_back(std::bind(&TauHandler_HH_bbtautau::LooseTau, this, _1)); 
  m_selectFcns.push_back(std::bind(&TauHandler_HH_bbtautau::passHH_bbtautauTau, this, _1));

  m_config.getif<bool>("MatchL1Jets", m_match_L1_jets);
}

bool TauHandler_HH_bbtautau::LooseTau(xAOD::TauJet * tau) {

  bool antitau = false;
  bool fake = false;
  m_config.getif<bool>("AntiTau",antitau);
  m_config.getif<bool>("Fake",fake);
  if (fake) antitau=true;
  bool passSel = true;
  if (passSel) m_cutflow->count("All");
  if( tau->pt() < 20000. ) passSel=false;
  if (passSel) m_cutflow->count("Pt Cut");
  if(  fabs( tau->eta()  ) > 2.5 ) passSel=false;
  if (passSel) m_cutflow->count("Eta Cut");
  if (!(Props::passTauSelector.get(tau))) passSel = false;
  if (passSel) m_cutflow->count("TauSelector");

  std::string selName="";
  m_config.getif<std::string>("selectionName",selName);

  if(selName=="lephad" && antitau && !Props::isAntiTau.get(tau)) passSel = false;

  if (m_debug) {
    std::cout << "HH: TauHandler pt="
	    << tau->pt()/1000.
	    << " eta=" << tau->eta()
	    << " phi=" << tau->phi()
	    << " " << Props::passTauSelector.get(tau)
	    << " passSel=" 
	    << passSel;

    std::cout << " BDT="
	    << Props::BDTScore.get(tau) 
	    << " " << Props::isBDTLoose.get(tau)
	    << " " << Props::isBDTMedium.get(tau)
	    << " " << Props::isBDTTight.get(tau);

    if (Props::RNNScore.exists(tau)) {
      std::cout << " RNN="
                << Props::RNNScore.get(tau)
                << " " << Props::isRNNLoose.get(tau)
                << " " << Props::isRNNMedium.get(tau)
                << " " << Props::isRNNTight.get(tau);
    }
  }
  Props::passPreSel.set(tau,passSel);
  Props::isHHLooseTau.set(tau,passSel);
  if(selName=="boosted"){
    //Set to false for boosted analysis (don't use resolved taus in the MET calculation)
    Props::forMETRebuild.set(tau, false);//Using this for MET rebuilding
  }else{
    Props::forMETRebuild.set(tau, passSel);//Using this for MET rebuilding
  }
  Props::isHHRandomTau.set(tau, false); // For later
  return passSel;
}
bool TauHandler_HH_bbtautau::passHH_bbtautauTau(xAOD::TauJet * tau)
{
  // Same as Loose Taus
  bool passSel = true;
  if (!(Props::passPreSel.get(tau))) passSel = false;
  //if (!(Props::isBDTMedium.get(tau))) passSel = false; //Signal Region (not in the config anymore so that we play around with CRs)

  if (m_debug) std::cout << " passed=" << passSel 
			 << std::endl;

  Props::isHHSignalTau.set(tau, passSel);
  if (passSel) m_cutflow->count("bbtautau");
  // std::cout << "HH:   passHH_bbtautauTau=" << passSel << std::endl;
  return passSel;
}

EL::StatusCode TauHandler_HH_bbtautau::writeCustomVariables(xAOD::TauJet* inTau, xAOD::TauJet* outTau, bool /*isKinVar*/, bool isWeightVar,const TString& /*sysName*/)
{
  // set something without having a pre-defined method
  if(!isWeightVar){
    Props::isHHLooseTau.copy(inTau, outTau);
    Props::isHHSignalTau.copy(inTau, outTau);
    Props::isHHRandomTau.copy(inTau, outTau);
    Props::TauORJetPt.copyIfExists(inTau, outTau);
    Props::TauORJetEta.copyIfExists(inTau, outTau);
    Props::TauORJetPhi.copyIfExists(inTau, outTau);
    Props::TauORJetE.copyIfExists(inTau, outTau);
  }
  return EL::StatusCode::SUCCESS;

}

EL::StatusCode TauHandler_HH_bbtautau::writeNominal(xAOD::TauJet* inTau, xAOD::TauJet* outTau, const TString& sysName) {
  EL_CHECK("TauHandler::writeNominal()", TauHandler::writeNominal(inTau, outTau, sysName));

  Props::seedJetWidth.copyIfExists(inTau, outTau);
  Props::seedJetJvt.copyIfExists(inTau, outTau);

  const xAOD::Jet *inJet  = inTau->jet();
  const bool WriteTrackWidthVector = false;
  auto ProcessTrackWidths = [&] (PROP< std::vector<float> > &TrackWidths, PROP< float > &TrackWidthPV0, PROP< float > &TrackWidthPVn) -> void
    {
      if(TrackWidths.exists(inJet))
	{
	  const std::vector<float> Widths = TrackWidths.get(inJet);

	  if(true == WriteTrackWidthVector)
	    {
	      TrackWidths.set(outTau, Widths);
	    };

	  TrackWidthPV0.set(outTau, Widths[0]);
	  TrackWidthPVn.set(outTau, Widths[inTau->vertex()->index()]);
	};
    };

  ProcessTrackWidths(Props::TrackWidthPt500, Props::TrackWidthPt500PV0, Props::TrackWidthPt500PVn);
  ProcessTrackWidths(Props::TrackWidthPt1000, Props::TrackWidthPt1000PV0, Props::TrackWidthPt1000PVn);

  Props::L1_match_J12.copyIfExists(inTau, outTau);
  Props::L1_match_J12_0ETA23.copyIfExists(inTau, outTau);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TauHandler_HH_bbtautau::writeKinematicVariations(xAOD::TauJet *inTau, xAOD::TauJet *outTau, const TString &sysName) {
  EL_CHECK("TauHandler::writeKinematicVariations()", TauHandler::writeKinematicVariations(inTau, outTau, sysName));

  Props::MCTruthClfOrigin.copy(inTau, outTau);
  Props::MCTruthClfType.copy(inTau, outTau);
  Props::TruthJetPartonID.copy(inTau, outTau);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TauHandler_HH_bbtautau::removeTausExcept(int index, int index2) {
  // What decides if an object is saved to the output file?
  for (std::pair<TString, xAOD::TauJetContainer*> tauSet : m_inContainer) {
    for (const auto& tau : *tauSet.second) {
      if (((int) tau->index() != index) && ((int) tau->index() != index2 || index2 == -1)) {
	Props::passPreSel.set(tau, false);
	Props::isHHLooseTau.set(tau,false);
	Props::isHHSignalTau.set(tau, false);
	Props::forMETRebuild.set(tau, false);
	Props::isHHRandomTau.set(tau, false);
      } else {
	Props::isHHRandomTau.set(tau, true);
      }
    }
  }
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode TauHandler_HH_bbtautau::initializeTools()
{
  EL_CHECK("TauHandler::initializeTools()",TauHandler::initializeTools());

  std::string selectionName="";
  m_config.getif<std::string>("selectionName", selectionName);

  std::unordered_map<std::string, PROP<int>*> triggersToMatch;

  //Triggers to match are selection dependent
  if(selectionName == "hadhad"){
      triggersToMatch.insert(
     {
      {"HLT_tau25_medium1_tracktwo", &Props::matchHLT_tau25_medium1_tracktwo },
      {"HLT_tau35_medium1_tracktwo", &Props::matchHLT_tau35_medium1_tracktwo },
      {"HLT_tau80_medium1_tracktwo_L1TAU60", &Props::matchHLT_tau80_medium1_tracktwo_L1TAU60 },
      {"HLT_tau125_medium1_tracktwo", &Props::matchHLT_tau125_medium1_tracktwo },
      {"HLT_tau160_medium1_tracktwo", &Props::matchHLT_tau160_medium1_tracktwo },
      {"HLT_tau160_medium1_tracktwo_L1TAU100", &Props::matchHLT_tau160_medium1_tracktwo_L1TAU100 },
      {"HLT_tau160_medium1_tracktwoEF_L1TAU100", &Props::matchHLT_tau160_medium1_tracktwoEF_L1TAU100},
      {"HLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &Props::matchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100},

      {"HLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo", &Props::matchHLT_tau35_loose1_tracktwo_tau25_loose1_tracktwo },
      {"HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1DR_TAU20ITAU12I_J25},
      {"HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo },
      {"HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_03dR30_L1DR_TAU20ITAU12I_J25 },
      {"HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_03dR30_L1DR_TAU20ITAU12I_J25},
      {"HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dR30_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dR30_L1DR_TAU20ITAU12I_J25},
      {"HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR_TAU20ITAU12I_J25 },
      {"HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM },
      {"HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1DR-TAU20ITAU12I-J25", &Props::matchHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1DR_TAU20ITAU12I_J25},
      {"HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM_4J12", &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM_4J12},
      {"HLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1TAU20IM_2TAU12IM_4J12.0ETA23", &Props::matchHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1TAU20IM_2TAU12IM_4J12_0ETA23},
      {"HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1TAU20IM_2TAU12IM_4J12.0ETA23", &Props::matchHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1TAU20IM_2TAU12IM_4J12_0ETA23},
      {"HLT_tau40_medium1_tracktwoEF_tau35_medium1_tracktwoEF", &Props::matchHLT_tau40_medium1_tracktwoEF_tau35_medium1_tracktwoEF},
      {"HLT_tau40_medium1_tracktwo_tau35_medium1_tracktwo", &Props::matchHLT_tau40_medium1_tracktwo_tau35_medium1_tracktwo },
      {"HLT_tau40_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA", &Props::matchHLT_tau40_mediumRNN_tracktwoMVA_tau35_mediumRNN_tracktwoMVA},
      {"HLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12", &Props::matchHLT_tau80_medium1_TAU60_tau50_medium1_L1TAU12 },
      {"HLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I", &Props::matchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau35_medium1_tracktwoEF_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I},
      {"HLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &Props::matchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40},
      {"HLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I", &Props::matchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I },
      {"HLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &Props::matchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 },
      {"HLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &Props::matchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 },
      {"HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I", &Props::matchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau35_mediumRNN_tracktwoMVA_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I},
      {"HLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &Props::matchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40}
     }
      );

  } else if(selectionName == "lephad"){
      triggersToMatch.insert( 
     {{"HLT_e17_lhmedium_tau25_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_tau25_medium1_tracktwo },
      {"HLT_e17_lhmedium_iloose_tau25_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_iloose_tau25_medium1_tracktwo },
      {"HLT_e17_lhmedium_nod0_tau25_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo},
      {"HLT_e17_medium_iloose_tau25_medium1_tracktwo", &Props::matchHLT_e17_medium_iloose_tau25_medium1_tracktwo },
      {"HLT_e17_medium_tau80_medium1_tracktwo", &Props::matchHLT_e17_medium_tau80_medium1_tracktwo },
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR-EM15TAU12I-J25", &Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR_EM15TAU12I_J25},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo},
      {"HLT_e17_lhmedium_nod0_iloose_tau35_medium1_tracktwo", &Props::matchHLT_e17_lhmedium_nod0_iloose_tau35_medium1_tracktwo},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12", &Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12},
      {"HLT_mu14_tau25_medium1_tracktwo", &Props::matchHLT_mu14_tau25_medium1_tracktwo },
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwo", &Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwo },
      {"HLT_mu14_iloose_tau25_medium1_tracktwo", &Props::matchHLT_mu14_iloose_tau25_medium1_tracktwo },
      {"HLT_mu14_tau35_medium1_tracktwo_L1TAU20", &Props::matchHLT_mu14_tau35_medium1_tracktwo_L1TAU20 },
      {"HLT_mu14_tau35_medium1_tracktwo", &Props::matchHLT_mu14_tau35_medium1_tracktwo },
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR-MU10TAU12I_TAU12I-J25", &Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1DR_MU10TAU12I_TAU12I_J25},
      {"HLT_mu14_ivarloose_tau35_medium1_tracktwo_L1MU10_TAU20IM_J25_2J20",&Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwo_L1MU10_TAU20IM_J25_2J20},
      {"HLT_mu14_ivarloose_tau35_medium1_tracktwo",&Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwo},
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12",&Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwo_L1MU10_TAU12IM_3J12},

      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA},
      {"HLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwoEF",&Props::matchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwoEF},
      {"HLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA",&Props::matchHLT_e24_lhmedium_nod0_ivarloose_tau35_mediumRNN_tracktwoMVA},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1DR-EM15TAU12I-J25",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1DR_EM15TAU12I_J25},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR-EM15TAU12I-J25",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR_EM15TAU12I_J25},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1EM15VHI_2TAU12IM_4J12",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1EM15VHI_2TAU12IM_4J12},
      {"HLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1EM15VHI_2TAU12IM_4J12",&Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1EM15VHI_2TAU12IM_4J12},

      {"HLT_mu14_ivarloose_tau35_medium1_tracktwoEF",&Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF},
      {"HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA",&Props::matchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA},
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwoEF",&Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF},
      {"HLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA",&Props::matchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA},
      {"HLT_mu14_ivarloose_tau35_medium1_tracktwoEF_L1MU10_TAU20IM_J25_2J20",&Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF_L1MU10_TAU20IM_J25_2J20},
      {"HLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA_L1MU10_TAU20IM_J25_2J20",&Props::matchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA_L1MU10_TAU20IM_J25_2J20},
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwoEF_L1DR-MU10TAU12I_TAU12I-J25",&Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_L1DR_MU10TAU12I_TAU12I_J25},
      {"HLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR-MU10TAU12I_TAU12I-J25",&Props::matchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR_MU10TAU12I_TAU12I_J25},
      {"HLT_mu14_ivarloose_tau25_medium1_tracktwoEF_L1MU10_TAU12IM_3J12",&Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwoEF_L1MU10_TAU12IM_3J12},
      {"HLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_L1MU10_TAU12IM_3J12",&Props::matchHLT_mu14_ivarloose_tau25_mediumRNN_tracktwoMVA_L1MU10_TAU12IM_3J12},

      {"HLT_tau25_medium1_tracktwo", &Props::matchHLT_tau25_medium1_tracktwo },
      {"HLT_tau35_medium1_tracktwo", &Props::matchHLT_tau35_medium1_tracktwo }
     } 
      );
  }
  m_triggersToMatch = triggersToMatch;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TauHandler_HH_bbtautau::decorate(xAOD::TauJet *tau) {
  EL_CHECK("TauHandler::decorate()", TauHandler::decorate(tau));

  // Adding decorations for tau truth studies
  Props::MCTruthClfOrigin.set(tau, -999);
  Props::MCTruthClfType.set(tau, -999);
  Props::TruthJetPartonID.set(tau, -999);

  if (m_eventInfoHandler.get_isMC()) {
    // MCTruthClassifier information of TruthParticle associated to tau
    const xAOD::TruthParticle *part =
      xAOD::TauHelpers::getLink<xAOD::TruthParticle>(tau, "truthParticleLink");
    if (part) {
      if (part->isAvailable<unsigned>("classifierParticleOrigin")) {
        const int origin = part->auxdata<unsigned>("classifierParticleOrigin");
        Props::MCTruthClfOrigin.set(tau, origin);
      } else {
        Warning("TauHandler_HH_bbtautau::decorate", "classifierParticleOrigin unavailable for TruthParticle");
      }
      if (part->isAvailable<unsigned>("classifierParticleType")) {
        const int type = part->auxdata<unsigned>("classifierParticleType");
        Props::MCTruthClfType.set(tau, type);
      } else {
        Warning("TauHandler_HH_bbtautau::decorate", "classifierParticleType unavailable for TruthParticle");
      }
    }

    // PartonTruthLabel ID of TruthJet associated to tau
    const xAOD::Jet *jet =
      xAOD::TauHelpers::getLink<xAOD::Jet>(tau, "truthJetLink");
    if (jet) {
      int pdgid;
      const auto success = jet->getAttribute<int>("PartonTruthLabelID", pdgid);
      if (success) {
        Props::TruthJetPartonID.set(tau, pdgid);
      } else {
        Warning("TauHandler_HH_bbtautau::decorate", "PartonTruthLabelID unavailable for Jet");
      }
    }
  }

  if (m_match_L1_jets) {
    EL_CHECK("TauHandler_HH_bbtautau::decorate()", matchL1Jet(tau));
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TauHandler_HH_bbtautau::matchL1Jet(xAOD::TauJet *tau) {
  // Matching of taus to L1 jet RoIs
  // - Only matching J12 for 4J12 trigger
  const xAOD::JetRoIContainer *jet_rois = nullptr;
  EL_CHECK("JetHandler_HH_bbtautau::matchL1Jets", m_event->retrieve(jet_rois, "LVL1JetRoIs"));

  const float match_dR = 0.4;
  bool match_J12 = false;
  bool match_J12_0ETA23 = false;

  for (const auto roi : *jet_rois) {
    if (roi->etLarge() <= 12000 || fabs(roi->eta()) > 3.2) {
      continue;
    }

    TLorentzVector roi_p4;
    roi_p4.SetPtEtaPhiM(roi->etLarge(), roi->eta(), roi->phi(), 1.0);

    const auto dR_offl_tau = roi_p4.DeltaR(tau->p4());
    if (dR_offl_tau < match_dR) {
      match_J12 = true;

      if (fabs(roi->eta()) < 2.3) {
        match_J12_0ETA23 = true;
      }
    }
  }

  Props::L1_match_J12.set(tau, match_J12);
  Props::L1_match_J12_0ETA23.set(tau, match_J12_0ETA23);

  return StatusCode::SUCCESS;
}
