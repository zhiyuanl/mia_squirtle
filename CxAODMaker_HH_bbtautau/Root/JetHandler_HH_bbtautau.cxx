#include <iostream>

// jet specific includes
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTrigger/JetRoIContainer.h"

#include "CxAODMaker_HH_bbtautau/JetHandler_HH_bbtautau.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"

JetHandler_HH_bbtautau::JetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                       EventInfoHandler & eventInfoHandler) :
  JetHandler(name, config, event, eventInfoHandler),
  m_btagtool(0)
{
  using std::placeholders::_1;
  m_selectFcns.clear(); 
  m_selectFcns.push_back(std::bind( &JetHandler_HH_bbtautau::passVetoJet, this, _1));
  m_selectFcns.push_back(std::bind( &JetHandler_HH_bbtautau::passSignalJet, this, _1));

  m_config.getif<std::string>("BTagCDI", m_btag_cdi);
  m_config.getif<std::string>("BTagJetCollection", m_btag_jet_collection);
  m_config.getif<std::string>("BTagTagger", m_btag_tagger);
  m_config.getif<std::string>("BTagWP", m_btag_wp);
  m_config.getif<bool>("MatchL1Jets", m_match_L1_jets);
}

JetHandler_HH_bbtautau::~JetHandler_HH_bbtautau(){
  //delete tools
  delete m_btagtool;
}

EL::StatusCode JetHandler_HH_bbtautau::initializeTools()
{
  //EL::StatusCode JetHandler::initializeTools();
  EL_CHECK("JetHandler::initializeTools()",JetHandler::initializeTools());

  m_btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
  m_btagtool->msg().setLevel( m_msgLevel );
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("MaxEta", 2.5));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("MinPt", 20000.));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("JetAuthor", m_btag_jet_collection));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("TaggerName", m_btag_tagger));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("FlvTagCutDefinitionsFileName", m_btag_cdi));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->setProperty("OperatingPoint", m_btag_wp));
  TOOL_CHECK("JetHandler()::initializeTools()",m_btagtool->initialize());

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetHandler_HH_bbtautau::decorate(xAOD::Jet * jet) 
{

  EL_CHECK("JetHandler()::decorate()",JetHandler::decorate(jet));

  // set this to something in case the IdentifyTruthParton or IdentifyVBFJets functions are not called
  // otherwise, if the matching is disabled, the code will crash
  int iTruthPartonLabelID = -1;
  if (m_isMC && jet->isAvailable<int>("PartonTruthLabelID"))
    iTruthPartonLabelID = jet->getAttribute<int>("PartonTruthLabelID");
  Props::TruthPartonLabelID.set(jet, iTruthPartonLabelID );
  Props::isVBFTruth.set(jet, -1);

  // set isBTag flag
  bool isBtagged = m_btagtool->accept(  *jet );
  Props::isBJet.set(jet, isBtagged);

  // Do L1 jet matching
  if (m_match_L1_jets) {
    EL_CHECK("JetHandler_HH_bbtautau::decorate()", matchL1Jet(jet));
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode JetHandler_HH_bbtautau::writeNominal(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName) {
  EL_CHECK("JetHandler()::writeNominal()", JetHandler::writeNominal(inJet, outJet, sysName));

  Props::L1_match_J12.copyIfExists(inJet, outJet);
  Props::L1_match_J12_0ETA23.copyIfExists(inJet, outJet);
  Props::L1_match_J25.copyIfExists(inJet, outJet);

  return StatusCode::SUCCESS;
}

EL::StatusCode JetHandler_HH_bbtautau::writeKinematicVariations(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName)
{
  EL_CHECK("JetHandler()::writeKinematicVariations()",JetHandler::writeKinematicVariations(inJet,outJet,sysName));

  Props::isBJet.copy(inJet, outJet);

  return EL::StatusCode::SUCCESS;
}

bool JetHandler_HH_bbtautau::passVetoJet(xAOD::Jet* jet)
{
  bool passCentral = checkCentralJet(jet,true);
  
  bool passForward = true;
  
  if(passForward) m_cutflow->count("Forward jet input",200);
  if (!(jet->pt() > 30000.)) passForward = false;
  if (passForward) m_cutflow->count("Forward jet pt",201);
  if (!(fabs(jet->eta()) >= 2.5)) passForward = false;
  if (!(fabs(jet->eta()) < 4.5)) passForward = false;
  if (passForward) m_cutflow->count("Forward jet |eta|",202);
  if (!Props::PassFJvtLoose.get(jet)) passForward = false;
  if (passForward) m_cutflow->count("Forward jet selected",203);
  
  bool passSel = true;
  if (passSel) m_cutflow->count("VetoJet input", 300);

  // JVT TWiki with recommendation: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibrationRel21
  if(!(Props::PassJvtMedium.get(jet))) passSel = false;

  if (passSel) m_cutflow->count("JVT");
  if (!(passCentral || passForward)) passSel = false;
  if (passSel) m_cutflow->count("CentralOrForwardJet");
  
  Props::passPreSel.set(jet, passSel);
  if (!(Props::goodJet.get(jet))) passSel = false;
  if (passSel) m_cutflow->count("GoodJet");
  
  Props::isVetoJet.set(jet, passSel);
  if (passSel) m_cutflow->count("VetoJet selected");
  Props::isVBFJet.set(jet, passSel);
    
  return passSel;
}

bool JetHandler_HH_bbtautau::passSignalJet(xAOD::Jet* jet)
{
  
  bool passSel = true;
  if (!(Props::isVetoJet.get(jet))) passSel = false;
  if (passSel) m_cutflow->count("SignalJet input", 400);
  if (!(checkCentralJet(jet,false))) passSel = false;
  if (passSel) m_cutflow->count("IsCentral");
  
  Props::isSignalJet.set(jet, passSel);
  if (passSel) m_cutflow->count("SignalJet selected");
  return passSel;
}

bool JetHandler_HH_bbtautau::checkCentralJet(xAOD::Jet* jet,bool isInCutFlow)
{
  bool passSel = true;
  if (passSel && isInCutFlow ) m_cutflow->count("Central jet input",100);
  if (!(jet->pt() > 20000.)) passSel = false;  
  if (passSel && isInCutFlow ) m_cutflow->count("Central jet pt");
  if (!(fabs(jet->eta()) < 2.5)) passSel = false;
  if (passSel && isInCutFlow ) m_cutflow->count("Central jet |eta|");

  if (passSel && isInCutFlow ) m_cutflow->count("Central jet selected");
  
  return passSel;
}

EL::StatusCode JetHandler_HH_bbtautau::writeCustomVariables(xAOD::Jet * inJet, xAOD::Jet * outJet, bool /*isKinVar*/, bool isWeightVar, const TString& /*sysName*/)
{

  if(!isWeightVar){
    Props::isVetoJet.copy(inJet, outJet);
    Props::isSignalJet.copy(inJet, outJet);
  }
  
  return EL::StatusCode::SUCCESS;
 }

EL::StatusCode JetHandler_HH_bbtautau::matchL1Jet(xAOD::Jet *jet) {
  // Matching of offline jets to L1 jet RoIs
  // - J12 for 4J12 trigger
  // - J25 for DTTs with additional J25
  const xAOD::JetRoIContainer *jet_rois = nullptr;
  EL_CHECK("JetHandler_HH_bbtautau::matchL1Jets", m_event->retrieve(jet_rois, "LVL1JetRoIs"));

  const float match_dR = 0.4;
  bool match_J12 = false;
  bool match_J12_0ETA23 = false;
  bool match_J25 = false;

  for (const auto roi : *jet_rois) {
    if (roi->etLarge() <= 12000 || fabs(roi->eta()) > 3.2) {
      continue;
    }

    TLorentzVector roi_p4;
    roi_p4.SetPtEtaPhiM(roi->etLarge(), roi->eta(), roi->phi(), 1.0);

    const auto dR_offl_jet = roi_p4.DeltaR(jet->p4());
    if (dR_offl_jet < match_dR) {
      match_J12 = true;

      if (fabs(roi->eta()) < 2.3) {
        match_J12_0ETA23 = true;
      }

      if (roi->etLarge() > 25000) {
        match_J25 = true;
      }
    }
  }

  Props::L1_match_J12.set(jet, match_J12);
  Props::L1_match_J12_0ETA23.set(jet, match_J12_0ETA23);
  Props::L1_match_J25.set(jet, match_J25);

  return StatusCode::SUCCESS;
}
