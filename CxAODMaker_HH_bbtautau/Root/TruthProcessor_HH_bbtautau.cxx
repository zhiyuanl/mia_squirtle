#include "CxAODMaker/TruthProcessor.h"
#include "CxAODMaker_HH_bbtautau/TruthProcessor_HH_bbtautau.h"

#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"

#include <iostream>

#include "xAODBase/IParticle.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODRootAccess/TStore.h"

TruthProcessor_HH_bbtautau::TruthProcessor_HH_bbtautau(const std::string& name, ConfigStore & config,
                               xAOD::TEvent * event, EventInfoHandler & eventInfoHandler)
  : TruthProcessor(name, config, event, eventInfoHandler)
{
  m_isMC = m_eventInfoHandler.get_isMC();
  m_tauContainerName = "TruthTaus";
  //m_config.getif<std::string>("truthTau", m_tauContainerName);
}


TruthProcessor_HH_bbtautau::~TruthProcessor_HH_bbtautau()
{
}

bool TruthProcessor_HH_bbtautau::passCustomTruthParticle(xAOD::TruthParticle * part, const std::string &containerName) {
  // do whatever is standard for truth particles
  TruthProcessor::passCustomTruthParticle(part, containerName);

  //std::cout << "containerName " << containerName   << std::endl;
  // // save all TruthTaus additionally:
  //if (containerName == m_tauContainerName)

  const auto isB = part->absPdgId() == 5;

  if (isB && isFromDecay(part, &isHiggs)) {
    Props::passPreSel.set(part, true);
  }

  return false;
}

EL::StatusCode TruthProcessor_HH_bbtautau::writeCustomOutputVariables(xAOD::TruthParticle *inPart, xAOD::TruthParticle *outPart) {
  // This method overrides the funtion in the core Truth_Processor
  
  //std::cout << "particle ID" <<std::abs(inPart->pdgId()) << std::endl;
 
  int parentID=0;
  bool IsFromLQ = false;
  bool IsTauFromHiggs = false;
  bool IsTauFromMeson = false;
  bool IsTauFromBaryon = false;
  bool IsTauFromW = false;
    
  if (inPart->nParents()>0 && inPart->parent(0)!=NULL ){
    if (std::abs(inPart->parent(0)->pdgId())!=std::abs(inPart->pdgId())) {
      parentID= inPart->parent(0)->pdgId();
      
      Props::parentID.set(inPart, parentID);
      Props::parentID.copy(inPart, outPart);
      
    }
  }
  
  if (inPart->nParents()>0 && inPart->parent(0)!=NULL){
    for (unsigned int i = 0; i < inPart->nParents(); ++i) {
    
    if (std::abs(inPart->pdgId())== 15 || std::abs(inPart->pdgId())== 5){ //&& inPart->status()==62){
      
      if (std::abs(inPart->parent(i)->pdgId())==43) {
	
	//std::cout << "tau or b from LQ status : "<<inPart->status() << std::endl;
	IsFromLQ = true;
      }
    }
    
    if (inPart->isW() || std::abs(inPart->pdgId())== 5){
      
      if (inPart->parent(i)->isTop()) {
    	//std::cout << "W or b from Top status : "<<inPart->status() << std::endl;
    	auto top = inPart->parent(i);
    	if (top->parent(0)!=NULL && top->parent(0)->pdgId()==43) {

	  IsFromLQ = true;
	  //std::cout << "top from LQ status : "<<inPart->status() << std::endl;
	}
      }
     
    }
    Props::IsFromLQ.set(inPart,IsFromLQ);
    Props::IsFromLQ.copyIfExists(inPart,outPart);
    
      if (m_isMC && std::abs(inPart->pdgId()) == 15) {
	if (inPart->parent(i)->isMeson()) {
	  IsTauFromMeson = true;
	  //std::cout << "Tau from Meson decay: "<< IsTauFromMeson << std::endl;
	}
	if (inPart->parent(i)->isBaryon()) {
	  IsTauFromBaryon = true;
	  //std::cout << "Tau from Baryon decay: " << std::endl;
	}
	if (inPart->parent(i)->isW()){ //|| inPart->parent(i)->isZ()){
	  IsTauFromW = true;
	  //std::cout << "Tau from W decay: "<< inPart->status() << std::endl;
	}
	if (inPart->parent(i)->isHiggs()) {
	  IsTauFromHiggs = true;
	  //std::cout << "Tau from Higgs decay: " << std::endl;
	}
	//if(inPart->parent(i)->pdgId() != 25 && fabs(inPart->parent(i)->pdgId()) != 15) IsTauFromB = true;
	if (IsTauFromHiggs && !IsTauFromMeson && !IsTauFromBaryon && !IsTauFromW) IsTauFromHiggs = true;
	
	Props::IsTauFromHiggs.set(inPart, IsTauFromHiggs);
	Props::IsTauFromHiggs.copyIfExists(inPart, outPart);
	Props::IsTauFromW.set(inPart, IsTauFromW);
	Props::IsTauFromW.copyIfExists(inPart, outPart);
	Props::pt_vis.copyIfExists(inPart, outPart);
	Props::phi_vis.copyIfExists(inPart, outPart);
	Props::m_vis.copyIfExists(inPart, outPart);
	Props::eta_vis.copyIfExists(inPart, outPart);
	Props::IsHadronicTau.copyIfExists(inPart, outPart);
      }
    }
  }
    return EL::StatusCode::SUCCESS;
}

