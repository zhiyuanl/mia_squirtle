#include "CxAODMaker_HH_bbtautau/MuonHandler_HH_bbtautau.h"
#include "iostream"

MuonHandler_HH_bbtautau::MuonHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent *event,
               EventInfoHandler & eventInfoHandler) :
  MuonHandler (name, config, event, eventInfoHandler)
{
   using std::placeholders::_1;
   m_selectFcns.clear(); 
   m_selectFcns.push_back(std::bind( &MuonHandler_HH_bbtautau::passHHLooseMuon, this, _1)); // to be pushed back in order of appearance!! wrong order causes runtime error.
   m_selectFcns.push_back(std::bind( &MuonHandler_HH_bbtautau::passHH_bbtautauMuon, this, _1));
   m_selectFcns.push_back(std::bind( &MuonHandler_HH_bbtautau::passHH_bbtautaullMuon, this, _1));
  }
bool MuonHandler_HH_bbtautau::passHHLooseMuon(xAOD::Muon * muon) 
{
  bool passSel = true;
  //if (!((muon->muonType() == xAOD::Muon::Combined) || (muon->muonType() == xAOD::Muon::SegmentTagged))) passSel = false;
  //if (passSel) m_cutflow->count("Muon Type");
  if (!(Props::acceptedMuonTool.get(muon))) passSel = false; 
  if (passSel) m_cutflow->count("Muon Tool");
  if (!(muon->pt() > 7000.)) passSel = false;
  if (passSel) m_cutflow->count("Pt");

  bool isIsoCR=false;
  m_config.getif<bool>("isIsoCR", isIsoCR);
  if (!isIsoCR && !(Props::isFixedCutLooseIso.get(muon)) ) passSel = false; // signal region
  else if (isIsoCR && Props::isFixedCutLooseIso.get(muon)) passSel = false;// inverse Iso control region

  if (passSel) m_cutflow->count("Pass Iso");
  
  Props::isHHLooseMuon.set(muon, passSel); 
  Props::passPreSel.set(muon, passSel);
  Props::forMETRebuild.set(muon, passSel);//Using this for MET rebuilding
  return passSel;
}

bool MuonHandler_HH_bbtautau::passHH_bbtautauMuon(xAOD::Muon * muon)
{
  bool passSel = true;
  //check loose selection is passed
  if(!(Props::isHHLooseMuon.get(muon))) passSel = false;
  if (!(Props::isMedium.get(muon)))passSel = false; //ichep triggers
  //common cuts
  if (!(muon->pt() > 15000.)) passSel = false;
  if (passSel) m_cutflow->count("Final Pt");

  //float ptcone = -999.;
  // muon->isolation(trackIso,xAOD::Iso::ptcone40);
  // if (!trackIso<0.06) passSel = false;
  // float ptcone = -999.;
  // muon->isolation(trackIso,xAOD::Iso::etcone20);
  // if (!trackIso<0.06) passSel = false;
  // if (passSel) m_cutflow->count("Iso cuts");
  Props::isBBTTSignalMuon.set(muon, passSel);
  
  return passSel;
}

bool MuonHandler_HH_bbtautau::passHH_bbtautaullMuon(xAOD::Muon * muon)
{
  bool passSel = true;

  if (!(Props::isHHLooseMuon.get(muon))) passSel = false;
  if (!(Props::isMedium.get(muon)))passSel = false;
  if (!(fabs(muon->eta()) < 2.5)) passSel = false;
  if (!(muon->pt() > 7000.)) passSel = false;
  if (passSel) m_cutflow->count("Final Pt");

  Props::isBBTTLLSignalMuon.set(muon, passSel);

  return passSel;
}


EL::StatusCode MuonHandler_HH_bbtautau::writeCustomVariables(xAOD::Muon * inMuon, xAOD::Muon * outMuon, bool /*isKinVar*/, bool isWeightVar,const TString& /*sysName*/) 
{
  if(!isWeightVar){
    Props::isBBTTSignalMuon.copy(inMuon, outMuon);
    Props::isBBTTLLSignalMuon.copy(inMuon, outMuon);
    Props::isHHLooseMuon.copy(inMuon, outMuon);
  }
  return EL::StatusCode::SUCCESS;

}

