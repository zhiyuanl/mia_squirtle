#include "CxAODMaker_HH_bbtautau/FatJetHandler_HH_bbtautau.h"

FatJetHandler_HH_bbtautau::FatJetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                             EventInfoHandler & eventInfoHandler) : 
  FatJetHandler(name, config, event, eventInfoHandler),
  m_btagtool60(nullptr),
  m_btagtool70(nullptr),
  m_btagtool77(nullptr),
  m_btagtool85(nullptr)
{
 using std::placeholders::_1;
 m_selectFcns.clear(); 
 m_selectFcns.push_back(std::bind( &FatJetHandler_HH_bbtautau::passFatJet, this, _1)); 
}

FatJetHandler_HH_bbtautau::~FatJetHandler_HH_bbtautau()
{
   if (m_btagtool60) delete m_btagtool60;
   if (m_btagtool70) delete m_btagtool70;
   if (m_btagtool77) delete m_btagtool77;
   if (m_btagtool85) delete m_btagtool85;
}

bool FatJetHandler_HH_bbtautau::passFatJet(xAOD::Jet* jet)
{
  bool passFatCut=false;
  if(jet->pt()>200000){
    if(fabs(jet->eta()) <2.0){
      passFatCut=true;
    }
  }
  Props::isFatJet.set(jet, passFatCut);
  //std::cout << "Is fat jet ? " << Props::isFatJet.get(jet) << std::endl;
  Props::passPreSel.set(jet, true);

 return passFatCut;
}

EL::StatusCode FatJetHandler_HH_bbtautau::writeCustomVariables(xAOD::Jet* inJet, xAOD::Jet* outJet, bool /*isKinVar*/, bool isWeightVar,const TString& /*sysName*/)
{
  // set something without having a pre-defined method
  if(!isWeightVar){
    Props::isFatJet.copy(inJet, outJet);
  }
  return EL::StatusCode::SUCCESS;

}

EL::StatusCode FatJetHandler_HH_bbtautau::initializeTools()
{

  //EL::StatusCode FatJetHandler::initializeTools();
  EL_CHECK("FatJetHandler::initializeTools()",FatJetHandler::initializeTools());

  m_btagtool60 = new BTaggingSelectionTool("BTaggingSelectionTool60");
  m_btagtool60->msg().setLevel( m_msgLevel );
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("MaxEta", 2.5));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("MinPt", 10000.));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("JetAuthor", "AntiKtVR30Rmax4Rmin02TrackJets"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("TaggerName", "MV2c10"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->setProperty("OperatingPoint", "FixedCutBEff_60"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool60->initialize());

  m_btagtool70 = new BTaggingSelectionTool("BTaggingSelectionTool70");
  m_btagtool70->msg().setLevel( m_msgLevel );
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("MaxEta", 2.5));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("MinPt", 10000.));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("JetAuthor", "AntiKtVR30Rmax4Rmin02TrackJets"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("TaggerName", "MV2c10"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->setProperty("OperatingPoint", "FixedCutBEff_70"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool70->initialize());

  m_btagtool77 = new BTaggingSelectionTool("BTaggingSelectionTool77");
  m_btagtool77->msg().setLevel( m_msgLevel );
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("MaxEta", 2.5));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("MinPt", 10000.));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("JetAuthor", "AntiKtVR30Rmax4Rmin02TrackJets"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("TaggerName", "MV2c10"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->setProperty("OperatingPoint", "FixedCutBEff_77"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool77->initialize());

  m_btagtool85 = new BTaggingSelectionTool("BTaggingSelectionTool85");
  m_btagtool85->msg().setLevel( m_msgLevel );
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("MaxEta", 2.5));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("MinPt", 10000.));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("JetAuthor", "AntiKtVR30Rmax4Rmin02TrackJets"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("TaggerName", "MV2c10"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->setProperty("OperatingPoint", "FixedCutBEff_85"));
  TOOL_CHECK("TrackJetHandler()::initializeTools()",m_btagtool85->initialize());




  return EL::StatusCode::SUCCESS;
}

EL::StatusCode FatJetHandler_HH_bbtautau::decorate(xAOD::Jet * jet)
{

  //EL::StatusCode JetHandler::initializeTools();
  EL_CHECK("FatJetHandler::decorate()",FatJetHandler::decorate(jet));

  // b-tagging of VR tack jets
  const ElementLink < xAOD::JetContainer > & linkToUngroomed = jet->getAttribute<ElementLink<xAOD::JetContainer>>("Parent");
  const xAOD::Jet* ungroomedJet = *linkToUngroomed;

  std::vector<const xAOD::Jet*> vAssocTrackJets;
  if (!ungroomedJet->getAssociatedObjects<xAOD::Jet>("GhostVR30Rmax4Rmin02TrackJet", vAssocTrackJets))
  {
    if(m_debug) std::cout << "FatJetHandler_HH_bbtautau::decorate() No associated VR jets found on parent jet." << std::endl;
    return StatusCode::SUCCESS;
  }

  std::vector<const xAOD::Jet*> vFilteredTrackJets;
  for(const auto *trackJet: vAssocTrackJets)
  {
    if (trackJet->pt() < 10000) continue;
    if (fabs(trackJet->eta()) > 2.5) continue;
    if (trackJet->numConstituents() < 2) continue;
    vFilteredTrackJets.push_back(trackJet);
  }
  std::sort(vFilteredTrackJets.begin(), vFilteredTrackJets.end(), [](const xAOD::Jet* lhs, const xAOD::Jet* rhs) -> bool { return(lhs->pt() > rhs->pt()); });

  int Nbtags60 = 0;
  int Nbtags70 = 0;
  int Nbtags77 = 0;
  int Nbtags85 = 0;
  int Ntrackjets = 0;
  std::vector<int> vTruthLabels;
  for(const auto* trackJet: vFilteredTrackJets)
  {
    int label = -1;
    trackJet->getAttribute("HadronConeExclTruthLabelID", label);
    vTruthLabels.push_back(label);
    if (m_btagtool60->accept( trackJet))
      Nbtags60++;
    if (m_btagtool70->accept( trackJet))
      Nbtags70++;
    if (m_btagtool77->accept( trackJet))
      Nbtags77++;
    if (m_btagtool85->accept( trackJet))
      Nbtags85++;

    Ntrackjets++;
    if (Ntrackjets >= 2) break; // only consider two leading track jets
  }

  Props::Nbtags60.set(jet, Nbtags60);
  Props::Nbtags70.set(jet, Nbtags70);
  Props::Nbtags77.set(jet, Nbtags77);
  Props::Nbtags85.set(jet, Nbtags85);
  Props::vHadronConeExclTruthLabelID.set(jet, vTruthLabels);

  //Loop through VR Trackjets and check dR separation
  bool VRJetOverlap = false;
  std::vector<const xAOD::Jet*>::iterator Iter1 = vAssocTrackJets.begin();
  std::vector<const xAOD::Jet*>::iterator EndIter1 = vAssocTrackJets.end();
  for( ;  Iter1 != EndIter1; ++Iter1 ){

    //Check jet 1 pT
    if( (*Iter1)->pt() < 10e3 ) { continue; }
    //Check jet 1 TrkJetNumConstituents
    if( (*Iter1)->numConstituents() < 2 ) { continue; }

    //Setup second iterator
    std::vector<const xAOD::Jet*>::iterator Iter2 = Iter1+1;
    std::vector<const xAOD::Jet*>::iterator EndIter2 = vAssocTrackJets.end();
    for( ; Iter2 != EndIter2; ++Iter2){

      //Check jet 2 pT
      if( (*Iter2)->pt() < 5e3 ) { continue; }
      //Check jet 2 TrkJetNumConstituents
      if( (*Iter2)->numConstituents() < 2 ) { continue; }
      //Now calculate spatial dR value
      double jetR1 = std::max(0.02, std::min(0.4, 30. / ((*Iter1)->pt() / 1e3)));
      double jetR2 = std::max(0.02, std::min(0.4, 30. / ((*Iter2)->pt() / 1e3)));
      double deltaR = (*Iter1)->p4().DeltaR((*Iter2)->p4());
      if (deltaR < std::min(jetR1, jetR2)) {
        VRJetOverlap = true;
        break;
      }
    }
  }

  Props::VRJetOverlap.set(jet, VRJetOverlap);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode FatJetHandler_HH_bbtautau::writeKinematicVariations(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName)
{

  EL_CHECK("FatJetHandler::writeKinematicVariations()",FatJetHandler::writeKinematicVariations(inJet,outJet,sysName));

  Props::Nbtags60.copy(inJet, outJet);
  Props::Nbtags70.copy(inJet, outJet);
  Props::Nbtags77.copy(inJet, outJet);
  Props::Nbtags85.copy(inJet, outJet);
  Props::vHadronConeExclTruthLabelID.copy(inJet, outJet);
  Props::VRJetOverlap.copy(inJet, outJet);

  //
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode FatJetHandler_HH_bbtautau::writeNominal(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& /*sysName*/)
{

  const TString& sysName="";

  EL_CHECK("FatJetHandler::writeNominal()",FatJetHandler::writeNominal(inJet,outJet,sysName));

  return EL::StatusCode::SUCCESS;
}

