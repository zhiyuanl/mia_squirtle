// EvetLoop includes
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "EventLoop/OutputStream.h"

#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "CxAODMaker_HH_bbtautau/TauHandler_HH_bbtautau.h"
#include "CxAODMaker_HH_bbtautau/DiTauJetHandler_HH_bbtautau.h"
#include "CxAODMaker_HH_bbtautau/JetHandler_HH_bbtautau.h"
#include "CxAODMaker_HH_bbtautau/ElectronHandler_HH_bbtautau.h"
#include "CxAODMaker_HH_bbtautau/MuonHandler_HH_bbtautau.h"
#include "CxAODMaker/PhotonHandler.h"
#include "CxAODMaker_HH_bbtautau/FatJetHandler_HH_bbtautau.h"
#include "CxAODMaker/METHandler.h"
#include "CxAODMaker_HH_bbtautau/TruthProcessor_HH_bbtautau.h"
#include "CxAODMaker/TruthJetHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/TrackJetHandler.h"
#include "CxAODMaker/EventSelector.h"
#include "CxAODMaker_HH_bbtautau/AnalysisBase_HH_bbtautau.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauHadHadSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauLepHadSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauLepLepSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauLepHadJetSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauHadHadJetSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauLepLepJetSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauBoostedSelection.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauBoostedJetSelection.h"
#include "CxAODTools_HH_bbtautau/TauHelpers.h"
//#include "CxAODMaker/JetRegression.h"
#include "CxAODMaker/JetSemileptonic.h"
#include "CxAODTools/OverlapRegisterAccessor.h"
#include "CxAODTools_HH_bbtautau/OverlapRemoval_HH_bbtautau.h"

#include "CxAODMaker_HH_bbtautau/DiTauMassHandler.h"

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisBase_HH_bbtautau)

AnalysisBase_HH_bbtautau::AnalysisBase_HH_bbtautau()
    : AnalysisBase(),
      m_preselector(nullptr),
      m_doDiTauMass(false),
      m_runFourTau(false) {
}

AnalysisBase_HH_bbtautau::~AnalysisBase_HH_bbtautau() {
}

EL::StatusCode AnalysisBase_HH_bbtautau::initialize() {
  EL_CHECK("AnalysisBase_HH_bbtautau::initialize()", AnalysisBase::initialize());

  m_config->getif<bool>("runFourTau", m_runFourTau);
  m_config->getif<bool>("useRNNTaus", m_useRNNTaus);
  m_config->getif<std::string>("TauIDWP", m_tauIDWP);

  m_passIDProp = getTauIDProp(m_tauIDWP, m_useRNNTaus);
  if (!m_passIDProp) {
    Error("AnalysisBase_HH_bbtautau::initialize()", "Invalid Tau-ID working point.");
    return EL::StatusCode::FAILURE;
  }

  m_config->getif<bool>("writeLHE3Weights", m_write_lhe3weights);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_HH_bbtautau::initializeSampleInfo() {
  std::cout << "Inside initializeSampleInfo" << std::endl;

  std::ifstream file;
  std::string sampleInfo_File = gSystem->Getenv("WorkDir_DIR");
  sampleInfo_File += "/data/FrameworkSub_HH_bbtautau/sample_info.txt";

  std::cout << sampleInfo_File << std::endl;

  file.open(sampleInfo_File.c_str());

  while (!file.eof()) {
    // read line
    std::string lineString;
    getline(file, lineString);
    // std::cout << lineString << std::endl;

    // skip empty lines
    // TODO - is there a better way to implement this check?
    // if (lineString.find(".") > 1000) {
    //   continue;
    // }

    // skip lines starting with #
    if (lineString.find("#") == 0) {
      continue;
    }

    // store in map
    std::stringstream line(lineString);
    int dsid;
    std::string shortname;
    std::string longname;
    std::string nominal;
    std::string veto;

    line >> dsid >> shortname >> longname >> nominal >> veto;

    if (m_NominalDSIDs.count(dsid) != 0) {
      Warning("initializeSampleInfo()", "Skipping duplicated mc_channel_number for line '%s'.", lineString.c_str());
      continue;
    }
    m_NominalDSIDs[dsid] = (nominal == "nominal");
  }
  file.close();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisBase_HH_bbtautau::initializeHandlers() {
  Info("initializeHandlers()", "Initialize handlers.");

  // initialize EventInfoHandler and read info of first event

  m_eventInfoHandler = new EventInfoHandler(*m_config, m_event);
  m_eventInfoHandler->set_isMC(m_isMC);
  m_eventInfoHandler->set_isDerivation(m_isDerivation);
  m_eventInfoHandler->set_derivationName(m_derivationName);
  m_eventInfoHandler->set_isAFII(m_isAFII);
  m_eventInfoHandler->set_pTag(m_pTag);
  m_eventInfoHandler->set_mcChanNr(m_mcChanNr);

  // check pile up reweight Files contain MC dataset before initializing PU reweight in EventInfoHandler
  if(m_isMC) EL_CHECK("AnalysisBase::initializeHandlers()", checkPileupRwFiles());

  EL_CHECK("initializeHandlers()",m_eventInfoHandler->initialize());
  EL_CHECK("initializeHandlers()", m_eventInfoHandler->executeEvent());


  bool isMC = m_eventInfoHandler->get_isMC();
  Info("initialize()", "First event is MC          = %i ", isMC);

  if (isMC) {
     m_truthProcessor = new TruthProcessor_HH_bbtautau("truthProcessor", *m_config, m_event, *m_eventInfoHandler);
     m_truthWZjetHandler    = registerHandler<TruthJetHandler>("truthWZJet");
  }

  // these have global pointer to be used e.g. in event selection
  m_muonHandler     = registerHandler<MuonHandler_HH_bbtautau>("muon");
  m_electronHandler = registerHandler<ElectronHandler_HH_bbtautau>("electron");
  m_tauHandler      = registerHandler<TauHandler_HH_bbtautau>("tau");
  m_ditauHandler    = registerHandler<DiTauJetHandler_HH_bbtautau>("diTauJet");
  m_photonHandler   = registerHandler<PhotonHandler>("photon");
  m_jetHandler      = registerHandler<JetHandler_HH_bbtautau>("jet");
  m_fatjetHandler   = registerHandler<FatJetHandler_HH_bbtautau>("fatJet");
  m_METHandler      = registerHandler<METHandler>("MET");
  m_trackjetHandler = registerHandler<TrackJetHandler>("vrtrackJet");

  // these are spectators: they are calibrated and written to output,
  //                       but are not used in the event selection

  registerHandler<JetHandler>("jetSpectator");

  registerHandler<METHandler>("METTrack");
  if (isMC) {
    registerHandler<METHandler>("METTruth");
  }

  // initialize METHandler (not a ObjectHandler)
  //  m_METHandler = new METHandler(m_config, m_event, *m_eventInfoHandler);

  // for tau truth matching
  if (m_tauHandler) {
     m_tauHandler->setTruthProcessor(m_truthProcessor);
  }

  m_config->getif<bool>("DiTauMassCalc",m_doDiTauMass);

  if (m_doDiTauMass) {
    m_diTauMassHandler = new DiTauMassHandler(*m_config, m_event, *m_eventInfoHandler, *m_METHandler);
    EL_CHECK("initializeHandlers()", m_diTauMassHandler->initialize());
    EL_CHECK("initializeHandlers()", m_diTauMassHandler->addVariations(m_variations));
  }

  // Set trigger stream info if data
  // need to catch embedding once the time has come
  bool m_isEmbedding=false;
  if( !isMC && !m_isEmbedding ){
     TString sampleName = wk()->metaData()->castString("sample_name");
     if(sampleName.Contains("Egamma")) m_eventInfoHandler->set_TriggerStream(1);
     if(sampleName.Contains("Muons")) m_eventInfoHandler->set_TriggerStream(2);
     if(sampleName.Contains("Jet")) m_eventInfoHandler->set_TriggerStream(3);
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_HH_bbtautau::initializeSelector() {
  Info("initializeSelector()", "Initialize selector.");

  // initialize EventSelector
  m_preselector = new EventSelector(*m_config);

  m_preselector->setJets(m_jetHandler);
  m_preselector->setFatJets(m_fatjetHandler);
  m_preselector->setTrackJets(m_trackjetHandler);
  m_preselector->setMuons(m_muonHandler);
  m_preselector->setTaus(m_tauHandler);
  m_preselector->setDiTaus(m_ditauHandler);
  m_preselector->setElectrons(m_electronHandler);
  m_preselector->setPhotons(m_photonHandler);
  m_preselector->setMET(m_METHandler);
  m_preselector->setEventInfo(m_eventInfoHandler);

  EL_CHECK("EventSelector::initialize",m_preselector->initialize());

  // Initalise main (second) selector
  EL_CHECK("initializeSelector()", AnalysisBase::initializeSelector());

  selectionName="";

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_HH_bbtautau::initializeSelection() {
  Info("initializeSelection()","...");

  // determine selection name

  //std::string selectionName = "";
  bool autoDetermineSelection;
  m_config->getif<bool>("autoDetermineSelection", autoDetermineSelection);
  if (!autoDetermineSelection) {
     m_config->getif<std::string>("selectionName", selectionName);
  }else {
    TString sampleName = wk()->metaData()->castString("sample_name");
    if      (sampleName.Contains("HIGG4D3")) selectionName = "hadhad";  //is naming scheme correct?
    else if (sampleName.Contains("HIGG4D2")) selectionName = "lephad";
    else if (sampleName.Contains("HIGG4D1")) selectionName = "leplep";
    else {
      Error("initialize()", "Could not auto determine selection!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize event selection

  bool applySelection = false;
  m_config->getif<bool>("applyEventSelection", applySelection);
  if (applySelection) {
    Info("initializeSelection()", "Applying selection: %s", selectionName.c_str());
    if (selectionName == "hadhad") {

      HHbbtautauHadHadSelection* preselector = new HHbbtautauHadHadSelection(m_config, m_eventInfoHandler);
      m_preselector->setSelection(preselector);

      HHbbtautauHadHadJetSelection* selector = new HHbbtautauHadHadJetSelection(m_config, m_eventInfoHandler);
      m_selector->setSelection(selector);

    } else if (selectionName == "lephad") {

      HHbbtautauLepHadSelection* preselector = new HHbbtautauLepHadSelection(m_config, m_eventInfoHandler);
      m_preselector->setSelection(preselector);

      HHbbtautauLepHadJetSelection* selector = new HHbbtautauLepHadJetSelection(m_config, m_eventInfoHandler);
      m_selector->setSelection(selector);

    } else if (selectionName == "leplep") {

      HHbbtautauLepLepSelection* preselector = new HHbbtautauLepLepSelection(m_config, m_eventInfoHandler);
      m_preselector->setSelection(preselector);

      HHbbtautauLepLepJetSelection* selector = new HHbbtautauLepLepJetSelection(m_config, m_eventInfoHandler);
      m_selector->setSelection(selector);

    } else if (selectionName == "boosted") {

      HHbbtautauBoostedSelection* preselector = new HHbbtautauBoostedSelection(m_config, m_eventInfoHandler);
      m_preselector->setSelection(preselector);

      HHbbtautauBoostedJetSelection* selector = new HHbbtautauBoostedJetSelection(m_config, m_eventInfoHandler);
      m_selector->setSelection(selector);
    } else {
      Error("initializeSelection()", "Unknown selection requested!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize overlap removal (possibly analysis specific)
  OverlapRemoval * overlapRemoval = new OverlapRemoval( *m_config );
  overlapRemoval->SetUseTauJetOR(false);
  EL_CHECK("initializeSelection()", overlapRemoval->initialize());

  OverlapRemoval * overlapRemoval2 = new OverlapRemoval_HH_bbtautau( *m_config );
  EL_CHECK("initializeSelection()", overlapRemoval2->initialize());

  //OverlapRemoval * overlapRemoval2 = new OverlapRemoval( *m_config );
  //EL_CHECK("initializeSelection()", overlapRemoval2->initialize());

  m_preselector->setOverlapRemoval( overlapRemoval );
  m_selector->setOverlapRemoval( overlapRemoval2 );

  // TODO make METHandler to cope with non-existing containers and
  //      move this check into event selection
  if ( !m_muonHandler || !m_electronHandler || !m_jetHandler) {
    Error("initialize()", "Not all collections for MET rebuilding are defined!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;

}


EL::StatusCode AnalysisBase_HH_bbtautau::fileExecute() {
  EL_CHECK("fileExecute()", AnalysisBase::fileExecute());

  // Retrieve sum of event weights for LHE3 weight variations
  if (m_write_lhe3weights) {
    EL_CHECK("fileExecute()", writeLHE3WeightHists());
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_HH_bbtautau::execute() {
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  if (m_debug) {
    Info("execute()", "Called.");
  }

  // print every 100 events, so we know where we are:
  if ( (m_eventCounter % 100) == 0 || m_debug ) {
    Info("execute()", "Event number (count) = %li", m_eventCounter);
  }
  ++m_eventCounter;


  //----------------------------
  // Event information
  //---------------------------
  EL_CHECK("execute()",m_eventInfoHandler->executeEvent());

  if(m_isMC){
    // sum the weights of this job
    m_eventWeightCounter += m_eventInfoHandler->get_MCEventWeight();
  }

  //----------------------------
  // Check PRW
  //---------------------------
  if (m_isMC) {
    const auto puweight = Props::PileupReweight.get(m_eventInfoHandler->getEventInfo());

    if (puweight <= 0) {
      const auto eventInfo = m_eventInfoHandler->getEventInfo();
      const auto randomRunNumber = Props::RandomRunNumber.get(eventInfo);
      const auto act_mu = eventInfo->actualInteractionsPerCrossing();
      const auto avg_mu = eventInfo->averageInteractionsPerCrossing();

      EL_CHECK("execute()", cleanUpEvent(false));
      Warning("execute()", "Skipping event with pileup-weight of: %f (act. mu: %f, avg. mu: %f, run number: %i)",
              puweight, act_mu, avg_mu, randomRunNumber);
      return EL::StatusCode::SUCCESS;
    }
  }

  //---------------------------
  // Primary vertex check
  //---------------------------

  bool hasPV = Props::hasPV.get(m_eventInfoHandler->getEventInfo());
  if(m_skipNoPV && !hasPV) {
    EL_CHECK("execute", cleanUpEvent(false));
    Warning("execute()","No Primary vertex found - skipping event");
    return EL::StatusCode::SUCCESS;
  }

  if (m_debug)
    std::cout << "event number=" << m_eventInfoHandler->get_eventNumber() << std::endl;

  //---------------------------
  // perform object calibrations
  //---------------------------

  // before any calibration call the truth processer
  // this reads the truth information so that later it can be used
  if (m_truthProcessor && m_isMC) {
    wk()->xaodStore()->setActive();
    EL_CHECK("execute()", m_truthProcessor->setObjects());
  }

  for (ObjectHandlerBase * handler : m_objectHandler) {
    // set EventLoops TStore to be the active one
    wk()->xaodStore()->setActive();
    EL_CHECK("execute()",handler->setObjects());
    EL_CHECK("execute()",handler->calibrate());
  }

  if (m_eventInfoHandler) {
    if (m_isMC) {
      if(m_jetHandler)            EL_CHECK("set_JvtSF",m_eventInfoHandler->set_JvtSF());
    }
  }

  //---------------------------
  // perform object selections
  //---------------------------
  if (m_debug)
    std::cout << "HH: " << std::endl
			 << "HH: " << std::endl
			 << "HH: ==================== NEW EVENT ==================================" << std::endl
			 << "HH: in AnalysisBase_HH_bbtautau::execute(): Object selections" << std::endl;
  for (ObjectHandlerBase * handler : m_objectHandler) {
    EL_CHECK("execute()",handler->select());
  }


//   //------------------------------
//   //perform jet energy regression
//   //------------------------------
//
//   if(m_regression && m_applyJetRegression) {
//     m_regression->applyRegression(m_event);
//   }

  //--------------------------------------------------------
  // count semileptonic jet decays
  // and apply jet corrections, also ptCorr from regression
  //--------------------------------------------------------
  if (m_jetHandler && m_applyJetSemileptonic) {
    if (m_electronHandler)      EL_CHECK("execute()",m_jetHandler->countElectronInJet   (m_semileptonic, m_electronHandler->getInParticleVariation("Nominal")));
    if (m_muonHandler)          EL_CHECK("execute()",m_jetHandler->correctMuonInJet     (m_semileptonic,m_muonHandler    ->getInParticleVariation("Nominal")));
    //if (m_regression)           EL_CHECK("execute()",m_jetHandler->correctRegressionForJet(m_semileptonic));
    if (m_isMC) {
      if (m_truthProcessor) {
        if (!m_truthProcessor->isNewStyle()) {
          EL_CHECK("execute()",m_jetHandler->IdentifyVBFJets(m_jetHandler->getInParticleVariation("Nominal"),m_truthProcessor->getInParticle("TruthParticles")));
        } else {
          EL_CHECK("execute()",m_jetHandler->IdentifyVBFJets(m_jetHandler->getInParticleVariation("Nominal"),m_truthProcessor->getInParticle("HardScatterParticles")));
        }
      }
    }
    if (m_isMC) {
        if (m_truthjetHandler)   EL_CHECK("execute()",m_jetHandler->matchTruthJet   (m_semileptonic,m_truthjetHandler->getInParticleVariation("Nominal"),"Truth",0.4));
        if (m_truthWZjetHandler) EL_CHECK("execute()",m_jetHandler->matchTruthJet   (m_semileptonic,m_truthWZjetHandler->getInParticleVariation("Nominal"),"TruthWZ",0.4));
      }
  }

  if (m_fatjetHandler) {

    if (m_isMC) {
        if (m_truthfatjetHandler)   EL_CHECK("execute()",m_fatjetHandler->matchTruthJet (m_semileptonic, m_truthfatjetHandler->getInParticleVariation("Nominal"),"Truth",1.0));
        if (m_truthWZfatjetHandler) EL_CHECK("execute()",m_fatjetHandler->matchTruthJet (m_semileptonic, m_truthWZfatjetHandler->getInParticleVariation("Nominal"),"TruthWZ",1.0));
      }
  }

   //set the number of truthWZJets
  if (m_isMC) {
    if(m_truthWZjetHandler) EL_CHECK("set_NTruthWZJets",m_eventInfoHandler->set_NTruthWZJets(m_truthWZjetHandler->getInParticleVariation("Nominal")));
  }

  //------------------------
  // compute codeTTBarDecay
  //------------------------
  // Always set this to something, in case the TruthProcessor has not been set up to run
  if (m_isMC && m_truthProcessor) {
    int codeTTBarDecay = -999;
    float final_pTV = -999;
    float final_pTL = -999;
    float final_EtaL = -999;
    bool computeCodeTTBarDecay = true;
    m_config->getif<bool>("computeCodeTTBarDecay", computeCodeTTBarDecay); // computing this variable can cause crashes with some derivations (e.g. HIGG4D1 ttbar)
    if (computeCodeTTBarDecay) {
      EL_CHECK("execute()", m_truthProcessor->getCodeTTBarDecay(codeTTBarDecay, final_pTV,final_pTL,final_EtaL));
    }
    EL_CHECK("execute()", m_eventInfoHandler->setCodeTTBarDecay(codeTTBarDecay));
    EL_CHECK("execute()", m_eventInfoHandler->setTTbarpTW( final_pTV ));
    EL_CHECK("execute()", m_eventInfoHandler->setTTbarpTL( final_pTL ));
    EL_CHECK("execute()", m_eventInfoHandler->setTTbarEtaL( final_EtaL ));

    // when this is activated, the muons from hadron decays are saved in the CxAOD and they are decorated with a flag for easy access
    bool findMuonsFromHadronDecays = true;
    m_config->getif<bool>("findMuonsFromHadronDecays", findMuonsFromHadronDecays); // computing this variable can cause crashes with some derivations (e.g. HIGG4D1 ttbar)
    if (findMuonsFromHadronDecays) {
      EL_CHECK("execute()", m_truthProcessor->findMuonsFromHadronDecay());
    }
  }

  if (m_truthProcessor)
    m_truthProcessor->decorate();

  //------------------------
  // compute Sherpa pTV
  //------------------------
  if (m_isMC && m_truthProcessor) {
    EL_CHECK("execute()", m_eventInfoHandler->setSherpapTV( m_truthProcessor->ComputeSherpapTV() ));
  }


  //------------------------
  // compute SUSYMET
  //------------------------
  if (m_isMC) {
    EL_CHECK("execute()", m_eventInfoHandler->setSUSYMET());
  }

  // first select which truth particles to be saved in the output
  // This needs to be done only after the calls to codeTTbarDecays, because this function call determines the
  // particles in the ttbar decay to be saved in the select() function
  if (m_isMC && m_truthProcessor) {
    EL_CHECK("execute()", m_truthProcessor->select());
  }

  //------------------------
  // do event pre selection (before random tau)
  //------------------------

  bool eventPassedPre = false;
  bool eventPassedRandom = true;
  bool eventPassed = false;
  // four tau
  int hasMoreThanTwoTau = 0;

  if (m_debug)
    std::cout << "HH: in AnalysisBase_HH_bbtautau::execute(): Event pre-selection before random tau is called" << std::endl;

  // HF: remove LepHad dependence from ptr?

  dynamic_cast<HHbbtautauSelection<ResultHHbbtautau> *>(m_preselector->getSelection())->clearResults();
  EL_CHECK("execute()",m_preselector->performSelection(eventPassedPre));
  std::map<TString,ResultHHbbtautau> results =
    dynamic_cast<HHbbtautauSelection<ResultHHbbtautau> *>(m_preselector->getSelection())->results();

  if (m_debug)
    Info("execute()", "eventPassed pre-selection = %i", eventPassedPre);


  //eventPassedPre = true;
  if (eventPassedPre) {
    //------------------------
    // Pick random tau
    //------------------------
    // TODO: turn into a function and implement for hadhad too
    if (m_debug)
      std::cout << "HH: in AnalysisBase_HH_bbtautau::execute(): Event passed pre-selection" << std::endl;
    bool antitau(false);
    m_config->getif<bool>("AntiTau",antitau);

    // Only done for nominal and use same random tau (if exists) for all other select
    if(results["Nominal"].trigger != eSkip) eventPassedRandom=doRandomTauSelection(results["Nominal"]);
    else eventPassedRandom = 1;

    if (m_debug){
      std::cout << "HH: " ;
      if (results["Nominal"].isSREvent)
	std::cout << "THIS IS A SR EVENT!" << std::endl;
      else
	std::cout << "This is a CR event" << std::endl;
      if (!eventPassedRandom) std::cout << "HH: Event FAILED random selection." << std::endl;
    }

    //------------------------
    // do event selection (after random tau -> includes jets and reruns OR)
    //------------------------
    if (m_debug)
      std::cout << "HH: in AnalysisBase_HH_bbtautau::execute(): call performSelection" << std::endl;

    dynamic_cast<HHbbtautauSelection<ResultHHbbtautau> *>(m_selector->getSelection())->setResults(results);
    EL_CHECK("execute()",m_selector->performSelection(eventPassed));
    results = dynamic_cast<HHbbtautauSelection<ResultHHbbtautau> *>(m_preselector->getSelection())->results();

    // run four tau
    if (m_runFourTau) {
      hasMoreThanTwoTau = Props::hasMoreThanTwoTau.get(m_eventInfoHandler->getEventInfo());
      EL_CHECK("execute()", m_eventInfoHandler->setForHadHad((eventPassed && eventPassedRandom)));
      EL_CHECK("execute()", m_eventInfoHandler->setForFourTauOnly(hasMoreThanTwoTau==1));
    }

    if (m_debug) {
      Info("execute()", "eventPassed selection = %i", eventPassed);
    }

    //------------------------
    // perform MET calculation after event selection
    //------------------------
    if( m_METHandler ) {
      EL_CHECK("execute()",m_METHandler->setMET());
    }
    if( m_METTrackHandler ) {
      EL_CHECK("execute()",m_METTrackHandler->setMET());
    }

    if ((eventPassedRandom && eventPassed) || (m_runFourTau && (hasMoreThanTwoTau==1))) {

      //----------------------
      // fill output container
      //----------------------
      if (m_debug) std::cout << "HH: Event passed all selection. Filling up output containers" << std::endl;

      for (ObjectHandlerBase * handler : m_objectHandler) {
	     // first round of passSel flag setting (required by flagOutputLinks()):
       EL_CHECK("execute()", handler->setPassSelFlags());
     }

     for (ObjectHandlerBase * handler : m_objectHandler) {
       // flag links and second round of passSel flag setting (adding linked objects):
       // note: this procedure supports only one layer of links
       EL_CHECK("execute()", handler->flagOutputLinks());
       EL_CHECK("execute()", handler->setPassSelForLinked());
       EL_CHECK("execute()", handler->fillOutputContainer());
       handler->countObjects();
     }

     for (ObjectHandlerBase * handler : m_objectHandler) {
       // fill output links (requires fillOutputContainer() called for all handlers)
       EL_CHECK("execute()", handler->fillOutputLinks());
     }

      m_histEventCount -> Fill(3); // nEvents selected out
      if (m_isMC)
	m_histEventCount -> Fill(6, m_eventInfoHandler->get_MCEventWeight()); // sumOfWeights selected out

      // initialize event info output container for writing variations
      EL_CHECK("execute()",m_eventInfoHandler->setOutputContainer());


      if (m_doDiTauMass) {
	if (m_debug) std::cout << "HH: in AnalysisBase_HH_bbtautau::execute(): doing di-tau mass" << std::endl;
	//----------------------
	// DiTau Mass (this is an event variable)
	//----------------------
	m_diTauMassHandler->setInputs(results);
	EL_CHECK("execute()", m_diTauMassHandler->setOutputContainer());
	EL_CHECK("execute()", m_diTauMassHandler->executeEvent());
      }

      if (m_debug) std::cout << "HH: Filling event variables" << std::endl;
      EL_CHECK("execute()",m_selector->fillEventVariables());
      if (m_debug) std::cout << "HH: filling output container" << std::endl;
      EL_CHECK("execute()", m_eventInfoHandler->fillOutputContainer());
      if (m_isMC && m_truthProcessor) {
        EL_CHECK("execute()", m_truthProcessor->fillOutputContainer());
      }

      // record overlap register
      if ( m_overlapRegAcc ) {
	if (m_debug) std::cout << "HH: record OR register" << std::endl;
	EL_CHECK("execute()",m_overlapRegAcc->recordRegister(m_event));
      }
    } // eventPassedRandom && eventPassed
  } // eventPassedPre


  //-------------------------
  // clean memory allocations
  //-------------------------
  if (m_debug) std::cout << "HH: clean memory allocation" << std::endl;
  for (ObjectHandlerBase * handler : m_objectHandler) {
    EL_CHECK("execute()",handler->clearEvent());
  }


  // skip all further execute() and postExecute() if !eventPassed
  if ((!eventPassed || !eventPassedRandom) && (!m_runFourTau || !(hasMoreThanTwoTau==1))) {
    if (m_debug) std::cout << "HH: event failed, clearing event" << std::endl;
    // event info is cleared here or in postExecute()
    EL_CHECK("execute()", m_eventInfoHandler->clearEvent());
    if (m_doDiTauMass) EL_CHECK("execute()", m_diTauMassHandler->clearEvent());
    wk()->skipEvent();
  }

  // clear overlap register (memory management - has no effect if register was recorded)
  if ( m_overlapRegAcc ) {
    EL_CHECK("execute()",m_overlapRegAcc->clearRegister());
  }

  m_linker.clear();

  if (m_debug) std::cout << "HH: end of event selection" << std::endl << std::endl << std::endl;

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode AnalysisBase_HH_bbtautau::finalize() {
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events

  Info("finalize()", "Finalize.");

  // finalize and close our output xAOD file ( and write MetaData tree)
  TFile * file = wk()->getOutputFile("CxAOD");
  TOOL_CHECK("finalize()",m_event->finishWritingTo( file ));

  //if ( m_applyJetRegression ) m_regression->finalize();
  //if ( m_applyJetSemileptonic ) m_semileptonic->finalize();

  // print object counters
  bool printCounts;
  m_config->getif<bool>("printObjectCounts", printCounts);
  if (printCounts) {
    Info("finalize()", "Printing number of objects for the selected events:");
    for (ObjectHandlerBase * handler : m_objectHandler) {
      // print the size of the arrays
      handler->printObjectCounts();
    }
  }

  // write cut flow counters
  for (ObjectHandlerBase * handler : m_objectHandler) {
    std::map<TString, CutFlowCounter*> counter = handler->getCutFlowCounter();
    for (auto cf : counter) {
      TH1D* cutflowHisto = cf.second->getCutFlow(std::string("CutFlow_") + cf.first + std::string("/"));
      wk()->addOutput(cutflowHisto);
    }
  }
  bool applySelection = false;
  m_config->getif<bool>("applyEventSelection", applySelection);
  if (applySelection) {
    CutFlowCounter counter_preselection = m_preselector->getCutFlowCounter();
    TH1D* cutflowHisto_preselection = counter_preselection.getCutFlow("CutFlow/");
    wk()->addOutput(cutflowHisto_preselection);


    CutFlowCounter counter_selection = m_selector->getCutFlowCounter();
    TH1D* cutflowHisto_selection = counter_selection.getCutFlow("CutFlow/");
    wk()->addOutput(cutflowHisto_selection);
  }

  // delete handlers
  delete m_eventInfoHandler;
  for (ObjectHandlerBase * handler : m_objectHandler) {
    delete handler;
    handler = 0;
  }

  // delete overlap register accessor
  delete m_overlapRegAcc;
  m_overlapRegAcc = nullptr;

  Info("finalize()", "Number of events:");
  Info("finalize()", "  in input file (meta data) = %li", (long) m_histEventCount -> GetBinContent(2));
  Info("finalize()", "  limit (config)            = %li", m_maxEvent);
  Info("finalize()", "  processed                 = %li", m_eventCounter);
  Info("finalize()", "  written to output         = %li", (long) m_histEventCount -> GetBinContent(3));

  return EL::StatusCode::SUCCESS;

}

EL::StatusCode AnalysisBase_HH_bbtautau::postExecute() {
  EL_CHECK("postExecute()", AnalysisBase::postExecute());
  if (m_doDiTauMass) EL_CHECK("postExecute()",m_diTauMassHandler->clearEvent());
  return EL::StatusCode::SUCCESS;
}

bool AnalysisBase_HH_bbtautau::doRandomTauSelection(ResultHHbbtautau& m_result){
  std::vector<const xAOD::TauJet*> vecSRTaus;
  int nSignalTaus = 0;

  if (m_debug) std::cout << "HH:  doRandom: checking all taus." << std::endl << "HH: ";
  for (const auto &tau : m_result.taus) {

    if (!Props::passOR.get(tau)) {
      if (m_debug) std::cout << " skip OR ";
      continue;
    }

    if (!Props::isHHSignalTau.get(tau)) {
      if (m_debug) std::cout << " skip nonHH ";
      continue;
    }

    if (m_passIDProp->get(tau)) {
      vecSRTaus.push_back(tau);
      nSignalTaus++;
    }

    if (m_debug) std::cout << "  tau pt=" << tau->pt() / 1000.;
  }
  if (m_debug) std::cout << std::endl;


  if (selectionName == "lephad") {
    // ===========================
    // lephad random tau selection
    // ===========================
    if (m_result.taus.size() > 0) {
      int irand = gRandom->Integer(m_result.taus.size());
      if (m_debug) std::cout << "HH: Random: " << m_result.taus.size() << " " << m_result.taus[irand] << std::endl;
      // Remove all other taus
      TauHandler_HH_bbtautau* tauHandler = dynamic_cast<TauHandler_HH_bbtautau*>(m_tauHandler);
      EL_CHECK("doRandomTauSelection", tauHandler->removeTausExcept(m_result.taus[irand]->index()));
    }
  } else if (selectionName == "hadhad") {
    // ===========================
    // hadhad random tau selection
    // ===========================
    int ntauAccept = 0;
    std::vector<const xAOD::TauJet *> matchedTausSTT, matchedTausDTT;

    matchedTausSTT = m_result.matchedTausSTT;
    matchedTausDTT = m_result.matchedTausDTT;
    ntauAccept = m_result.taus.size();

    if (m_debug) {
      std::cout << "HH: Found "
                << ntauAccept << " accepted, "
                << nSignalTaus << " signal taus."
                << " STTTrig=" << matchedTausSTT.size()
                << " DTTTrig=" << matchedTausDTT.size()
                << std::endl;
      for (const auto& tau : matchedTausSTT) {std::cout << " HH: sgl pt=" << tau->pt()/1000.;}
      for (const auto& tau : matchedTausDTT) {std::cout << " HH: dbl pt=" << tau->pt()/1000.;}
    }

    // Check that event was triggered
    if (!m_result.trigger) return false;

    // Not more than two signal taus
    if (nSignalTaus > 2) return false;

    // Check if SR
    if (nSignalTaus == 2) {
      bool isSRTriggered = false;
      int nTausDTT = 0;
      for (const auto &tau : vecSRTaus) {
        // STT matched
        if (m_result.trigger == eSTT) {
          auto isSTTMatch = std::find(matchedTausSTT.cbegin(), matchedTausSTT.cend(), tau) != matchedTausSTT.cend();
          if (isSTTMatch) {
            isSRTriggered = true;
            break;
          }
        } else {
          // DTT matched
          auto isDTTMatch = std::find(matchedTausDTT.cbegin(), matchedTausDTT.cend(), tau) != matchedTausDTT.cend();
          if (isDTTMatch) {
            nTausDTT++;
          }
          if (nTausDTT == 2) {
            isSRTriggered = true;
          }
        }
      }
      TauHandler_HH_bbtautau* tauHandler = dynamic_cast<TauHandler_HH_bbtautau*>(m_tauHandler);
      EL_CHECK("doRandomTauSelection",
               tauHandler->removeTausExcept(vecSRTaus.at(0)->index(), vecSRTaus.at(1)->index()));
      return isSRTriggered;
    }

    // Less than two signal taus
    m_result.isSREvent = false;

    // if there are 2 or more taus make a random selection
    // assume that the event is triggered
    // so we have at least 1 matched tau to a sngl trigger
    // or at least 2 matched taus to a ditau trigger
    const xAOD::TauJet *tau0 = nullptr;
    const xAOD::TauJet *tau1 = nullptr;

    if (m_result.trigger == eSTT) {
      tau0 = pickRandomTau(matchedTausSTT, nullptr); // random STT matched
      tau1 = pickRandomTau(m_result.taus, tau0); // any other tau
    } else if (m_result.trigger  == eDTT ){
      // if there is no single trigger matched tau
      // both taus must come from the pool of matched ditau trigger objects
      // on of them must be at least 40GeV
      if (matchedTausDTT.size() < 2) return false;

      tau0 = pickRandomTau(matchedTausDTT, nullptr);
      float maxpt = tau0->pt();
      do {
        tau1 = pickRandomTau(matchedTausDTT, tau0);
        maxpt = (tau1->pt() > maxpt) ? tau1->pt() : maxpt;
      } while (maxpt < 40000.);
    }

    // Remove all other taus
    if(tau0 != NULL && tau1 != NULL){
      if (m_debug) std::cout << "HH: randomly chose as taus "
                             << tau0
                             << " pt=" << tau0->pt()/1000.
                             << " and "
                             << tau1
                             << " pt=" << tau1->pt()/1000.
                             << std::endl;
      TauHandler_HH_bbtautau* tauHandler = dynamic_cast<TauHandler_HH_bbtautau*>(m_tauHandler);
      EL_CHECK("execute()", tauHandler->removeTausExcept(tau0->index(), tau1->index()));
    }
  } else if (selectionName != "boosted" && selectionName != "leplep"){
    Error("doRandomTauSelection()","unknown selection name %s", selectionName.c_str());
  }

  return true;
} // doRandomTauSelection


const xAOD::TauJet* AnalysisBase_HH_bbtautau::pickRandomTau(  std::vector<const xAOD::TauJet*> & taus,
							      const xAOD::TauJet* vetoTau)
{
  // pick a random tau from a vector of tau pointers
  // the selected tau ptr must not point to the same tau as the vetoTau ptr
  int ntaus=taus.size();
  if (ntaus==0) return NULL;
  if (ntaus==1) {
    if (taus.at(0) == vetoTau)
      return NULL;
    else
      return taus.at(0);
  }
  int tauidx;
  do {
    tauidx = gRandom->Integer(ntaus);
  } while (taus.at(tauidx) == vetoTau);
  return taus.at(tauidx);
} // ::pickRandomTau


EL::StatusCode AnalysisBase_HH_bbtautau::writeLHE3WeightHists() {
  // Get CutBookKeepers for systematic variations
  const xAOD::CutBookkeeperContainer *completeCBC = nullptr;
  EL_CHECK("getLHE3WeightHists()", m_event->retrieveMetaInput(completeCBC, "CutBookkeepers"));

  // Find the LHE3Weight cutbookkeeper with the highest cycle number
  std::map<const std::string, const xAOD::CutBookkeeper *> cbk_map;
  for (const auto &cbk : *completeCBC) {
    const auto &name = cbk->name();
    const auto &input_stream = cbk->inputStream();
    const auto &cycle = cbk->cycle();

    if (input_stream != "StreamAOD" || name.find("LHE3Weight_") == std::string::npos) {
      continue;
    }

    // Check if already in map
    if (cbk_map.find(name) == cbk_map.cend()) {
      // Not in map
      cbk_map[name] = cbk;
    } else {
      // Already in map
      // Only overwrite if cycle number is higher
      if (cycle > cbk_map[name]->cycle()) {
        cbk_map[name] = cbk;
      }
    }
  }

  // Create histograms and fill them with event counts / sum of weights
  for (const auto &cbk_item : cbk_map) {
    const auto &key = cbk_item.first;
    const auto &cbk = cbk_item.second;
    const auto &histname = key;

    // Create histogram if it doesn't exist yet
    if (m_lhe3weights.find(histname) == m_lhe3weights.cend()) {
      auto hist = std::make_unique<TH1D>(("LHE3Weights/" + histname).c_str(),
                                         histname.c_str(), 2, 0, 2);
      hist->GetXaxis()->SetBinLabel(1, "nEvents initial");
      hist->GetXaxis()->SetBinLabel(2, "sumOfWeights initial");
      m_lhe3weights.insert({histname, hist.get()});
      wk()->addOutput(hist.release());
    }

    const uint64_t nAcceptedEvents = cbk->nAcceptedEvents();
    const double sumOfEventWeights = cbk->sumOfEventWeights();

    m_lhe3weights.at(histname)->Fill(0.0, static_cast<double>(nAcceptedEvents));
    m_lhe3weights.at(histname)->Fill(1.0, sumOfEventWeights);
  }

  return EL::StatusCode::SUCCESS;
}
