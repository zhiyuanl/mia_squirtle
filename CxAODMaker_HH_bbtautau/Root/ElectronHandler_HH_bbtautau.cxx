#include "CxAODMaker_HH_bbtautau/ElectronHandler_HH_bbtautau.h"
#include <iostream>
ElectronHandler_HH_bbtautau::ElectronHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent *event,
               EventInfoHandler & eventInfoHandler) :
  ElectronHandler (name, config, event, eventInfoHandler)
{
  using std::placeholders::_1;
  m_selectFcns.clear(); 
  m_selectFcns.push_back(std::bind( &ElectronHandler_HH_bbtautau::passHHLooseElectron, this, _1));
  m_selectFcns.push_back(std::bind( &ElectronHandler_HH_bbtautau::isBBTTSignalElectron, this, _1));
  m_selectFcns.push_back(std::bind( &ElectronHandler_HH_bbtautau::isBBTTLLSignalElectron, this, _1));
}

bool ElectronHandler_HH_bbtautau::passHHLooseElectron(xAOD::Electron * electron) 
{
   
  bool passSel = true;
   
  if(!Props::isLooseLH.exists(electron)) {
     Warning("ElectronHandler()::passHHLooseElectron()","isLooseLH variable doesn't exist, check your derivation file !");
     passSel=false;
  } else{
     if (!Props::isLooseLH.get(electron)) passSel = false;
  }
  if (passSel) m_cutflow->count("isLooseLH");
  if (!(electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON))) passSel = false;
  if (passSel) m_cutflow->count("isGoodOQ");
  if (fabs(electron->eta()) > 1.37 && fabs(electron->eta()) < 1.52 )passSel = false; //crack
  if (!(fabs(electron->eta()) < 2.47)) passSel = false;
  if (passSel) m_cutflow->count("|eta|");
  if (!(electron->pt() > 7000.)) passSel = false;
  if (passSel) m_cutflow->count("pt loose");

  bool isIsoCR=false;
  m_config.getif<bool>("isIsoCR", isIsoCR);
  bool applyIsoCut=true;
  m_config.getif<bool>("applyIsoCut", applyIsoCut);

  if (applyIsoCut) {
    const xAOD::TrackParticle *trackPart = electron->trackParticle();
    if(!Props::isFixedCutLooseIso.exists(electron)) {  
      if (trackPart) Warning("ElectronHandler()::isBBTTSignalElectron()","isLooseIso variable doesn't exist, check your derivation file !");
       passSel=false;
    } else{
      if (!isIsoCR && !(Props::isFixedCutLooseIso.get(electron)) )passSel = false; //Signal region

      else if (isIsoCR && Props::isFixedCutLooseIso.get(electron)) passSel = false; //Inverse iso CR
      
    }
  }
  if (passSel) m_cutflow->count("Pass Iso");
  Props::isHHLooseElectron.set(electron, passSel);
  Props::passPreSel.set(electron, passSel);
  Props::forMETRebuild.set(electron, passSel);//Using this for MET rebuilding
  return passSel;

}
bool ElectronHandler_HH_bbtautau::isBBTTSignalElectron(xAOD::Electron * electron) 
{

  bool passSel = true;
 
  if (!Props::isHHLooseElectron.get(electron)) passSel = false;
  if (passSel) m_cutflow->count("LooseElectron");

  const xAOD::TrackParticle *trackPart = electron->trackParticle();
  if(!Props::isMediumLH.exists(electron)) {  // CBG?
     if (trackPart) Warning("ElectronHandler()::isBBTTSignalElectron()","isMediumLH variable doesn't exist, check your derivation file !");
     passSel=false;
  } else{
    if (!Props::isTightLH.get(electron)) passSel = false; //moved to tight for ICHEP triggers
  }
  if (passSel) m_cutflow->count("isMediumLH");
  if (!(electron->pt() > 18000.)) passSel = false;
  if (passSel) m_cutflow->count("pt cut");
  Props::isBBTTSignalElectron.set(electron, passSel);

  return passSel;

}

bool ElectronHandler_HH_bbtautau::isBBTTLLSignalElectron(xAOD::Electron * electron)
{

  bool passSel = true;

  if (!Props::isHHLooseElectron.get(electron)) passSel = false;
  if (passSel) m_cutflow->count("LooseElectron");

  const xAOD::TrackParticle *trackPart = electron->trackParticle();
  if (!Props::isMediumLH.exists(electron)) {
    if (trackPart) Warning("ElectronHandler()::isBBTTLLSignalElectron()","isMediumLH variable doesn't exist, check your derivation file !");
    passSel=false;
  } else{
    if (!Props::isMediumLH.get(electron)) passSel = false;
  }
  if (passSel) m_cutflow->count("isMediumLH");
  if (!(electron->pt() > 10000.)) passSel = false;
  if (passSel) m_cutflow->count("pt cut");
  Props::isBBTTLLSignalElectron.set(electron, passSel);

  return passSel;

}


EL::StatusCode ElectronHandler_HH_bbtautau::writeCustomVariables(xAOD::Electron * inElectron, xAOD::Electron * outElectron, bool /*isKinVar*/, bool isWeightVar,const TString& /*sysName*/) 
{
  if(!isWeightVar){
    Props::isHHLooseElectron.copy(inElectron, outElectron);
    Props::isBBTTSignalElectron.copy(inElectron, outElectron);
    Props::isBBTTLLSignalElectron.copy(inElectron, outElectron);
  }
  
  return EL::StatusCode::SUCCESS;
}
