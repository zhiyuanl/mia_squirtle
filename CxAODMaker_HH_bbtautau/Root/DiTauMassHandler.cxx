#include "CxAODMaker_HH_bbtautau/DiTauMassHandler.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"
#include "EventLoop/StatusCode.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODCore/AuxInfoBase.h"
#include "xAODCore/ShallowAuxInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "CxAODTools/ConfigStore.h"
#include "CxAODTools/ReturnCheck.h"
#include "DiTauMassTools/MissingMassTool.h"
#include <TString.h>
#include <iostream>

// Fill same event info but via different tool to avoid storing the MMC for variations that aren't relavent
// Can be called like all the other tools from the analysis and can be overwriten for different analyses
// Need to deal with syst varaitions that affect MMC output
// Add a getSelection() to event selector and use this to get the selected objects rather than querrying SG
//  - Put this in a func call to fill the inputs to the MMC such that just this can be overwritten
// Think this was to be called from AnalysisBase a bit like jet regression witj allowing to overwrite this and a flag to turn on/off
//   - Not fully sure about this though

DiTauMassHandler::DiTauMassHandler(ConfigStore & config, xAOD::TEvent * event, EventInfoHandler & eventInfoHandler, METHandler& metHandler) :
  m_config(config),
  m_debug(false),
  m_msgLevel(MSG::WARNING),
  m_event(event),
  m_eventInfoHandler(eventInfoHandler),
  m_METHandler(metHandler),
  m_DiTauMass(nullptr)
{
  m_config.getif<bool>("debug", m_debug);
}

DiTauMassHandler::~DiTauMassHandler() {
  delete m_DiTauMass;
}

EL::StatusCode DiTauMassHandler::initialize() {
  if (m_debug) std::cout << "Initialising DiTauMassHandler" << std::endl;
  bool add = TH1::AddDirectoryStatus();
  m_DiTauMass = new MissingMassTool("MissingMassTool");
  if(!m_DiTauMass) return EL::StatusCode::FAILURE;
  TH1::AddDirectory(false);  // to prevent crash on switching file when using m_MMC directly
  EL_CHECK("MMCDecorate()",m_DiTauMass->setProperty("Decorate",false));
  EL_CHECK("MMCcheck()",m_DiTauMass->initialize());
  TH1::AddDirectory(add);
  return  EL::StatusCode::SUCCESS;
}

EL::StatusCode DiTauMassHandler::addVariations(const std::vector<TString> &variations) {
  // here we filter the variations that affect the DiTau mass
  // This doesn't take into account variations that are off?
  for (TString variation : variations) {

    if (!m_eventInfoHandler.get_isMC() && !variation.EqualTo("Nominal")) continue;
    std::cout<<"all variations : "<<variation<<std::endl;
    bool allow = false;
    allow |= variation.Contains("Nominal");
    allow |= variation.Contains("_SME_TES_");
    allow |= variation.Contains("MUONS_SCALE");
    allow |= variation.Contains("MUONS_ID");
    allow |= variation.Contains("MUONS_MS");
    allow |= variation.Contains("EG_RESOLUTION");
    allow |= variation.Contains("EG_SCALE");
    allow |= variation.Contains("MET");
    allow |= variation.Contains("JET_");
    // if( variation.Contains("MET_")){
    //   variation.ReplaceAll("ResoPara", "ResoPara__1up");
    //   variation.ReplaceAll("ResoPerp", "ResoPerp__1up");
    //   variation.ReplaceAll("ScaleUp", "Scale__1up");
    //   variation.ReplaceAll("ScaleDown", "Scale__1down");
    // }
    //allow &= !variation.Contains("_EFF_");
    if (!allow) continue;
    m_variations.push_back(variation);
    std::cout<<"pushed back : "<<variation<<std::endl;
  }

  return EL::StatusCode::SUCCESS;
}

std::vector<TString> DiTauMassHandler::getAllVariations() {
  return m_variations;
}

EL::StatusCode DiTauMassHandler::executeEvent() {

  const int mmcjets = 1;  // recommended

  for (const TString& sysName : getAllVariations()) {
    ResultHHbbtautau result = m_inputs[sysName];
    TString outvariation = sysName;
    if (sysName.Contains("MET_")) {
      outvariation.ReplaceAll("ResoPara", "ResoPara__1up");
      outvariation.ReplaceAll("ResoPerp", "ResoPerp__1up");
      outvariation.ReplaceAll("ScaleUp", "Scale__1up");
      outvariation.ReplaceAll("ScaleDown", "Scale__1down");
    }
    const xAOD::EventInfo* evtInfoOut = m_eventInfosOut[outvariation];

    if (m_debug) std::cout << "Running DiTauMassHandler for " << sysName << " with #taus = " << result.taus.size() << std::endl;

    std::string m_selectionName;
    m_config.getif<std::string>("selectionName", m_selectionName);

    bool found_objects = false;
    CP::CorrectionCode code;
    if (m_selectionName == "leplep") {
      if (result.electrons.size() == 2) {
	      code = m_DiTauMass->apply(*evtInfoOut, result.electrons.at(0), result.electrons.at(1), m_METHandler.getMET(sysName), mmcjets);
        found_objects = true;
      } else if (result.muons.size() == 2) {
	      code = m_DiTauMass->apply(*evtInfoOut, result.muons.at(0), result.muons.at(1), m_METHandler.getMET(sysName), mmcjets);
        found_objects = true;
      } else if (result.electrons.size() == 1 && result.muons.size() == 1) {
        // for consistency, so that object 0 has bigger pt than object one in the mmc output
        if (result.electrons.at(0)->pt() > result.muons.at(0)->pt()) {
	        code = m_DiTauMass->apply(*evtInfoOut, result.electrons.at(0), result.muons.at(0), m_METHandler.getMET(sysName), mmcjets);
        } else {
	        code = m_DiTauMass->apply(*evtInfoOut, result.muons.at(0), result.electrons.at(0), m_METHandler.getMET(sysName), mmcjets);
        }
        found_objects = true;
      }
    }
    if (m_selectionName == "lephad") {
      if (result.mu && result.taus.size() > 0) {
	      code = m_DiTauMass->apply(*evtInfoOut, result.taus.at(0), result.mu, m_METHandler.getMET(sysName), mmcjets);
        found_objects = true;
      } else if (result.el && result.taus.size() > 0) {
	      code = m_DiTauMass->apply(*evtInfoOut, result.taus.at(0), result.el, m_METHandler.getMET(sysName), mmcjets);
        found_objects = true;
      }
    }
    if (m_selectionName == "hadhad" ) {
      if (result.taus.size() > 1) {
	      code = m_DiTauMass->apply(*evtInfoOut, result.taus.at(0), result.taus.at(1), m_METHandler.getMET(sysName), mmcjets);
        found_objects = true;
      }
    }

    if (code != CP::CorrectionCode::Ok) {
      return EL::StatusCode::FAILURE;
    }

    // BR: Why is mlnu3p used? This is supposed for LFV decays ...
    const int status = m_DiTauMass->GetFitStatus(MMCFitMethod::MLNU3P);
    Props::mmc_fit_status.set(evtInfoOut, status);

    TLorentzVector res_vec(0, 0, 0, 0);
    TLorentzVector nu0_vec(0, 0, 0, 0);
    TLorentzVector nu1_vec(0, 0, 0, 0);
    if (status == 1 && found_objects) {
      res_vec = m_DiTauMass->GetResonanceVec(MMCFitMethod::MLNU3P);
      nu0_vec = m_DiTauMass->GetNeutrino4vec(MMCFitMethod::MLNU3P, 0);
      nu1_vec = m_DiTauMass->GetNeutrino4vec(MMCFitMethod::MLNU3P, 1);
    }

    // Resonance
    Props::mmc_mlnu3p_4vect_pt.set(evtInfoOut, res_vec.Pt());
    Props::mmc_mlnu3p_4vect_eta.set(evtInfoOut, res_vec.Eta());
    Props::mmc_mlnu3p_4vect_phi.set(evtInfoOut, res_vec.Phi());
    Props::mmc_mlnu3p_4vect_m.set(evtInfoOut, res_vec.M());

    // nu0
    Props::mmc_mlnu3p_nu0_4vect_pt.set(evtInfoOut, nu0_vec.Pt());
    Props::mmc_mlnu3p_nu0_4vect_eta.set(evtInfoOut, nu0_vec.Eta());
    Props::mmc_mlnu3p_nu0_4vect_phi.set(evtInfoOut, nu0_vec.Phi());
    Props::mmc_mlnu3p_nu0_4vect_m.set(evtInfoOut, nu0_vec.M());

    // nu1
    Props::mmc_mlnu3p_nu1_4vect_pt.set(evtInfoOut, nu1_vec.Pt());
    Props::mmc_mlnu3p_nu1_4vect_eta.set(evtInfoOut, nu1_vec.Eta());
    Props::mmc_mlnu3p_nu1_4vect_phi.set(evtInfoOut, nu1_vec.Phi());
    Props::mmc_mlnu3p_nu1_4vect_m.set(evtInfoOut, nu1_vec.M());
  }
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiTauMassHandler::setOutputContainer() {
  if (m_debug) {
    Info("DiTauMassHandler::setOutputContainer()", "Called.");
  }

  // create containers
  xAOD::EventInfo * eventInfoOutNominal = new xAOD::EventInfo();
  xAOD::AuxInfoBase * eventInfoOutNominalAux = new xAOD::AuxInfoBase();
  eventInfoOutNominal->setStore(eventInfoOutNominalAux);

  m_eventInfosOut["Nominal"] = eventInfoOutNominal;

  // record event info (yes, can be done before setting the actual values)
  if ( ! m_event->record(eventInfoOutNominal, "DiTauMass___Nominal") ) {
    return EL::StatusCode::FAILURE;
  }
  if ( ! m_event->record(eventInfoOutNominalAux, "DiTauMass___NominalAux.") ) {
    return EL::StatusCode::FAILURE;
  }

  // create a shallow copies e.g. for systematic variations of the event weights
  for (TString variation : m_variations) {
    if (variation == "Nominal") continue;
    if( variation.Contains("MET_")){
      variation.ReplaceAll("ResoPara", "ResoPara__1up");
      variation.ReplaceAll("ResoPerp", "ResoPerp__1up");
      variation.ReplaceAll("ScaleUp", "Scale__1up");
      variation.ReplaceAll("ScaleDown", "Scale__1down");
    }
    std::pair< xAOD::EventInfo *, xAOD::ShallowAuxInfo *> eventInfoOutSC = shallowCopyObject(*eventInfoOutNominal);
    if ( ! m_event->record(eventInfoOutSC.first, ("DiTauMass___" + variation).Data())) {
      return EL::StatusCode::FAILURE;
    }
    if ( ! m_event->record(eventInfoOutSC.second, ("DiTauMass___" + variation + "Aux.").Data())) {
      return EL::StatusCode::FAILURE;
    }

    m_eventInfosOut[variation] = eventInfoOutSC.first;
  }
  return EL::StatusCode::SUCCESS;
}

void DiTauMassHandler::writeOutputVariables(xAOD::EventInfo* /*eventInfoIn*/, xAOD::EventInfo* /*eventInfoOut*/, const TString& /*sysName*/) {
  // Copy Props
  return;
}

xAOD::EventInfo * DiTauMassHandler::getOutEventInfoVariation(const TString &variation, bool fallbackToNominal) {

  if (m_debug) {
    Info("DiTauMassHandler::getOutEventInfoVariation()", "Called for variation '%s'.", variation.Data());
  }
  if (m_eventInfosOut.count(variation)) {
    return m_eventInfosOut[variation];
  }
  if (fallbackToNominal && m_eventInfosOut.count("Nominal")) {
    return m_eventInfosOut["Nominal"];
  }
  return nullptr;
}


EL::StatusCode DiTauMassHandler::fillOutputContainer() {

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiTauMassHandler::clearEvent() {

  m_eventInfosOut.clear();

  return EL::StatusCode::SUCCESS;
}
