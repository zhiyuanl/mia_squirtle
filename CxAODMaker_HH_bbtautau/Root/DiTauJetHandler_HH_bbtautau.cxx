#include "CxAODMaker_HH_bbtautau/DiTauJetHandler_HH_bbtautau.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"
#include "tauRecTools/DiTauDiscriminantTool.h"
#include "tauRecTools/DiTauIDVarCalculator.h"
#include "xAODCore/AuxStoreAccessorMacros.h"
//#include "xAODTracking/TrackParticle.h"
//#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTau/DiTauJet.h"
#include "xAODTau/DiTauJetAuxContainer.h"
#include <vector>
#include <iostream>

typedef std::vector< ElementLink< xAOD::TrackParticleContainer > >  TrackParticleLinks_t;

EL::StatusCode DiTauJetHandler_HH_bbtautau::initializeTools()
{
  Info("DiTauJetHandler_HH_bbtautau::initializeTools", "Initialize Tools DiTauJetHandler");

  // ----------------------------------------------------------------------------
  // initialize tool to calculate ID variables
  m_IDVarCalculator = new tauRecTools::DiTauIDVarCalculator("IDVarCalculator");
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_IDVarCalculator->initialize() );

  // initialize ditau discriminant tool
  m_DiscrTool = new tauRecTools::DiTauDiscriminantTool("DiTauDiscriminantTool");
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_DiscrTool->initialize() );

  // initialize truth matching tool
  m_TruthMatchTool = new TauAnalysisTools::DiTauTruthMatchingTool("DiTauTruthMatchingTool");
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_TruthMatchTool->initialize() );

  // initialize efficiency correction tool
  m_EffCorrTool = new TauAnalysisTools::DiTauEfficiencyCorrectionsTool("DiTauEfficiencyCorrectionsTool");
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_EffCorrTool->setProperty("IDLevel", int(TauAnalysisTools::JETIDBDTMEDIUM)) );
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_EffCorrTool->initialize() );

  // initialize energy correction tool
  m_SmearingTool = new TauAnalysisTools::DiTauSmearingTool("DiTauSmearingTool");
  TOOL_CHECK("DiTauJetHandler::initializeTools()", m_SmearingTool->initialize() );

  m_isMC = m_eventInfoHandler.get_isMC();

  // register ISystematicsTools
  // _____________________________________________
  m_sysToolList.clear();
  m_sysToolList.push_back(m_EffCorrTool);
  m_sysToolList.push_back(m_SmearingTool);

  return EL::StatusCode::SUCCESS;

}

DiTauJetHandler_HH_bbtautau::DiTauJetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                             EventInfoHandler & eventInfoHandler) : 
  DiTauJetHandler(name, config, event, eventInfoHandler),
  m_isMC(0) 
{
 using std::placeholders::_1;
 m_selectFcns.clear(); 
 m_selectFcns.push_back(std::bind( &DiTauJetHandler_HH_bbtautau::passDiTauSel, this, _1)); 
}

bool DiTauJetHandler_HH_bbtautau::passDiTauSel(xAOD::DiTauJet* ditau)
{
  if(m_debug) std::cout << "Inside pass pre-sel function" << std::endl;
  bool passDiTauCut=false;
  static const SG::AuxElement::ConstAccessor<int> acc_Nsubjets("n_subjets");
  if (Props::BDTScore.get(ditau) > 0.4 && acc_Nsubjets(*ditau) > 1)
  {
    if(fabs(ditau->eta()) <2.5){
      passDiTauCut=true;
    }
  }
  Props::isDiTauJet.set(ditau, passDiTauCut);
  Props::passPreSel.set(ditau, passDiTauCut);

  if(m_debug) std::cout << "PassPreSel?: " << Props::passPreSel.get(ditau) << std::endl;

 return passDiTauCut;
}

EL::StatusCode DiTauJetHandler_HH_bbtautau::setVariablesCustom(xAOD::DiTauJet* inDiTau, xAOD::DiTauJet* outDiTau, bool /*isKinVar*/, bool isWeightVar)
{
  // set something without having a pre-defined method
  if(!isWeightVar){
    Props::isDiTauJet.copy(inDiTau, outDiTau);
  }
  return EL::StatusCode::SUCCESS;

}

EL::StatusCode DiTauJetHandler_HH_bbtautau::writeNominal(xAOD::DiTauJet* inTau, xAOD::DiTauJet* outTau, const TString& /*sysName*/) {
  Props::isDiTauJet.copy(inTau,outTau);
  Props::BDTScore.copy(inTau,outTau);
  Props::n_subjets.copy(inTau,outTau);
  Props::subjet_lead_pt.copy(inTau,outTau);
  Props::subjet_lead_eta.copy(inTau,outTau);
  Props::subjet_lead_phi.copy(inTau,outTau);
  Props::subjet_lead_e.copy(inTau,outTau);
  Props::subjet_subl_pt.copy(inTau,outTau);
  Props::subjet_subl_eta.copy(inTau,outTau);
  Props::subjet_subl_phi.copy(inTau,outTau);
  Props::subjet_subl_e.copy(inTau,outTau);
  Props::charge.copy(inTau,outTau);
  Props::subjet_lead_ntracks.copy(inTau,outTau);
  Props::subjet_subl_ntracks.copy(inTau,outTau);
  Props::subjet_lead_charge.copy(inTau,outTau);
  Props::subjet_subl_charge.copy(inTau,outTau);
  if (m_isMC) Props::TruthMatch.copy(inTau,outTau);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiTauJetHandler_HH_bbtautau::decorate(xAOD::DiTauJet * ditau) {
  /* Set ditau variables - derived ones*/

  Props::n_subjets.set(ditau, ditau->auxdata<int>("n_subjets"));
  Props::subjet_lead_pt.set(ditau, ditau->subjetPt(0));
  Props::subjet_lead_eta.set(ditau, ditau->subjetEta(0));
  Props::subjet_lead_phi.set(ditau, ditau->subjetPhi(0));
  Props::subjet_lead_e.set(ditau, ditau->subjetE(0));
  Props::subjet_subl_pt.set(ditau, ditau->subjetPt(1));
  Props::subjet_subl_eta.set(ditau, ditau->subjetEta(1));
  Props::subjet_subl_phi.set(ditau, ditau->subjetPhi(1));
  Props::subjet_subl_e.set(ditau, ditau->subjetE(1));
  // charge decoration
  if (!ditau->isAvailable< TrackParticleLinks_t >("trackLinks") )
  {
    Warning("DiTauJetHandler::decorate", "DiTau track links not available.");
    return StatusCode::FAILURE;
  } 
  int charge = 0;
  int lead_ntracks = 0;
  int subl_ntracks = 0;
  int lead_charge = 0;
  int subl_charge = 0;
  for (const auto xTrack: ditau->trackLinks())
  {
    if (!xTrack.isValid()) 
    {
      Warning("DiTauJetHandler::decorate", "DiTau track link is not valid.");
      continue;
    }
    
    for (int i=0; i<2; ++i) // loop over two leading subjets
    {
      TLorentzVector tlvSubjet = TLorentzVector();
      tlvSubjet.SetPtEtaPhiE(ditau->subjetPt(i),
                             ditau->subjetEta(i),
                             ditau->subjetPhi(i),
                             ditau->subjetE(i));
      double dR = tlvSubjet.DeltaR((*xTrack)->p4());
   
      if (dR < 0.1)
      {
        charge += (*xTrack)->charge();
        if (i == 0)
        {
          lead_charge += (*xTrack)->charge();
          lead_ntracks++;
        }
        else if (i == 1)
        {
          subl_charge += (*xTrack)->charge();
          subl_ntracks++;
        }
        break; // prevents double counting of tracks
      }
    } // loop over subjets
  } // loop over tracks
  Props::charge.set(ditau, charge);
  Props::subjet_lead_ntracks.set(ditau, lead_ntracks);
  Props::subjet_subl_ntracks.set(ditau, subl_ntracks);
  Props::subjet_lead_charge.set(ditau, lead_charge);
  Props::subjet_subl_charge.set(ditau, subl_charge);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiTauJetHandler_HH_bbtautau::calibrateCopies(xAOD::DiTauJetContainer * particles, const CP::SystematicSet & sysSet)
{

  CP_CHECK( "DiTauJetHandler_HH_bbtautau::calibrateCopies()", m_EffCorrTool->applySystematicVariation(sysSet), m_debug );
  CP_CHECK( "DiTauJetHandler_HH_bbtautau::calibrateCopies()", m_SmearingTool->applySystematicVariation(sysSet), m_debug );

  for (xAOD::DiTauJet * ditau : *particles)
  {
    // DiTau Identification
    TOOL_CHECK("DiTauJetHandler::decorate()", m_IDVarCalculator->calculateIDVariables(*ditau) );
    double BDTScore = m_DiscrTool->getJetBDTScore(*ditau);
    bool isBDTMedium = (BDTScore > 0.72);
    double effSF = 1.0;
    double nominalJetPt = ditau->pt();

    // truth matching decorations
    if (m_isMC)
    {
      m_TruthMatchTool->getTruth(*ditau);
      static const SG::AuxElement::ConstAccessor<char> acc_IsTruthHadronic("IsTruthHadronic");
      Props::TruthMatch.set(ditau, (int)acc_IsTruthHadronic(*ditau));
    }

    // efficiency correction
    if (isBDTMedium && m_isMC)
      CP_CHECK( "DiTauJetHandler_HH_bbtautau::calibrateCopies()", m_EffCorrTool->getEfficiencyScaleFactor(*ditau, effSF), m_debug );

    // energy correction
    if (m_isMC)
      CP_CHECK( "DiTauJetHandler_HH_bbtautau::calibrateCopies()", m_SmearingTool->applyCorrection(*ditau), m_debug );
    
    double correctedJetPt = ditau->pt();

    TLorentzVector tlvSubj0;
    TLorentzVector tlvSubj1;
    tlvSubj0.SetPtEtaPhiE(ditau->subjetPt(0),  ditau->subjetEta(0), ditau->subjetPhi(0), ditau->subjetE(0));
    tlvSubj1.SetPtEtaPhiE(ditau->subjetPt(1),  ditau->subjetEta(1), ditau->subjetPhi(1), ditau->subjetE(1));
    TLorentzVector tlvDiTauSubs = tlvSubj0 + tlvSubj1;
    if (m_isMC)
      tlvDiTauSubs.SetPtEtaPhiM(correctedJetPt/nominalJetPt * tlvDiTauSubs.Pt(), tlvDiTauSubs.Eta(), tlvDiTauSubs.Phi(), tlvDiTauSubs.M() );
    

    // decorate ditau
    Props::effSF.set(ditau, effSF);
    Props::BDTScore.set(ditau, BDTScore);
    Props::subjets_pt.set(ditau, tlvDiTauSubs.Pt());
    Props::subjets_eta.set(ditau, tlvDiTauSubs.Eta());
    Props::subjets_phi.set(ditau, tlvDiTauSubs.Phi());
    Props::subjets_e.set(ditau, tlvDiTauSubs.E());

    if (decorate(ditau) != EL::StatusCode::SUCCESS) return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode DiTauJetHandler_HH_bbtautau::writeWeightVariations(xAOD::DiTauJet* inDiTau, xAOD::DiTauJet* outDiTau, const TString& sysName)
{
  if (m_isMC)
    Props::effSF.copy(inDiTau, outDiTau);
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode DiTauJetHandler_HH_bbtautau::writeKinematicVariations(xAOD::DiTauJet* inTau, xAOD::DiTauJet* outTau, const TString& /*sysName*/)
{
  // set four momentum
  setP4( inTau , outTau );
  Props::subjets_pt.copy(inTau,outTau);
  Props::subjets_eta.copy(inTau,outTau);
  Props::subjets_phi.copy(inTau,outTau);
  Props::subjets_e.copy(inTau,outTau);

  return EL::StatusCode::SUCCESS;
}
