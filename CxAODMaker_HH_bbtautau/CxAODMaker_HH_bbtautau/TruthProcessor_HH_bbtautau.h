// Dear emacs, this is -*-c++-*-
//
/*
 * \class TruthProcessor_HH_bbtautau
 *
 * \ingroup CxAODMaker_HH_bbtautau
 *
 * \brief This implements a derived class for reading and decorating truth-related objects in the HH->bbtautau analysis.
 *
 */

#ifndef CxAODMaker_HH_bbtautau_TruthProcessor_H
#define CxAODMaker_HH_bbtautau_TruthProcessor_H

#include "CxAODMaker/TruthProcessor.h"

#include <vector>
#include <set>
#include <TString.h>
#include <string>

// Analysis includes
#include "CxAODTools/ConfigStore.h"

// infrastructure includes
#include "EventLoop/StatusCode.h"

#include "TError.h"

#include "xAODRootAccess/TEvent.h"

// Analysis includes
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools/ReturnCheck.h"
#include "CxAODTools/ParticleLinker.h"

#include <utility>
#include <iterator>

class EventInfoHandler;

// EDM includes (which rootcint doesn't like)
#ifndef __MAKECINT__

#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"

#endif // not __MAKECINT__

#include "xAODTruth/TruthParticleContainer.h"

class TruthProcessor_HH_bbtautau : public TruthProcessor {
 
public:

  /// \brief Constructor
  /// \param name Name of the class
  /// \param config The object used to access the configuration database
  /// \param event The event store
  /// \param eventInfoHandler The handler used to access the EventInfo object per event.
  TruthProcessor_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
                       EventInfoHandler & eventInfoHandler);
  
  /// \brief Destructor
  virtual ~TruthProcessor_HH_bbtautau();

  /// \brief Copy extra information from the input particle to the output one. Called by writeOutputVariables.
  /// \param inPart Input particle from the derivation.
  /// \param outPart Output particle to be written in the CxAOD.
  /// \return Status code.
  
  virtual EL::StatusCode writeCustomOutputVariables(xAOD::TruthParticle * inPart, xAOD::TruthParticle * outPart) override;

  /// \brief Called by passTruthParticle. Should be overwritten.
  /// \param part Particle to consider.
  /// \param containerName Container where the particle was.
  /// \return Whether to write it in the CxAOD.
  virtual bool passCustomTruthParticle(xAOD::TruthParticle * part, const std::string &containerName)override;

  private:
    bool m_isMC;
    std::string m_tauContainerName;

};

#endif
