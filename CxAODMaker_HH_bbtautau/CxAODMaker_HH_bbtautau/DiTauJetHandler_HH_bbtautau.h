#ifndef CxAODMaker_DiTauJetHandler_HH_bbtautau_H
#define CxAODMaker_DiTauJetHandler_HH_bbtautau_H

#include "CxAODMaker/DiTauJetHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODTools/CommonProperties.h"
#include "TauAnalysisTools/DiTauTruthMatchingTool.h"
#include "TauAnalysisTools/DiTauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/DiTauSmearingTool.h"

class DiTauJetHandler_HH_bbtautau : public DiTauJetHandler {
  
 public:
  
  DiTauJetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
		     EventInfoHandler & eventInfoHandler);
  
 protected:
  bool m_isMC;
  TauAnalysisTools::DiTauTruthMatchingTool * m_TruthMatchTool;
  TauAnalysisTools::DiTauEfficiencyCorrectionsTool * m_EffCorrTool;
  TauAnalysisTools::DiTauSmearingTool * m_SmearingTool;

  virtual EL::StatusCode initializeTools() override;
  EL::StatusCode decorate(xAOD::DiTauJet * ditau) override;
  EL::StatusCode writeNominal(xAOD::DiTauJet* inTau, xAOD::DiTauJet* outTau, const TString& /*sysName*/) override;
  EL::StatusCode calibrateCopies(xAOD::DiTauJetContainer * particles, const CP::SystematicSet & sysSet) override;
  EL::StatusCode writeWeightVariations(xAOD::DiTauJet* inDitau, xAOD::DiTauJet* outDitau, const TString& sysName) override;
  EL::StatusCode writeKinematicVariations(xAOD::DiTauJet* inDitau, xAOD::DiTauJet* outDitau, const TString& sysName) override;


 private:
  
  // selection functions
  bool passDiTauSel(xAOD::DiTauJet* ditau);
  virtual EL::StatusCode setVariablesCustom(xAOD::DiTauJet * inDiTau, xAOD::DiTauJet * outDiTau, bool isKinVar, bool isWeightVar);

};

#endif
