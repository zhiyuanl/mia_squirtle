#ifndef CxAODMaker_AnalysisBase_HH_bbtautau_H
#define CxAODMaker_AnalysisBase_HH_bbtautau_H

#include "CxAODMaker/AnalysisBase.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/DiTauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"
#include "xAODMissingET/MissingET.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauResult.h"


class TH1D;
class EventSelector;
class DiTauMassHandler;
template <typename T> class PROP;

class AnalysisBase_HH_bbtautau : public AnalysisBase {
  ClassDefOverride(AnalysisBase_HH_bbtautau, 1);

protected:
  virtual EL::StatusCode initialize() override;
  virtual EL::StatusCode initializeSampleInfo() override;
  virtual EL::StatusCode initializeHandlers() override;
  virtual EL::StatusCode initializeSelection() override;
  virtual EL::StatusCode initializeSelector() override;

  virtual EL::StatusCode fileExecute() override;
  virtual EL::StatusCode execute() override;
  virtual EL::StatusCode finalize() override;
  virtual EL::StatusCode postExecute() override;

  virtual bool doRandomTauSelection(ResultHHbbtautau &);
  virtual const xAOD::TauJet* pickRandomTau(  std::vector<const xAOD::TauJet*> & taus,
					      const xAOD::TauJet* vetoTau);
  std::string selectionName;

  bool m_runFourTau; //!
  bool m_useRNNTaus = false; //!
  std::string m_tauIDWP = "medium"; //!
  PROP<int> *m_passIDProp = nullptr; //!

  // For writing out the sum of weights for LHE3 weight variations
  bool m_write_lhe3weights = false; //!
  std::map<const std::string, TH1D*> m_lhe3weights; //!
  virtual EL::StatusCode writeLHE3WeightHists();

public:

  AnalysisBase_HH_bbtautau();
  ~AnalysisBase_HH_bbtautau();

private:
#ifndef __MAKECINT__
  EventSelector* m_preselector; //!
  DiTauMassHandler* m_diTauMassHandler; //!
  bool m_doDiTauMass;
#endif // not __MAKECINT__


};
#endif
