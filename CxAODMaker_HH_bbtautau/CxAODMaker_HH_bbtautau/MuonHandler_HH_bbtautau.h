#ifndef CxAODMaker_MuonHandler_HH_bbtautau_H
#define CxAODMaker_MuonHandler_HH_bbtautau_H

#include "CxAODMaker/MuonHandler.h"
#include "CxAODTools/CommonProperties.h"
class MuonHandler_HH_bbtautau : public MuonHandler {

public:

  MuonHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
         EventInfoHandler & eventInfoHandler);

protected:
  bool passHHLooseMuon(xAOD::Muon * muon);
  bool passHH_bbtautauMuon(xAOD::Muon * muon);
  bool passHH_bbtautaullMuon(xAOD::Muon * muon);
  
  virtual EL::StatusCode writeCustomVariables(xAOD::Muon * inMuon, xAOD::Muon * outMuon, bool isKinVar, bool isWeightVar,const TString& sysName) override;

};


#endif
