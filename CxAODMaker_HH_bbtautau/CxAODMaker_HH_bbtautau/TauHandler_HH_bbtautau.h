#ifndef CxAODMaker_TauHandler_HH_bbtautau_H
#define CxAODMaker_TauHandler_HH_bbtautau_H

#include "CxAODMaker/TauHandler.h"
#include "CxAODTools/CommonProperties.h"
class TauHandler_HH_bbtautau : public TauHandler {

public:

  TauHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
         EventInfoHandler & eventInfoHandler);

  EL::StatusCode removeTausExcept(int index, int index2 = -1);
  
protected:

  bool passHH_bbtautauTau(xAOD::TauJet * tau);
  bool LooseTau(xAOD::TauJet * tau);
  virtual EL::StatusCode writeCustomVariables(xAOD::TauJet * inTau, xAOD::TauJet * outTau, bool isKinVar, bool isWeightVar,const TString& sysName) override;
  virtual EL::StatusCode writeNominal(xAOD::TauJet* inTau, xAOD::TauJet* outTau, const TString& sysName) override;
  virtual EL::StatusCode writeKinematicVariations(xAOD::TauJet *inTau, xAOD::TauJet *outTau, const TString &sysName) override;
  virtual EL::StatusCode initializeTools();
  virtual EL::StatusCode decorate(xAOD::TauJet *tau);

private:
  bool m_match_L1_jets = false;
  EL::StatusCode matchL1Jet(xAOD::TauJet *tau);
};

#endif

