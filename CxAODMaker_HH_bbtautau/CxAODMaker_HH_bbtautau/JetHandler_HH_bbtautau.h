#ifndef CxAODMaker_JetHandler_HH_bbtautau_H
#define CxAODMaker_JetHandler_HH_bbtautau_H

#include "CxAODMaker/JetHandler.h"
#include "CxAODTools/CommonProperties.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"

class JetHandler_HH_bbtautau : public JetHandler {

public:

  JetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
             EventInfoHandler & eventInfoHandler);

  virtual ~JetHandler_HH_bbtautau();

protected:
  
  // selection functions
  bool passVetoJet(xAOD::Jet* jet);
  bool passSignalJet(xAOD::Jet* jet);
  bool checkCentralJet(xAOD::Jet* jet,bool isInCutFlow=0);

  BTaggingSelectionTool* m_btagtool;

  virtual EL::StatusCode writeCustomVariables(xAOD::Jet * inPart, xAOD::Jet * outPart, bool isKinVar, bool isWeightVar, const TString& sysName);
  virtual EL::StatusCode initializeTools();
  virtual EL::StatusCode writeNominal(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName) override;
  virtual EL::StatusCode writeKinematicVariations(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName);
  virtual EL::StatusCode decorate(xAOD::Jet*);

private:
  EL::StatusCode matchL1Jet(xAOD::Jet *);

private:
  std::string m_btag_cdi = "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-06-24_v1.root";
  std::string m_btag_jet_collection = "AntiKt4EMTopoJets";
  std::string m_btag_tagger = "MV2c10";
  std::string m_btag_wp = "FixedCutBEff_70";
  bool m_match_L1_jets = false;
};

#endif
