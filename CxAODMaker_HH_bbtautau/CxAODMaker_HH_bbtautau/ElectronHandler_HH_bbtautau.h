#ifndef CxAODMaker_ElectronHandler_HH_bbtautau_H
#define CxAODMaker_ElectronHandler_HH_bbtautau_H

#include "CxAODMaker/ElectronHandler.h"
#include "CxAODTools/CommonProperties.h"
class ElectronHandler_HH_bbtautau : public ElectronHandler {

public:
  ElectronHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
         EventInfoHandler & eventInfoHandler);

protected:
 //Selection functions
  bool passHHLooseElectron(xAOD::Electron * electron);
  bool isBBTTSignalElectron(xAOD::Electron * electron);
  bool isBBTTLLSignalElectron(xAOD::Electron * electron);
  //Override function (calls base)
  virtual EL::StatusCode writeCustomVariables(xAOD::Electron* inPart, xAOD::Electron* outPart,
					      bool isKinVar, bool isWeightVar,const TString& sysName) override;

};
#endif
