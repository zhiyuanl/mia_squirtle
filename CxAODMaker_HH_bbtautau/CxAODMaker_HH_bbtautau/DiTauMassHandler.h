// Dear emacs, this is -*-c++-*-
#ifndef CxAODMaker_DiTauMassHandler_H
#define CxAODMaker_DiTauMassHandler_H

// Framework includes
#ifndef __MAKECINT__
#include "CxAODTools/CommonProperties.h"
#include "AsgTools/MsgLevel.h"
#endif // not __MAKECINT__
#include "xAODEventInfo/EventInfo.h"
#include "EventLoop/StatusCode.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/METHandler.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/DiTauJetContainer.h"
#include <TString.h>
#include <vector>

#include "CxAODTools_HH_bbtautau/HHbbtautauResult.h"

class ConfigStore;

class MissingMassTool;

class DiTauMassHandler {


public:
  DiTauMassHandler(ConfigStore & config, xAOD::TEvent * event, EventInfoHandler & eventInfoHandler, METHandler & metHandler);
  virtual ~DiTauMassHandler();

  // initialize tools and stuff
  virtual EL::StatusCode initialize();
  // add systematic variations that affect the MMC
  virtual EL::StatusCode addVariations(const std::vector<TString> &variations);
  // read the info from the event
  virtual EL::StatusCode executeEvent();
  // initialize output container for all variations
  // a preceeding call of executeEvent() is required
  virtual EL::StatusCode setOutputContainer();
  // fill/record output container
  virtual EL::StatusCode fillOutputContainer();
  // get all variations that affect the event info
  virtual std::vector<TString> getAllVariations();
  // get output event info variation (e.g. for writing event variables)
  // clean shallow copy
  EL::StatusCode clearEvent();

  virtual void setInputs(std::map<TString, ResultHHbbtautau>& results) {m_inputs = results;}

protected:

  ConfigStore & m_config;
  bool m_debug;
  MSG::Level m_msgLevel;

  xAOD::TEvent * m_event;

  EventInfoHandler& m_eventInfoHandler;

  METHandler& m_METHandler;

  MissingMassTool* m_DiTauMass; //!

  std::vector<TString> m_variations;

  std::map<TString, ResultHHbbtautau> m_inputs;
  std::map<TString, xAOD::EventInfo*> m_eventInfosOut;

  virtual void writeOutputVariables(xAOD::EventInfo* eventInfoIn, xAOD::EventInfo* eventInfoOut,const TString& sysName);

  // get output diTau event info variation (e.g. for writing event variables)
  xAOD::EventInfo * getOutEventInfoVariation(const TString &variation, bool fallbackToNominal = true);
};

#endif
