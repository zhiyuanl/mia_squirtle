#ifndef CxAODMaker_FatJetHandler_HH_bbtautau_H
#define CxAODMaker_FatJetHandler_HH_bbtautau_H

#include "CxAODMaker/FatJetHandler.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
class FatJetHandler_HH_bbtautau : public FatJetHandler {
  
 public:
  
  FatJetHandler_HH_bbtautau(const std::string& name, ConfigStore & config, xAOD::TEvent * event,
		     EventInfoHandler & eventInfoHandler);

  ~FatJetHandler_HH_bbtautau();

  EL::StatusCode initializeTools() override;
  
 protected:
  
  BTaggingSelectionTool* m_btagtool60;
  BTaggingSelectionTool* m_btagtool70;
  BTaggingSelectionTool* m_btagtool77;
  BTaggingSelectionTool* m_btagtool85;

  // selection functions
  bool passFatJet(xAOD::Jet* jet);
  virtual EL::StatusCode writeCustomVariables(xAOD::Jet * inJet, xAOD::Jet * outJet, bool isKinVar, bool isWeightVar,const TString& sysName) override;
  virtual EL::StatusCode writeKinematicVariations(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName);
  virtual EL::StatusCode writeNominal(xAOD::Jet* inJet, xAOD::Jet* outJet, const TString& sysName);
  virtual EL::StatusCode decorate(xAOD::Jet * jet) override;
};

#endif
