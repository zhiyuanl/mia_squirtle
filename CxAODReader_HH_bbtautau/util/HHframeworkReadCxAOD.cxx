#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/DiskListLocal.h"

#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/DiskListXRD.h"
#include "EventLoop/Job.h"

#include "EventLoop/LSFDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/Driver.h"

#include "SampleHandler/Sample.h"

#include <stdlib.h> 
#include <vector> 

#include <TSystem.h> 
#include <TFile.h> 

#include "CxAODTools/ConfigStore.h"

#include "CxAODReader_HH_bbtautau/AnalysisReader_hhbbtt.h"

void tag(SH::SampleHandler& sh, const std::string& tag);

int main(int argc, char* argv[]) {

  // Take the submit directory from the input if provided:
  std::string configfile = "framework-read";
  std::string submitDir  = "submitDir";
  std::string configPath = "data/CxAODReader_HH_bbtautau/"+configfile+".cfg";
  std::string sample     = "select";
  std::string variation  = "select";
  std::string driver     = "";

  if (argc > 1){
    configfile = argv[1];
    configPath = "data/CxAODReader_HH_bbtautau/"+configfile+".cfg";
    Info("HHframeworkReadCxAOD","Reading config file from command line : %s", configPath.c_str());
  }
  if (argc > 2) submitDir  = argv[2];
  if (argc > 3) sample     = argv[3];
  if (argc > 4) variation  = argv[4];
  if (argc > 5) driver     = argv[5];

  // read run config
  static ConfigStore* config = ConfigStore::createStore(configPath);

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  std::vector<std::string> sample_names;
  // list all samples here - if they don't exist it will skip
  sample_names.push_back("data");
  sample_names.push_back("data15");
  sample_names.push_back("data16");
  sample_names.push_back("data17");
  sample_names.push_back("singletop_s");
  sample_names.push_back("singletop_t");
  sample_names.push_back("singletop_Wt");
  sample_names.push_back("ttbar_PwPy8EG");
  sample_names.push_back("WtaunuB_NNPDF");
  sample_names.push_back("WtaunuC_NNPDF");
  sample_names.push_back("WtaunuL_NNPDF");
  sample_names.push_back("ZtautauB_NNLOPDF");
  sample_names.push_back("ZtautauC_NNLOPDF");
  sample_names.push_back("ZtautauL_NNLOPDF");
  sample_names.push_back("dijet");
  sample_names.push_back("DYtautauLO");
  sample_names.push_back("ggH125_tautauhh");
  sample_names.push_back("LQ3LQ31000");
  sample_names.push_back("LQ3LQ3250");
  sample_names.push_back("LQ3LQ3500");
  sample_names.push_back("NonResHH_bbtt");
  sample_names.push_back("RSG_C10_M1000");
  sample_names.push_back("RSG_C10_M260");
  sample_names.push_back("RSG_C10_M300");
  sample_names.push_back("RSG_C10_M400");
  sample_names.push_back("RSG_C10_M500");
  sample_names.push_back("RSG_C10_M600");
  sample_names.push_back("RSG_C10_M700");
  sample_names.push_back("RSG_C10_M800");
  sample_names.push_back("RSG_C10_M900");
  sample_names.push_back("RSG_C20_M300");
  sample_names.push_back("RSG_C20_M700");
  sample_names.push_back("RSG_C10_M1000_4Taus");
  sample_names.push_back("RSG_C10_M3000_4Taus");
  sample_names.push_back("RSG_C10_M4000_4Taus");
  sample_names.push_back("RSG_C10_M5000_4Taus");

  sample_names.push_back("singletop_Wt_dilep");
  sample_names.push_back("ttbar");
  sample_names.push_back("ttbarH_tautau");
  sample_names.push_back("ttbar_Syst");
  sample_names.push_back("VBFH125_tautauhh");
  sample_names.push_back("WH125_inc");
  sample_names.push_back("Wtaunu");
  sample_names.push_back("WtaunuB");
  sample_names.push_back("WtaunuC");
  sample_names.push_back("WtaunuL");
  sample_names.push_back("Wtaunu_MG");
  sample_names.push_back("Wtaunu_Pw");
  sample_names.push_back("Wtaunu_221");
  sample_names.push_back("Wmunu_Pw");
  sample_names.push_back("Wenu_Pw");
  sample_names.push_back("WW");
  sample_names.push_back("WW_Pw");
  sample_names.push_back("WZ");
  sample_names.push_back("WZ_Pw");
  sample_names.push_back("VH");
  sample_names.push_back("Xtohh1000");
  sample_names.push_back("Xtohh2000");
  sample_names.push_back("Xtohh1000_Herwig");
  sample_names.push_back("Xtohh1200_Herwig");
  sample_names.push_back("Xtohh1400_Herwig");
  sample_names.push_back("Xtohh1600_Herwig");
  sample_names.push_back("Xtohh1800_Herwig");
  sample_names.push_back("Xtohh2000_Herwig");
  sample_names.push_back("Xtohh2500_Herwig");
  sample_names.push_back("Xtohh3000_Herwig");
  sample_names.push_back("Xtohh260");
  sample_names.push_back("Xtohh300");
  sample_names.push_back("Xtohh400");
  sample_names.push_back("Xtohh500");
  sample_names.push_back("Xtohh600");
  sample_names.push_back("Xtohh700");
  sample_names.push_back("Xtohh800");
  sample_names.push_back("ZH125_inc");
  sample_names.push_back("ZHll125");
  sample_names.push_back("ZHll125J_MINLO");
  sample_names.push_back("Ztautau");
  sample_names.push_back("ZtautauB");
  sample_names.push_back("ZtautauC");
  sample_names.push_back("ZtautauL");
  sample_names.push_back("Zmumu");
  sample_names.push_back("ZmumuB");
  sample_names.push_back("ZmumuC");
  sample_names.push_back("ZmumuL");
  sample_names.push_back("Zmumu_Pw");
  sample_names.push_back("Zee");
  sample_names.push_back("ZeeB");
  sample_names.push_back("ZeeC");
  sample_names.push_back("ZeeL");
  sample_names.push_back("Ztautau_MG");
  sample_names.push_back("Ztautau_Pw");
  sample_names.push_back("ZZ");
  sample_names.push_back("ZZ_Pw");
  sample_names.push_back("Zee_221");
  sample_names.push_back("Zee_Pw");
  sample_names.push_back("Ztautau_221");
  sample_names.push_back("Ztautau_Pw");
  sample_names.push_back("stopWt");
  sample_names.push_back("stopschan");
  sample_names.push_back("stoptchan");
  // VBF non-resonant samples
  sample_names.push_back("vbf_hh_l0cvv0cv1");
  sample_names.push_back("vbf_hh_l1cvv0cv1");
  sample_names.push_back("vbf_hh_l1cvv0p5cv1");
  sample_names.push_back("vbf_hh_l1cvv1cv1");
  sample_names.push_back("vbf_hh_l1cvv1p5cv1");
  sample_names.push_back("vbf_hh_l1cvv2cv1");
  sample_names.push_back("vbf_hh_l0cvv1cv1");
  sample_names.push_back("vbf_hh_l1cvv0cv0p5");
  sample_names.push_back("vbf_hh_l1cvv0cv0");
  sample_names.push_back("vbf_hh_l1cvv1cv0p5");
  sample_names.push_back("vbf_hh_l2cvv1cv1");
  sample_names.push_back("vbf");

  // Possibility to skip some of the samples above
  // Read bools from config file and if false remove sample from vector
  std::vector<std::string>::iterator itr;
  for (itr = sample_names.begin(); itr != sample_names.end();) {
    bool includeSample = false;
    config->getif<bool>(*itr, includeSample);
    if ( !includeSample && sample == "select") itr = sample_names.erase(itr);
    else if (sample != "select" && *itr != sample) itr = sample_names.erase(itr);
    else ++itr;
  }

  std::string dataset_dir = config->get<std::string>("dataset_dir");
  
  std::string eostype = "";
  std::string prefix_atlas = "/eos/atlas";
  std::string prefix_user = "/eos/user";
  if ( dataset_dir.substr(0, prefix_atlas.size()).compare(prefix_atlas) == 0 ) {
    eostype = "eosatlas.cern.ch";
    std::cout << "Will read datasets from ATLAS EOS directory " << dataset_dir << std::endl;
  } else if ( dataset_dir.substr(0, prefix_user.size()).compare(prefix_user) == 0 ) {
    eostype = "eosuser.cern.ch";
    std::cout << "Will read datasets from user EOS directory " << dataset_dir << std::endl;
  } else {
    std::cout << "Will read datasets from local directory " << dataset_dir << std::endl;
  }

  // fill local variable 'variations' as wanted
  // Now only one systematics at the time
  std::vector<std::string> variations;
  if(variation != "select") variations.push_back(variation);

  // Query - I had to put each background in a separate directory
  // for samplehandler to have sensible sample names etc.
  // Is it possible to define a search string for directories and assign all those to a sample name?
  // SH::scanDir (sampleHandler, list); // apparently it will come

  // create the sample handler
  SH::SampleHandler sampleHandler;
  for (unsigned int isamp(0) ; isamp<sample_names.size() ; isamp++) {
    std::string sample_name(sample_names.at(isamp));
    std::string sample_dir(dataset_dir+sample_name);

    if (eostype == "") {
      bool direxists=gSystem->OpenDirectory (sample_dir.c_str());
      if (!direxists) {
      	std::cout << " No sample exists: " << sample_name << " , skipping: "  << sample_dir << std::endl;
	      continue;
      }
    }
    
    // eos, local disk or afs
    if (eostype != "" ) {
      SH::DiskListXRD list(eostype.c_str(), sample_dir, true);
      //SH::DiskListEOS list(sample_dir,"root://eosatlas/"+sample_dir );
     
      //SH::scanSingleDir (sampleHandler, sample_name, list, "*CxAOD*") ; 
      SH::ScanDir()
               .filePattern("*CxAOD*")
               .sampleName(sample_name)
               .scan(sampleHandler, list);
    } else {
      SH::DiskListLocal list(sample_dir);
      // tuples are downloaded to same directory as CxAOD so specify CxAOD root file pattern
      //SH::scanSingleDir (sampleHandler, sample_name, list, "*CxAOD*") ;
      SH::ScanDir()
               .filePattern("*CxAOD*")
               .sampleName(sample_name)
               .scan(sampleHandler, list);
    }
       
    //
    SH::Sample* sample=sampleHandler.get(sample_name);
    int nsampleFiles=sample->numFiles();
    std::cout << "Sample name " << sample_name << " with nfiles : " <<  nsampleFiles  << std::endl;
  }


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // Print what we found:
  sampleHandler.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);

  // remove submit dir before running the job
  //job.options()->setDouble(EL::Job::optRemoveSubmitDir, 1);

  // create algorithm, set job options, maniplate members and add our analysis to the job:
  AnalysisReader* algorithm = new AnalysisReader_hhbbtt();
  algorithm->setConfig(config);

  // then overwrite 'variations' in config
  bool overwrite = false;
  if(variation != "select"){
    overwrite = true;
    config->put<std::vector<std::string> >("variations", variations, overwrite);
  }
  
  //limit number of events to maxEvents - set in config
  job.options()->setDouble (EL::Job::optMaxEvents, config->get<int>("maxEvents"));

  // try different access
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop#Access_the_Data_Through_xAOD_EDM
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);
  //job.options()->setString (EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class);
  int nFilesPerJob = 10;
  config->getif<int>("nFilesPerJob", nFilesPerJob);

  // add algorithm to job
  job.algsAdd(algorithm);

  // Run the job using the local/direct driver:

  //std::string driver = "direct";
  if(driver == "") config->getif<std::string>("driver",driver);
  if (driver=="direct"){
      EL::DirectDriver*  eldriver = new EL::DirectDriver;
      eldriver->submit(job, submitDir);
  } else if (driver=="LSF"){
      EL::LSFDriver* eldriver = new EL::LSFDriver;
      eldriver->options()->setString (EL::Job::optSubmitFlags, "-L /bin/bash");
      eldriver->shellInit = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
      
      job.options()->setDouble (EL::Job::optFilesPerWorker, 20);
      job.options()->setString (EL::Job::optSubmitFlags, "-q 1nh"); //1nh 8nm
      eldriver->submitOnly(job, submitDir);
  } else if (driver == "condor") {  // Spyros: add condor driver
    EL::CondorDriver* eldriver = new EL::CondorDriver;
    eldriver->shellInit =
        "export "
        "ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && "
        "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh";
    job.options()->setDouble(EL::Job::optFilesPerWorker, nFilesPerJob);
    std::string condor_config = "";
    std::string condor_queue = "longlunch"; //Choose one of these: espresso, microcentury, longlunch, workday, tomorrow, testmatch, nextweek
    config->getif<std::string>("condor_queue", condor_queue);
	 // for the moment fix to run on CentOS7 machines
	 condor_config.append(" \n");
	 condor_config.append("requirements = (OpSysAndVer =?= \"CentOS7\")");
	 condor_config.append(" \n");
    //condor_config.append("requirements = (OpSysAndVer =?= \"CentOS7\"");
    if (condor_queue != "none")
      condor_config.append(("+JobFlavour = \"" + condor_queue + "\"").c_str());

    std::string accountingGroup = "none";
    config->getif<std::string>("accountingGroup", accountingGroup);
    if (accountingGroup != "none") {
      condor_config.append(" \n");
      condor_config.append(("accounting_group = " + accountingGroup).c_str());
    }

    job.options()->setString(EL::Job::optCondorConf, condor_config);
    eldriver->submitOnly(job, submitDir);
  }  else {
    Error("hsg5framework", "Unknown driver '%s'", driver.c_str());
    return 0;
  }
  return 0;
}
