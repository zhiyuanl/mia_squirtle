#include "PlottingTool/Config.h"
#include "PlottingTool/PlotMaker.h"

#include <iostream>


void makePlotsHadHad(std::string InputPath="./",std::string InputFile="inputsFile") {

  Config config;

  // should we overwrite existing plots or only do missing ones?
  config.setOverwrite(true);

  /////////
  /// Input 

  // path to the input file: can be local or on eos
  config.setInputPath(InputPath);

  // Since your input file name might not follow the conventions above,
  // it can be overwritten using the line below
  // Note: specify the name WITHOUT ".root" at the end
  config.setInputFile(InputFile);

  // If input histograms are stored in a subdirectory of the input file, specify below
  // config.setInputTDirectory("OneLepton");

  // read lumi from input file
  std::string lumi_str("13.2");
  TFile* inputfile=TFile::Open(config.getInputFileFullName("").c_str(),"READ");
  TH1* hlumi= (inputfile) ? (TH1*)inputfile->Get("lumi") : 0;  
  if(hlumi) {
    cout << "Reading luminosity from inputFile" << endl;
    float lumi_fb=hlumi->GetBinContent(1)/1000.0;
    lumi_str=Form("%.2f",lumi_fb);
    inputfile->Close();
  }
  cout << "Using luminosity " << lumi_str << endl;

  ////////////////
  /// General info
  config.setAnalysis( Config::HadHad,  // Type = ZeroLepton, OneLepton or TwoLepton: Appears on the plots
		      "Internal",         // Status: Appear on the plots next to ATLAS label
		      "Moriond2016",      // Name = "LHCP2013": only useful for the above naming convention
		      "13",                // Center of mass energy: Appears on the plots
		      lumi_str,           // Integrated luminosity: Appears on the plots
		      "125",              // Mass of the generated higgs boson: only useful for the above naming convention
		      "UTFSM",           // Name of your institute: only usefule for the above naming convention
		      "fram15");          // Input file version: only useful for the above naming convention


  //////////
  /// Output

  // path to output plots
  config.setOutputPath("submitDir_27Sep_step2/plots");

  // path to sys plots
  config.setSystematicDir("Systematics");
  config.readSetup();

  // BDT transformations:
  config.addBDTTransformation(""); // no transformation

  //config.addBDTTransformation("trafo6", // name to be reffered to later                         
  //      		      6,       // Algorithm: 6 is trafo D in Daniel's tool                
  //      		      6, 2);  // Transformation parameters                                 
  //config.addBDTTransformation("trafo12", // name to be reffered to later                        
  //      		      12,       // Algorithm: 12 is trafo F in Daniel's tool               
  //      		      4.5, 4.5);  // Transformation parameters                             
  //                                                                                               
  /* Choose the algorithm for transformation.                                                      
   * Available are:                                                                                
   * 1: bkgUnc < maxUnc                                                                            
   * 5: optimized for signal bins                                                                  
   * 6: "trafoD"                                                                                   
   * 11: "trafoE"                                                                                  
   * 12: "trafoF"                                                                                  
   * Default is 6                                                                                  
   */
  
  /// Samples
 
  // declare the samples
  //config.addDataSample("dummy",       // name: as it appears in the histogram names
  //      	       "Data",       // title: as it should appear in the legend 
  //      	       1);           // color: this is marker(line+fill) color for data(MC) 
  config.addDataSample("data",       // name: as it appears in the histogram names
  	       "Data",       // title: as it should appear in the legend 
  	       1);           // color: this is marker(line+fill) color for data(MC) 
  // SM Higgses

  //config.addSignalSample("RS_G_hh_bbtt_hh_c10_M260", "HH", kRed);
  config.addSignalSample("RS_G_hh_bbtt_hh_c10_M300", "HH", kRed);
  /*config.addSignalSample("RS_G_hh_bbtt_hh_c10_M400", "HH", kRed);
  config.addSignalSample("RS_G_hh_bbtt_hh_c10_M500", "HH", kRed);
  config.addSignalSample("RS_G_hh_bbtt_hh_c10_M600", "HH", kRed);
  config.addSignalSample("RS_G_hh_bbtt_hh_c10_M700", "HH", kRed);
  config.addSignalSample("RS_G_hh_bbtt_hh_c10_M800", "HH", kRed);*/

  /*config.addSignalSample("qqZllH125", "ZH 125", kRed);
  config.addSignalSample("ggZllH125", "ZH 125", kRed);
  config.addSignalSample("qqZvvH125", "ZH 125", kRed); 
  config.addSignalSample("ggZvvH125", "ZH 125", kRed); 
  config.addSignalSample("qqWlvH125", "WH 125", kRed + 1);*/
  ////////////////////////////////////////////////////////////////////////////
  //config.addBackgroundSample("qqZvvH125", "ZH 125", kRed); 
  //config.addBackgroundSample("ggZveveH125", "ZH 125", kRed); 
  //config.addBackgroundSample("ggZvmvmH125", "ZH 125", kRed); 
  //config.addBackgroundSample("ggZvtvtH125", "ZH 125", kRed); 
  //config.addBackgroundSample("qqWlvH125", "WH 125", kRed+1); 
  /////////////////////////////////////////////////////////////////////////////
  // A -> Zh SIGNALS
  //config.addSignalSample("HVTZHvvqq2000", "HVT 2000", kRed+1);
  //config.addSignalSample("HVTZHvvqq2000", "HVT 2000 (x0.1)", kRed+1);
  //config.addSignalSample("HVTZHvvqq1000", "HVT 1000", kMagenta+1);
  //config.addSignalSample("AZhvvbb1000", "A->ZH 1000", kRed+1);
  /////////////////////////////////////////////////////////////////////////////
  config.addBackgroundSample("ttbar", "ttbar", kOrange);
  config.addBackgroundSample("stops", "s+t chan", kOrange - 1); // Two samples sharing the same title appear only once in the legend
  config.addBackgroundSample("stopt", "s+t chan", kOrange - 1);
  config.addBackgroundSample("stopWt", "Wt", kYellow - 7);
  config.addBackgroundSample("Wv22l",  "W+l",  kGreen - 9);
  config.addBackgroundSample("Wv22cl", "W+cl", kGreen - 6);
  config.addBackgroundSample("Wv22cc", "W+cc", kGreen + 1);
  config.addBackgroundSample("Wv22bl", "W+bl", kGreen + 2);
  config.addBackgroundSample("Wv22bc", "W+bc", kGreen + 3);
  config.addBackgroundSample("Wv22bb", "W+bb", kGreen + 4);
  config.addBackgroundSample("Zv22l",  "Z+l",  kAzure - 9);
  config.addBackgroundSample("Zv22cl", "Z+cl", kAzure - 8);
  config.addBackgroundSample("Zv22cc", "Z+cc", kAzure - 4);
  config.addBackgroundSample("Zv22bl", "Z+bl", kAzure + 1);
  config.addBackgroundSample("Zv22bc", "Z+bc", kAzure + 2);
  config.addBackgroundSample("Zv22bb", "Z+bb", kAzure + 3);
  /*config.addBackgroundSample("Zl",  "Z+l",  kAzure - 9);
  config.addBackgroundSample("Zcl", "Z+cl", kAzure - 8);
  config.addBackgroundSample("Zcc", "Z+cc", kAzure - 4);
  config.addBackgroundSample("Zbl", "Z+bl", kAzure + 1);
  config.addBackgroundSample("Zbc", "Z+bc", kAzure + 2);
  config.addBackgroundSample("Zbb", "Z+bb", kAzure + 3);*/
  //config.addBackgroundSample("WW", "WW", kGray + 3);
  //config.addBackgroundSample("WZ", "WZ", kGray);
  //config.addBackgroundSample("ZZ", "ZZ", kGray + 1);
  config.addBackgroundSample("WW_improved", "WW", kGray + 3);
  config.addBackgroundSample("WZ_improved", "WZ", kGray);
  config.addBackgroundSample("ZZ_improved", "ZZ", kGray + 1);
  config.addBackgroundSample("fakes", "fakes", kPink+1);
  //  config.addBackgroundSample("multijetEl", "multijet", kPink + 1);
  //  config.addBackgroundSample("multijetMu", "multijet", kPink + 1);


  //  std::string scaleFactorsTag = "SF_1L_nJ_0107";
  // config.readScaleFactors("scaleFactors/"+scaleFactorsTag+".txt");
  // config.setOutputPath(config.getOutputPath()+"/"+scaleFactorsTag);

  // To optimize plot readability , samples that contribute less than a fraction X are dropped from the legend
  // config.setLegendThreshold(1.e-8);

  // To draw the histograms in no-stack mode
  config.setNoSignalStack(true);
  //  config.setNoStack();

  // set total background name
  // this is not mandatory but would save CPU and memory
  // Note: if any background scale factors are different than one, 
  //       this cannot be used and will be ignored
  // config.setTotalBackgroundName("bkg");

  ///////////////////////////////////
  // Variables: RESOLVED HH/BSM HH->bbtautau
  //config.addVariable(Config::BDTOutput, "BDT", "BDT discriminant", -1, 1, 5);
  //config.addVariable(Config::BDTOutput, "mBB", "m(bb) [GeV]", 0, 500, 1);
  // Dijet variables:
  config.addVariable(Config::BDTInput,"NSignalJets","N signal jets",0,10,1);
  config.addVariable(Config::BDTInput,"NBJets","N b-jets",0,5,1);
  config.addVariable(Config::BDTInput,"NJets","N jets",0,10,1);

  config.addVariable(Config::BDTInput,"FinalTauEta_3Prong","Tau #eta (3P)",-3,3,1);
  config.addVariable(Config::BDTInput,"FinalTau1Eta_1Prong","Leading Tau #eta (1P)",-3,3,1);

  config.addVariable(Config::BDTInput,"Tau0Eta_3Prong","Leading Tau #eta (3P)",-3,3,1);
  config.addVariable(Config::BDTInput,"Tau1Eta_3Prong","Sub-leading Tau #eta (1P)",-3,3,1);
  config.addVariable(Config::BDTInput,"TauEta_3Prong","Tau #eta (3P)",-3,3,1);


  config.addVariable(Config::BDTInput,"Jet0Eta","Leading Jet #eta ",-5,5,1);
  config.addVariable(Config::BDTInput,"Jet1Eta","Sub-leading Jet #eta",-5,5,1);
  config.addVariable(Config::BDTInput,"JetEta","Jet #eta",-5,5,1);

  config.addVariable(Config::BDTInput,"SignalJet0Eta","Leading signal jets #eta",-5,5,1);
  config.addVariable(Config::BDTInput,"SignalJet1Eta","Sub-leading signal jets #eta",-5,5,1);
  config.addVariable(Config::BDTInput,"SignalJetEta","Signal Jet #eta",-5,5,1);

  config.addVariable(Config::BDTInput,"JetPt","Jet p_{T} (GeV)",0,500,1);
  config.addVariable(Config::BDTInput,"Jet0Pt","Leading jet p_{T} (GeV)",0,500,1);
  config.addVariable(Config::BDTInput,"Jet1Pt","Sub-leading jet p_{T} (GeV)",0,500,1);

  config.addVariable(Config::BDTInput,"SignalJetPt","Signal jet p_{T} (GeV)",0,500,1);

  config.addVariable(Config::BDTInput,"TauPt_3Prong","sub-lead #tau (1P) Jet ID BDTScore",0,500,1);
  config.addVariable(Config::BDTInput,"Tau1Pt_1Prong","sub-lead #tau (1P) p_{T} GeV",0,500,1);

  config.addVariable(Config::BDTInput,"FinalTau1Pt_1Prong","sub-lead #tau (1P) p_{T} GeV",0,500,1);
  config.addVariable(Config::BDTInput,"FinalTau1Pt_3Prong","sub-lead #tau (3P) p_{T} GeV",0,500,1);

  config.addVariable(Config::BDTInput,"FinalTauPt_1Prong","sub-lead #tau (1P) p_{T} GeV",0,500,1);
  config.addVariable(Config::BDTInput,"FinalTauPt_3Prong","sub-lead #tau (3P) p_{T} GeV",0,500,1);

  config.addVariable(Config::BDTInput,"Tau0BDTScore_3Prong","leading #tau (3P) Jet ID BDTScore",0,1,1);
  config.addVariable(Config::BDTInput,"Tau1BDTScore_3Prong","sub-lead #tau (3P) Jet ID BDTScore",0,1,1);
  config.addVariable(Config::BDTInput,"TauBDTScore_3Prong","#tau (3P) Jet ID BDTScore",0,1,1);
  config.addVariable(Config::BDTInput,"TauBDTScore_1Prong","#tau (1P) Jet ID BDTScore",0,1,1);

  config.addVariable(Config::BDTInput,"JetPhi","Jet #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"Jet0Phi","leading jet #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"Jet1Phi","sub-lead jet #Phi",-5,5,1);

  config.addVariable(Config::BDTInput,"SignalJet0Phi","leading jet #Phi",-5,5,1);

  config.addVariable(Config::BDTInput,"Tau0Phi_1Prong","leading #tau (1P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"Tau0Phi_3Prong","leading #tau (3P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"FinalTau1Phi_3Prong","sub-lead #tau (3P) #Phi",-5,5,1);

  config.addVariable(Config::BDTInput,"Jet0tagWeight","leading jet tag weight",-10,10,1);
  config.addVariable(Config::BDTInput,"Jet1tagWeight","sub-lead jet tag weight",-10,10,1);
  config.addVariable(Config::BDTInput,"JettagWeight","jet tag weight",-0.5,4.5,1);

  config.addVariable(Config::BDTInput,"SignalJet0tagWeight","leading jet tag weight",-10,10,1);
  config.addVariable(Config::BDTInput,"SignalJet1tagWeight","sub-leading jet tag weight",-10,10,1);
  config.addVariable(Config::BDTInput,"SignalJettagWeight","jet tag weight",-10,10,1);

  config.addVariable(Config::BDTInput,"Jet0isTagged","leading jet isTagged",-0.5,1.5,1);
  config.addVariable(Config::BDTInput,"Jet1isTagged","sub-lead jet isTagged",-0.5,1.5,1);
  config.addVariable(Config::BDTInput,"JetisTagged","Jet isTagged",-0.5,1.5,1);

  config.addVariable(Config::BDTInput,"SignalJet1isTagged","sub-lead signal jet (1P) isTagged",-0.5,1.5,1);

  config.addVariable(Config::BDTInput,"Tau0nTracks","leading #tau nTracks",-0.5,4.5,1);
  config.addVariable(Config::BDTInput,"TaunTracks","#tau nTracks",-0.5,4.5,1);

  config.addVariable(Config::BDTInput,"FinalTau1nTracks","sub-lead #tau nTracks",-0.5,4.5,1);
  config.addVariable(Config::BDTInput,"FinalTaunTracks","#tau nTracks",-0.5,4.5,1);

  config.addVariable(Config::BDTInput,"FinalTau1BDTScore_1Prong","sub-lead #tau (1P) Jet ID BDTScore",0,1,1);

  config.addVariable(Config::BDTInput,"NSignalTaus","N signal-taus",0,5,1);
  config.addVariable(Config::BDTInput,"NAntiTaus","N anti-taus",0,5,1);

  config.addVariable(Config::BDTInput,"SignalJet0Pt","Leading signal jet p_{T} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"SignalJet1Phi","Sub-leading signal jet #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"SignalJetPhi","Signal jet #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"SignalJetisTagged","Signal jet isTagged",-0.5,1.5,1);

  config.addVariable(Config::BDTInput,"TauPhi_3Prong","#tau (3P) #Phi",-5,5,1);

  config.addVariable(Config::BDTInput,"FinalTau0nTracks","leading #tau nTracks",-0.5,4.5,1);
  config.addVariable(Config::BDTInput,"FinalTauEta_1Prong","#tau (1P) #eta",-3,3,1);
  config.addVariable(Config::BDTInput,"FinalTau0Eta_3Prong","leading #tau (3P) #eta",-3,3,1);
  config.addVariable(Config::BDTInput,"FinalTau0Pt_3Prong","leading #tau (3P) p_{T} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"FinalTau0Phi_3Prong","leading #tau (3P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"FinalTau0BDTScore_3Prong","leading #tau (3P) Jet ID BDTScore",0,1,1);

  config.addVariable(Config::BDTInput,"FinalTau1Eta_3Prong","sub-lead #tau (3P) #eta",-3,3,1);
  config.addVariable(Config::BDTInput,"FinalTau1BDTScore_3Prong","sub-lead #tau (3P) Jet ID BDTScore",0,1,1);
  config.addVariable(Config::BDTInput,"FinalTauPhi_3Prong","#tau (3P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"FinalTauBDTScore_3Prong","#tau (3P) Jet ID BDTScore",0,1,1);

  config.addVariable(Config::BDTInput,"Tau0Pt_1Prong","leading #tau (1P) p_{T} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"Tau0BDTScore_1Prong","leading #tau (1P) Jet BDTScore ",0,1,1);
  config.addVariable(Config::BDTInput,"TauEta_1Prong","#tau (1P) #eta",-3,3,1);
  config.addVariable(Config::BDTInput,"TauPhi_1Prong","#tau (1P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"TauPt_1Prong","#tau (1P) p_{T} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"FinalTau0Pt_1Prong","leading #tau (1P) p_{T} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"FinalTau0BDTScore_1Prong","leading #tau (1P) Jet ID BDTScore",0,1,1);
  config.addVariable(Config::BDTInput,"FinalTauPhi_1Prong","#tau (1P) #Phi",-5,5,1);
  config.addVariable(Config::BDTInput,"FinalTauBDTScore_1Prong","#tau (1P) Jet ID BDTScore",0,1,1);

  config.addVariable(Config::BDTInput,"mTauTau","m_{#tau#tau}^{vis} [GeV]",20,250,1);
  config.addVariable(Config::BDTInput,"pTTT","p_{#tau#tau, T}^{vis} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"mmc","m_{#tau#tau}^{mmc} [GeV]",20,250,1);
  config.addVariable(Config::BDTInput,"mmcpT","p_{#tau#tau, T}^{mmc} [GeV]",0,500,1);
  config.addVariable(Config::BDTInput,"mBB","m_{jj} [GeV]",20,250,1);
  config.addVariable(Config::BDTInput,"pTBB","p_{T}^{jj}",0,500,1);
  config.addVariable(Config::BDTInput,"mX","m_{HH} [GeV]",200,1200,1);
  config.addVariable(Config::BDTInput,"mXmmc","m_{HH,mmc} [GeV]",200,1200,1);
  config.addVariable(Config::BDTInput,"MET","MET [GeV]",0,400,1);
  config.addVariable(Config::BDTInput,"MTW_Max","M_{T}^{W, max}",0,600,1);
  config.addVariable(Config::BDTInput,"MTW_Clos","m_{T}^{W, clos}",0,300,1);
  config.addVariable(Config::BDTInput,"m_mTtot","M_{T}^{tot}",0,500,1);
  config.addVariable(Config::BDTInput,"m_mt2","M_{T2}",0,500,1);
  config.addVariable(Config::BDTInput,"METPhiCentrality","MEtPhiCentrality",-1.5,1.5,1);
  config.addVariable(Config::BDTInput,"DeltaRJJ","#Delta R_{jj}",0,5,1);
  config.addVariable(Config::BDTInput,"DeltaRTauTau","#Delta R_{#tau#tau}",0,5,1);
  config.addVariable(Config::BDTInput,"DeltaPhiJJ","#Delta #phi_{jj}",-5,5,1);
  config.addVariable(Config::BDTInput,"DeltaPhiTauTau","#Delta#phi_{#tau#tau}",-5,5,1);

  /*Add more variables here
  config.addVariable(Config::BDTInput,"sub-lead #tau (1P) Jet ID BDTScore",,,);
  config.addVariable(Config::BDTInput,"sub-lead #tau (1P) Jet ID BDTScore",,,);
  config.addVariable(Config::BDTInput,"",,,);
  config.addVariable(Config::BDTInput,"",,,);
  config.addVariable(Config::BDTInput,"",,,);
  config.addVariable(Config::BDTInput,"",,,);*/



  /*config.addVariable(Config::BDTInput, "MET_Track", "MET_Track [GeV]", 0, 450, 3);
  config.addVariable(Config::BDTInput, "nJ", "nJ", -0.5, 10.5, 1);
  config.addVariable(Config::BDTInput, "Njets", "Njets", 0, 25, 1);
  config.addVariable(Config::BDTInput, "pTB1", "p_{T}(b_{1}) [GeV]", 0, 500, 5);
  config.addVariable(Config::BDTInput, "pTB2", "p_{T}(b_{2}) [GeV]", 0, 500, 5);
  config.addVariable(Config::BDTInput, "EtaB1", "#{eta}(b_{1})", -5, 5, 5);
  config.addVariable(Config::BDTInput, "EtaB2", "#{eta}(b_{2})", -5, 5, 5);
  config.addVariable(Config::BDTInput, "dEtaBB", "|#Delta#eta(b,b)|", 0, 5, 5);
  config.addVariable(Config::BDTInput, "dRBB", "#DeltaR(b,b)", 0, 3, 5);
  config.addVariable(Config::BDTInput, "MV1cB1", "MV1c(b_{1})", 0, 1, 5);
  config.addVariable(Config::BDTInput, "MV1cB2", "MV1c(b_{2})", 0, 1, 5);
  config.addVariable(Config::BDTInput, "MV2cB1", "MV2cB1", -1, 1, 5);
  config.addVariable(Config::BDTInput, "MV2cB2", "MV2cB2", -1, 1, 5);
  config.addVariable(Config::BDTInput, "dPhiVBB", "#Delta#phi(V,bb)", 0, 3.15, 5);
  config.addVariable(Config::BDTInput, "dPhiMETMPT", "#Delta#phi(MET,MPT)", 0, 3.15, 5);
  config.addVariable(Config::BDTInput, "dPhiMETdijet", "#Delta#phi(MET,dijet)", 0, 3.15, 5);
  config.addVariable(Config::BDTInput, "MindPhiMETJet", "min(#Delta#phi(MET,jet))", 0, 3.15, 5);
  config.addVariable(Config::BDTInput, "MEff", "MEff = p_{T}(b_{1}) + p_{T}(b_{2}) + MET [GeV]", 0, 1000, 5);
  config.addVariable(Config::BDTInput, "MEff_Track", "MEff = p_{T}(b_{1}) + p_{T}(b_{2}) + MPT [GeV]", 0, 1000, 5);
  config.addVariable(Config::BDTInput, "mBBJ", "m(bbj) [GeV]", 0, 500, 10);
  config.addVariable(Config::BDTInput, "MEff3", "MEff3 = p_{T}(b_{1}) + p_{T}(b_{2}) + p_{T}(b_{3}) + MET [GeV]", 0, 1000, 10);
  config.addVariable(Config::BDTInput, "pTJ3", "p_{T}(j_{3}) [GeV]", 0, 500, 10);
  config.addVariable(Config::BDTInput, "HT", "HT [GeV]", 0, 1500, 4);
  config.addVariable(Config::BDTInput, "pTBB", "p_{T}(bb) [GeV]", 0, 2000, 5);
  config.addVariable(Config::BDTInput, "pTBBoverMET", "p_{T}(bb)/MET", 0, 500, 5);*/

  //////////////////////////
  // Variables: BOOSTED AZh
  // Njets
// //config.addVariable(Config::BDTInput, "NSignalJets", "N signal jets", 0, 10, 1);
// config.addVariable(Config::BDTInput, "NFatJets", "N fat jets", 0, 10, 1);
// config.addVariable(Config::BDTInput, "NMatchedTrackJetLeadFatJet", "N track jets matched to leading fat jet", 0, 10, 1);
// config.addVariable(Config::BDTInput, "NBTagMatchedTrackJetLeadFatJet", "N b-tagged track jets matched to leading fat jet", 0, 10, 1);
// config.addVariable(Config::BDTInput, "NBTagUnmatchedTrackJetLeadFatJet", "N b-tagged track jets not matched to leading fat jet", 0, 10, 1);
  // // analysis var
  //config.addVariable(Config::BDTInput, "mVH", "m_{VH} [GeV]", 0, 3000, 10); // 100 GeV/bin
  //config.addVariable(Config::BDTInput, "mVH_uncorr", "m_{VH} unrescaled [GeV]", 0, 3000, 2);
  //config.addVariable(Config::BDTInput, "pTVHOverMEff", "p_{T}(VH)/(p_{T}(V)+p_{T}(H))", 0, 1, 4);
  //config.addVariable(Config::BDTInput, "METOverMEff", "p_{T}(V)/(p_{T}(V)+p_{T}(H))", 0, 1, 4);
  //config.addVariable(Config::BDTInput, "pTLeadFatJetOverMEff", "p_{T}(H)/(p_{T}(V)+p_{T}(H))", 0, 1, 4);
  //config.addVariable(Config::BDTInput, "MEff", "MEff = p_{T}(fat jet) + MET [GeV]", 0, 2500, 2);
  // config.addVariable(Config::BDTInput, "MET", "MET [GeV]", 0, 1500, 4);  // 100 GeV/bin
// //config.addVariable(Config::BDTInput, "MET_Track", "MPT [GeV]", 0, 1000, 4);
// // // dR/dPhi
// config.addVariable(Config::BDTInput, "dPhiMETLeadFatJet", "#Delta#phi(MET, lead fat-jet)", 0, 180, 2);
// config.addVariable(Config::BDTInput, "dPhiMETMPT", "#Delta#phi(MET, MPT)", 0, 180, 2);
// config.addVariable(Config::BDTInput, "MindPhiMETJet", "min(#Delta#phi(MET, jet))", 0, 180, 2);
// // // lead vs sublead fatjet
// // //config.addVariable(Config::BDTInput, "MLeadFatJetSubLeadFatJet", "Mass of leading + subleading fat jet", 0, 3000, 5);
// // //config.addVariable(Config::BDTInput, "dRLFatSubLFat", "#Delta R leading vs sub-leading", 1, 5, 5);
// // //config.addVariable(Config::BDTInput, "dPhiLFatSubLFat", "#Delta #phi leading vs sub-leading", 0, 180, 5);
// // // lead fat jet
  //config.addVariable(Config::BDTInput, "MLeadFatJet", "Mass leading fat-jet [GeV]", 0, 500, 5); // 25 GeV/bin
  //config.addVariable(Config::BDTInput, "pTLeadFatJet", "p_{T} leading fat-jet [GeV]", 0, 2500, 10); // 250 GeV/bin
  //config.addVariable(Config::BDTInput, "etaLeadFatJet", "#eta leading fat-jet", -3, 3, 4);
  //config.addVariable(Config::BDTInput, "phiLeadFatJet", "#phi leading fat-jet", -180, 180, 2);
  // sub-lead fat jet
  //config.addVariable(Config::BDTInput, "MSubLeadFatJet", "Mass sub-leading fat-jet [GeV]", 0, 250, 5);
  //config.addVariable(Config::BDTInput, "pTSubLeadFatJet", "p_{T} sub-leading fat-jet [GeV]", 0, 2500, 5);
  //config.addVariable(Config::BDTInput, "etaSubLeadFatJet", "#eta sub-leading fat-jet", -3, 3, 5);
  //config.addVariable(Config::BDTInput, "phiSubLeadFatJet", "#phi sub-leading fat-jet", -180, 180, 5);
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  config.clearRegions();

  // REGIONS SM VHbb //////////////////////////////////////////////////////////////
  std::string ntags[5] = {"0ptag", "0tag", "1tag", "2tag"};//, "3ptag"};
  std::string njets[5] = {"0pjet"};
  std::string ptvregions[5] = {"0"};
  std::string Regions[8] = {"AA_OS","AA_SS","AM_OS","AM_SS","MA_OS","MA_SS","MM_OS","MM_SS"};
  //std::string selections[5] = {"Pres", "Sel"};
  // LOW MET BINS //
  //for (int ireg = 0; ireg < 2; ireg++ ){
     for ( int imBB = 0; imBB < 8; imBB++ ) {
        for ( int iptv = 0; iptv < 1; iptv++ ) {
           for ( int ijets = 0; ijets < 1; ijets++ ) {
              for ( int itags = 1; itags < 4; itags++) {
              //if ( njets[ijets] != "2jet" ) continue;
              //if ( ntags[itags] != "2tag" ) continue;
              //if ( ptvregions[iptv] != "150" ) continue;
                 config.addRegion((std::string)TString::Format("%s%s_%sptv_%s", ntags[itags].c_str(), njets[ijets].c_str(), ptvregions[iptv].c_str(), Regions[imBB].c_str()),Regions[imBB],ntags[itags]);
              }
           }
        }
     }
  //}
  
  // ADD SYSTEMATICS
  config.clearSystematics();
  //config.addSystematic("SysMET_JetTrk_ScaleUp", true);
  //config.addSystematic("SysMET_JetTrk_ScaleDown", true);
  //config.addSystematic("SysMUON_ISO_SYS", false);
  //config.addSystematic("SysMUON_ISO_STAT", false);
  //config.addSystematic("SysEG_SCALE_ALL", false);
  //config.addSystematic("SysEG_RESOLUTION_ALL", false);
  //config.addSystematic("SysMUONS_MS", false);
  //config.addSystematic("SysMUONS_ID", false);
  //config.addSystematic("SysMUONS_SCALE", false);
  //config.addSystematic("SysJET_JER_SINGLE_NP", true);
  //config.addSystematic("SysJET_GroupedNP_3", false);
  //config.addSystematic("SysMUON_EFF_SYS", false);
  //config.addSystematic("SysMUON_EFF_STAT", false);
  //config.addSystematic("SysJET_GroupedNP_1", false);
  //config.addSystematic("SysEL_EFF_ID_TotalCorrUncertainty", false);
  //config.addSystematic("SysMET_SoftTrk_ResoPerp", true);
  //config.addSystematic("SysJET_GroupedNP_2", false);
  //config.addSystematic("SysMET_SoftTrk_Scale", false);
  //config.addSystematic("SysMET_SoftTrk_ResoPara", true);
  //config.addSystematic("SysFT_EFF_extrapolation_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_extrapolation_from_charm_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_Light_0_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_Light_1_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_Light_2_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_Light_3_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_Light_4_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_C_3_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_C_2_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_C_1_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_C_0_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_B_2_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_B_1_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysFT_EFF_Eigen_B_0_AntiKt4EMTopoJets", false);
  //config.addSystematic("SysJET_Hbb_Run1_pT", false);
  //config.addSystematic("SysMETTrigStat", false);
  
  ////////////////////////
  /// Detailed systematics
  // One can ask for detailed systematic plots for specific variables in specific region
  // This will make one plot per nuisance parameter
  // config.addDetailedSystematicsPlots("mva",               // name of the variable: as declared above
  // 				     "trafo6",            // name of the transformation: relevant for BDT outputs
  // 				     "2tag2jet_vpt0_120", // name of the region: as declared above
  // 				     "Systematics: ");    // slide title: used to decide which plots to appear on the same slide
  // config.addDetailedSystematicsPlots("mva", "trafo6", "2tag2jet_vpt120", "Systematics: ");
  // config.addDetailedSystematicsPlots("mBB", "", "2tag2jet_vpt0_120", "Systematics: ");
  // config.addDetailedSystematicsPlots("mBB", "", "2tag2jet_vpt120", "Systematics: ");
  // config.addDetailedSystematicsPlots("pTB1", "", "2tag2jet_vpt0_120", "Systematics: ");
  // config.addDetailedSystematicsPlots("pTB1", "", "2tag2jet_vpt120", "Systematics: ");
  // config.addDetailedSystematicsPlots("pTB2", "", "2tag2jet_vpt0_120", "Systematics: ");
  // config.addDetailedSystematicsPlots("pTB2", "", "2tag2jet_vpt120", "Systematics: ");
  // config.addDetailedSystematicsPlots("MET", "", "2tag2jet_vpt0_120", "Systematics: ");
  // config.addDetailedSystematicsPlots("MET", "", "2tag2jet_vpt120", "Systematics: ");
  
  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2lltag2jet_vpt120", "Systematics (high pTV): ");
  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2mmtag2jet_vpt120", "Systematics (high pTV): ");
  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2tttag2jet_vpt120", "Systematics (high pTV): ");

  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2lltag2jet_vpt0_120", "Systematics (low pTV): ");
  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2mmtag2jet_vpt0_120", "Systematics (low pTV): ");
  // config.addDetailedSystematicsPlots("mvaWlvH125", "trafo6", "2tttag2jet_vpt0_120", "Systematics (low pTV): ");
  
  // config.addDetailedSystematicsPlots("mBB", "", "2lltag2jet_vpt120", "Systematics (high pTV): ");
  // config.addDetailedSystematicsPlots("mBB", "", "2mmtag2jet_vpt120", "Systematics (high pTV): ");
  // config.addDetailedSystematicsPlots("mBB", "", "2tttag2jet_vpt120", "Systematics (high pTV): ");

  // config.addDetailedSystematicsPlots("mBB", "", "2lltag2jet_vpt0_120", "Systematics (low pTV): ");
  // config.addDetailedSystematicsPlots("mBB", "", "2mmtag2jet_vpt0_120", "Systematics (low pTV): ");
  // config.addDetailedSystematicsPlots("mBB", "", "2tttag2jet_vpt0_120", "Systematics (low pTV): ");

  // config.addDetailedSystematicsPlots("mva", "trafoF", "1tag2jet_vpt0_120",   "Systematics 2J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2lltag2jet_vpt0_120", "Systematics 2J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2mmtag2jet_vpt0_120", "Systematics 2J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2tttag2jet_vpt0_120", "Systematics 2J (low pTV): ");

  // config.addDetailedSystematicsPlots("mva", "trafoF", "1tag2jet_vpt120",   "Systematics 2J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2lltag2jet_vpt120", "Systematics 2J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2mmtag2jet_vpt120", "Systematics 2J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2tttag2jet_vpt120", "Systematics 2J (high pTV): ");

  // config.addDetailedSystematicsPlots("mva", "trafoF", "1tag3jet_vpt0_120",   "Systematics 3J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2lltag3jet_vpt0_120", "Systematics 3J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2mmtag3jet_vpt0_120", "Systematics 3J (low pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2tttag3jet_vpt0_120", "Systematics 3J (low pTV): ");

  // config.addDetailedSystematicsPlots("mva", "trafoF", "1tag3jet_vpt120",   "Systematics 3J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2lltag3jet_vpt120", "Systematics 3J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2mmtag3jet_vpt120", "Systematics 3J (high pTV): ");
  // config.addDetailedSystematicsPlots("mva", "trafoF", "2tttag3jet_vpt120", "Systematics 3J (high pTV): ");
  
  // for(int iCR=0; iCR<config.getNRegions(); iCR++) {
  //   Config::Region region = config.getRegion(iCR);
  //   for(int iVar=0; iVar<config.getNVariables(); iVar++) {
  //     Config::Variable var = config.getVariable(iVar);
  //     std::string title = "Systematics: "+var.name+" ";
  //     if(region.name.find("0tag") != std::string::npos ) {
  // 	title += "0 tag";
  //     }else if(region.name.find("1tag") != std::string::npos) {
  // 	title += "1 tag";
  //     }else{
  // 	title += "2 tag";
  // 	if(region.name.find("2jet") != std::string::npos) {
  // 	  title += " , 2 jets";
  // 	}else{
  // 	  title += " , 3 jets";
  // 	}
  // 	if(region.name.find("vpt120") != std::string::npos) {
  // 	  title += " (high pTV)";
  // 	}else {
  // 	  title += " (low pTV)";
  // 	}
  //     }
  //     config.addDetailedSystematicsPlots(var.name, 
  // 					 "", //var.type == Config::BDTOutput ? "trafo6" : "", 
  // 					 region.name,
  // 					 title);
  //   }
  // }
					 


   /////////////////////////
   /// Computing sensitivity
   
   // One can output expected sensitivity numbers based on some specific plots and regions
   // The sensitivity metric used is one of two:
   //  - Normal approximation:
   //    S/sqrt(B + dB*dB)
   //  - Log likelihood ratio:
   //    sqrt(2*((S+B)*lob(1+S/B)-S))
   // It is computed for each bin and then added in quadrature
   // Several histograms from different regions can be combined (one variable per region)
   // NOTE: the concerned variables and regions and transformations 
   //       need to be added for ploting above.
   
   // define the regions to combine
   //   config.addSensitivityPlot( "mva",             // name to uniquely identify a sensitivity calculation
   // 			     "mva",             // variable name used for this region
   // 			     "2tag2jet_vpt120", // name of the region
   // 			     "trafo6");         // name of the transformation in case of BDT
   //   config.addSensitivityPlot( "mva", "mva", "2tag3jet_vpt120", "trafo6");
   //   config.addSensitivityPlot( "mva", "mva", "2tag2jet_vpt0_120", "trafo6");
   //   config.addSensitivityPlot( "mva", "mva", "2tag3jet_vpt0_120", "trafo6");
   
   //   // you can combine the same plots in different ways
   //   config.addSensitivityPlot( "mva 2t2j", "mva", "2tag2jet_vpt120", "trafo6");
   //   config.addSensitivityPlot( "mva 2t2j", "mva", "2tag2jet_vpt0_120", "trafo6");
   //   config.addSensitivityPlot( "mva 2t3j", "mva", "2tag3jet_vpt120", "trafo6");
   //   config.addSensitivityPlot( "mva 2t3j", "mva", "2tag3jet_vpt0_120", "trafo6");
   
   //   config.addSensitivityPlot( "mva high ptv", "mva", "2tag2jet_vpt120", "trafo6");
   //   config.addSensitivityPlot( "mva high ptv", "mva", "2tag3jet_vpt120", "trafo6");
   //   config.addSensitivityPlot( "mva low ptv", "mva", "2tag2jet_vpt0_120", "trafo6");
   //   config.addSensitivityPlot( "mva low ptv", "mva", "2tag3jet_vpt0_120", "trafo6");
   
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag3jet_vpt200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag2jet_vpt200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag3jet_vpt200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag2jet_vpt200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag3jet_vpt200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag2jet_vpt200", "");
   
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag3jet_vpt160_200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag2jet_vpt160_200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag3jet_vpt160_200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag2jet_vpt160_200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag3jet_vpt160_200", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag2jet_vpt160_200", "");
   
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag3jet_vpt120_160", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag2jet_vpt120_160", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag3jet_vpt120_160", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag2jet_vpt120_160", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag3jet_vpt120_160", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag2jet_vpt120_160", "");
   
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag3jet_vpt90_120", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag2jet_vpt90_120", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag3jet_vpt90_120", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag2jet_vpt90_120", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag3jet_vpt90_120", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag2jet_vpt90_120", "");
   
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag3jet_vpt0_90", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2lltag2jet_vpt0_90", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag3jet_vpt0_90", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2mmtag2jet_vpt0_90", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag3jet_vpt0_90", "");
   //   config.addSensitivityPlot( "cuts", "mjj", "2tttag2jet_vpt0_90", "");

/// config.addSensitivityPlot( "mBBlow", "mBB",  "2tag2jet_150_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBlow", "mBB",  "2tag3jet_150_200ptv_mBBincl", "");
////
/// config.addSensitivityPlot( "mBBlow4p", "mBB", "2tag2jet_150_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBlow4p", "mBB", "2tag3jet_150_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBlow4p", "mBB", "2tag4pjet_150_200ptv_mBBincl", "");
////
/// config.addSensitivityPlot( "mBBhigh", "mBB",  "2tag2jet_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBhigh", "mBB",  "2tag3jet_200ptv_mBBincl", "");
////
/// config.addSensitivityPlot( "mBBhigh4p", "mBB", "2tag2jet_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBhigh4p", "mBB", "2tag3jet_200ptv_mBBincl", "");
/// config.addSensitivityPlot( "mBBhigh4p", "mBB", "2tag4pjet_200ptv_mBBincl", "");
////
// config.addSensitivityPlot( "mBB", "mBB", "2tag2jet_150_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB", "mBB", "2tag3jet_150_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB", "mBB", "2tag2jet_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB", "mBB", "2tag3jet_200ptv_mBBincl", "");
////
 //config.addSensitivityPlot( "BDT", "BDT", "2tag2jet_150ptv_mBBincl", "");
 //config.addSensitivityPlot( "BDT", "BDT", "2tag3jet_150ptv_mBBincl", "");
////
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag2jet_150_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag3jet_150_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag4pjet_150_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag2jet_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag3jet_200ptv_mBBincl", "");
// config.addSensitivityPlot( "mBB4p", "mBB", "2tag4pjet_200ptv_mBBincl", "");

  //config.addSensitivityPlot( "mBBtrafo6", "mBB", "2tag2jet_150ptv_SR", "trafo6");
  //config.addSensitivityPlot( "mBBtrafo6", "mBB", "2tag3jet_150ptv_SR", "trafo6");

  //config.addSensitivityPlot( "mBB4ptrafo6", "mBB", "2tag2jet_150ptv_SR",  "trafo6");
  //config.addSensitivityPlot( "mBB4ptrafo6", "mBB", "2tag3jet_150ptv_SR",  "trafo6");
  //config.addSensitivityPlot( "mBB4ptrafo6", "mBB", "2tag4pjet_150ptv_SR", "trafo6");

  //config.addSensitivityPlot( "BDT", "BDT", "2tag2jet_150ptv_SR", "");
  //config.addSensitivityPlot( "BDT", "BDT", "2tag3jet_150ptv_SR", "");

  //config.addSensitivityPlot( "BDT4p", "BDT", "2tag2jet_150ptv_SR", "");
  //config.addSensitivityPlot( "BDT4p", "BDT", "2tag3jet_150ptv_SR", "");
  //config.addSensitivityPlot( "BDT4p", "BDT", "2tag4pjet_150ptv_SR", "");

  //config.addSensitivityPlot( "BDTtrafo12", "BDT", "2tag2jet_150ptv_SR", "trafo12");
  //config.addSensitivityPlot( "BDTtrafo12", "BDT", "2tag3jet_150ptv_SR", "trafo12");

  //config.addSensitivityPlot( "BDT4ptrafo12", "BDT", "2tag2jet_150ptv_SR",  "trafo12");
  //config.addSensitivityPlot( "BDT4ptrafo12", "BDT", "2tag3jet_150ptv_SR",  "trafo12");
  //config.addSensitivityPlot( "BDT4ptrafo12", "BDT", "2tag4pjet_150ptv_SR", "trafo12");

  ///////////////////////////
  /// Now ready to make plots

  // check for errors in configuration
  if(!config.isValid()) {
    std::cout << "Error in configuration: ===> Aborting..." << std::endl;
    return;
  }

  // we have all what we need to know
  // ==> now make the plots
  PlotMaker plotter;
  // BLINDING FOR mBB
  /*for ( int iptv = 0; iptv < 3; iptv++ ) {
     for ( int ijets = 0; ijets < 3; ijets++ ) {
         std::string region = (std::string)TString::Format("2tag%s_%sptv_mBBincl", njets[ijets].c_str(), ptvregions[iptv].c_str());
         config.addWindowBlinding("mBB", region, "", 100, 149.99); 
     }
  }*/
  // BLINDING FOR ALL OTHER DISTRIBUTIONS
  //config.setThresholdBlinding(0.05);
  /////////////////////////////////////////
  plotter.makePlots(config);
}
