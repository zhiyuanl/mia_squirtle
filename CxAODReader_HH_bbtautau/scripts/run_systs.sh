while read s
do
   # Run 1 systematic for each sample at the time, 1 job for 1 sample and 1 systematics
   echo "Now runninng on systematic with name: " ${s}
   HHframeworkReadCxAOD submitDir_${s}/ framework-read select ${s} LSF
done < syst_list.txt

