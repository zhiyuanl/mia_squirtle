#include <EventLoop/IWorker.h>

#include "TSystem.h"

#include <CxAODReader_HH_bbtautau/AnalysisReader_hhbbtt.h>

#include <CxAODTools_HH_bbtautau/HHbbtautauLepHadSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauHadHadSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauLepHadJetSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauHadHadJetSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauBoostedSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauBoostedJetSelection.h>

// #include "Mt2/mt2_bisect.h"

#define length(array) (sizeof(array)/sizeof(*(array)))

ClassImp(AnalysisReader_hhbbtt)

AnalysisReader_hhbbtt::AnalysisReader_hhbbtt() :
        AnalysisReader(),
  m_analysisType(""),
  m_doTruthTagging("false"),
  m_tree(nullptr),
  m_use2DbTagCut("false"),
  m_fillCR("false"),
  m_FF_File(nullptr),
  m_FF_File_boosted(nullptr), //boostedFF
  m_2DFF(nullptr),
  m_antiTau(true),
  m_corrsAndSysts(nullptr),
  m_setEventFlavour(false)
{
  m_trkBTagLimit = -0.3098; // added for Higgs Tagger

}

AnalysisReader_hhbbtt :: ~AnalysisReader_hhbbtt()
{
  delete m_FF_File;
  delete m_FF_File_boosted; //boostedFF
}

EL::StatusCode AnalysisReader_hhbbtt::initializeChannel(){
  EL_CHECK("AnalysisReader_hhbbtt::initializeChannel()", AnalysisReader::initializeChannel());

  m_config->getif<bool>("setEventFlavour", m_setEventFlavour);
  if (!m_setEventFlavour) {
    if(m_mcChannel>=364100 && m_mcChannel<=364113){
      m_truthHistNameSvc->set_sample("ZmumuSh221");
      m_histNameSvc->set_sample("ZmumuSh221");
    } else if(m_mcChannel>=364114 && m_mcChannel<=364127){
      m_truthHistNameSvc->set_sample("ZeeSh221");
      m_histNameSvc->set_sample("ZeeSh221");
    } else if(m_mcChannel>=364128 && m_mcChannel<=364141){
      m_truthHistNameSvc->set_sample("ZtautauSh221");
      m_histNameSvc->set_sample("ZtautauSh221");
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt :: initializeSelection()
{

  m_config->getif<bool>("AntiTau", m_antiTau); // keep this false for the cutflow: this would keep only SR events

  m_config->getif<string>("analysisType", m_analysisType);
  m_config->getif<string>("analysisStrategy", m_analysisStrategy);

  // For 2D cut only use merged
  m_config->getif<bool>("use2DbTagCut", m_use2DbTagCut);
  if (m_use2DbTagCut){
    if(m_analysisStrategy != "Merged"){
      Error("initializeSelection()","Running with 2DbTagCut. Only possible for merged analysis, but %s is chosen. Exiting!",m_analysisStrategy.c_str());
      return EL::StatusCode::FAILURE;
    }
    Warning("initializeSelection()","Running with 2DbTagCut. b-tagging for calo jets disabled!!");
  }

  Info("initializeSelection()", "Initialize analysis '%s'.", m_analysisType.c_str());

  if (m_analysisType== "hadhad") {

    m_eventSelection = new HHbbtautauHadHadSelection(m_config);
    m_eventPostSelection = new HHbbtautauHadHadJetSelection(m_config);

    m_fillFunction = std::bind(&AnalysisReader_hhbbtt::fill_hadhad, this);

  } else if (m_analysisType=="lephad") { 

    m_eventSelection = new HHbbtautauLepHadSelection(m_config);
    m_eventPostSelection = new HHbbtautauLepHadJetSelection(m_config);

    m_fillFunction = std::bind(&AnalysisReader_hhbbtt::fill_lephad, this);

  }else if (m_analysisType == "boosted") {

    m_eventSelection = new HHbbtautauBoostedSelection(m_config);
    m_eventPostSelection = new HHbbtautauBoostedJetSelection(m_config);

    m_fillFunction = std::bind(&AnalysisReader_hhbbtt::fill_boosted, this);

  } else {
    Error("initializeSelection()", "Invalid analysis type %s", m_analysisType.c_str());
    return EL::StatusCode::FAILURE;
  }

  // TODO move to base class?
  m_writeMVATree = false;
  m_readMVA = false;
  m_config->getif< bool >("writeMVATree", m_writeMVATree);
  m_config->getif< bool >("readMVA", m_readMVA);
  // initialize MVATree
  if (!m_tree)
    m_tree = new MVATree_hhbbtt(m_writeMVATree, m_readMVA, m_analysisType, wk(), m_variations, false);

  //do truth tagging?
  m_config->getif<bool>("doTruthTagging", m_doTruthTagging);

  return EL::StatusCode::SUCCESS;
}

//Added this initialization since it was currectly getting called in generic reader and running on unwanted samples

EL::StatusCode AnalysisReader_hhbbtt::initializeCorrsAndSysts ()
{

  m_doTruthTagging&=m_isMC;
  Info("initializeSelection()","truth tagging1? %i",m_doTruthTagging);
  if (!m_isMC) return EL::StatusCode::SUCCESS;

  std::string comEnergy    = m_config->get<std::string>("COMEnergy");

  if ((comEnergy != "8TeV") || (comEnergy != "7TeV")) comEnergy = "13TeV";
  TString csname;
  std::string debugname;

  //if (m_analysisType == "hadhad") { csname = comEnergy + "_HadHad"; debugname = comEnergy + "_HadHad"; }
  if (m_analysisType == "hadhad" || m_analysisType == "boosted") { csname = comEnergy + "_ZeroLepton"; debugname = comEnergy + "_ZeroLepton"; }
  if (m_analysisType == "lephad") { csname = comEnergy + "_OneLepton"; debugname = comEnergy + "_OneLepton"; }

  Info("initializeCorrsAndSysts()", "Initializing CorrsAndSysts for %s", debugname.c_str());

  m_corrsAndSysts = new CorrsAndSysts(csname);

  return EL::StatusCode::SUCCESS;
} // initializeCorrsAndSysts    


EL::StatusCode AnalysisReader_hhbbtt::initializeTools ()
{
  EL_CHECK("AnalysisReader_hhbbtt::initializeTools()", AnalysisReader::initializeTools());

  // moved here after removal in the base AnalysisReader
  EL_CHECK("initialize()",initializeCorrsAndSysts());

  m_getFFInputs=false;
  m_config->getif<bool>("fillFakeHists", m_getFFInputs);
  m_applyFF=false;
  m_config->getif<bool>("ApplyQCDFF", m_applyFF);
  bool m_FillSR=false;
  m_config->getif<bool>("FillSR", m_FillSR);
  bool m_FillSS=false;
  m_config->getif<bool>("FillSS", m_FillSS);

  std::string rootcore_path="";

  m_config->getif<bool>("applyFF", m_applyFF);

  if(m_applyFF) rootcore_path = gSystem->Getenv("WorkDir_DIR");
  std::cout << "rootcore_path is: " << rootcore_path << std::endl;
  if(m_analysisType == "hadhad" && m_applyFF) { // had-had
    m_FF_File = TFile::Open((rootcore_path+"/data/CxAODReader_HH_bbtautau/TauHHffPyFull.root").c_str(),"READ");
    if(m_FF_File==nullptr){
      std::cout << "File was not open" << std::endl;
      return EL::StatusCode::FAILURE;
    }

    // load fakes histograms
    for (std::string y : {"Nominal", "Subtraction_bkg__1up", "Subtraction_bkg__1down", "ANTITAU_BDT_CUT"}) {
      std::string FFDir("");
      std::string FFDirDTT("");
      std::string FFDirSTT("");
      FFDir = "Nom";
      FFDirDTT = "NomDTT";
      FFDirSTT = "NomSTT";
      if (y == "Subtraction_bkg__1up") {
        FFDir = "SubUp";
        FFDirDTT = "SubUpDTT";
        FFDirSTT = "SubUpSTT";
      } else if (y == "Subtraction_bkg__1down") {
        FFDir = "SubDown";
        FFDirDTT = "SubDownDTT";
        FFDirSTT = "SubDownSTT";
      } else if (y == "ANTITAU_BDT_CUT") {
        FFDir = "Nom";
        FFDirDTT = "NomDTT";
        FFDirSTT = "NomSTT";
      }

      std::map<std::string, std::string> systNameToFile_map = {
        { "Both", FFDir },
        { "DTT", FFDirDTT },
        { "STT", FFDirSTT }
      };

      for (int i : {0,1,2}) {
        std::string unbinStr = "Unbin"+std::to_string(i);
        auto stringTag = std::to_string(i);
        for ( std::string trig : {"Both", "DTT", "STT"} ){
          m_tauFFHadHadUnbin[y][i][trig] = (TH1F*) m_FF_File->Get((systNameToFile_map[trig]+"/"+unbinStr).c_str());
          m_tauFFHadHadUnbin[y][i][trig]->SetDirectory(0);
        }
        if(i>1) continue;
        m_tauFFHadHad[y][i][211] = (TH2F*) m_FF_File->Get((FFDirDTT+"/hFF"+stringTag+"TagTau0PtVsTau1Pt1P1P").c_str());
        m_tauFFHadHad[y][i][211]->SetDirectory(0);
        m_tauFFHadHad[y][i][213] = (TH2F*) m_FF_File->Get((FFDirDTT+"/hFF"+stringTag+"TagTau0PtVsTau1Pt1P3P").c_str());
        m_tauFFHadHad[y][i][213]->SetDirectory(0);
        m_tauFFHadHad[y][i][231] = (TH2F*) m_FF_File->Get((FFDirDTT+"/hFF"+stringTag+"TagTau0PtVsTau1Pt3P1P").c_str());
        m_tauFFHadHad[y][i][231]->SetDirectory(0);
        m_tauFFHadHad[y][i][233] = (TH2F*) m_FF_File->Get((FFDirDTT+"/hFF"+stringTag+"TagTau0PtVsTau1Pt3P3P").c_str());
        m_tauFFHadHad[y][i][233]->SetDirectory(0);
      }

      m_HadHadTF[y] = (TH1F*) m_FF_File->Get((FFDir+"/TFHist").c_str());
      m_HadHadTF[y]->SetDirectory(0);
    }
  } else if(m_analysisType == "lephad" && m_applyFF) { // lep-had TODO
    m_FF_File = TFile::Open((rootcore_path+"/data/CxAODReader_HH_bbtautau/QCDFF.root").c_str(),"READ");
    if(m_FF_File==nullptr){
      std::cout << "File was not open" << std::endl;
      return EL::StatusCode::FAILURE;
    }
  } else if(m_analysisType == "boosted") { // boostedFF
    m_applyFF = false;
    m_calculateFF = false;
    m_config->getif<bool>("applyFF", m_applyFF);
    m_config->getif<bool>("calculateFF", m_calculateFF);
    m_config->getif<bool>("useSideBandQCDMethod", m_useSideBandQCDMethod);
    if(m_calculateFF) m_applyFF = false;
    if(m_applyFF){
      m_FF_File_boosted = TFile::Open((rootcore_path+"/data/CxAODReader_HH_bbtautau/FFqcd_boosted.root").c_str(),"READ");
      if(m_FF_File_boosted==nullptr){
        std::cout << "File was not open" << std::endl;
        return EL::StatusCode::FAILURE;
      }

      std::cout << "Getting boosted FF histograms" << std::endl;
      for (const auto k: * m_FF_File_boosted->GetListOfKeys()) 
      {
        if (std::string(k->GetName()).find("h_FF_Ptbinned_") == string::npos) continue;
        if (std::string(k->GetName()).find("_finebinned") != string::npos) continue;
        m_BoostedFFMap[k->GetName()] = (TH1*)m_FF_File_boosted->Get(k->GetName());
        if (!m_BoostedFFMap[k->GetName()]) 
        {
          Warning("initializeTools", "Can't find %s in the FFqcd_boosted file", k->GetName());
          return EL::StatusCode::FAILURE;
        }
      }
      
      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaCentralbinned_0tag from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_0tagEtaCentral_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaCentralbinned_0tag");
      if(!m_1DFF_0tagEtaCentral_boosted) return EL::StatusCode::FAILURE;
      
      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaForwardbinned_0tag from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_0tagEtaForward_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaForwardbinned_0tag");
      if(!m_1DFF_0tagEtaForward_boosted) return EL::StatusCode::FAILURE;
      
      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaCentralbinned_SS_2tracks from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_SS_2tracks_EtaCentral_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaCentralbinned_SS_2tracks");
      if(!m_1DFF_SS_2tracks_EtaCentral_boosted) return EL::StatusCode::FAILURE;

      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaForwardbinned_SS_2tracks from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_SS_2tracks_EtaForward_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaForwardbinned_SS_2tracks");
      if(!m_1DFF_SS_2tracks_EtaForward_boosted) return EL::StatusCode::FAILURE;

      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaCentralbinned_SS_4tracks from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_SS_4tracks_EtaCentral_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaCentralbinned_SS_4tracks");
      if(!m_1DFF_SS_4tracks_EtaCentral_boosted) return EL::StatusCode::FAILURE;

      std::cout<<"Getting FakeFactor hist named: h_FF_Pt_EtaForwardbinned_SS_4tracks from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_SS_4tracks_EtaForward_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Pt_EtaForwardbinned_SS_4tracks");
      if(!m_1DFF_SS_4tracks_EtaForward_boosted) return EL::StatusCode::FAILURE;

      std::cout<<"Getting FakeFactor hist named: h_FF_Ptbinned_SS_6tracks from: /data/CxAODReader_HH_bbtautau/FFqcd_boosted.root"<<std::endl;
      m_1DFF_SS_6tracks_boosted = (TH1D*)m_FF_File_boosted->Get("h_FF_Ptbinned_SS_6tracks");
      if(!m_1DFF_SS_6tracks_boosted) return EL::StatusCode::FAILURE;

    }else if(m_applyFF && m_useSideBandQCDMethod){
      m_FF_File = TFile::Open((rootcore_path+"/data/CxAODReader_HH_bbtautau/FFBoosted.root").c_str(),"READ");
      if(m_FF_File==nullptr){
        std::cout << "File was not open" << std::endl;
        return EL::StatusCode::FAILURE;
      }
      TH1F *htemp1 =(TH1F*)m_FF_File->Get("h_N01");
      TH1F *htemp2 =(TH1F*)m_FF_File->Get("h_N02");
      m_weightFactor1=htemp1->Integral();
      m_weightFactor2=htemp2->Integral();
    }
  }

  m_FillSRSel260_300 = false;
  m_config->getif< bool >("FillSRSel260_300", m_FillSRSel260_300);
  m_FillSRSel400 = false;
  m_config->getif< bool >("FillSRSel400", m_FillSRSel400);
  m_FillSRSel500_600_700 = false;
  m_config->getif< bool >("FillSRSel500_600_700", m_FillSRSel500_600_700);
  m_FillSRSel800_900_1000 = false;
  m_config->getif< bool >("FillSRSel800_900_1000", m_FillSRSel800_900_1000);

  return EL::StatusCode::SUCCESS;
} //initializeTools

EL::StatusCode AnalysisReader_hhbbtt::initializeIsMC ()
{
  Info("initializeIsMC()", "Initialize isMC.");

  // get nominal event info
  // -------------------------------------------------------------
  const xAOD::EventInfo *eventInfo = m_eventInfoReader->getObjects("Nominal");

  if (!eventInfo) return EL::StatusCode::FAILURE;

  // get MC flag - different info on data/MC files
  // -----------------------------------------------
  m_isMC = Props::isMC.get(eventInfo);
  Info("initializeIsMC()", "isMC = %i", m_isMC);

  return EL::StatusCode::SUCCESS;
} // initializeIsMC



/*EL::StatusCode AnalysisReader_hhbbtt :: initializeOR2()
{
  m_overlapRemoval2 = new OverlapRemoval_HH_bbtautau( *m_config );
  EL_CHECK("AnalysisReader::initializeOR2()",m_overlapRemoval2->initialize());

  return EL::StatusCode::SUCCESS;
}*/

EL::StatusCode AnalysisReader_hhbbtt::initializeVariations() {
  EL_CHECK("AnalysisReader_hhbbtt::initializeVariations()",
	   AnalysisReader::initializeVariations());
  bool nominalOnly = false;
  m_config->getif<bool>("nominalOnly", nominalOnly);

  // if (re)compute pileup reweight, then add PRW_DATASF to variations
  if (m_isMC && !nominalOnly && (m_computePileupReweight || m_recomputePileupReweight)) {
    m_variations.push_back("PRW_DATASF__1up");
    m_variations.push_back("PRW_DATASF__1down");
  }
  
  return EL::StatusCode::SUCCESS;
} // initializeVariations

EL::StatusCode AnalysisReader_hhbbtt::setObjectsForOR(const xAOD::ElectronContainer * electrons,
				     const xAOD::PhotonContainer * /*photons*/,
				     const xAOD::MuonContainer * muons,
				     const xAOD::TauJetContainer * taus,
				     const xAOD::JetContainer * jets,
                 const xAOD::JetContainer* fatjets,
                 const xAOD::DiTauJetContainer* ditaus)
{
  
  if(electrons){
    for(const xAOD::Electron * elec : *electrons){
      Props::passPreSel.set(elec, Props::isHHLooseElectron.get(elec));
    }
  }
  
  if(muons){
    for(const xAOD::Muon * muon : *muons){
      Props::passPreSel.set(muon, Props::isHHLooseMuon.get(muon));
    }
  }
  
  if(jets){
    for(const xAOD::Jet * jet : *jets){
      Props::passPreSel.set(jet, Props::isVetoJet.get(jet));
    }
  }
  
  if(taus){
    for(const xAOD::TauJet * tau : *taus){
      Props::passPreSel.set(tau, Props::passTauSelector.get(tau));
    }
  }

  if (fatjets) {
    for (const xAOD::Jet* jet : *fatjets) {
      Props::passPreSel.set(jet,Props::isFatJet.get(jet));
    }
  }

  if (ditaus) {
    for (const xAOD::DiTauJet* ditau : *ditaus) {
      Props::passPreSel.set(ditau,1);
    }
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt :: fill_bTaggerHists(const xAOD::Jet* jet)
{
  // fills histograms per jet and jet flavour for a list of b-taggers
  std::string flav = "Data";

  if (m_isMC) {
    int label = Props::HadronConeExclTruthLabelID.get(jet);

    flav = "L";

    if (label == 4) flav = "C";
    else if (label == 5) flav = "B";
  }

  //float  SV1_IP3D = Props::SV1_IP3D.get(jet);
  float  MV2c10   = Props::MV2c10.get(jet);
  double BVar     = BTagProps::tagWeight.get(jet);

  if (m_isMC) {
    double BEff = BTagProps::eff.get(jet);
    m_histSvc->BookFillHist("eff_"   + flav, 110, -0.1, 1.1, BEff,  m_weight);
    m_histSvc->BookFillHist("pT_eff_"  + flav, 400,   0, 400, 110, 0.0, 1.1, jet->pt() / 1e3, BEff,  m_weight);
    m_histSvc->BookFillHist("eta_eff_" + flav, 100,  -5,   5, 110, 0.0, 1.1, jet->eta(), BEff,     m_weight);
  }
  m_histSvc->BookFillHist("btag_weight_"   + flav, 110, -1.1, 1.1, BVar,   m_weight);
  //m_histSvc->BookFillHist("SV1_IP3D_" + flav, 110,  -30,  80, SV1_IP3D, m_weight);
  m_histSvc->BookFillHist("MV2c10_"   + flav, 110, -1.1, 1.1, MV2c10,   m_weight);

  

  return EL::StatusCode::SUCCESS;
}

void AnalysisReader_hhbbtt::compute_btagging (const std::vector<const xAOD::Jet*> &/*signalJets*/)
{
  if (m_trackJets) {
    m_bTagTool->setJetAuthor(m_trackJetReader->getContainerName());
    for (auto jet : *m_trackJets) {
      BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
    }
  }

  if (m_jets) {
    m_bTagTool->setJetAuthor(m_jetReader->getContainerName());
    for (auto jet : *m_jets) {
      if(!m_use2DbTagCut) BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
      else BTagProps::isTagged.set(jet, false);

      BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));

      if (m_isMC && !m_use2DbTagCut) BTagProps::eff.set(jet, m_bTagTool->getEfficiency(*jet));
      else BTagProps::eff.set(jet, -999.);
    }

  }
  //Temporarily removed treatment of fat jets in lephad, but do not remove we might need it back
  /*
 if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      using FatJetType = typename std::remove_pointer<decltype(fatjet)>::type;
      static FatJetType::ConstAccessor<vector<ElementLink<DataVector<xAOD::IParticle>>>> 
        GhostAccessor ("GhostAntiKt2TrackJet");
      if (Props::isFatJet.get(fatjet)) {
        // determine number of b-tags in a fat jet
        //  number of tracks will be the track jets with closest DR with the ghost matched track jets
        auto nTags            = 0;
        std::vector<const xAOD::Jet*> trackJetsInFatJet;

        if (m_trackJets) {
          // loop over the ghost associated track jets
          for (auto gTrackParticle : GhostAccessor(*fatjet)) {
            if (!gTrackParticle.isValid()) continue;
            const auto &temp = **gTrackParticle;
            auto gTrackJet = dynamic_cast<const xAOD::Jet*>(&temp);
            auto minDR = 100.0;
            const xAOD::Jet *minDRTrackJet = nullptr;

            for (auto trackJet : *m_trackJets) {
              auto Deta    = trackJet->eta() - gTrackJet->eta(),
                   Dphi = trackJet->phi() - gTrackJet->phi(),
                   DR   = sqrt(Deta * Deta + Dphi * Dphi);

              if (minDR > DR) {
                minDR = DR;
                minDRTrackJet = trackJet;
              }
            }
            if (minDRTrackJet) trackJetsInFatJet.push_back(minDRTrackJet);
          }
        }

        for (auto trackJet : trackJetsInFatJet)
          if (BTagProps::isTagged.get(trackJet)) ++nTags;

        Props::nTrackJets.set(fatjet, trackJetsInFatJet.size());
        Props::nBTags.set(fatjet, nTags);

        // finished b-tagging criteria

        // determine number of true b-jets in a fat jet with simple DR
        auto nBJets = 0;

        if (m_isMC) {
          for (auto trackJet : trackJetsInFatJet) {
            auto label = -999;
            if (Props::HadronConeExclTruthLabelID.exists(trackJet)) label = Props::HadronConeExclTruthLabelID.get(trackJet);
            else if (Props::TruthLabelID.exists(trackJet)) label = Props::TruthLabelID.get(trackJet);
            if (label == 5) ++nBJets;
          }
        }
        else nBJets = -1;  // Data
        Props::nTrueBJets.set(fatjet, nBJets);

        // finished true b-jet association criteria
      }
    }
 }//end if loop of m_fatjets
*/

} // compute_btagging

// temporarily removed fatjet_selection and implemented a different way to do it, but please do not permentantly remove

void AnalysisReader_hhbbtt::compute_TRF_tagging (const std::vector<const xAOD::Jet*>& signalJets)
{
  bool ttag_track_jets=false;
  if (m_trackJets) {
    m_bTagTool->setJetAuthor(m_trackJetReader->getContainerName());
    if(ttag_track_jets)    m_bTagTool->truth_tag_jets(m_eventInfo->eventNumber(),*m_trackJets,m_config);
    else{
      for (auto jet : *m_trackJets) {
	BTagProps::isTagged.set(jet, static_cast<decltype(BTagProps::isTagged.get(jet))>(m_bTagTool->isTagged(*jet)));
	BTagProps::tagWeight.set(jet, Props::MV2c10.get(jet));
      }
    }
  }

  if (m_jets) {//we assume signal jets are taken from the larger jet container
    m_bTagTool->setJetAuthor(m_jetReader->getContainerName());
    m_bTagTool->truth_tag_jets(m_eventInfo->eventNumber(),signalJets,m_config);
  }

  if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      Props::nTrackJets.set(fatjet, 0);
      Props::nBTags.set(fatjet, 0);
      if (m_isMC) Props::nTrueBJets.set(fatjet, 0);
      else Props::nTrueBJets.set(fatjet, -1);
    }
  }

} // compute_TRF_tagging


/*
void AnalysisReader_hhbbtt::fatjet_selection ()
{
  if (m_fatJets) {
    for (auto fatjet : *m_fatJets) {
      if (Props::isFatJet.get(fatjet)) {
        // remove fatjet close to leptons with a simple DR cut
        auto fatjeteta = fatjet->eta();
        auto fatjetphi = fatjet->phi();

        for (auto electron : *m_electrons) {
          auto Deta    = electron->eta() - fatjeteta,
               Dphi = electron->phi() - fatjetphi,
               DR   = sqrt(Deta * Deta + Dphi * Dphi);

          if (DR < 1.0) {
            Props::isFatJet.set(fatjet, static_cast<int>(false));
            break;
          }
        }

        if (!Props::isFatJet.get(fatjet)) continue;

        for (auto muon : *m_muons) {
          auto Deta    = muon->eta() - fatjeteta,
               Dphi = muon->phi() - fatjetphi,
               DR   = sqrt(Deta * Deta + Dphi * Dphi);

          if (DR < 1.0) {
            Props::isFatJet.set(fatjet, static_cast<int>(false));
            break;
          }
        }

	// if (!Props::isFatJet.get(fatjet)) continue;

        // finished lepton isolation criteria
      }
    }
  }
} // fatjet_selection

*/



 //the function below defines 1 btag and 2 btag regions based on how many btagged jets there are 
void AnalysisReader_hhbbtt::tagjet_selection(
					   std::vector<const xAOD::Jet*> signalJets,
					   std::vector<const xAOD::Jet*> forwardJets,
					   std::vector<const xAOD::Jet*> &selectedJets,
					   int &tagcatExcl,
					   int &tagcatIncl)
{
  string tagStrategy, tagAlgorithm;

  m_config->getif<std::string>("tagStrategy", tagStrategy);   // AllSignalJets,Leading2SignalJets,LeadingSignalJets
  m_config->getif<std::string>("tagAlgorithm", tagAlgorithm); // FlavLabel,FlavTag

  selectedJets.clear();
  tagcatExcl = -1;
  tagcatIncl = -1;

  /////////////////////////////////////////////////////////////
  // **B-Tagging Selection**
  bool Lead1BTag   = false;
  bool Lead2BTag   = false;
  int  Ind1BTag    = -1;
  int  Ind2BTag    = -1;
  int  nbtag       = 0;
  int  jetidx      = 0;
  int  nSignalJet  = signalJets.size();
  int  nForwardJet = forwardJets.size();
  const xAOD::Jet *Jet1;
  const xAOD::Jet *Jet2;
  const xAOD::Jet *Jet3;

  if (m_isMC && tagAlgorithm == "FlavLabel") //use truth label to select b-jets (MC only!)
  {
    if (Props::HadronConeExclTruthLabelID.get(signalJets.at(0)) == 5) Lead1BTag = true;
    if (Props::HadronConeExclTruthLabelID.get(signalJets.at(1)) == 5) Lead2BTag = true;
  }
  else // if (tagAlgorithm == "FlavTag") use b-tagging to select b-jets (the only option for data)
  {
    //truth tagging (MC only)
    // if(m_isMC && m_doTruthTagging){
    //   if (BTagProps::isTruthTagged.get(signalJets.at(0)) == 1) Lead1BTag = true;
    //   if (BTagProps::isTruthTagged.get(signalJets.at(1)) == 1) Lead2BTag = true;
    // }
   
    //if direct tagging
    // else{
    if (BTagProps::isTagged.get(signalJets.at(0)) == 1) Lead1BTag = true; //isTagged property is set in compute_btagging function
    if (BTagProps::isTagged.get(signalJets.at(1)) == 1) Lead2BTag = true;
      //}
  }

  //set ind1btag to btagged jet with highest pt, ind1btag to btagged jet with 2nd highest pt
  for (const xAOD::Jet *jet : signalJets)
  {
    if (m_isMC && tagAlgorithm == "FlavLabel") //use truth label to select b-jets (MC only!)
    {
      if (Props::HadronConeExclTruthLabelID.get(jet) == 5)
      {
        nbtag++;

        if (Ind1BTag < 0) Ind1BTag = jetidx;
        else if (Ind2BTag < 0) Ind2BTag = jetidx;
      }
    }
    else //if (tagAlgorithm == "FlavTag") use b-tagging to select b-jets (the only option for data)
    {
      //if truth tagging (MC only)
      // if(m_isMC && m_doTruthTagging){
      // 	if (BTagProps::isTruthTagged.get(jet) == 1)
      // 	      {
      // 	         nbtag++;
	    
      // 	         if (Ind1BTag < 0) Ind1BTag = jetidx;
      // 	         else if (Ind2BTag < 0) Ind2BTag = jetidx;
      // 	      }
      // }
      //if direct tagging 
      // else{
	      if (BTagProps::isTagged.get(jet) == 1)
	      {
	         nbtag++;
	    
	         if (Ind1BTag < 0) Ind1BTag = jetidx;
	         else if (Ind2BTag < 0) Ind2BTag = jetidx;
	      }
	      //}
    }
    jetidx++;
  }

  /////////////////////////////////////////////////////////////
  // **Tag-Category Definition**
  //define regions based on weather you want 0/1/2 btag categories in regions with 2 or more signal jets (first tagStrategy), or if you want 0/1/2 bag categories in regions with exactly 2 signal jets 
  //not sure the difference between tagcatExcl =0/1/2 when 0/1/2 btags, tagcatIncl is for what? 

  if ((tagStrategy == "AllSignalJets") || (tagStrategy == "LeadingSignalJets"))
  {
    if (signalJets.at(0)->pt() / 1000. > 45.)
    {
      tagcatIncl = 0; 

      if (nbtag == 0) tagcatExcl = 0;
      Jet1 = signalJets.at(0);
      Jet2 = signalJets.at(1);

      if (nSignalJet >= 3) Jet3 = signalJets.at(2);
      else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
    }

    if ((nbtag >= 1) && (signalJets.at(Ind1BTag)->pt() / 1000. > 45.))
    {
      tagcatIncl = 1;

      if (nbtag == 1) tagcatExcl = 1;

      // if(tagStrategy == "LeadingSignalJets") { if (!Lead1BTag) return EL::StatusCode::SUCCESS; }
      if (tagStrategy == "LeadingSignalJets") {
        if (!Lead1BTag) {
          tagcatExcl = -1;
          Jet1 = signalJets.at(Ind1BTag);
        }
      }

      if (Ind1BTag == 0) Jet2 = signalJets.at(1);
      else Jet2 = signalJets.at(0);

      if (nSignalJet >= 3)
      {
        if (Ind1BTag < 2) Jet3 = signalJets.at(2);
        else Jet3 = signalJets.at(1);
      }
      else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
    }

    if ((nbtag >= 2) && (signalJets.at(Ind1BTag)->pt() / 1000. > 45.))
    {
      tagcatIncl = 2;

      if (nbtag == 2) tagcatExcl = 2;

      if (tagStrategy == "LeadingSignalJets") if (!Lead1BTag) tagcatExcl = -1; 
      Jet1 = signalJets.at(Ind1BTag);
      Jet2 = signalJets.at(Ind2BTag);
      int jetidx3 = 0;

      if (nSignalJet >= 3) // may change with if/else to be consistent with 1tag
      {
        for (const xAOD::Jet *jet : signalJets)
        {
          if ((jetidx3 != Ind1BTag) && (jetidx3 != Ind2BTag)) { Jet3 = jet; break; }
          jetidx3++;
        }
      }
      else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
    }
  }

  /////////////////////////////////////////////////////////////
  if (tagStrategy == "Leading2SignalJets")
  {
    tagcatIncl = 0;

    if ((Lead1BTag == false) && (Lead2BTag == false) && (signalJets.at(0)->pt() / 1000. > 45.)) tagcatExcl = 0;

    if (((Lead1BTag == true) || (Lead2BTag == true)) && (signalJets.at(0)->pt() / 1000. > 45.)) tagcatIncl = 1;

    if ((((Lead1BTag == true) && (Lead2BTag == false)) || ((Lead1BTag == false) && (Lead2BTag == true))) && (signalJets.at(0)->pt() / 1000. > 45.)) tagcatExcl = 1;

    if ((Lead1BTag == true) && (Lead2BTag == true) && (signalJets.at(0)->pt() / 1000. > 45.)) { tagcatExcl = 2; tagcatIncl = 2; }

    Jet1 = signalJets.at(0);
    Jet2 = signalJets.at(1);

    if (nSignalJet >= 3) Jet3 = signalJets.at(2);
    else if (nForwardJet >= 1) Jet3 = forwardJets.at(0);
  }

  selectedJets.push_back(Jet1);
  selectedJets.push_back(Jet2);

  //set is tagged PROPERTY AGAIN (over wrtting is btag in compute btagging?) for leading /sublead signal jets chosen in taggins trategy loop

  BTagProps::isTagged.set(Jet1, static_cast<decltype(BTagProps::isTagged.get(Jet1))>(m_bTagTool->isTagged(*Jet1)));
  BTagProps::isTagged.set(Jet2, static_cast<decltype(BTagProps::isTagged.get(Jet2))>(m_bTagTool->isTagged(*Jet2)));

  if ((nSignalJet >= 3) || (nForwardJet >= 1)) selectedJets.push_back(Jet3);

  /////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////
} // tagjet_selection

EL::StatusCode AnalysisReader_hhbbtt::fill_nJetHistos(std::vector<const xAOD::Jet*> jets, string jetType) {
  int  nJet  = jets.size();
  bool isSig = (jetType == "Sig");

  m_histSvc->BookFillHist("N" + jetType + "Jets", 11, -0.5, 10.5, nJet, m_weight);

  for (const xAOD::Jet *jet : jets) {
    m_histSvc->BookFillHist("Pt" + jetType + "Jets", 100,  0, 100, jet->pt() / 1e3, m_weight);
    m_histSvc->BookFillHist("Eta" + jetType + "Jets", 100, -5,   5, jet->eta(),    m_weight);
  }

  if (isSig) {
    for (const xAOD::Jet *jet : jets) fill_bTaggerHists(jet);
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::setevent_flavour(std::vector<const xAOD::Jet*> selectedJets )
{
  int jet0flav = -999;
  int jet1flav = -999;
  //if(Props::ConeTruthLabelID.exists(selectedJets.at(0))){ //in rel20
  if (Props::HadronConeExclTruthLabelID.exists(selectedJets.at(0))) { // in rel20
     //jet0flav = Props::ConeTruthLabelID.get(selectedJets.at(0));
     //jet1flav = Props::ConeTruthLabelID.get(selectedJets.at(1));
     jet0flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(0));
     jet1flav = Props::HadronConeExclTruthLabelID.get(selectedJets.at(1));
  }
  if(jet0flav < 0 || jet1flav < 0){
     Error("AnalysisReader_hhbbtt::setevent_flavour","Failed to retrieve jet flavour! Exiting.");
     return EL::StatusCode::FAILURE;
  }

  m_histNameSvc -> set_eventFlavour(jet0flav, jet1flav);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::setevent_flavour(const std::vector<int>& vFlavours )
{
  int jet0flav = -999;
  int jet1flav = -999;
  jet0flav = vFlavours.size() > 0 ? vFlavours.at(0) : 0;
  jet1flav = vFlavours.size() > 1 ? vFlavours.at(1) : 0;
  
  if(jet0flav < 0 || jet1flav < 0){
     Error("AnalysisReader_hhbbtt::setevent_flavour","Failed to retrieve jet flavour! Exiting.");
     return EL::StatusCode::FAILURE;
  }
  m_histNameSvc -> set_eventFlavour(jet0flav, jet1flav);

  return EL::StatusCode::SUCCESS;
}

std::vector<const xAOD::Jet*> AnalysisReader_hhbbtt::trackJetsForBTag(const xAOD::Jet* fatJet, const std::vector<const xAOD::Jet*>& vTrackJets)
{
  using FatJetType = typename std::remove_pointer<decltype(fatJet)>::type;
  std::string sLinkName = "AntiKtVR30Rmax4Rmin02TrackJets";
  m_config->getif<std::string>("trackLinkName", sLinkName);
  static FatJetType::ConstAccessor<vector<ElementLink<DataVector<xAOD::IParticle>>>> GhostAccessor(sLinkName.c_str());
  
  std::vector<const xAOD::Jet*> vTrackJetsForBTag;
  
  if( !Props::isFatJet.get(fatJet) ) {
    if ( m_debug ) std::cout << "INFO   The fatJet is not a fat jet! Skip it!" << std::endl;
  } else {
    if ( GhostAccessor.isAvailable(*fatJet) ) {
      for ( auto trackJet : vTrackJets ) {
        for ( auto gTrackParticle : GhostAccessor(*fatJet) ) {
          if ( !gTrackParticle.isValid() ) continue;
          if ( *gTrackParticle == trackJet ) { // match
            vTrackJetsForBTag.push_back(trackJet);
            break;
          }
        }
        if ( vTrackJetsForBTag.size() > 1) break; // leading two
      }
    } else {
      if ( m_debug ) std::cout << "INFO   Ghost link is not available for this fatjet!" << std::endl;
    }
  }

  // set nBTags
  int nBTags(0);
  for ( const xAOD::Jet *jet : vTrackJetsForBTag ) {
    if ( BTagProps::isTagged.get(jet) ) nBTags++;
  }
  Props::nBTags.set(fatJet, nBTags);

  return vTrackJetsForBTag;
}

std::vector<const xAOD::Jet*> 
AnalysisReader_hhbbtt::trackJetsForSF(const xAOD::Jet* fatJet_Sel, 
                                      const std::vector<const xAOD::Jet*>& vFatJets_4b, 
                                      const std::vector<const xAOD::Jet*>& vTrackJets)
{
  
  std::vector<const xAOD::Jet*> vTrackJetsForSF, vTrackJets_Sel, vTrackJets_4b;
  std::vector<int> vTruthLabels;
  int nAddBTags(0);
  
  // process selected fatjet
  vTrackJets_Sel = trackJetsForBTag(fatJet_Sel, vTrackJets);
  for ( const xAOD::Jet *jet : vTrackJets_Sel ) {
    int label = -1;
    if ( m_isMC ) label = Props::HadronConeExclTruthLabelID.get(jet);
    vTruthLabels.push_back(label);
  }
  if ( m_setEventFlavour && m_isMC && !m_isSignal ) setevent_flavour(vTruthLabels);
  m_FJNbtag = Props::nBTags.get(fatJet_Sel);

  // process fatjets for 4b orthogonality
  for ( auto const& fatJet_4b : vFatJets_4b )
  {
    auto vTrackJetsHere = trackJetsForBTag(fatJet_4b, vTrackJets);
    vTrackJets_4b.insert(vTrackJets_4b.end(), vTrackJetsHere.begin(), vTrackJetsHere.end());
    vTrackJetsHere.clear();
    nAddBTags += Props::nBTags.get(fatJet_4b);
  }
  Props::nAddBTags.set(fatJet_Sel, nAddBTags);
  
  // track jets for b-tagging weights
  bool bAddBtagVeto = false;
  m_config->getif<bool>("additionalBtagVeto", bAddBtagVeto);
  vTrackJetsForSF = vTrackJets_Sel;
  if ( bAddBtagVeto && fabs(m_SelDTsubjets.DeltaPhi(m_METVec))<1. && (m_FJNbtag == 1||m_FJNbtag == 2) )
    vTrackJetsForSF.insert(vTrackJetsForSF.end(), vTrackJets_4b.begin(), vTrackJets_4b.end());
  
  vTruthLabels.clear();
  vTrackJets_Sel.clear();
  vTrackJets_4b.clear();
  
  return vTrackJetsForSF;
  
}

bool AnalysisReader_hhbbtt::passVRJetOR(const std::vector<const xAOD::Jet*>& vTrackJetsForSF)
{
  if ( vTrackJetsForSF.size() == 0 ) return true;
  if ( Props::VRJetOverlap.exists(vTrackJetsForSF.at(0)) ) {
    for ( const xAOD::Jet* jet : vTrackJetsForSF ) {
      if ( Props::VRJetOverlap.get(jet) ) return false;
    }
  } 
  return true;
}

/*float AnalysisReader_hhbbtt::computeBTagSFWeight (std::vector<const xAOD::Jet*> &signalJets)
{
  using CP::CorrectionCode;
  using CP::SystematicCode;
  using CP::SystematicSet;

  float weight = 1;

  std::unordered_map<std::string, float> btageffweights;
  auto systs = m_bTagTool->affectingSystematics();

  // Set all weights to 1
  for (auto var : systs) {
    btageffweights[var.name()] = 1.0;
  }

  for (auto jet : signalJets) {

    float sf(1.0);
    auto  GeV = 1e3;

    // Code crashes without this!
    if (fabs(jet->eta()) > 2.5) continue;
    if (fabs(jet->pt()) < 20. * GeV) continue;

    //std::cout << "Seg fault" << std::endl;

    CorrectionCode result;
    if (static_cast<bool>(BTagProps::isTagged.get(jet))) result = m_bTagTool->getBTaggingEfficiencyTool().getScaleFactor(*jet, sf);
    else result = m_bTagTool->getBTaggingEfficiencyTool().getInefficiencyScaleFactor(*jet, sf);
    weight *= sf;

    int label(1000);
    if (Props::HadronConeExclTruthLabelID.exists(jet)) label = Props::HadronConeExclTruthLabelID.get(jet);
    else if (Props::TruthLabelID.exists(jet)) label = Props::TruthLabelID.get(jet);

    if (result != CorrectionCode::Ok) Warning("computeBTagSFWeight", "Get efficiency failed (jet details): eta %f, pt %f, sf %f, MV2c10 %f, flavor %d", jet->eta(), jet->pt(), sf, Props::MV2c10.get(jet), label);

    for (auto var : systs) {
      SystematicSet set;
      set.insert(var);
      auto sresult = m_bTagTool->applySystematicVariation(set);

      if (sresult != SystematicCode::Ok) {
        //	std::cout << var.name() << " apply systematic variation FAILED " << std::endl;
      }

      if (static_cast<bool>(BTagProps::isTagged.get(jet))) result = m_bTagTool->getBTaggingEfficiencyTool().getScaleFactor(*jet, sf);
      else result = m_bTagTool->getBTaggingEfficiencyTool().getInefficiencyScaleFactor(*jet, sf);

      if (result != CorrectionCode::Ok) {
        // std::cout << var.name() << " getScaleFactor FAILED" << std::endl;
      } else {
        //	std::cout << var.name() << " " <<m_hbbname[var.name()] << sf << std::endl;
        btageffweights[var.name()] *= sf;
      }
      // Info("computeBTagSFWeight", "pT: %f, eta: %f, label: %d, SF: %f, var: %s", jet->pt(), jet->eta(), label, sf, var.name().c_str());
    }

    // don't forget to switch back off the systematics...
    SystematicSet defaultSet;
    auto dummyResult = m_bTagTool->applySystematicVariation(defaultSet);

    if (dummyResult != SystematicCode::Ok) Warning("computeBTagSFWeight", "Problem disabling systematics setting!");
  }
  if (m_currentVar == "Nominal") {
    for (auto pair : btageffweights)
      m_weightSysts.push_back({pair.first, pair.second/weight});
  }
  // Info("computeBTagSFWeight", "event weight: %f", weight);
  return weight;
} */ 
  


std::pair<int,int> AnalysisReader_hhbbtt :: HiggsTag(const xAOD::Jet *fatjet, std::string wp) {
  // LASER: A simplified version of the Higgs tagger due to current limitations in CxAOD Framework
  // Input:
  //       1) const xAOD::Jet *fatjet [fatjet to tag]
  //       2) std::string wp [working point]
  // Output:
  //       1) std::pair result [result.first = pass/fail tagger, result.second = number of btagged track jets]
  // Result codes look like:
  //      -1: wrong configuration, kill everything
  //      -2: couldn't get associated track jets
  //      -3: less than 2 associated track jets
  //      -4: something went wrong in Laser's logic
  //       0: a fatjet that did not pass the mass/substructure tagger cuts
  //       1: a fatjet that did pass the mass/substructure tagger cuts
  // NB: No muon-in-jet correction applied (due to aforementioned limitations)
  
  std::pair<int,int> result;
  
  // Step 0: check working point and define cut values
  float lowmasscut; // low mass window cut
  float highmasscut; // high mass window cut
  float D2_a0, D2_a1, D2_a2, D2_a3, D2_a4; // D2 fit parameters (4th order polynomial)
  if (wp == "loose") {
    lowmasscut = 76.;
    highmasscut = 146.;
  }
  else if (wp == "medium") {
    lowmasscut = 93.;
    highmasscut = 134.;
  }
  else if (wp == "tight") {
    lowmasscut = 93.;
    highmasscut = 134.;
    D2_a0 = 11.3684217088;
    D2_a1 = -0.0834101931325;
    D2_a2 = 0.000244968552399;
    D2_a3 = -3.09799473883e-07;
    D2_a4 = 1.44703493877e-10;
  }
  else {
    if(m_debug) std::cout << "ERROR!!! Provided working point is not defined!  It must be either loose, medium, or tight!" << std::endl;
    result.first = -1;
    return result;
  }
  // Step 1: get the associated track jets
  std::vector<const xAOD::Jet*> trkJets, goodTrkJets;
  if (fatjet->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", trkJets)) {
    if(m_debug) std::cout << "Debug HiggsTagger: Step 1 in trk jets loos" << std::endl;
    int ntrkjets = 0;
    for (const xAOD::Jet *trkJ : trkJets) {
      if (!trkJ) continue; // if the trackjet is not valid then skip it
      if (!(trkJ->pt() / 1000. > 10. && fabs(trkJ->eta()) < 2.5)) continue;
      goodTrkJets.push_back(trkJ);
      ntrkjets++;
      m_histSvc->BookFillHist("HiggsTagger_trkJets_pt", 400, 0, 2000, trkJ->pt()/1e3, m_weight);
      m_histSvc->BookFillHist("HiggsTagger_trkJets_MV2c10", 200, -1, 1, Props::MV2c10.get(trkJ), m_weight);
      m_histSvc->BookFillHist("HiggsTagger_trkJets_2d", 400, 0, 2000, 200, -1, 1, trkJ->pt()/1e3, Props::MV2c10.get(trkJ), 1.);
    }
    m_histSvc->BookFillHist("HiggsTagger_trkJets_n", 10, 0, 10, ntrkjets, m_weight);
  }
  else {
    result.first = -2;
    return result; // no associated track jets
  }
  
  // Step 2: count the number of btagged associated track jets
  if (goodTrkJets.size() < 2) {
    result.first = -3;
    return result;
  }
  std::sort(goodTrkJets.begin(), goodTrkJets.end(), EventSelection::sort_pt);
  const xAOD::Jet *trkJ_lead = goodTrkJets.at(0);
  const xAOD::Jet *trkJ_sublead = goodTrkJets.at(1);
  int nbtaggedtrkjets = 0;
  if (Props::MV2c10.get(trkJ_lead) > m_trkBTagLimit) nbtaggedtrkjets++;
  if (Props::MV2c10.get(trkJ_sublead) > m_trkBTagLimit) nbtaggedtrkjets++;
  m_histSvc->BookFillHist("HiggsTagger_trkJets_nbtagged", 10, 0, 10, nbtaggedtrkjets, m_weight);
  result.second = nbtaggedtrkjets; // how many btagged track jets are there
  
  // Step 3: apply mass window cut
  std::string btagregion;
  if (result.second == 0) btagregion = "0btag"; // number of btagged track jets defines which region
  if (result.second == 1) btagregion = "1btag";
  if (result.second >= 2) btagregion = "2btag";
  std::cout << "NI: Debug HiggsTagger: Step 3 result should not be negative" << result.second << "btagregion  "<< btagregion  << std::endl;
  float fatjetmass = fatjet->m() / 1000.;
  float fatjetD2 = Props::D2.get(fatjet);
  float fatjetC2 = Props::C2.get(fatjet);
  m_histSvc->BookFillHist("HiggsTagger_premasscut_m", 100, 0, 500, fatjetmass, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_premasscut_d2", 100, 0, 5, fatjetD2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_premasscut_c2", 20, 0, 1, fatjetC2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_premasscut_m", 100, 0, 500, fatjetmass, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_premasscut_d2", 100, 0, 5, fatjetD2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_premasscut_c2", 20, 0, 1, fatjetC2, m_weight);
  if (!(fatjetmass > lowmasscut && fatjetmass < highmasscut)) {
    result.first = 0;
    return result; // an unsuccessfully tagged fatjet, pass to the control region
  }
  m_histSvc->BookFillHist("HiggsTagger_postmasscut_d2", 100, 0, 5, fatjetD2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_postmasscut_c2", 20, 0, 1, fatjetC2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_postmasscut_d2", 100, 0, 5, fatjetD2, m_weight);
  m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_postmasscut_c2", 20, 0, 1, fatjetC2, m_weight);
  

  // Step 4: apply substructure cut if we are at the tight working point
  if (wp != "tight") {
    result.first = 1;
    return result; // a succesfully tagged fatjet at the loose or medium working points
  }
  else {
    float D2cut = D2_a0 + (D2_a1 * fatjet->pt() / 1000.) + (D2_a2 * pow(fatjet->pt() / 1000., 2)) + (D2_a3 * pow(fatjet->pt() / 1000., 3)) + (D2_a4 * pow(fatjet->pt()/ 1000., 4));
    if (fatjetD2 > D2cut) {
      result.first = 1;
      m_histSvc->BookFillHist("HiggsTagger_postd2cut_c2", 20, 0, 1, fatjetC2, m_weight);
      m_histSvc->BookFillHist("HiggsTagger_"+btagregion+"_postd2cut_c2", 20, 0, 1, fatjetC2, m_weight);
      return result; // a successfully tagged fatjet at the tight working point
    }
    else {
      result.first = 0;
      return result; // an unsuccessfully tagged fatjet, pass to the control region
    }
  }
  
  // If you got here, then something went wrong
  result.first = -3;
  return result; // something went wrong, start yelling
}//end of HiggsTag

EL::StatusCode AnalysisReader_hhbbtt :: fill_lephad()
{
  if(m_debug) std::cout << "FILLLEPHAD " << std::endl;
  // m_tree->SetVariation(m_currentVar);
  ResultHHbbtautau selectionResult = ((HHbbtautauLepHadSelection*)m_eventSelection)->result();

  bool passSel=true;
  bool passPreSel = true;

  SelectionContainers container;
  container.evtinfo   = m_eventInfo;
  container.met       = m_met;
  container.electrons = m_electrons;
  container.photons   = m_photons;
  container.muons     = m_muons;
  container.taus      = m_taus;
  container.jets      = m_jets;
  container.fatjets   = m_fatJets;
  container.trackjets = m_trackJets;

  ((HHbbtautauBoostedJetSelection*)m_eventPostSelection)->setResult(selectionResult);
  passPreSel &= m_eventPostSelection->passPreSelection(container,false);
  passSel &= m_eventPostSelection->passSelection(container,false);
  if(!passPreSel || !passSel) return EL::StatusCode::SUCCESS;

  ResultHHbbtautau postSelectionResult = ((HHbbtautauBoostedJetSelection*)m_eventPostSelection)->result();

  
  //do final event selection
  const xAOD::Electron* electron = postSelectionResult.el;
  const xAOD::Muon* muon = postSelectionResult.mu;
  const xAOD::MissingET* met = postSelectionResult.met;
  std::vector<const xAOD::TauJet*> taus = postSelectionResult.taus; 
  std::vector<const xAOD::Jet*> signalJets = postSelectionResult.signalJets;
  std::vector<const xAOD::Jet*> forwardJets = postSelectionResult.forwardJets;
  if (m_debug) std::cout << "AT START Event  " << m_eventInfo->eventNumber() << " num btag signal jets " << signalJets.size()  <<std::endl;

  int nSignalJet = signalJets.size();
  int nForwardJet = forwardJets.size();
  int nJet = nSignalJet + nForwardJet;

  //  m_weight *= Props::leptonSF.get(m_eventInfo);

  /////////////////////////////////////////////////////////////
  // **SF** : lepton SF : comment out trigger scale factors for now
  ////m_weight *= Props::leptonSF.get(m_eventInfo);
 
  /////////////////////////////////////////////////////////////
  // **Selection & SF**: trigger
  // trigger selection : nominal case
  //double triggerSF_nominal = 1.;
  //if ( !m_triggerTool->getTriggerDecision(m_eventInfo, triggerSF_nominal, electron, 0, muon, 0, met, 0, m_puReweightingTool->GetRandomRunNumber(m_eventInfo), "Nominal") ) return EL::StatusCode::SUCCESS;
  //if (m_isMC) m_weight *= triggerSF_nominal;
  // handle systematics
  //if (m_isMC && m_currentVar=="Nominal") {
  //  for (size_t i = 0; i < m_triggerSystList.size(); i++) {
      // not computing useless systematics
  //    if (electron && m_triggerSystList.at(i).find("MUON_EFF_Trig") !=std::string::npos) continue;
  //    if (muon && m_triggerSystList.at(i).find("ELECTRON_EFF_Trig") !=std::string::npos) continue;
      // get decision + weight
  //    double triggerSF = 1.;
  //    if ( !m_triggerTool->getTriggerDecision(m_eventInfo, triggerSF, electron, 0, muon, 0, met, 0, m_puReweightingTool->GetRandomRunNumber(m_eventInfo), m_triggerSystList.at(i)) ) return EL::StatusCode::SUCCESS;
  //    if (triggerSF_nominal>0) m_weightSysts.push_back({m_triggerSystList.at(i), triggerSF/triggerSF_nominal});
  //    else Error("fill_1Lep()", "Nominal trigger SF=0!, The systematics will not be generated.");
  //  }
  //}

  //leptons
  TLorentzVector lepVec;
  if (muon) {
    lepVec = muon->p4();
  } else if (electron) {
    lepVec = electron->p4();
  } else {
    Error("fill_lephad", "Missing lepton!");
    return EL::StatusCode::FAILURE;
  }
  //met
  TLorentzVector metVec;
  metVec.SetPtEtaPhiM(met->met(), 0, met->phi(), 0);

  // Taus
  int ntaus=taus.size();  
  if (ntaus==0) return EL::StatusCode::SUCCESS;
  
  TLorentzVector tauVec = taus.at(0)->p4();
  if (taus.size()>1){
    if(taus.at(0)->pt() < taus.at(1)->pt())tauVec = taus.at(1)->p4();
  }


  if (m_debug) std::cout << " lepvec pt "  <<lepVec.Pt()/1e3 <<  " met  "  << metVec.Pt()/1e3 <<std::endl;
 
  // exit if we don't have at least 2 jets
  if (!(nJet >= 2)) return EL::StatusCode::SUCCESS;

  //fatjet_selection();
  compute_btagging(signalJets); //sets the isTagged and TagWeight property for signal jets, and eff property for MC signal jets

  fill_nJetHistos(signalJets, "Sig");
  fill_nJetHistos(forwardJets, "Fwd");
  
  // cut on nSignalJet, exit if not at least 2 signal jets
  if (!(nSignalJet >= 2)) return EL::StatusCode::SUCCESS; 
  

  // leading jet pt cut. sorting is done by event selection, exit if leading jet pt isn't >45 GeV
  if (signalJets.at(0)->pt()/1000. < 45.) return EL::StatusCode::SUCCESS; 

  // **Jets Definition** : define jet pair/triplet for anti-QCD cut // what?
  TLorentzVector j1VecPresel, j2VecPresel, j3VecPresel;
  j1VecPresel = signalJets.at(0)->p4();
  j2VecPresel = signalJets.at(1)->p4();

  if (nSignalJet  >= 3) j3VecPresel = signalJets.at(2)->p4();
  else if (nForwardJet >= 1) j3VecPresel = forwardJets.at(0)->p4(); 

  // **Jets Definition** : jet pair for mBB (+ possibly third jet)
  TLorentzVector j1Vec, j2Vec, j3Vec;

  // **Jets Definition** : jet pair for mBB rescaling
  TLorentzVector j1VecRW,j2VecRW;


  //Nina commented out this chunk for debugging
  
   // **B-tagging-Jet Selection** : 
  int tagcatExcl=-1;
  int tagcatIncl=-1;
  std::vector<const xAOD::Jet*> selectedJets; // here selected jets are all jets
  selectedJets.clear(); 
  //here we are just identifiying 0/1/2 btagged categories in regions that have either exactly 2 or >2 jets as specified by tagStraegy


  //after this step our selected jets are chosen to be our Signal jets that are btagged based on tag stragy! 
  if (m_debug) std::cout << " STEP 4: about to define tagjet_selections  " << std::endl; 
  tagjet_selection(signalJets, forwardJets, selectedJets, tagcatExcl, tagcatIncl);
  
  if(tagcatExcl==-1) return EL::StatusCode::SUCCESS; // Here if tagcatExcl is not set the code exits
  
  

  if (m_debug) std::cout << " STEP 5: defined tagjet_selection  " << std::endl;

  if (m_isMC) m_weight *= computeBTagSFWeight(signalJets); // leave out Btag SF for now
  //if truth tagging

  
  
  if(m_isMC && m_doTruthTagging){
    //only 2tag for the moment
    if(tagcatExcl!=2) return EL::StatusCode::SUCCESS;
    m_weight *= BTagProps::truthTagEventWeight.get(m_eventInfo); //NI, we dont have truthTagEventWeight property why do we need this? its not in VHbb boosted on mono_VH, it is in VHbb
  }
 
  
  m_histNameSvc->set_nTag(tagcatExcl); //this means we make histograms for 0/1/2 tagged regions
  //  m_histNameSvc->set_nJet(nJet); // the tag strategy for now is set to tag 2 signal jets

  // tagcatIncl is not used for now
  /*
    tagcatIncl==0 --> 0ptag, !1ptag, !2ptag
    tagcatIncl==1 --> 0ptag, 1ptag, !2ptag
    tagcatIncl==2 --> 0ptag, 1ptag, 2ptag
   */
  
  //Nina added temporarily for code check 
  //j1Vec = signalJets.at(0)->p4(); 
  //j2Vec = signalJets.at(1)->p4();
  // if(signalJets.size() >= 3) j3Vec = signalJets.at(2)->p4(); //3rd jet

  j1Vec = selectedJets.at(0)->p4(); // j1Vec is leading jet in whole event, it was btagged in tagjet_selection
  j2Vec = selectedJets.at(1)->p4(); // j1vec is subleading jet in whole event

  if(selectedJets.size() >= 3) j3Vec = selectedJets.at(2)->p4(); //3rd jet

  if( nJet>=4 && m_fillCR ){
     m_histNameSvc->set_description("topCR"); //top CR
  }
   
  TLorentzVector HbbVec = j1Vec + j2Vec;
  TLorentzVector Hlephad = tauVec + lepVec;

  //  if (Hlephad.M()/1e3 < 5.){ // if Hlephad Mass is less than 5 GeV
  //m_histSvc -> BookFillHist("ptDiff",   100, 0,20,fabs(lepVec.Pt()-tauVec.Pt())/1e3, m_weight); 
  //m_histSvc -> BookFillHist("DRlephad",   100, 0,0.2,lepVec.DeltaR(tauVec), m_weight);
  // if(muon)m_histSvc -> BookFillHist("muORe",   2, 0,1 ,0, m_weight);
  //if(electron)m_histSvc -> BookFillHist("muORe",   2, 0,1 ,1, m_weight);
  //} 
 
  // **Selection** : mbb invariant mass window + mbb-sidebands
  // rescaled quantities are : j1VecRW, j2VecRW
  // double mbb_weight = 1;
  // int mbbRegion=0;
  // j1VecRW = j1Vec;
  // j2VecRW = j2Vec;
  // if(mbbwindow) 
  //   {
  //     if(HVec.M() < 95e3 || HVec.M() > 140e3) mbbRegion=1;
  //     else 
  // 	{  
  // 	  mbb_weight = 125.0 / (HVec.M()*1e-3); // Jets reweighting
  // 	  j1VecRW = (j1Vec)*mbb_weight;
  // 	  j2VecRW = (j2Vec)*mbb_weight;	 
  // 	}
  //   } 
  // if(mbbRegion==1 && m_histNameSvc->get_description()=="SR") m_histNameSvc->set_description("mBBcr");
  // else if (mbbRegion==1 && m_histNameSvc->get_description()=="topCR") return EL::StatusCode::SUCCESS;
  /*
  // reset MVA tree variables
  m_tree->Reset();
  
  m_tree->EventWeight = m_weight;
  m_tree->EventNumber = m_eventInfo->eventNumber();
  
  m_tree->dPhiLBmin = fabs( lepVec.DeltaPhi(j1Vec) );
  if (m_tree->dPhiLBmin > fabs( lepVec.DeltaPhi(j2Vec) )) {
    m_tree->dPhiLBmin = fabs( lepVec.DeltaPhi(j2Vec) );
  }
  m_tree->MET = met->met();
  */

  double bdt_pTL = lepVec.Pt();
  
  // fill jet histos and set nJet, flavour for histo names
  //fill_jetHistos(signalJets, forwardJets);
  ////fill_jetSelectedHistos(signalJets, forwardJets, selectedJets); //This line does not work, must debug!!
 
  //  m_tree->sample = m_histNameSvc->getFullSample();

  //HbbVec

  if (m_debug) std::cout << " MyReader Event  " << m_eventInfo->eventNumber() << " weight before histograms are drawn "<< m_weight  << std::endl; 

  m_histSvc -> BookFillHist("HbbPt",   30, 0, 1000, HbbVec.Pt()/1e3, m_weight);
  m_histSvc -> BookFillHist("HbbM",   30, 0, 500, HbbVec.M()/1e3, m_weight);
  m_histSvc -> BookFillHist("HbbEta",   20, -5, 5, HbbVec.Eta(), m_weight);
  m_histSvc -> BookFillHist("HbbPhi",   20, -4, 4, HbbVec.Phi(), m_weight);

  //m_histSvc -> BookFillHist("dPhiVBB", 100, 0, 3.15, m_tree->dPhiVBB, m_weight);
  //m_histSvc -> BookFillHist("MET",   100, 0, 500, m_tree->MET/1e3, m_weight);
  //m_histSvc -> BookFillHist("pTV",   100, 0, 500, m_tree->pTV/1e3, m_weight);
  m_histSvc -> BookFillHist("pTL",   100, 0, 500, bdt_pTL/1e3, m_weight);

  //lephad vector
  m_histSvc -> BookFillHist("HlephadPt",   30, 0, 1000, Hlephad.Pt()/1e3, m_weight);
  m_histSvc -> BookFillHist("HlephadM",   30, 0, 500, Hlephad.M()/1e3, m_weight);
  m_histSvc -> BookFillHist("HlephadEta",   20, -5, 5, Hlephad.Eta(), m_weight);
  m_histSvc -> BookFillHist("HlephadPhi",   20, -4, 4, Hlephad.Phi(), m_weight);

  //lep vector
  m_histSvc -> BookFillHist("LepPt",   30, 0, 600, lepVec.Pt()/1e3, m_weight);
  m_histSvc -> BookFillHist("LepM",   10, 0, 1, lepVec.M()/1e3, m_weight);
  m_histSvc -> BookFillHist("LepEta",   20, -4, 4, lepVec.Eta(), m_weight);
  m_histSvc -> BookFillHist("LepPhi",   20, -4, 4, lepVec.Phi(), m_weight);

  //tau vector
  m_histSvc -> BookFillHist("TauPt",   30, 0, 700, tauVec.Pt()/1e3, m_weight);
  m_histSvc -> BookFillHist("TauM",   10, 0, 2, tauVec.M()/1e3, m_weight);
  m_histSvc -> BookFillHist("TauEta",   20, -3, 3, tauVec.Eta(), m_weight);
  m_histSvc -> BookFillHist("TauPhi",   20, -4, 4, tauVec.Phi(), m_weight);

  // lep and tau together
  m_histSvc -> BookFillHist("ptDiff",   20, 0,200,fabs(lepVec.Pt()-tauVec.Pt())/1e3, m_weight);
  m_histSvc -> BookFillHist("DRlephad",   5, 0, 5.0,lepVec.DeltaR(tauVec), m_weight);
  if(muon)m_histSvc -> BookFillHist("muORe",   2, 0,1 ,0, m_weight);
  if(electron)m_histSvc -> BookFillHist("muORe",   2, 0,1 ,1, m_weight);
 

  m_histSvc->BookFillHist("met", 30, 0, 100, metVec.Pt(), m_weight);


  // fill MVA tree
  //  if (m_histNameSvc->get_isNominal()) {    m_tree->Fill();
  //}

  return EL::StatusCode::SUCCESS;
} 



EL::StatusCode AnalysisReader_hhbbtt :: fill_hadhad()
{

  // this Sherpa weight is no longer calculated in the core class
  // but it is also probably not needed in rel 21
  // may be remove it?

  // Sherpa2.2 Njet weight (from Corrs&Sys)
  // ----------------------------------
  CAS::EventType type = CAS::NONAME;
  if ( ((m_mcChannel >= 363102) && (m_mcChannel <= 363122 ))            //Zvv
    || ((m_mcChannel >= 363361) && (m_mcChannel <= 363435 )) )          //Zll
     type = CAS::Z;
  else if ( ((m_mcChannel >= 363331) && (m_mcChannel <= 363354))        //Wtauv
         || ((m_mcChannel >= 363436) && (m_mcChannel <= 363483)) )      //Wlv
     type = CAS::W;
  float sherpa22weight = m_corrsAndSysts->Get_BkgNJetCorrection(type,(Props::NTruthWZJets20.exists(m_eventInfo) ? Props::NTruthWZJets20.get(m_eventInfo) : -1));
  Props::NTruthJetWeight.set(m_eventInfo,sherpa22weight);
  m_weight *= sherpa22weight;

  if(m_debug) std::cout << "FILLHADHAD: " << m_weight << " m_isMC " << m_isMC << std::endl;

  m_histNameSvc->set_description("PreSel");
  m_histNameSvc->set_nTag(0);

  if(m_writeMVATree || m_readMVA) m_tree->SetVariation(m_currentVar);

  // this check is done in CxAODReader, in the setEventWeight function for V+jets
  // the new ttbar sample has weights of O(700) for all events, so uncommenting this rejects all events
  //if(m_isMC && fabs(Props::MCEventWeight.get(m_eventInfo)) > 10.){
  //  Info("execute ()", "Warning: mc weight too high %f",m_weight);
  //  return EL::StatusCode::SUCCESS;
  //}

  //std::cout << "NowRunningOn: " << m_eventInfo->eventNumber() << " pileuprndRunNumber: " << Props::PileupRdmRun.get(m_eventInfo) << " randomrunumber: " << Props::RandomRunNumber.get(m_eventInfo) << st$

  bool passSel=true;
  bool passPreSel = true;

  SelectionContainers container;
  container.evtinfo   = m_eventInfo;
  container.met       = m_met;
  container.electrons = m_electrons;
  container.photons   = m_photons;
  container.muons     = m_muons;
  container.taus      = m_taus;
  container.jets      = m_jets;
  container.fatjets   = m_fatJets;
  container.trackjets = m_trackJets;
  //container.truthParticles = m_truthParts;

  //passPreSel &= m_eventSelection->passPreSelection(container,true);
  //passSel    &= m_eventSelection->passSelection(container,false);

  //if(!passSel || !passPreSel) return EL::StatusCode::SUCCESS;

  ResultHHbbtautau selectionResult = ((HHbbtautauHadHadSelection*)m_eventSelection)->result();
  ((HHbbtautauHadHadJetSelection*)m_eventPostSelection)->setResult(selectionResult);

  passPreSel &= m_eventPostSelection->passPreSelection(container,false);
  passSel &= m_eventPostSelection->passSelection(container,false);


  if(!passPreSel || !passSel) return EL::StatusCode::SUCCESS;

  ResultHHbbtautau postSelectionResult = ((HHbbtautauHadHadJetSelection*)m_eventPostSelection)->result();

  //fill after selection with final objects
  std::vector<const xAOD::TauJet*> tauvector = postSelectionResult.taus;
  std::vector<const xAOD::Jet*> jetvector = postSelectionResult.signalJets;
  std::vector<const xAOD::Jet*> fjetvector = postSelectionResult.forwardJets;
  const xAOD::MissingET* met = postSelectionResult.met;
  Trigger TriggerDecision = postSelectionResult.trigger;

  bool doTriggerStudy = false;
  if(doTriggerStudy) {
    if(Props::passHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo.get(m_eventInfo)) m_histNameSvc->set_description("Trigger_DTT");
    fill_triggerPlots();
  }

  //-------------------------------------------------------------------------------------------------------

  if(m_debug) std::cout << "Total Number of Taus " << tauvector.size()<<std::endl;

 
  double TriggerSF=1;
  TriggerSF= Props::trigSF.get(m_eventInfo);
  
  if(m_debug) std::cout<<"TriggerSF "<<TriggerSF<<std::endl;

  m_weight*=TriggerSF;// apply trigger SF

  std::vector<const xAOD::Jet*> sjets;//vector containing all the Signal Jets
  std::vector<const xAOD::TauJet*> signalTaus;//vector containing all the Signal Taus
  std::vector<const xAOD::TauJet*> antiTaus;//vector containing all the AntiTaus

  int NumberSignalJet45=0;
  int NumberSignalJet50=0;
  int NumberSignalJet70=0;
  int NumberSignalJet80=0;

  for(unsigned int i=0; i<jetvector.size(); i++){
    if(!Props::isSignalJet.get(jetvector[i])) continue;
    if(jetvector[i]->pt()<=20e+3) continue;
    sjets.push_back(jetvector[i]);//fill vector of final signal jets with pt 20 cut
    if(jetvector[i]->pt()<=45e+3) continue;
    NumberSignalJet45++;
    if(jetvector[i]->pt()<=50e+3) continue;
    NumberSignalJet50++;
    if(jetvector[i]->pt()<=70e+3) continue;
    NumberSignalJet70++;
    if(jetvector[i]->pt()<=80e+3) continue;
    NumberSignalJet80++;
  }
   
  if(m_debug) std::cout << "Number of signal Jets: " <<sjets.size()<< std::endl;

  int NumberTau30 = 0; // can include anti-taus
  for (size_t i = 0; i<tauvector.size(); i++) {
    if(tauvector[i]->pt()<=30e+3) continue;
    NumberTau30++;
  }

  int NumberSignalTau30=0;
  int NumberSignalTau40=0;
  int NumberSignalTau100=0;
  int NumberSignalTau140=0;
  int NumberSignalTau160=0;
  int NumberSignalTau180=0;
  int NumberSignalTau200=0; 

  for (size_t i=0; i < tauvector.size(); i++) {
    if(!Props::isBDTMedium.get(tauvector[i])) continue;
    if(tauvector[i]->pt()<=20e+3) continue;
    signalTaus.push_back(tauvector[i]);//fill vector of final signal taus with pt 20 cut
    if(tauvector[i]->pt()<=30e+3) continue;
    NumberSignalTau30++;
    if(tauvector[i]->pt()<=40e+3) continue;
    NumberSignalTau40++;
    if(tauvector[i]->pt()<=100e+3) continue;
    NumberSignalTau100++;
    if(tauvector[i]->pt()<=140e+3) continue;
    NumberSignalTau140++;
    if(tauvector[i]->pt()<=160e+3) continue;
    NumberSignalTau160++;
    if(tauvector[i]->pt()<=180e+3) continue;
    NumberSignalTau180++;
    if(tauvector[i]->pt()<=200e+3) continue;
    NumberSignalTau200++;
  }

  if(m_debug) std::cout << "Number of signal BDTMedium Taus: " <<signalTaus.size()<< std::endl;

  for (size_t i = 0; i < tauvector.size(); i++) {
    if (!Props::isAntiTau.get(tauvector[i])) continue;
    if (!Props::isHHRandomTau.get(tauvector[i])) continue;
    if (tauvector[i]->pt()<=20e+3) continue;
    antiTaus.push_back(tauvector[i]);//fill vector of final antitaus with pt 20 cut
  }
  
  if(m_debug) std::cout << "Number of AntiTaus: " <<antiTaus.size()<< std::endl;
  
  
  // need to get trigger decision again for this!
  m_PassTriggerSelection = false;
  m_PassTriggerJetSelection = false;
  if (TriggerDecision == eDTT || TriggerDecision == eSTT) {
    m_PassTriggerSelection = true;
    if (TriggerDecision == eDTT && NumberSignalJet80 >= 1 && NumberTau30 >= 2) { // Added 30 GeV tau pt cut -- was in MIA, but not reader
      m_PassTriggerJetSelection = true;
    } else if (TriggerDecision == eSTT) {
      m_PassTriggerJetSelection = true;
    }
  }

  //btagging
  if(m_doTruthTagging && m_isMC){
    compute_TRF_tagging(sjets);
  }
  else{
    compute_btagging(sjets);// compute the b-tagging and assign the isTagged property for all the signal jets with pT>20 GeV
  }

  /*if(m_isMC){
    btagWeight=computeBTagSFWeight(sjets, m_jetReader->getContainerName());
    if(m_debug) std::cout << "BTagWeight: "<<btagWeight<< std::endl;

    if(m_doTruthTagging){
      BTagProps::truthTagEventWeight.set(m_eventInfo, btagWeight);
    }
  }

  m_weight *= btagWeight;// apply btag weight*/

  m_nBJets=0;
  for(unsigned int i=0; i<sjets.size();i++) if(BTagProps::isTagged.get(sjets.at(i))) m_nBJets++;

  if(m_debug) std::cout << "MMC fit status: " << Props::mmc_fit_status.get(m_mmc) << std::endl;
  //starting defining variables for the analysis
  if(Props::mmc_fit_status.get(m_mmc) == 1) m_MMCVec.SetPtEtaPhiM(Props::mmc_mlnu3p_4vect_pt.get(m_mmc),Props::mmc_mlnu3p_4vect_eta.get(m_mmc),Props::mmc_mlnu3p_4vect_phi.get(m_mmc),Props::mmc_mlnu3p_4vect_m.get(m_mmc));
  else if (Props::mmc_fit_status.get(m_mmc) == 0) m_MMCVec.SetPtEtaPhiM(-1,-1,-1,-1);
  if(m_debug) std::cout << "MMC " << m_MMCVec.M() << " nBJets " << m_nBJets << std::endl;

  m_METVec.SetPtEtaPhiM(met->met(),0,met->phi(),0);

  std::vector<const xAOD::TauJet*> finalTaus;
  m_region="";

  if(signalTaus.size()==2){
    finalTaus.push_back(signalTaus.at(0));
    finalTaus.push_back(signalTaus.at(1));
    m_region = "MM";
  }else if(signalTaus.size()==1 && antiTaus.size()==1){
    //Fill pT ordered finalTaus vector
    if(signalTaus.at(0)->pt()>antiTaus.at(0)->pt()){
      m_region = "MA";
      finalTaus.push_back(signalTaus.at(0));
      finalTaus.push_back(antiTaus.at(0));
    }else if(signalTaus.at(0)->pt()< antiTaus.at(0)->pt()){
      m_region = "AM";
      finalTaus.push_back(antiTaus.at(0));
      finalTaus.push_back(signalTaus.at(0));
    }
  }else if(antiTaus.size()==2){
    finalTaus.push_back(antiTaus.at(0));
    finalTaus.push_back(antiTaus.at(1));
    m_region = "AA";
  }

  m_histNameSvc->set_analysisType(HistNameSvc::AnalysisType::HHres);

  int pair_charge=Props::charge.get(finalTaus.at(0))*Props::charge.get(finalTaus.at(1));

  if(pair_charge<0) m_region +="_OS";
  else if(pair_charge>0) m_region += "_SS";

  m_RegA = false;
  m_RegB = false;
  m_RegC = false;
  m_RegD = false;

  //Dictionary for regions conventions (for cutflow only)
  if (m_region == "MM_OS") m_RegA = true;
  if (m_region == "MM_SS") m_RegB = true;
  if (m_region == "AA_OS" || m_region == "AM_OS" || m_region == "MA_OS") m_RegC = true;
  if (m_region == "AA_SS" || m_region == "AM_SS" || m_region == "MA_SS") m_RegD = true;

  // **Calculate** : b-tagging SF
  if (m_isMC) {

    float btagWeight=1;

    btagWeight = computeBTagSFWeight(sjets, m_jetReader->getContainerName());
    if(m_debug) std::cout << "BTagWeight: " << btagWeight << std::endl;

    if(m_doTruthTagging){
      BTagProps::truthTagEventWeight.set(m_eventInfo,btagWeight);
      if(m_debug) std::cout << "Truth tag event weight: " << BTagProps::truthTagEventWeight.get(m_eventInfo) << std::endl;
    }

    m_weight  *= btagWeight;
    setevent_flavour(sjets); //Working

    float tauEff = Props::effSF.get(finalTaus.at(0))*Props::effSF.get(finalTaus.at(1));
    if(m_debug) std::cout << "Tau efficiency weight: " << tauEff << std::endl;
    m_weight *= tauEff;

    m_weight *=Props::trigSF.get(m_eventInfo);

  }

  // create vector of central and forward jets
  std::vector<const xAOD::Jet *> ajets;
  for (auto fjet : fjetvector) ajets.push_back(fjet);
  for (auto sjet : sjets) ajets.push_back(sjet);

  // calculate VBF information here to add it in the cutflow
  m_hasVBF = false;
  float vbfmjj = 200;
  for (size_t i1 = 0; i1 < ajets.size(); ++i1) {
    for (size_t i2 = 0; i2 < ajets.size(); ++i2) {
      if (i1 == i2) continue;
      float tdeta = std::fabs(ajets[i1]->eta() - ajets[i2]->eta());
      float tmjj = (ajets[i1]->p4() + ajets[i2]->p4()).M()/1e3;
      if (tmjj > vbfmjj && tdeta > 5.0) {
        m_hasVBF = true;
        if (ajets[i1]->pt() > ajets[i2]->pt()) {
          m_vbfj1 = ajets[i1]->p4();
          m_vbfj2 = ajets[i2]->p4();
        } else {
          m_vbfj2 = ajets[i1]->p4();
          m_vbfj1 = ajets[i2]->p4();
        }
      }
    }
  }

  // find VBF jets without delta eta cut
  m_hasVBF_nodeta = false;
  vbfmjj = 200;
  for (size_t i1 = 0; i1 < ajets.size(); ++i1) {
    for (size_t i2 = 0; i2 < ajets.size(); ++i2) {
      if (i1 == i2) continue;
      float tmjj = (ajets[i1]->p4() + ajets[i2]->p4()).M()/1e3;
      if (tmjj > vbfmjj) {
        m_hasVBF_nodeta = true;
        if (ajets[i1]->pt() > ajets[i2]->pt()) {
          m_vbfj1_nodeta = ajets[i1]->p4();
          m_vbfj2_nodeta = ajets[i2]->p4();
        } else {
          m_vbfj2_nodeta = ajets[i1]->p4();
          m_vbfj1_nodeta = ajets[i2]->p4();
        }
      }
    }
  }


  bool PassCutFlowPres = fill_cutFlow(selectionResult, sjets,finalTaus);
  if(!PassCutFlowPres) return EL::StatusCode::SUCCESS;

  if(m_debug) std::cout << "Passed cut flow for region: " << m_region << " nTags: " << m_nBJets << std::endl;

  m_histNameSvc->set_nTag(m_nBJets);
  m_histNameSvc->set_description(m_region);

  std::string regionFF = get2DProngRegion(finalTaus);

  //applying QCD FF from AA_OS template for estimating QCD in signal region
  //and AA_SS is used to estimate the closure
  m_config->getif<bool>("applyFF", m_applyFF);
  if( (m_region == "AA_OS" || m_region == "AA_SS" ||
       m_region == "AM_OS" || m_region == "AM_SS" ||
       m_region == "MA_OS" || m_region == "MA_SS") && m_applyFF) {
    EL_CHECK("AnalysisReader::fillhadhad()",applyFF(regionFF,finalTaus, TriggerDecision));
  }

  if(m_writeMVATree) m_tree->Reset();

  fill_xHistos(sjets,finalTaus);
  fill_jetHistos(jetvector);
  fill_tauHistos(tauvector);
  fill_sjetHistos(sjets);

  fill_vbfHistos(finalTaus);

  if(m_getFFInputs) fill_tauFFHistos(regionFF,finalTaus);

  if(m_readMVA){
     m_tree->ReadMVA();
     // fill histos with BDT output
     m_histSvc->BookFillHist("BDT", 100, -1, 1, m_tree->BDT, m_weight);
  }

  //fill MVA tree
  //if(m_histNameSvc->get_isNominal() && m_writeMVATree) m_tree->Fill();
  // Also fill it for variations, because we want to check the effect of the systematic variations in training
  // Just fill it for the SR
  // applyFF changes the region to MM OS if it is AA OS after applying the FFs
  if (m_writeMVATree && m_region == "MM_OS") m_tree->Fill();

  //Apply SR CUTS
  bool passMbbCut = m_bb.M()>80 && m_bb.M()<135;
  bool passMMCCut = m_MMCVec.M()>85 && m_MMCVec.M()<135;
  if(m_region=="MM_OS" && passMbbCut && passMMCCut) fill_SRPlots();

  //Apply ZCR Cuts
  if(m_nBJets==1 && m_MMCVec.M()) fill_SRPlots();

  if(m_debug) std::cout << "Exiting successfully" << std::endl;

  return EL::StatusCode::SUCCESS;

}// end of fill had had


EL::StatusCode AnalysisReader_hhbbtt :: fill_boosted()
{

  bool passSel=true;
  bool passPreSel = true;

  SelectionContainers container;
  container.evtinfo   = m_eventInfo;
  container.met       = m_met;
  container.electrons = m_electrons;
  container.photons   = m_photons;
  container.muons     = m_muons;
  container.taus      = m_taus;
  container.jets      = m_jets;
  container.fatjets   = m_fatJets;
  container.trackjets = m_trackJets;
  container.ditaus    = m_ditaus;
  container.truthParticles = m_truthParts;

  //passPreSel &= m_eventSelection->passPreSelection(container,true);
  //passSel    &= m_eventSelection->passSelection(container,false);

  //if(!passSel || !passPreSel) return EL::StatusCode::SUCCESS;

  ResultHHbbtautau selectionResult = ((HHbbtautauBoostedSelection*)m_eventSelection)->result();
  ((HHbbtautauBoostedJetSelection*)m_eventPostSelection)->setResult(selectionResult);

  passPreSel &= m_eventPostSelection->passPreSelection(container,false);
  passSel &= m_eventPostSelection->passSelection(container,false);

  m_histNameSvc->set_description("PrePreSel");

  // trigger selection
  int passHLT_j360_a10r_L1J100 = Props::passHLT_j360_a10r_L1J100.get(m_eventInfo);
  int passHLT_j360_a10_lcw_L1J100 = Props::passHLT_j360_a10_lcw_L1J100.get(m_eventInfo);
  int passHLT_j400_a10r_L1J100 = Props::passHLT_j400_a10r_L1J100.get(m_eventInfo);
  int passHLT_j400_a10_lcw_L1J100 = Props::passHLT_j400_a10_lcw_L1J100.get(m_eventInfo);
  int passHLT_j420_a10r_L1J100 = Props::passHLT_j420_a10r_L1J100.get(m_eventInfo);
  int passHLT_j420_a10_lcw_L1J100 = Props::passHLT_j420_a10_lcw_L1J100.get(m_eventInfo);
  int passHLT_j440_a10r_L1J100 = Props::passHLT_j440_a10r_L1J100.get(m_eventInfo);
  int passHLT_j460_a10r_L1J100 = Props::passHLT_j460_a10r_L1J100.get(m_eventInfo);
  int passHLT_j460_a10_lcw_subjes_L1J100 = Props::passHLT_j460_a10_lcw_subjes_L1J100.get(m_eventInfo);
  int passHLT_j480_a10r_L1J100 = Props::passHLT_j480_a10r_L1J100.get(m_eventInfo);
  int passHLT_j480_a10_lcw_subjes_L1J100 = Props::passHLT_j480_a10_lcw_subjes_L1J100.get(m_eventInfo);

  bool pass_jet_trigger = passHLT_j360_a10r_L1J100 ||
                          passHLT_j360_a10_lcw_L1J100 ||
                          passHLT_j400_a10r_L1J100 ||
                          passHLT_j400_a10_lcw_L1J100 ||
                          passHLT_j420_a10r_L1J100 ||
                          passHLT_j420_a10_lcw_L1J100 ||
                          passHLT_j440_a10r_L1J100 ||
                          passHLT_j460_a10r_L1J100 ||
                          passHLT_j460_a10_lcw_subjes_L1J100 ||
                          passHLT_j480_a10r_L1J100 ||
                          passHLT_j480_a10_lcw_subjes_L1J100;
  if (!pass_jet_trigger) return EL::StatusCode::SUCCESS;


  fill_cutflow_boosted("Initial");
  if(!passPreSel) return EL::StatusCode::SUCCESS;
  if(!passSel) return EL::StatusCode::SUCCESS;

  if(m_writeMVATree || m_readMVA) m_tree->SetVariation(m_currentVar);
  
  ResultHHbbtautau postSelectionResult = ((HHbbtautauBoostedJetSelection*)m_eventPostSelection)->result();
  
  std::vector<const xAOD::Jet*> fatJets     = postSelectionResult.fatJets; //includes 200 GeV cut, eta <2.4, mass >50 GeV
  std::vector<const xAOD::Jet*> trackJets   = postSelectionResult.trackJets;
  std::vector<const xAOD::DiTauJet*> diTaus      = postSelectionResult.ditaus;
  const xAOD::MissingET* met                = postSelectionResult.met;

  m_isSignal = false;
  if(m_histNameSvc->getFullSample().find("hh_bbtt")!= std::string::npos) m_isSignal = true;
  if(m_histNameSvc->getFullSample().find("hh_4tau")!= std::string::npos) m_isSignal = true;
  if(m_histNameSvc->getFullSample().find("Xtohh")!= std::string::npos) m_isSignal = true;
  if(m_histNameSvc->getFullSample().find("Hhhbbtautau")!= std::string::npos) m_isSignal = true;

  if(m_debug) std::cout << "is Signal sample? " << m_isSignal << " " << m_histNameSvc->getFullSample() << std::endl;
  
  // *** met
  m_METVec.SetPtEtaPhiM(met->met(),0,met->phi(),0);
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       MET: " << m_METVec.Pt()*1e-3 << std::endl;
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       N fat jets: "<< fatJets.size()<<"; N ditaus "<<diTaus.size()<<std::endl;
  
  if(m_isMC){
    if(m_debug) std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       Truth particles object size: " <<m_truthParts->size()<<std::endl;
    xAOD::TruthParticleContainer::const_iterator thItr = m_truthParts->begin();
    xAOD::TruthParticleContainer::const_iterator thItrE = m_truthParts->end();

    std::vector<TLorentzVector> taus;
    std::vector<TLorentzVector> bis;

    TLorentzVector temp;

    for( ; thItr != thItrE; ++thItr ){
      //xAOD::TruthParticle *truthP = *thItr;
      float pt = (*thItr)->pt();
      float eta = (*thItr)->eta();
      float phi = (*thItr)->phi();
      float m   = (*thItr)->m();
      int pdgId = abs((*thItr)->pdgId());
      int status = (*thItr)->status();

      temp.SetPtEtaPhiM(pt,eta,phi,m);
      if(pdgId == 15 && status != 23) taus.push_back(temp);
      else if(pdgId == 5) bis.push_back(temp);

      if(m_debug) std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       --> pdgID: "<<(*thItr)->pdgId()<<" status: "<<(*thItr)->status()<<std::endl;
    }
  }

  m_config->getif< bool >("applyAlternativeSelection", m_applyAlternativeSelection);
  if (m_applyAlternativeSelection){

    std::vector<const xAOD::DiTauJet*> my_ditauJets;

    my_ditauJets.clear();
    for(auto ditau:diTaus){
       if((ditau->pt()*1e-3>300 && ditau->pt()*1e-3<1500) && fabs(ditau->eta())<2.0) my_ditauJets.push_back(ditau);
    }

    m_my_fatJets.clear();
    for(auto fatjet:fatJets){
       if((fatjet->pt()*1e-3>300 && fatjet->pt()*1e-3<1500) && fatjet->m()*1e-3>50 && fabs(fatjet->eta())<2.0) m_my_fatJets.push_back(fatjet);
    }

    if(my_ditauJets.size() == 0 || m_my_fatJets.size() < 2 ) return EL::StatusCode::SUCCESS;

    return applyAlternativeSelection(my_ditauJets);
  }

  if(m_METVec.Pt()/1e3 < 10.) return EL::StatusCode::SUCCESS;
  fill_cutflow_boosted("10GeVMETCut");
  
  // select ditau
  m_isFakes = false;
  m_SelDT = NULL;
  m_SelDTChargeProd = 0;
  m_SelFJ = NULL;
  m_FJNbtag = -1;
  select_DT(diTaus, 0.72);

  // select fake ditau if no good ditau was found (only in data and bkg MC)
  if (m_SelDT == NULL && !m_isSignal)
  {
    m_isFakes = true;
    m_histNameSvc->set_sample("fakes");
    select_fakeDT(diTaus, 0.4);
  }

  // return if neither good or fake ditau was found
  if (m_SelDT == NULL) return EL::StatusCode::SUCCESS;

  if (m_isMC) m_weight *= Props::effSF.get(m_SelDT);

  applyDiTauSF_boosted();
  
  // define two leading diTau subjets
  m_SelDTsubj0.SetPtEtaPhiE(Props::subjet_lead_pt.get(m_SelDT),  Props::subjet_lead_eta.get(m_SelDT), 
                            Props::subjet_lead_phi.get(m_SelDT), Props::subjet_lead_e.get(m_SelDT));
  m_SelDTsubj1.SetPtEtaPhiE(Props::subjet_subl_pt.get(m_SelDT),  Props::subjet_subl_eta.get(m_SelDT), 
                            Props::subjet_subl_phi.get(m_SelDT), Props::subjet_subl_e.get(m_SelDT));
  m_SelDTsubjets.SetPtEtaPhiE(Props::subjets_pt.get(m_SelDT),  Props::subjets_eta.get(m_SelDT), 
                              Props::subjets_phi.get(m_SelDT), Props::subjets_e.get(m_SelDT));
  // TLorentzVector DT_here = m_SelDTsubj0 + m_SelDTsubj1;
  TLorentzVector DT_here = m_SelDTsubjets;

  m_combineTruthMatchedSamples = false;
  m_config->getif<bool>("combineTruthMatchedSamples", m_combineTruthMatchedSamples);
  if (m_combineTruthMatchedSamples)
  {
    int isTruthMatch = 0;
    if (m_isMC) isTruthMatch = Props::TruthMatch.get(m_SelDT);

    if (isTruthMatch && m_histNameSvc->getFullSample() != "fakes" && !m_isSignal)
    {
      m_histNameSvc->set_sample("TruthMatchedDiTau");
    }
  }

  // trigger based leading LRJ selection
  if(fatJets.size() < 1) return EL::StatusCode::SUCCESS;

  if(postSelectionResult.trigger == eFJT360)
  {
    if(fatJets.at(0)->p4().Pt()/1e3 < 410.0) return EL::StatusCode::SUCCESS;
  }
  else if(postSelectionResult.trigger == eFJT400)
  {
    if(fatJets.at(0)->p4().Pt()/1e3 < 450.0) return EL::StatusCode::SUCCESS;
  }
  else if(postSelectionResult.trigger == eFJT420)
  {
    if(fatJets.at(0)->p4().Pt()/1e3 < 460.0) return EL::StatusCode::SUCCESS;
  }
  else if(postSelectionResult.trigger == eFJT440)
  {
    if(fatJets.at(0)->p4().Pt()/1e3 < 480.0) return EL::StatusCode::SUCCESS;
  }
  else if(postSelectionResult.trigger == eFJT460)
  {
    if(fatJets.at(0)->p4().Pt()/1e3 < 500.0) return EL::StatusCode::SUCCESS;
  }
  else
  {
    return EL::StatusCode::SUCCESS;
  }

  // select large-R jet
  // select_FJ(fatJets, "NoMcut"); // uses old fixed-radius track jet b-tagging
  select_FJ_VRbtagged(fatJets);
  if (m_SelFJ == NULL) return EL::StatusCode::SUCCESS; // only selectFatJet
  std::vector<const xAOD::Jet*> vFatJets_4b = select_FJs_4b(fatJets);
  
  // btagging track jets, assign isTagged
  std::vector<const xAOD::Jet*> vDummy;
  compute_btagging(vDummy);
  // return track jets for SF. and set nbtag properties
  std::vector<const xAOD::Jet*> vTrackJetsForSF = trackJetsForSF(m_SelFJ, vFatJets_4b, trackJets);
  // if(m_FJNbtag != 0 && m_FJNbtag != 1 && m_FJNbtag != 2) return EL::StatusCode::SUCCESS;

  // veto events with collimated vr track jets
  bool bVRJetORVeto(false);
  m_config->getif<bool>("doVRJetOR", bVRJetORVeto);
  if (bVRJetORVeto && !passVRJetOR(vTrackJetsForSF)) return EL::StatusCode::SUCCESS;

  // calculate btag SFs. trigger SF(1)
  float fBtagSF = 1.;
  float fTriggerSF = 1.;
  if (m_isMC) {
    fBtagSF = computeBTagSFWeight(vTrackJetsForSF, m_trackJetReader->getContainerName());
    m_weight *= fBtagSF;
    if ( Props::trigSF.exists(m_eventInfo) )
      fTriggerSF = Props::trigSF.get(m_eventInfo);
    m_weight *= fTriggerSF;
    if ( m_debug ) std::cout << "AnalysisReader::fill_boosted()   INFO     btag, trigger SFs: " << fBtagSF << ", " << fTriggerSF << std::endl;
  }
  vTrackJetsForSF.clear();
    
  if (m_FJNbtag == 2) applyZhfSF_boosted();

  // either calculate fake factors and return or apply fake factors (in OS/SS categories)
  if (!m_isSignal && m_calculateFF)
    EL_CHECK("AnalysisReader::fill_boosted()", calculateFF_boosted());
  if (m_calculateFF) return EL::StatusCode::SUCCESS;

  if (!m_isSignal && m_applyFF && m_isFakes && m_SelDTChargeProd == -1)
    EL_CHECK("AnalysisReader::fill_boosted()",applyFF_boosted()); // or applyFF_boosted_SignNtrackSplit

  // OS cut
  if(m_SelDTChargeProd != -1) return EL::StatusCode::SUCCESS;

  // event has passed DT and FJ selection
  fill_cutflow_boosted("SelectDTandFJ");

  if(m_debug){
    for(unsigned int ff = 0; ff < fatJets.size(); ff++){
      std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       --> FatJets: pT "<<fatJets[ff]->p4().Pt()/1e3<<" eta: "<<fatJets[ff]->p4().Eta()
               <<" phi: "<<fatJets[ff]->p4().Phi()<<" xbbResult_1tagNoMcut: "<<Props::xbbResult_1tagNoMcut.get(fatJets[ff])<<std::endl;
    }
    std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       Selected fatjet: "<<m_SelFJ->p4().Pt()/1e3<<" eta: "<<m_SelFJ->p4().Eta()
             <<" phi: "<<m_SelFJ->p4().Phi()<<" xbbResult: "<<Props::xbbResult_1tagNoMcut.get(m_SelFJ)<<std::endl;
    std::cout<<"AnalysisReader_hhbbtt::fill_boosted()  INFO       Selected di-tau: "<<m_SelDT->p4().Pt()/1e3<<" eta: "<<m_SelDT->p4().Eta()
             <<" phi: "<<m_SelDT->p4().Phi()<<" BDT score: "<<Props::BDTScore.get(m_SelDT)<<std::endl;
  }
  
  // Correcting MASS of the selected diTau
  TLorentzVector  metVec;
  metVec.SetPtEtaPhiM(met->met(), DT_here.Eta(), met->phi(), 0);
  m_CorrSelDT = DT_here + metVec;
  
  // delta phi between ditau and MET
  float dPhiDTMET = fabs(DT_here.DeltaPhi(m_METVec));

  // veto events with additional btag
  bool bAddBtagVeto(false);
  m_config->getif<bool>("additionalBtagVeto", bAddBtagVeto);
  if (bAddBtagVeto && Props::nAddBTags.get(m_SelFJ) > 0 && dPhiDTMET < 1. && (m_FJNbtag == 1||m_FJNbtag == 2)) return EL::StatusCode::SUCCESS;

  // fill plots in different regions
  // PreSel: 0/1/2-tag categories, dPhi inclusive 
  // QCD-CR: 0/1/2-tag, dPhi > 1
  // Z-CR: 0-tag, dPhi < 1
  // SR: 1/2-tag, dPhi < 1
  if(m_FJNbtag == 0) fill_cutflow_boosted("*PreSel(0tag)");
  if(m_FJNbtag == 1) fill_cutflow_boosted("*PreSel(1tag)");
  if(m_FJNbtag == 2 && (m_isMC || m_isFakes || m_isSignal)) fill_cutflow_boosted("*PreSel(2tag)");
  if (dPhiDTMET < 1.)
  {
    if(m_FJNbtag == 0) fill_cutflow_boosted("*ZCR(0tag)");
    if(m_FJNbtag == 1 && (m_isMC || m_isFakes || m_isSignal)) fill_cutflow_boosted("*SR(1tag)");
    if(m_FJNbtag == 2 && (m_isMC || m_isFakes || m_isSignal)) fill_cutflow_boosted("*SR(2tag)");    
  }
  else
  {
    if(m_FJNbtag == 0) fill_cutflow_boosted("*QCDCR(0tag)");
    if(m_FJNbtag == 1) fill_cutflow_boosted("*QCDCR(1tag)");
    if(m_FJNbtag == 2) fill_cutflow_boosted("*QCDCR(2tag)");    
  }
  fill_boostedPlots("PreSel");
  if (dPhiDTMET < 1.) {
    fill_boostedPlots("SR");
    m_region = "SR";
  } else {
    fill_boostedPlots("QCDCR");
    m_region = "QCDCR";
  }
  // tight mass window regions
  // if (m_SelFJ->p4().M()/1e3 < 70 || m_SelFJ->p4().M()/1e3 > 150 ) 
  // {
  //   // fill_boostedPlots("PreSelTightMassSideband");
  //   if (dPhiDTMET < 1.) fill_boostedPlots("SRTightMassSideband");
  //   else fill_boostedPlots("QCDCRTightMassSideband");
  // }
  // else if (m_SelFJ->p4().M()/1e3 > 70 && m_SelFJ->p4().M()/1e3 < 150 ) 
  // {
  //   // fill_boostedPlots("PreSelTightMassWindow");
  //   if (dPhiDTMET < 1.) fill_boostedPlots("SRTightMassWindow");
  //   else fill_boostedPlots("QCDCRTightMassWindow");
  // }

  if (m_writeMVATree) fill_boostedTree();
  
  return EL::StatusCode::SUCCESS;

}//end of boosted



EL::StatusCode AnalysisReader_hhbbtt::applyAlternativeSelection(std::vector<const xAOD::DiTauJet*> my_ditauJets)
{
    // Truth matching function for optimization studies
  //if(m_isMC) fillTruthEffPlots(my_ditauJets);
  std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap;

  // Set description to "ALL" events in CxAOD
  m_histNameSvc->set_description("All");

  // Find number of BTagged track jets in fat jets
  findNBTagTJ();

  // Fill fatjet plots and variables
  fillFatJetPlots();

  // Fill ditaujet plots and variables
  fillDiTauJetPlots(my_ditauJets);

  // Fill fatJet/diTauJet dRMap
  dRMap = fillDRMap(my_ditauJets);

  // Get selected DiTauJet
  getSelDT(my_ditauJets);
  // Get selected FatJet
  getSelFJ(dRMap);

  //if(m_nBTagTJ==-1) std::cout << "m_nBTagTJ==-1" << std::endl;
  // If not selected FJ and DiTau are found return
  if(m_MatchFJets.empty()) return EL::StatusCode::SUCCESS;
  if(m_SelFJ == NULL || m_SelDT == NULL || m_nBTagTJ == -1) return EL::StatusCode::SUCCESS;

  //if (m_isMC) m_weight *= Props::effSF.get(m_SelDT);

  if(m_writeMVATree){
     m_tree->Reset();
     m_tree->SetVariation(m_currentVar);
  }


  // Fill full system variables
  fillFullSystemVars(dRMap);
  fillFullSystemPlots();

  m_histNameSvc->set_nTag(m_nBTagTJ);

  bool passSideBand= applySideBandSelection();
  bool passPreSel  = applyPreSelection();
  bool passSR      = applySRSelection();
  bool passQCDCR   = applyQCDSelection();
  bool passTTBarCR = applyTTBarSelection();

  if(passSR) fillSRPlots();
  else if(passQCDCR) fillQCDCRPlots();
  else if(passSideBand) fillSideBandPlots();
  else if(passTTBarCR) fillTTBarCRPlots();
  else if(passPreSel) std::cout << "Event passed pre-selection" << std::endl;

  if(m_writeMVATree) m_tree->Fill();

  //if((passSR && passQCDCR) || (passSR && passTTBarCR) || (passQCDCR && passTTBarCR)) std::cout << "Warning SR and background CRs are not orthogonal" << std::endl;

  return EL::StatusCode::SUCCESS;

}

void AnalysisReader_hhbbtt::findNBTagTJ()
{

  int nJet=0;
  m_have1BTagTJ.clear();m_have2BTagTJ.clear();

  for (auto jet: m_my_fatJets) {

    //Dummy function at the mmoment
    std::cout << "Fat-Jet: (pt " << jet->p4().Pt() << " , eta: " << jet->p4().Eta() << " )" <<std::endl;

    /*if(Props::xbbResult_1tagNoMcut.get(jet)==BoostedXbbTagger::InvalidJet) m_have1BTagTJ.push_back(-1);
    else if(Props::xbbResult_1tagNoMcut.get(jet)==BoostedXbbTagger::MassPassBTagPassJSSPass) m_have1BTagTJ.push_back(1);
    else if(Props::xbbResult_1tagNoMcut.get(jet)==BoostedXbbTagger::MassPassBTagFailJSSPass) m_have1BTagTJ.push_back(0);
    else m_have1BTagTJ.push_back(0);

    if(Props::xbbResult_2tagNoMcut.get(jet)==BoostedXbbTagger::InvalidJet) m_have2BTagTJ.push_back(-1);
    else if(Props::xbbResult_2tagNoMcut.get(jet)==BoostedXbbTagger::MassPassBTagPassJSSPass) m_have2BTagTJ.push_back(1);
    else if(Props::xbbResult_2tagNoMcut.get(jet)==BoostedXbbTagger::MassPassBTagFailJSSPass) m_have2BTagTJ.push_back(0);
    else m_have2BTagTJ.push_back(0);*/

    /*std::cout << "Value: " << Props::xbbResult_2tagNoMcut.get(jet) << " " << Props::xbbResult_1tagNoMcut.get(jet) << std::endl;
    std::cout << "Value: " << m_have1BTagTJ.at(nJet) << " " << m_have2BTagTJ.at(nJet) << std::endl;

    if(Props::xbbResult_2tagNoMcut.get(jet)!=BoostedXbbTagger::MassPassBTagPassJSSPass) std::cout << "XbbResult 2: " << Props::xbbResult_2tagNoMcut.get(jet) << " Mass " << jet->p4().M()*1e-3 << std::endl;
    if(Props::xbbResult_1tagNoMcut.get(jet)!=BoostedXbbTagger::MassPassBTagPassJSSPass) std::cout << "XbbResult 1: " << Props::xbbResult_1tagNoMcut.get(jet) << " Mass " << jet->p4().M()*1e-3 << std::endl;

    if(Props::xbbResult_2tagNoMcut.get(jet)==BoostedXbbTagger::BTagPass || Props::xbbResult_1tagNoMcut.get(jet)==BoostedXbbTagger::BTagPass) std::cout << "BTag pass: " << std::endl;*/

    nJet++;
  }

  //std::cout <<"Exiting findNMTagTJ function" << std::endl;

  return;

}

void AnalysisReader_hhbbtt::fillFatJetPlots()
{

  auto nJet=0;
  m_TaggedFJets.clear();
  m_NonTaggedFJets.clear();

  std::vector<const xAOD::Jet*> trkJets;
  for (auto jet: m_my_fatJets) {
    trkJets.clear();
    m_histSvc -> BookFillHist("fatJet_pt",   100, 0, 2000, jet->p4().Pt()*1e-3, m_weight);
    m_histSvc -> BookFillHist("fatJet_eta",   50, -3, 3, jet->p4().Eta(), m_weight);
    m_histSvc -> BookFillHist("fatJet_phi",   50, -4, 4, jet->p4().Phi(), m_weight);
    m_histSvc -> BookFillHist("fatJet_m",   100, 0, 2000, jet->p4().M()*1e-3, m_weight);

    m_histSvc -> BookFillHist("XbbResult_1tagNoMcut", 11, -1, 10, Props::xbbResult_1tagNoMcut.get(jet), m_weight);
    m_histSvc -> BookFillHist("XbbResult_1tag68Mcut", 11, -1, 10, Props::xbbResult_1tag68Mcut.get(jet), m_weight);
    m_histSvc -> BookFillHist("XbbResult_1tag90Mcut", 11, -1, 10, Props::xbbResult_1tag90Mcut.get(jet), m_weight);
    m_histSvc -> BookFillHist("XbbResult_2tagNoMcut", 11, -1, 10, Props::xbbResult_2tagNoMcut.get(jet), m_weight);
    m_histSvc -> BookFillHist("XbbResult_2tag68Mcut", 11, -1, 10, Props::xbbResult_2tag68Mcut.get(jet), m_weight);
    m_histSvc -> BookFillHist("XbbResult_2tag90Mcut", 11, -1, 10, Props::xbbResult_2tag90Mcut.get(jet), m_weight);

    /*if(m_have1BTagTJ.at(nJet) == 1 || m_have2BTagTJ.at(nJet) == 1) m_TaggedFJets.push_back(jet);
    else if(m_have1BTagTJ.at(nJet) < 0 && m_have2BTagTJ.at(nJet) < 0) m_NonTaggedFJets.push_back(jet);*/

    if (jet->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", trkJets)) {
      int ntrkjets = 0;
      for (const xAOD::Jet *trkJ : trkJets) {
        if (!trkJ) continue; // if the trackjet is not valid then skip it
        if (!(trkJ->pt() / 1000. > 10. && fabs(trkJ->eta()) < 2.5)) continue;
        ntrkjets++;
        m_histSvc->BookFillHist("trkJets_pt", 400, 0, 2000, trkJ->pt()/1e3, m_weight);
        m_histSvc->BookFillHist("trkJets_MV2c10", 200, -1, 1, Props::MV2c10.get(trkJ), m_weight);
        m_histSvc->BookFillHist("trkJets_2d", 400, 0, 2000, 200, -1, 1, trkJ->pt()/1e3, Props::MV2c10.get(trkJ), 1.);
      }
      m_histSvc->BookFillHist("trkJets_n", 10, 0, 10, ntrkjets, m_weight);
    }

    nJet++;

  }


}

void AnalysisReader_hhbbtt::fillDiTauJetPlots(std::vector<const xAOD::DiTauJet*> my_ditauJets)
{

  TLorentzVector lead_subjet, sublead_subjet, ditau_subjets, ditau_subjets_met;

  for (auto ditau: my_ditauJets){
    if(!ditau || ditau==NULL) return;
    if(!Props::BDTScore.exists(ditau)) continue;
    m_histSvc -> BookFillHist("diTauJet_bdtScore", 20, 0, 1, Props::BDTScore.get(ditau), m_weight);
    m_histSvc -> BookFillHist("subjet_lead_pt", 100, 0, 2000, Props::subjet_lead_pt.get(ditau)*1e-3,m_weight);
    m_histSvc -> BookFillHist("subjet_lead_eta", 50, -4, 4, Props::subjet_lead_eta.get(ditau), m_weight);
    m_histSvc -> BookFillHist("subjet_lead_phi", 50, -4, 4, Props::subjet_lead_phi.get(ditau),m_weight);
    m_histSvc -> BookFillHist("subjet_lead_e", 100, 0, 2000, Props::subjet_lead_e.get(ditau)*1e-3,m_weight);
    m_histSvc -> BookFillHist("subjet_subl_pt", 100, 0, 2000, Props::subjet_subl_pt.get(ditau)*1e-3,m_weight);
    m_histSvc -> BookFillHist("subjet_subl_eta", 50, -4, 4, Props::subjet_subl_eta.get(ditau), m_weight);
    m_histSvc -> BookFillHist("subjet_subl_phi", 50, -4, 4, Props::subjet_subl_phi.get(ditau),m_weight);
    m_histSvc -> BookFillHist("subjet_subl_e", 100, 0, 2000, Props::subjet_subl_e.get(ditau)*1e-3,m_weight);
    m_histSvc -> BookFillHist("n_subjets", 10, 0, 10, Props::n_subjets.get(ditau),m_weight);
    m_histSvc -> BookFillHist("diTauJet_pt",   100, 0, 2000, ditau->p4().Pt()*1e-3, m_weight);
    m_histSvc -> BookFillHist("diTauJet_eta",   50, -4, 4, ditau->p4().Eta(), m_weight);
    m_histSvc -> BookFillHist("diTauJet_phi",   50, -4, 4, ditau->p4().Phi(), m_weight);
    m_histSvc -> BookFillHist("diTauJet_m",   100, 0, 2000, ditau->p4().M()*1e-3, m_weight);
    if(Props::n_subjets.get(ditau)>1){
      lead_subjet.SetPtEtaPhiE(Props::subjet_lead_pt.get(ditau),Props::subjet_lead_eta.get(ditau),Props::subjet_lead_phi.get(ditau),Props::subjet_lead_e.get(ditau));
      sublead_subjet.SetPtEtaPhiE(Props::subjet_subl_pt.get(ditau),Props::subjet_subl_eta.get(ditau),Props::subjet_subl_phi.get(ditau),Props::subjet_subl_e.get(ditau));
      ditau_subjets = lead_subjet+sublead_subjet;
      m_histSvc -> BookFillHist("diTauJet_subJets_M",100,0,2000,ditau_subjets.M()*1e-3,m_weight);
      ditau_subjets_met = lead_subjet+sublead_subjet+m_METVec;
      m_histSvc -> BookFillHist("diTauJet_subJetsMET_M",100,0,2000,ditau_subjets_met.M()*1e-3,m_weight);
    }
  }

}


std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> AnalysisReader_hhbbtt::fillDRMap(std::vector<const xAOD::DiTauJet*> my_ditauJets)
{

  std::pair <const xAOD::Jet*, const xAOD::DiTauJet*> par;
  std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap;
  dRMap.clear();
  //std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> m_dRMap;
  for (auto ditau: my_ditauJets) {
    if(!ditau || ditau==NULL) continue;
    for (auto fatjet: m_my_fatJets){
      if(!fatjet || fatjet==NULL) continue;
       par.first = fatjet; par.second = ditau;
       float dR = ditau->p4().DeltaR(fatjet->p4());
       m_histSvc->BookFillHist("deltaR", 100, 0, 5, dR, m_weight);
       dRMap[par] = dR;
    }
  }

  return dRMap;
}

void AnalysisReader_hhbbtt::getSelDT(std::vector<const xAOD::DiTauJet*> my_ditauJets)
{

  std::vector<float> BDTScore;
  int Ns = 0;
  for (auto ditau: my_ditauJets){
    if(Props::BDTScore.exists(ditau)) BDTScore.push_back(Props::BDTScore.get(ditau));
    Ns++;
  }

  m_histSvc->BookFillHist("Nditaus", 5, 0, 5, Ns, m_weight);

  if(BDTScore.size()==0){
    m_SelDT=NULL;
    return;
  }

  float ditaujet_bdtMax = *max_element(BDTScore.begin(), BDTScore.end());
  if(m_debug) std::cout << "bdt_max: " << ditaujet_bdtMax << std::endl;

  int index=-1, j=0;
  for (auto ditau: my_ditauJets){
    if(!Props::BDTScore.exists(ditau)) continue;
    if(ditaujet_bdtMax==Props::BDTScore.get(ditau)) index=j; // Select the diTAu object if it has the Max BDT score!!
    j++;
  }

  // Select the ditaujet with the higher BDT score
  if(index>-1) m_SelDT = my_ditauJets.at(index);
  else m_SelDT=NULL;

  if(m_debug) std::cout << "index: " << index << std::endl;
}

void AnalysisReader_hhbbtt::getSelFJ(std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap)
{
  std::pair <const xAOD::Jet*, const xAOD::DiTauJet*> par;
  par.second = m_SelDT;
  m_SelFJets.clear();
  m_MatchFJets.clear();
  int Ns=0;
  for (auto fatjet: m_my_fatJets){
     par.first = fatjet;
     float dR=dRMap[par];
     if(m_debug) std::cout << "dR(FJ,DTJ): " <<dR<<std::endl; 
     if(dR>0.5) m_SelFJets.push_back(fatjet);
     else m_MatchFJets.push_back(fatjet);
     Ns++;
  }

  m_histSvc->BookFillHist("Nfjets", 5, 0, 5, Ns, m_weight);

  if(m_SelFJets.size()==1) m_SelFJ=m_SelFJets.at(0);
  else if(m_SelFJets.size()>1){
    if(m_debug) std::cout << "more than one FJ found taking the leading one for now..." << std::endl;
    m_SelFJ=m_SelFJets.at(0);
  }else if(m_SelFJets.size()==0){
    m_SelFJ=NULL;
    return;
  }

  if(m_TaggedFJets.size()==0 && m_NonTaggedFJets.size()==0){
    m_nBTagTJ = 0;
    return;
  }else if(m_TaggedFJets.size()==0 && m_NonTaggedFJets.size()!=0){
    m_nBTagTJ = -1;
    return;
  }

  if(m_debug){
  std::cout << "@ tagged fjet vector size " <<m_TaggedFJets.size()<< std::endl; 
  std::cout << "@ tagged fjet vector size " <<m_have1BTagTJ.size()<< " " << m_have1BTagTJ.at(0) << std::endl; 
  std::cout << "@ tagged fjet vector size " <<m_have2BTagTJ.size()<< " " << m_have2BTagTJ.at(0) << std::endl;
  }

  int ijet=0, index=-1;
  for(auto fjet: m_my_fatJets){
    //std::cout << m_have1BTagTJ.at(ijet) << " " << m_have2BTagTJ.at(ijet) << std::endl;
    if((m_have1BTagTJ.at(ijet) == 1 || m_have2BTagTJ.at(ijet) == 1) && fjet == m_SelFJ) index = ijet;
    ijet++;
  }

  if(index == -1){
     m_nBTagTJ = 0;
     return;
  }else if(index>=0){
     // This is the number of B-tagged track jets inside a fat jet
     if(m_have2BTagTJ.at(index)==1) m_nBTagTJ = 2;
     else if(m_have1BTagTJ.at(index)==1) m_nBTagTJ = 1;
  }


}

bool AnalysisReader_hhbbtt::applyPreSelection()
{

  /*if(Props::n_subjets.get(m_SelDT) == 0) return false;  
  //m_histNameSvc->set_description("PreSel_PostDiTauJetPtCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 1 " << m_SelDT->p4().Pt() << std::endl;

  if(m_SelDT->p4().Pt()*1e-3<450) return false;
  //m_histNameSvc->set_description("PreSel_PostDiTauJetPtCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 2 " << m_SelDT->p4().Pt() << std::endl;

  if(m_SelFJ->p4().Pt()*1e-3<600) return false;

  //m_histNameSvc->set_description("PreSel_PostFatJetPtCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 3 " << m_SelFJ->p4().Pt() << std::endl;*/

  if(m_dR<1.0) return false;

  m_histNameSvc->set_description("PreSel_PostDRCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 4 " << m_dR << std::endl;

  if(m_dEta>1.7) return false;

  m_histNameSvc->set_description("PreSel_PostDEtaCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 4 " << m_dR << std::endl;

  return true;
}

bool AnalysisReader_hhbbtt::applySRSelection()
{

  if(fabs(m_dPhi)>TMath::Pi()/2) return false;
  //m_histNameSvc->set_description("SR_PostDPhiCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applySRSelection 1 " << m_dPhi << std::endl;

  if(Props::BDTScore.get(m_SelDT)<0.6) return false;
  //m_histNameSvc->set_description("SR_PostDiTauBDTScoreCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applySRSelection 2 " << Props::BDTScore.get(m_SelDT) << std::endl;

  if(m_nBTagTJ == 0) return false;
  m_histNameSvc->set_description("SR_PostnBTagTJCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applySRSelection 3 " << m_nBTagTJ << std::endl;

  if(Props::n_subjets.get(m_SelDT) > 4 ) return false;

  m_histNameSvc->set_description("SR_PostNSubJetsCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 5 " << Props::n_subjets.get(m_SelDT) << std::endl;

  if(m_Xhh2 > 1 ) return false;

  m_histNameSvc->set_description("SR_PostEllipseCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 6 " << m_Xhh2 << std::endl;


  return true;

}

bool AnalysisReader_hhbbtt::applyQCDSelection()
{

  //if(fabs(m_dPhi)>TMath::Pi()/2) return false;
  //m_histNameSvc->set_description("QCDCR_PostDPhiCut");
  //fillFullSystemPlots();

  //if(m_debug) std::cout << "@ applyQCDCRSelection 1 " << m_dPhi << std::endl;

  if(Props::BDTScore.get(m_SelDT)>=0.6) return false;
  //m_histNameSvc->set_description("QCDCR_PostDiTauBDTScoreCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyQCDCRSelection 2 " << Props::BDTScore.get(m_SelDT) << std::endl;

  //if(m_nBTagTJ > 0) return false;
  //m_histNameSvc->set_description("QCDCR_PostnBTagTJCut");
  //fillFullSystemPlots();

  //if(m_debug) std::cout << "@ applyQCDCRSelection 3 " << m_nBTagTJ << std::endl;

  /*if(m_ElCutX <= 85){
    m_histNameSvc->set_description("QCDCR_PostEllipseXCut");
    fillFullSystemPlots();
    if(m_debug) std::cout << "@ applyQCDCRSelection 4 " << m_ElCutX << std::endl;
  }


  if(m_ElCutY <= 85){
    m_histNameSvc->set_description("QCDCR_PostEllipseYCut");
    fillFullSystemPlots();
    if(m_debug) std::cout << "@ applyQCDCRSelection 5 " << m_ElCutY << std::endl;
  }*/

  if(m_Xhh2 <= 1 || m_Xhh3 > 1 ) return false;

  m_histNameSvc->set_description("QCDCR_PostEllipseCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyPreSelection 6 " << m_Xhh2 << std::endl;

  return true;
}


bool AnalysisReader_hhbbtt::applySideBandSelection()
{

  if(m_debug) std::cout << "@ applySideBandSelection 2 " << Props::BDTScore.get(m_SelDT) << std::endl;

  if(m_Xhh3 <= 1 || m_Xhh4 > 1 ) return false;

  if(m_applyBoostedFF){

    m_histNameSvc->set_sample("MultiJet");
    m_histNameSvc->set_description("SR_PostEllipseCut");

    float temp_weight=m_weight;

    if(m_isMC) m_weight *= -1;
    else { 
     if(m_FJNbtag==1) m_weight *= m_weightFactor1;
     else if(m_FJNbtag==2) m_weight *= m_weightFactor2;
     else if(m_FJNbtag==0) m_weight = temp_weight;
    }
    fillFullSystemPlots();

    m_weight = temp_weight;

    /*m_histNameSvc->set_nTag(2);
    if(m_isMC) m_weight *= -1;
    else m_weight *= m_weightFactor2;
    fillFullSystemPlots();

    m_weight = temp_weight;*/

  }

  if(Props::BDTScore.get(m_SelDT)<0.6) {
    m_histNameSvc->set_description("SBAnti2Tau_PostEllipseCut");
    if(!m_applyBoostedFF) fillFullSystemPlots();
  }else if(Props::BDTScore.get(m_SelDT)>=0.6){
    m_histNameSvc->set_description("SBMedium2Tau_PostEllipseCut");
    if(!m_applyBoostedFF) fillFullSystemPlots();
  }

  if(m_debug) std::cout << "@ applySideBandSelection 6 " << m_Xhh2 << std::endl;

  return true;
}


void AnalysisReader_hhbbtt::fillFullSystemVars(std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap)
{

  // DPhi between selected DiTauJet and MET
  m_dPhi = m_SelDT->p4().DeltaPhi(m_METVec);
  m_dPhiFJ = m_SelFJ->p4().DeltaPhi(m_METVec);
  
  m_dEta = fabs(m_SelDT->p4().Eta()-m_SelFJ->p4().Eta());

  m_dR   = m_SelDT->p4().DeltaR(m_SelFJ->p4());

  if(m_debug) std::cout << "DR is: "<< m_dR << std::endl;
  if(m_writeMVATree) m_tree->boostedDR=m_dR;

  TLorentzVector corrDTau;
  m_corrDTau = m_SelDT->p4()+m_METVec;
  m_corrDTau2 = m_SelDT->p4()+m_METVec;

  m_fullMass = (m_SelDT->p4()+m_SelFJ->p4()).M();

  std::pair <const xAOD::Jet*, const xAOD::DiTauJet*> par;
  par.second = m_SelDT;
  std::cout << "Point 1 " << m_my_fatJets.size() << std::endl;
  for(auto fatjet: m_my_fatJets){
     par.first = fatjet;
     float dR=dRMap[par];
     std::cout << "dR: " << dR << std::endl;
     if(dR<0.5) m_SelFJ2 = fatjet;
  }
  std::cout << "Point 2" << std::endl;

  if(m_debug) std::cout << "Match FJets vector size: " << m_MatchFJets.size() << std::endl;
  if(!m_MatchFJets.empty()){
     m_SelFJ2 = m_MatchFJets.at(0);

    const xAOD::Jet* LeadFJ;
    const xAOD::Jet* SubLFJ;

    if(m_SelFJ->pt()>m_SelFJ2->pt()){
      LeadFJ = m_SelFJ;
      SubLFJ = m_SelFJ2;
    }else{
      LeadFJ = m_SelFJ2;
      SubLFJ = m_SelFJ;
    }

    float mLeadFJ = LeadFJ->m()*1e-3;
    float mSubLFJ = SubLFJ->m()*1e-3;

    float C = (mLeadFJ-120)/20;
    float D = (mSubLFJ-100)/25;

    float E = (mLeadFJ-120)/30;
    float F = (mSubLFJ-100)/35;

    float G = (mLeadFJ-120)/40;
    float H = (mSubLFJ-100)/45;

    m_Xhh2 = sqrt(C*C+D*D);
    m_Xhh3 = sqrt(E*E+F*F);
    m_Xhh4 = sqrt(G*G+H*H);

    m_MCut2 = sqrt(pow(mLeadFJ-124,2)+pow(mSubLFJ-115,2));
  }


  float mFJ = m_SelFJ->m()*1e-3;
  float mDT = m_SelDT->m()*1e-3;

  float A = (mFJ-124)/(0.1*mFJ);
  float B = (mDT-80)/(0.5*mDT);

  m_Xhh  = sqrt(A*A+B*B);

  m_MCut = sqrt(pow(mFJ-124,2)+pow(mDT-80,2));

  m_MTW_Max = sqrt( mTsqr(m_SelDT->p4(),m_METVec) );
  m_MTW_Clos = sqrt( mTsqr(m_SelFJ->p4(),m_METVec) );

  m_ElCutX = sqrt(pow(m_MTW_Max-255,2)+pow(m_MTW_Clos,2));
  m_ElCutY = sqrt(pow(m_MTW_Max,2)+pow(m_MTW_Clos-255,2));

  if(m_debug) std::cout << "Sel DiTau M: " << m_SelDT->p4().M() << " Corr DiTauM: " << m_corrDTau.M() << std::endl;

}


void AnalysisReader_hhbbtt::fillFullSystemPlots()
{

  m_histSvc->BookFillHist("diTauMassvsfjJetMass", 100, 0, 500, 100, 0, 500, m_SelDT->p4().M()*1e-3, m_SelFJ->p4().M()*1e-3, m_weight);
  m_histSvc->BookFillHist("CorrDiTauMassvsfjJetMass", 100, 0, 500, 100, 0, 500, m_corrDTau.M()*1e-3, m_SelFJ->p4().M()*1e-3, m_weight);
  m_histSvc->BookFillHist("CorrDiTauMassvsfjJetMass", 100, 0, 500, 100, 0, 500, m_corrDTau.M()*1e-3, m_SelFJ->p4().M()*1e-3, m_weight);
  m_histSvc->BookFillHist("dPhidTauMET", 100, -5, 5, m_dPhi,m_weight);
  m_histSvc->BookFillHist("dPhifJetMET", 100, -5, 5, m_dPhiFJ,m_weight);
  m_histSvc->BookFillHist("dPhidTauMETdPhifJetMET", 100, -5, 5, 100, -5, 5, m_dPhi, m_dPhiFJ, m_weight);
  m_histSvc->BookFillHist("dEta",100,0,5,m_dEta,m_weight);
  m_histSvc->BookFillHist("MET", 100, 0, 1500, m_METVec.Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("nBTagTJ", 4, 0, 4, m_nBTagTJ, m_weight);
  m_histSvc->BookFillHist("FullXMass",100,200,8000,m_fullMass*1e-3,m_weight);
  m_histSvc->BookFillHist("deltaR", 100, 0, 5, m_dR, m_weight);
  m_histSvc->BookFillHist("Xhh",40,0,20,m_Xhh,m_weight);
  m_histSvc->BookFillHist("MCut",100,0,200,m_MCut,m_weight);
  m_histSvc->BookFillHist("MTW_Max",100,0,2000,m_MTW_Max,m_weight);
  m_histSvc->BookFillHist("MTW_Clos",100,0,2000,m_MTW_Clos,m_weight);
  m_histSvc->BookFillHist("MTW2D", 400, 0, 2000, 400, 0, 2000, m_MTW_Clos, m_MTW_Max, m_weight);
  m_histSvc->BookFillHist("SelfatJet_pt",   100, 500, 3000, m_SelFJ->p4().Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("SelfatJet_eta",   50, -3, 3, m_SelFJ->p4().Eta(), m_weight);
  m_histSvc->BookFillHist("SelfatJet_phi",   50, -4, 4, m_SelFJ->p4().Phi(), m_weight);
  m_histSvc->BookFillHist("SelfatJet_m",   100, 0, 2000, m_SelFJ->p4().M()*1e-3, m_weight);
  m_histSvc->BookFillHist("SelditauJet_pt",   100, 400, 3000, m_SelDT->p4().Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("SelditauJet_eta",   50, -3, 3, m_SelDT->p4().Eta(), m_weight);
  m_histSvc->BookFillHist("SelditauJet_phi",   50, -4, 4, m_SelDT->p4().Phi(), m_weight);
  m_histSvc->BookFillHist("SelditauJet_m",   50, 0, 2000, m_SelDT->p4().M()*1e-3, m_weight);
  m_histSvc->BookFillHist("SeldiTauJet_bdtScore", 20, 0, 1, Props::BDTScore.get(m_SelDT), m_weight);
  m_histSvc->BookFillHist("Selsubjet_lead_pt", 100, 0, 2000, Props::subjet_lead_pt.get(m_SelDT)*1e-3,m_weight);
  m_histSvc->BookFillHist("Selsubjet_lead_eta", 50, -4, 4, Props::subjet_lead_eta.get(m_SelDT), m_weight);
  m_histSvc->BookFillHist("Selsubjet_lead_phi", 50, -4, 4, Props::subjet_lead_phi.get(m_SelDT),m_weight);
  m_histSvc->BookFillHist("Selsubjet_lead_e", 100, 0, 2000, Props::subjet_lead_e.get(m_SelDT)*1e-3,m_weight);
  m_histSvc->BookFillHist("Selsubjet_subl_pt", 100, 0, 2000, Props::subjet_subl_pt.get(m_SelDT)*1e-3,m_weight);
  m_histSvc->BookFillHist("Selsubjet_subl_eta", 50, -4, 4, Props::subjet_subl_eta.get(m_SelDT), m_weight);
  m_histSvc->BookFillHist("Selsubjet_subl_phi", 50, -4, 4, Props::subjet_subl_phi.get(m_SelDT),m_weight);
  m_histSvc->BookFillHist("Selsubjet_subl_e", 100, 0, 2000, Props::subjet_subl_e.get(m_SelDT)*1e-3,m_weight);
  m_histSvc->BookFillHist("Seln_subjets", 10, 0, 10, Props::n_subjets.get(m_SelDT),m_weight);

  TLorentzVector lead_subjet, sublead_subjet, ditau_subjets, ditau_subjets_met;
  if(Props::n_subjets.get(m_SelDT) >1){
     lead_subjet.SetPtEtaPhiE(Props::subjet_lead_pt.get(m_SelDT),Props::subjet_lead_eta.get(m_SelDT),Props::subjet_lead_phi.get(m_SelDT),Props::subjet_lead_e.get(m_SelDT));
     sublead_subjet.SetPtEtaPhiE(Props::subjet_subl_pt.get(m_SelDT),Props::subjet_subl_eta.get(m_SelDT),Props::subjet_subl_phi.get(m_SelDT),Props::subjet_subl_e.get(m_SelDT));
     ditau_subjets = lead_subjet+sublead_subjet;
     m_histSvc -> BookFillHist("SeldiTauJet_subJets_M",100,0,2000,ditau_subjets.M()*1e-3,m_weight);
     ditau_subjets_met = lead_subjet+sublead_subjet+m_METVec;
     m_histSvc -> BookFillHist("SeldiTauJet_subJetsMET_M",100,0,2000,ditau_subjets_met.M()*1e-3,m_weight);
     m_histSvc -> BookFillHist("DiTauSubJMassvsfjJetMass", 100, 0, 500, 100, 0, 500, ditau_subjets.M()*1e-3, m_SelFJ->p4().M()*1e-3, m_weight);
  }

  std::vector<const xAOD::Jet*> trkJets, seltrkJets;
  if (m_SelFJ->getAssociatedObjects<xAOD::Jet>("GhostAntiKt2TrackJet", trkJets)) {
    for (const xAOD::Jet *trkJ : trkJets) {
      if (!trkJ) continue; // if the trackjet is not valid then skip it
      if (!(trkJ->pt() / 1000. > 10. && fabs(trkJ->eta()) < 2.5)) continue;
      seltrkJets.push_back(trkJ);
    }
  }
  m_nTJ=seltrkJets.size();
  m_histSvc->BookFillHist("nTJ", 10, 0, 10, seltrkJets.size(), m_weight);
  for(auto jet:seltrkJets){
     m_histSvc->BookFillHist("Seltj_pt", 100, 0, 2000, jet->pt()*1e-3,m_weight);
     m_histSvc->BookFillHist("Seltj_eta", 50, -4, 4, jet->eta(), m_weight);
     m_histSvc->BookFillHist("Seltj_phi", 50, -4, 4, jet->phi(),m_weight);
     m_histSvc->BookFillHist("Seltj_e", 100, 0, 2000, jet->e()*1e-3,m_weight);
     m_histSvc->BookFillHist("Seltj_MV2c10", 100, -1, 1, Props::MV2c10.get(jet),m_weight);
  }

  if(!m_MatchFJets.empty()){
    const xAOD::Jet* LeadFJ;
    const xAOD::Jet* SubLFJ;

    if(m_SelFJ->pt()>m_SelFJ2->pt()){
      LeadFJ = m_SelFJ;
      SubLFJ = m_SelFJ2;
    }else{
      LeadFJ = m_SelFJ2;
      SubLFJ = m_SelFJ;
    }

    float mLeadFJ = LeadFJ->m()*1e-3;
    float mSubLFJ = SubLFJ->m()*1e-3;

    m_histSvc->BookFillHist("FatJetMasses2DXhh",100,0,250,100,0,250,mLeadFJ,mSubLFJ,m_Xhh2);
    m_histSvc->BookFillHist("FatJetMasses2DMCut",100,0,250,100,0,250,mLeadFJ,mSubLFJ,m_MCut2);
    m_histSvc->BookFillHist("FatJetMasses2D",100,0,250,100,0,250,mLeadFJ,mSubLFJ,m_weight);

    m_histSvc->BookFillHist("Xhh2",40,0,20,m_Xhh2,m_weight);
    m_histSvc->BookFillHist("MCut2",100,0,200,m_MCut2,m_weight);
  }

  m_histSvc->BookFillHist("FJDT2DMassXhh",100,0,250,100,0,250,m_SelDT->m()*1e-3,m_SelFJ->m()*1e-3,m_Xhh);
  m_histSvc->BookFillHist("FJDT2DMassCut",100,0,250,100,0,250,m_SelDT->m()*1e-3,m_SelFJ->m()*1e-3,m_MCut);
  m_histSvc->BookFillHist("FJDT2D",100,0,250,100,0,250,m_SelDT->m()*1e-3,m_SelFJ->m()*1e-3,m_weight);

  //float 
  //m_histSvc->BookFillHist("FatJetMasses2D2",100,0,250,100,0,250,m_)

}

bool AnalysisReader_hhbbtt::applyTTBarSelection()
{

  //if(fabs(m_dPhi)<TMath::Pi()/2) return false;
  //m_histNameSvc->set_description("TTBarCR_PostDPhiCut");
  //fillFullSystemPlots();

  //if(m_debug) std::cout << "@ applyTTBarCRSelection 1 " << m_dPhi << std::endl;

  if(Props::BDTScore.get(m_SelDT)>=0.6) return false;
  //m_histNameSvc->set_description("QCDCR_PostDiTauBDTScoreCut");
  //fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyTTBarCRSelection 1 " << Props::BDTScore.get(m_SelDT) << std::endl;

  if(m_nBTagTJ == 0) return false;

  m_histNameSvc->set_description("TTBarCR_PostnBTagTJCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyTTBarCRSelection 2 " << m_nBTagTJ << std::endl;

  if(m_SelFJ->p4().M()*1e-3 < 140.0 || m_SelFJ->p4().M()*1e-3 > 200.0) return false;
  if(m_SelDT->p4().M()*1e-3 < 120.0 || m_SelDT->p4().M()*1e-3 > 220.0) return false;

  m_histNameSvc->set_description("TTBarCR_Post2DSquareMCut");
  fillFullSystemPlots();

  if(m_debug) std::cout << "@ applyTTBarCRSelection 3 " << m_SelFJ->p4().M()*1e-3 << " " << m_SelDT->p4().M()*1e-3 << std::endl;

  return true;
}

void AnalysisReader_hhbbtt::fillSRPlots()
{
}

void AnalysisReader_hhbbtt::fillQCDCRPlots()
{
}

void AnalysisReader_hhbbtt::fillSideBandPlots()
{
}

void AnalysisReader_hhbbtt::fillTTBarCRPlots()
{
}




void AnalysisReader_hhbbtt::fill_xHistos(std::vector<const xAOD::Jet*> jets, std::vector<const xAOD::TauJet*> taus)
{

  if(m_debug) std::cout << "On fill_xHistos function" << std::endl;
  std::vector<TLorentzVector> leadjets;
  leadjets.push_back(jets.at(0)->p4());leadjets.push_back(jets.at(1)->p4());

  m_bb=jets.at(0)->p4()+jets.at(1)->p4();
  TLorentzVector bb2 = m_bb;
  TLorentzVector tautau=taus.at(0)->p4()+taus.at(1)->p4();

  TLorentzVector X=m_bb+tautau;
  m_Xmmc=m_bb+m_MMCVec*1e3;

  //if(fabs(m_MMCVec.M()*1e-3-125)<25 && !m_isMC) std::cout << "m_MMCVec.M() : " << m_MMCVec.M() << " Xmmc.M() : " << Xmmc.M() << std::endl;

  m_deltaRJJ=jets.at(0)->p4().DeltaR(jets.at(1)->p4());
  m_deltaPhiJJ=jets.at(0)->p4().DeltaPhi(jets.at(1)->p4());
  m_deltaEtaJJ=jets.at(0)->eta()-jets.at(1)->eta();

  m_deltaRTT=taus.at(0)->p4().DeltaR(taus.at(1)->p4());
  m_deltaPhiTT=taus.at(0)->p4().DeltaPhi(taus.at(1)->p4());
  m_deltaEtaTT=taus.at(0)->eta()-taus.at(1)->eta();

  m_MTW_Max = sqrt( mTsqr(taus.at(0)->p4(),m_METVec) );
  m_mTtot = sqrt( mTsqr(taus.at(0)->p4(),taus.at(1)->p4()) + mTsqr(taus.at(0)->p4(),m_METVec) + mTsqr(taus.at(1)->p4(),m_METVec) );

  TLorentzVector met_tau = m_METVec + taus.at(0)->p4() + taus.at(1)->p4();

  // m_mt2 = getMT2(leadjets,met_tau);

  TLorentzVector tauCloseMet=taus.at(0)->p4();
  if(fabs(taus.at(1)->phi()-m_METVec.Phi())< fabs(taus.at(0)->phi()-m_METVec.Phi())){
    tauCloseMet=taus.at(1)->p4();
  }

  // if(m_debug) std::cout << "mT2: " << m_mt2 << " mmc: " << m_MMCVec.M() << " mmc pT: " << Props::mmc_mlnu3p_4vect_pt.get(m_mmc) << std::endl;

  m_MTW_Clos = sqrt( mTsqr(tauCloseMet,m_METVec) );

  float A = sin(m_METVec.Phi()-taus.at(0)->phi())/sin(taus.at(1)->phi()-taus.at(0)->phi());
  float B = sin(taus.at(1)->phi()-m_METVec.Phi())/sin(taus.at(1)->phi()-taus.at(0)->phi());
  m_METCentrality = (A+B)/sqrt(A*A+B*B);

  float pTBins[13]={0.,20.0,40.0,60.0,80.0,100.0,120.0,140.0,160.0,180.0,220.0,300.0,500.0};
  float METBins[10]={0.,12.,24.,36.,48.,60.,80.,120.,200.,400.};

  m_dRDiJetDiTau=m_bb.DeltaR(m_MMCVec);
  m_dPhiDiJetDiTau=m_bb.DeltaPhi(m_MMCVec);

  //scaling the dijet and ditau mass to the higgs mass
  float scalebb = 125e3/m_bb.M();
  m_bbScaled=DiJetVec2*scalebb;
  float scalemmc = 125e3/(m_MMCVec.M()*1e3);
  m_MMCVecScaled= m_MMCVec*scalemmc;

  m_XmmcScaled=m_bbScaled+m_MMCVecScaled*1e3;


  m_histSvc->BookFillHist("pTBB", 12, pTBins, m_bb.Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("pTTT", 12, pTBins, tautau.Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("mBB", 23, 20, 250, m_bb.M()*1e-3, m_weight);
  m_histSvc->BookFillHist("mTauTau", 23, 20, 250, tautau.M()*1e-3, m_weight);
  m_histSvc->BookFillHist("mmc",23,20,250,m_MMCVec.M(),m_weight);
  m_histSvc->BookFillHist("mmcpT", 12, pTBins, m_MMCVec.Pt(), m_weight);
  m_histSvc->BookFillHist("mX", 100, 0, 1200, X.M()*1e-3, m_weight);
  m_histSvc->BookFillHist("mXmmc", 100, 0, 1200, m_Xmmc.M()*1e-3, m_weight);
  m_histSvc->BookFillHist("MET", 9,METBins, m_METVec.Pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("MTW_Max", 30, 0, 600, m_MTW_Max, m_weight);
  m_histSvc->BookFillHist("MTW_Clos", 20, 0, 300, m_MTW_Clos, m_weight);
  m_histSvc->BookFillHist("m_mTtot",30,0,600,m_mTtot, m_weight);
  // m_histSvc->BookFillHist("m_mt2",21,80,500,m_mt2*1e-3, m_weight);
  m_histSvc->BookFillHist("METPhiCentrality", 25, -1.5, 1.5, m_METCentrality, m_weight);

  m_histSvc->BookFillHist("DeltaRJJ", 20, 0, 5, m_deltaRJJ, m_weight);
  m_histSvc->BookFillHist("DeltaRTauTau", 20, 0, 5, m_deltaRTT, m_weight);
  m_histSvc->BookFillHist("DeltaEtaTauTau", 20, -5, 5, m_deltaEtaTT, m_weight);
  m_histSvc->BookFillHist("DeltaEtaJJ", 20, -5, 5, m_deltaEtaJJ, m_weight);
  m_histSvc->BookFillHist("DeltaPhiJJ", 20, -5, 5, m_deltaPhiJJ, m_weight);
  m_histSvc->BookFillHist("DeltaPhiTauTau", 20, -5, 5, m_deltaPhiTT, m_weight);

  m_histSvc->BookFillHist("2DFinalTTBB", 100, 0, 500, 100, 0, 500, m_bb.Pt()/1e3, tautau.Pt()/1e3, m_weight);

  std::string hist_name = m_histNameSvc->getFullHistName("mmc");
  if(m_debug) std::cout << "Full Hist Name: " << hist_name << std::endl;

  if(m_writeMVATree){
    // this removes the cross section and luminosity from the event weight
    //double mcWeight = Props::MCEventWeight.get(m_eventInfo);
    //double sumOfWeights = m_sumOfWeightsProvider->getsumOfWeights(m_mcChannel);
    m_tree->EventWeightNoXSec  = m_weight;
    m_tree->EventWeightNoXSec /= Props::LumiWeight.get(m_eventInfo);
    //if (sumOfWeights != 0)
    //  m_tree->EventWeightNoXSec /= sumOfWeights;

    m_tree->EventWeight = m_weight;
    m_tree->EventNumber = m_eventInfo->eventNumber();
    m_tree->sample = m_histNameSvc->getFullSample();

    m_tree->diTauVisM = tautau.M()/1e3;
    m_tree->diTauVisPt = tautau.Pt()/1e3;
    m_tree->diTauVisEta = tautau.Eta();
    m_tree->diTauVisPhi = tautau.Phi();

    m_tree->diTauDR   = m_deltaRTT;
    m_tree->diTauDEta = m_deltaEtaTT;
    m_tree->diTauDPhi = m_deltaPhiTT;

   m_tree->diJetM = m_bb.M()/1e3;
   m_tree->diJetPt = m_bb.Pt()/1e3;
   m_tree->diJetEta = m_bb.Eta();
   m_tree->diJetPhi = m_bb.Phi();

   m_tree->diJetDR = m_deltaRJJ;
   m_tree->diJetDEta = m_deltaEtaJJ;
   m_tree->diJetDPhi = m_deltaPhiJJ;
   
   m_tree->diTauMMCM = m_MMCVec.M();
   m_tree->diTauMMCPt = m_MMCVec.Pt();
   m_tree->diTauMMCEta = m_MMCVec.Eta();
   m_tree->diTauMMCPhi = m_MMCVec.Phi();
     
   m_tree->diHiggsM  = m_Xmmc.M()/1e3;
   m_tree->diHiggsPt = m_Xmmc.Pt()/1e3;

   m_tree->diHiggsMScaled =m_XmmcScaled.M()/1e3;

   m_tree->diJetdiTauDR = m_deltaRDiJetDiTau;
   m_tree->diJetdiTauDPhi = m_deltaPhiDiJetDiTau;

   m_tree->MTW_Max =m_MTW_Max;
   m_tree->MTW_Clos = m_MTW_Clos;
   m_tree->METCentrality = m_METCentrality;
   
   m_tree->MET = m_METVec.Pt()/1e3;

   m_tree->diJetVBFM = -1;
   m_tree->diJetVBFPt = -1;
   m_tree->diJetVBFEta = -1;
   m_tree->diJetVBFPhi = -1;
   m_tree->diJetVBFDR = -1;
   m_tree->diJetVBFDEta = -1;
   if (m_hasVBF) {
     m_tree->diJetVBFM = (m_vbfj1 + m_vbfj2).M()/1e3;
     m_tree->diJetVBFPt = (m_vbfj1 + m_vbfj2).Perp()/1e3;
     m_tree->diJetVBFEta = (m_vbfj1 + m_vbfj2).Eta();
     m_tree->diJetVBFPhi = (m_vbfj1 + m_vbfj2).Phi();
     m_tree->diJetVBFDR = m_vbfj1.DeltaR(m_vbfj2);
     m_tree->diJetVBFDEta = std::fabs(m_vbfj1.Eta() - m_vbfj2.Eta());
   }

  }

  


}

void AnalysisReader_hhbbtt::fill_vbfHistos(std::vector<const xAOD::TauJet*> taus)
{

  if(m_debug) std::cout << "On fill_vbfHistos function" << std::endl;

  float pTBins[13]={0.,20.0,40.0,60.0,80.0,100.0,120.0,140.0,160.0,180.0,220.0,300.0,500.0};
  float METBins[10]={0.,12.,24.,36.,48.,60.,80.,120.,200.,400.};

  if (m_hasVBF) {
    m_histSvc->BookFillHist("vbf_mjj", 30, 0, 3000, (m_vbfj1 + m_vbfj2).M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_ptjj", 30, 0, 1500, (m_vbfj1 + m_vbfj2).Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_detajj", 20, 0, 10, std::fabs(m_vbfj1.Eta() - m_vbfj2.Eta()), m_weight);
    float dphi = std::fabs(m_vbfj1.Phi() - m_vbfj2.Phi());
    if (dphi > M_PI) dphi -= 2*M_PI;
    m_histSvc->BookFillHist("vbf_dphijj", 20, 0, 10, dphi, m_weight);
    m_histSvc->BookFillHist("vbf_drjj", 20, 0, 10, m_vbfj1.DeltaR(m_vbfj2), m_weight);
    m_histSvc->BookFillHist("vbf_j1pt", 50, 0, 500, m_vbfj1.Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_j1eta", 50, -5, 5, m_vbfj1.Eta(), m_weight);
    m_histSvc->BookFillHist("vbf_j1phi", 64, -3.2, 3.2, m_vbfj1.Phi(), m_weight);
    m_histSvc->BookFillHist("vbf_j2pt", 50, 0, 500, m_vbfj2.Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_j2eta", 50, -5, 5, m_vbfj2.Eta(), m_weight);
    m_histSvc->BookFillHist("vbf_j2phi", 64, -3.2, 3.2, m_vbfj2.Phi(), m_weight);

    TLorentzVector tautau=taus.at(0)->p4()+taus.at(1)->p4();
    TLorentzVector X=m_bb+tautau;

    m_histSvc->BookFillHist("vbf_pTBB", 12, pTBins, m_bb.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_pTTT", 12, pTBins, tautau.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mBB", 23, 20, 250, m_bb.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mTauTau", 23, 20, 250, tautau.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mmc",23,20,250,m_MMCVec.M(),m_weight);
    m_histSvc->BookFillHist("vbf_mmcpT", 12, pTBins, m_MMCVec.Pt(), m_weight);
    m_histSvc->BookFillHist("vbf_mX", 100, 0, 1200, X.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mXmmc", 100, 0, 1200, m_Xmmc.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_MET", 9,METBins, m_METVec.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_MTW_Max", 30, 0, 600, m_MTW_Max, m_weight);
    m_histSvc->BookFillHist("vbf_MTW_Clos", 20, 0, 300, m_MTW_Clos, m_weight);
    m_histSvc->BookFillHist("vbf_m_mTtot",30,0,600,m_mTtot, m_weight);
    m_histSvc->BookFillHist("vbf_METPhiCentrality", 25, -1.5, 1.5, m_METCentrality, m_weight);

    m_histSvc->BookFillHist("vbf_DeltaRJJ", 20, 0, 5, m_deltaRJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaRTauTau", 20, 0, 5, m_deltaRTT, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaEtaTauTau", 20, -5, 5, m_deltaEtaTT, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaEtaJJ", 20, -5, 5, m_deltaEtaJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaPhiJJ", 20, -5, 5, m_deltaPhiJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaPhiTauTau", 20, -5, 5, m_deltaPhiTT, m_weight);

    m_histSvc->BookFillHist("vbf_2DFinalTTBB", 100, 0, 500, 100, 0, 500, m_bb.Pt()/1e3, tautau.Pt()/1e3, m_weight);
  }

  if (m_hasVBF_nodeta) {
    m_histSvc->BookFillHist("vbf_mjj_nodeta", 30, 0, 3000, (m_vbfj1_nodeta + m_vbfj2_nodeta).M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_ptjj_nodeta", 30, 0, 1500, (m_vbfj1_nodeta + m_vbfj2_nodeta).Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_detajj_nodeta", 20, 0, 10, std::fabs(m_vbfj1_nodeta.Eta() - m_vbfj2_nodeta.Eta()), m_weight);
    float dphi = std::fabs(m_vbfj1_nodeta.Phi() - m_vbfj2_nodeta.Phi());
    if (dphi > M_PI) dphi -= 2*M_PI;
    m_histSvc->BookFillHist("vbf_dphijj_nodeta", 20, 0, 10, dphi, m_weight);
    m_histSvc->BookFillHist("vbf_drjj_nodeta", 20, 0, 10, m_vbfj1_nodeta.DeltaR(m_vbfj2_nodeta), m_weight);
    m_histSvc->BookFillHist("vbf_j1pt_nodeta", 50, 0, 500, m_vbfj1_nodeta.Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_j1eta_nodeta", 50, -5, 5, m_vbfj1_nodeta.Eta(), m_weight);
    m_histSvc->BookFillHist("vbf_j1phi_nodeta", 64, -3.2, 3.2, m_vbfj1_nodeta.Phi(), m_weight);
    m_histSvc->BookFillHist("vbf_j2pt_nodeta", 50, 0, 500, m_vbfj2_nodeta.Perp()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_j2eta_nodeta", 50, -5, 5, m_vbfj2_nodeta.Eta(), m_weight);
    m_histSvc->BookFillHist("vbf_j2phi_nodeta", 64, -3.2, 3.2, m_vbfj2_nodeta.Phi(), m_weight);

    TLorentzVector tautau=taus.at(0)->p4()+taus.at(1)->p4();
    TLorentzVector X=m_bb+tautau;

    m_histSvc->BookFillHist("vbf_pTBB_nodeta", 12, pTBins, m_bb.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_pTTT_nodeta", 12, pTBins, tautau.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mBB_nodeta", 23, 20, 250, m_bb.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mTauTau_nodeta", 23, 20, 250, tautau.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mmc_nodeta", 23,20,250,m_MMCVec.M(),m_weight);
    m_histSvc->BookFillHist("vbf_mmcpT_nodeta", 12, pTBins, m_MMCVec.Pt(), m_weight);
    m_histSvc->BookFillHist("vbf_mX_nodeta", 100, 0, 1200, X.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_mXmmc_nodeta", 100, 0, 1200, m_Xmmc.M()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_MET_nodeta", 9,METBins, m_METVec.Pt()*1e-3, m_weight);
    m_histSvc->BookFillHist("vbf_MTW_Max_nodeta", 30, 0, 600, m_MTW_Max, m_weight);
    m_histSvc->BookFillHist("vbf_MTW_Clos_nodeta", 20, 0, 300, m_MTW_Clos, m_weight);
    m_histSvc->BookFillHist("vbf_m_mTtot",30,0,600,m_mTtot, m_weight);
    m_histSvc->BookFillHist("vbf_METPhiCentrality_nodeta", 25, -1.5, 1.5, m_METCentrality, m_weight);

    m_histSvc->BookFillHist("vbf_DeltaRJJ_nodeta", 20, 0, 5, m_deltaRJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaRTauTau_nodeta", 20, 0, 5, m_deltaRTT, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaEtaTauTau_nodeta", 20, -5, 5, m_deltaEtaTT, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaEtaJJ_nodeta", 20, -5, 5, m_deltaEtaJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaPhiJJ_nodeta", 20, -5, 5, m_deltaPhiJJ, m_weight);
    m_histSvc->BookFillHist("vbf_DeltaPhiTauTau_nodeta", 20, -5, 5, m_deltaPhiTT, m_weight);

    m_histSvc->BookFillHist("vbf_2DFinalTTBB_nodeta", 100, 0, 500, 100, 0, 500, m_bb.Pt()/1e3, tautau.Pt()/1e3, m_weight);
  }

}

void AnalysisReader_hhbbtt::fill_SRPlots()
{
       //Filling histograms after signal selection - cut-based
       if(m_FillSRSel260_300){
         m_histSvc->BookFillHist("Sel_M260300_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
         m_histSvc->BookFillHist("Sel_M260300_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
       }
       if(m_FillSRSel400){
         if(m_bb.Pt()>100){//DiJet pt cut for M400
            m_histSvc->BookFillHist("Sel_M400_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
            m_histSvc->BookFillHist("Sel_M400_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
         }
       }
       if(m_FillSRSel500_600_700){
         if(m_bb.Pt()>150){//DiJet pt cut for M500600700
            m_histSvc->BookFillHist("Sel_M500600700_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
            m_histSvc->BookFillHist("Sel_M500600700_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
         }
       }
       if(m_FillSRSel800_900_1000){
         if(m_bb.Pt()>200){//DiJet pt cut for M8009001000
            m_histSvc->BookFillHist("Sel_M8009001000_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
            m_histSvc->BookFillHist("Sel_M8009001000_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
         }
       }


   //Filling histograms after signal selection - BDT cut for MVA studies
   if(m_readMVA){
     if(m_tree->BDT>0){
       m_histSvc->BookFillHist("BDT0_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT0_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.1){
       m_histSvc->BookFillHist("BDT1_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT1_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.2){
       m_histSvc->BookFillHist("BDT2_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT2_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.3){
       m_histSvc->BookFillHist("BDT3_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT3_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.4){
       m_histSvc->BookFillHist("BDT4_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT4_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.5){
       m_histSvc->BookFillHist("BDT5_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT5_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.6){
       m_histSvc->BookFillHist("BDT6_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT6_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.7){
       m_histSvc->BookFillHist("BDT7_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT7_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.8){
       m_histSvc->BookFillHist("BDT8_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT8_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
     if(m_tree->BDT>0.9){
       m_histSvc->BookFillHist("BDT9_DiHiggsM", 300/2, 0, 3000, m_Xmmc.M(), m_weight);
       m_histSvc->BookFillHist("BDT9_DiHiggsMScaled", 300/2, 0, 3000, m_XmmcScaled.M(), m_weight);
     }
   }

}

void AnalysisReader_hhbbtt::fill_eventLVars()
{
  if(m_writeMVATree){
   m_tree->EventWeight = m_weight;
   m_tree->EventNumber = m_eventInfo->eventNumber();
   m_tree->sample = m_histNameSvc->getFullSample();
  }
}

bool AnalysisReader_hhbbtt::fill_cutFlow(ResultHHbbtautau &selectionResult, std::vector<const xAOD::Jet*> jets, std::vector<const xAOD::TauJet*> taus)
{
  //NEW cut-flow-----------------------------------------------------------
  bool passCutFlow=false;

  m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 0, 1);//start
  m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,0, m_weight);

  if(m_RegA || m_RegB || m_RegC || m_RegD) {

    if (!m_antiTau && !selectionResult.isSREvent)
      return false;

    if (m_antiTau) { // if this is true, keep the event anyway, as well as the SR -- WARNING! This is different from MIA
      //if (selectionResult.isSREvent) return EL::StatusCode::SUCCESS;
      if (m_currentVar == "ANTITAU_BDT_CUT") { 
        for (const auto& itau : selectionResult.taus ) { 
          if (Props::BDTScore.get(itau) < 0.45) return false;
        }
      }  
    } // m_antiTau

    m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 1, 1);
    m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,1, m_weight);
    
    if (m_PassTriggerSelection) {//trigger DTT or STT
      m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 2, 1);
      m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,2, m_weight);
      
      if (taus.at(0)->pt()/1e3 > 40 && taus.at(1)->pt()/1e3 > 20) {//tau pt 40,20
        m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 3, 1);
        m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,3, m_weight);
     
    	if (jets.size() > 1) { // 2 signal jets
          m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 4, 1);//jet pt 20,20
          m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,4, m_weight);
     
          if (m_MMCVec.M() > 60) {
            m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 5, 1);//mmc>60
            m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,5, m_weight);
       
            if (jets.at(0)->pt()/1e3 > 45) { // Leading jet with pT > 45 GeV
              m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 6, 1);
              m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,6, m_weight);
      
              if (m_PassTriggerJetSelection) {
                m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 7, 1); //jet pt cut 80 and *sub*-leading tau pT > 30 GeV for DTT
                m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,7, m_weight);
         
                if(m_debug) std::cout << "m_nBJets: " << m_nBJets << std::endl;
                if(m_nBJets==0){
                  if(m_debug) std::cout << "At 0 tag" << std::endl;
                  m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 8, 1);//0Tag
                  m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0.,20.,8, m_weight);
                }else if(m_nBJets==1){
                  if(m_debug) std::cout << "At 1 tag" << std::endl;
                  m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 9, 1);//1Tag
                  m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0.,20.,9, m_weight);
                }else if(m_nBJets==2){
                  if(m_debug) std::cout << "At 2 tag" << std::endl;
                  m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 10, 1);//2Tag
                  m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,10, m_weight);

                  if (m_hasVBF_nodeta) {
                    m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 12, 1);// VBF mjj>600 GeV, no delta eta req.
                    m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,12, m_weight);
                  }
                  if (m_hasVBF) {
                    m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 13, 1);// VBF mjj>600 GeV, for jets with |deta| > 5
                    m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,13, m_weight);
                  }
                }else if(m_nBJets>=3){
                  if(m_debug) std::cout << "At 3+ tag" << std::endl;
                  m_histSvc->BookFillHist("CutFlow_New", 20, 0.,20., 11, 1);//>=3Tag
                  m_histSvc->BookFillHist("CutFlow_New_Weighted", 20, 0., 20.,11, m_weight);
                }

                if(m_nBJets<3){
                  passCutFlow=true;
                }
              }
            }
          }
	}
      }
    }
  }

  if(m_debug) std::cout << "PassCutFlow: " << passCutFlow << std::endl;

  return passCutFlow;
}

void AnalysisReader_hhbbtt::fill_jetHistos(std::vector<const xAOD::Jet*> jets)
{
  //Leading jet histos
  m_histSvc->BookFillHist("Jet0Eta",50,-5,5, jets.at(0)->eta(), m_weight);
  m_histSvc->BookFillHist("Jet0Phi",50,-5,5, jets.at(0)->phi(), m_weight);
  m_histSvc->BookFillHist("Jet0Pt",100,0,1000, jets.at(0)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("Jet0isTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(0)), m_weight);
  m_histSvc->BookFillHist("Jet0tagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(0)), m_weight);

  //Sub-Leading jet histos
  m_histSvc->BookFillHist("Jet1Eta",50,-5,5, jets.at(1)->eta(), m_weight);
  m_histSvc->BookFillHist("Jet1Phi",50,-5,5, jets.at(1)->phi(), m_weight);
  m_histSvc->BookFillHist("Jet1Pt",100,0,1000, jets.at(1)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("Jet1isTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(1)), m_weight);
  m_histSvc->BookFillHist("Jet1tagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(1)), m_weight);

  //Inclusive jet histos
  for(unsigned int i=0;i<jets.size();i++){
     m_histSvc->BookFillHist("JetEta",50,-5,5, jets.at(i)->eta(), m_weight);
     m_histSvc->BookFillHist("JetPhi",50,-5,5, jets.at(i)->phi(), m_weight);
     m_histSvc->BookFillHist("JetPt",100,0,1000, jets.at(i)->pt()*1e-3, m_weight);
     m_histSvc->BookFillHist("JetisTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(i)), m_weight);
     m_histSvc->BookFillHist("JettagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(i)), m_weight);
  }

  m_histSvc->BookFillHist("NJets", 10, 0, 10, jets.size(), m_weight);
  m_histSvc->BookFillHist("NBJets", 10, 0, 10, m_nBJets, m_weight);

  if(m_writeMVATree){
    m_tree->Jet1Pt = jets.at(0)->pt()/1e3;
    m_tree->Jet1Eta = jets.at(0)->eta();
    m_tree->Jet1Phi = jets.at(0)->phi();
    m_tree->Jet1M = jets.at(0)->m()/1e3;

    m_tree->Jet2Pt = jets.at(1)->pt()/1e3;
    m_tree->Jet2Eta = jets.at(1)->eta();
    m_tree->Jet2Phi = jets.at(1)->phi();
    m_tree->Jet2M = jets.at(1)->m()/1e3;

    m_tree->NJets = jets.size();
    m_tree->NJetsbtagged = m_nBJets;
  }

}

void AnalysisReader_hhbbtt::fill_tauHistos(std::vector<const xAOD::TauJet*> taus)
{
  m_histNameSvc->set_nProng(-1);
  m_histSvc->BookFillHist("Tau0nTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(0)),m_weight);
  m_histSvc->BookFillHist("Tau1nTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(1)),m_weight);

  //Leading jet histos
  m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(0)));
  m_histSvc->BookFillHist("Tau0Eta",30,-3,3, taus.at(0)->eta(), m_weight);
  m_histSvc->BookFillHist("Tau0Pt",100,0,1000, taus.at(0)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("Tau0Phi",30,-5,5, taus.at(0)->phi(), m_weight);
  m_histSvc->BookFillHist("Tau0BDTScore",20,0.35,1,Props::BDTScore.get(taus.at(0)), m_weight);

  //Sub-Leading jet histos
  m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(1)));
  m_histSvc->BookFillHist("Tau1Eta",30,-3,3, taus.at(1)->eta(), m_weight);
  m_histSvc->BookFillHist("Tau1Pt",100,0,1000, taus.at(1)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("Tau1Phi",30,-5,5, taus.at(1)->phi(), m_weight);
  m_histSvc->BookFillHist("Tau1BDTScore",20,0.35,1,Props::BDTScore.get(taus.at(1)), m_weight);

  //int nTaus=0,nSignalTaus=0,nAntiTaus=0;
  int nSignalTaus=0,nAntiTaus=0;
  //Inclusive jet histos
  for(unsigned int i=0;i<taus.size();i++){
     m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(i)));
     m_histSvc->BookFillHist("TauEta",50,-5,5, taus.at(i)->eta(), m_weight);
     m_histSvc->BookFillHist("TauPhi",50,-5,5, taus.at(i)->phi(), m_weight);
     m_histSvc->BookFillHist("TauPt",100,0,1000, taus.at(i)->pt()*1e-3, m_weight);
     m_histSvc->BookFillHist("TauBDTScore",20,0.35,1, Props::BDTScore.get(taus.at(i)), m_weight);
     m_histNameSvc->set_nProng(-1);
     m_histSvc->BookFillHist("TaunTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(i)),m_weight);
     if(Props::isBDTMedium.get(taus.at(i))) nSignalTaus++;
     if(!Props::isBDTMedium.get(taus.at(i))) nAntiTaus++;
  }

  m_histSvc->BookFillHist("NTaus", 10, 0, 10, taus.size(), m_weight);
  m_histSvc->BookFillHist("NSignalTaus", 10, 0, 10, nSignalTaus, m_weight);
  m_histSvc->BookFillHist("NAntiTaus", 10, 0, 10, nAntiTaus, m_weight);

  if(m_writeMVATree){
    m_tree->Tau1Pt = taus.at(0)->pt()/1e3;
    m_tree->Tau1Eta = taus.at(0)->eta();
    m_tree->Tau1Phi = taus.at(0)->phi();
    m_tree->Tau1M = taus.at(0)->m()/1e3;

    m_tree->Tau2Pt = taus.at(1)->pt()/1e3;
    m_tree->Tau2Eta = taus.at(1)->eta();
    m_tree->Tau2Phi = taus.at(1)->phi();
    m_tree->Tau2M = taus.at(1)->m()/1e3;
  }

}

void AnalysisReader_hhbbtt::fill_sjetHistos(std::vector<const xAOD::Jet*> jets)
{
  m_histSvc->BookFillHist("NSignalJets", 10, 0, 10, jets.size(), m_weight);

  //Leading jet histos
  m_histSvc->BookFillHist("SignalJet0Eta",50,-5,5, jets.at(0)->eta(), m_weight);
  m_histSvc->BookFillHist("SignalJet0Phi",50,-5,5, jets.at(0)->phi(), m_weight);
  m_histSvc->BookFillHist("SignalJet0Pt",100,0,1000, jets.at(0)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("SignalJet0isTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(0)), m_weight);
  m_histSvc->BookFillHist("SignalJet0tagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(0)), m_weight);

  //Sub-Leading jet histos
  m_histSvc->BookFillHist("SignalJet1Eta",50,-5,5, jets.at(1)->eta(), m_weight);
  m_histSvc->BookFillHist("SignalJet1Phi",50,-5,5, jets.at(1)->phi(), m_weight);
  m_histSvc->BookFillHist("SignalJet1Pt",100,0,1000, jets.at(1)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("SignalJet1isTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(1)), m_weight);
  m_histSvc->BookFillHist("SignalJet1tagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(1)), m_weight);

  int nBJets=0;
  //Inclusive jet histos
  for(unsigned int i=0;i<jets.size();i++){
     m_histSvc->BookFillHist("SignalJetEta",50,-5,5, jets.at(i)->eta(), m_weight);
     m_histSvc->BookFillHist("SignalJetPhi",50,-5,5, jets.at(i)->phi(), m_weight);
     m_histSvc->BookFillHist("SignalJetPt",100,0,1000, jets.at(i)->pt()*1e-3, m_weight);
     m_histSvc->BookFillHist("SignalJetisTagged",2,-0.5,1.5, BTagProps::isTagged.get(jets.at(i)), m_weight);
     m_histSvc->BookFillHist("SignalJettagWeight",20,-2.0,2.0, BTagProps::tagWeight.get(jets.at(i)), m_weight);
     if(BTagProps::isTagged.get(jets.at(i))) nBJets++;
  }

  m_histSvc->BookFillHist("NJets", 10, 0, 10, jets.size(), m_weight);
  m_histSvc->BookFillHist("NBJets", 10, 0, 10, nBJets, m_weight);

}

std::string AnalysisReader_hhbbtt::get2DProngRegion(std::vector<const xAOD::TauJet*> taus)
{
   std::string string_out="";

   if(Props::nTracks.get(taus.at(0)) == 1 && Props::nTracks.get(taus.at(1)) == 3){
       string_out="nProngs13";
   }else if(Props::nTracks.get(taus.at(0)) == 3 && Props::nTracks.get(taus.at(1)) == 1){
       string_out="nProngs31";
   }else if(Props::nTracks.get(taus.at(0)) == 1 && Props::nTracks.get(taus.at(1)) == 1){
       string_out="nProngs11";
   }else if(Props::nTracks.get(taus.at(0)) == 3 && Props::nTracks.get(taus.at(1)) == 3){
       string_out="nProngs33";
   }

   return string_out;
}

void AnalysisReader_hhbbtt::fill_tauFFHistos(std::string regionFF,std::vector<const xAOD::TauJet*> taus)
{

  const int NBinspT1=8;
  const int NBinspT2=6;
  const int NBinsEta2=10;
  
  float FF_pT1_Edges[NBinspT1+1]={40.0,50.0,60.0,70.0,80.0,100.0,140.0,240.0,1000.0};
  float FF_pT2_Edges[NBinspT2+1]={20.0,30.0,40.0,50.0,60.0,100.0,1000.0};
  float FF_eta_Edges[NBinsEta2+1]={-3.0,-2.4,-1.8,-1.2,-0.6,0.0,0.6,1.2,1.8,2.4,3.0};

  m_histSvc->BookFillHist("2DFFPt1Pt2", NBinspT1, FF_pT1_Edges, NBinspT2, FF_pT2_Edges, taus.at(0)->pt()/1e3, taus.at(1)->pt()/1e3, m_weight);
  m_histSvc->BookFillHist("2DFFPt1Eta1", NBinspT1, FF_pT1_Edges, NBinsEta2, FF_eta_Edges, taus.at(0)->pt()/1e3, taus.at(1)->eta(), m_weight);

  int tauTTag0=fabs(Props::TATTruthMatch.get(taus.at(0)));
  int tauTTag1=fabs(Props::TATTruthMatch.get(taus.at(1)));


  if( (m_isMC && tauTTag0 == 15 && tauTTag1 == 15) || !m_isMC){
     m_histSvc->BookFillHist(("2DFFPt1Pt2_"+regionFF).c_str(), NBinspT1, FF_pT1_Edges, NBinspT2, FF_pT2_Edges, taus.at(0)->pt()/1e3, taus.at(1)->pt()/1e3, m_weight);
  }

  m_histNameSvc->set_nProng(-1);
  m_histSvc->BookFillHist("FinalTau0nTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(0)),m_weight);
  m_histSvc->BookFillHist("FinalTau1nTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(1)),m_weight);

  m_histNameSvc->set_nProng(-1);
  m_histSvc->BookFillHist("2DFinalTauPt1Pt2", 100, 0, 500, 100, 0, 500, taus.at(1)->pt()/1e3, taus.at(0)->pt()/1e3, m_weight);

  //Leading jet histos
  m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(0)));
  m_histSvc->BookFillHist("FinalTau0Eta",30,-3,3, taus.at(0)->eta(), m_weight);
  m_histSvc->BookFillHist("FinalTau0Pt",100,0,1000, taus.at(0)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("FinalTau0Phi",30,-5,5, taus.at(0)->phi(), m_weight);
  m_histSvc->BookFillHist("FinalTau0BDTScore",20,0.35,1,Props::BDTScore.get(taus.at(0)), m_weight);
  if(m_isMC){
    int tauTTag=fabs(Props::TATTruthMatch.get(taus.at(0)));
    if(tauTTag == 15) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompTau",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 1) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompD",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 2) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompU",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 3) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompS",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 4) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompC",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 5) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompB",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 13) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompMuon",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 11) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompElectron",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 21) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompGluon",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
    if(tauTTag == 0) m_histSvc->BookFillHist("FinalTau0BDTScore_FCompNoMatched",20,0.35,1,Props::BDTScore.get(taus.at(0)), 1);
  }


  //Sub-Leading jet histos
  m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(1)));
  m_histSvc->BookFillHist("FinalTau1Eta",30,-3,3, taus.at(1)->eta(), m_weight);
  m_histSvc->BookFillHist("FinalTau1Pt",100,0,1000, taus.at(1)->pt()*1e-3, m_weight);
  m_histSvc->BookFillHist("FinalTau1Phi",30,-5,5, taus.at(1)->phi(), m_weight);
  m_histSvc->BookFillHist("FinalTau1BDTScore",20,0.35,1,Props::BDTScore.get(taus.at(1)), m_weight);
  if(m_isMC){
    int tauTTag=fabs(Props::TATTruthMatch.get(taus.at(1)));
    if(tauTTag == 15) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompTau",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 1) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompD",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 2) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompU",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 3) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompS",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 4) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompC",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 5) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompB",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 13) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompMuon",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 11) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompElectron",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 21) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompGluon",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
    if(tauTTag == 0) m_histSvc->BookFillHist("FinalTau1BDTScore_FCompNoMatched",20,0.35,1,Props::BDTScore.get(taus.at(1)), 1);
  }

  //Inclusive jet histos
  for(unsigned int i=0;i<taus.size();i++){
     m_histNameSvc->set_nProng(Props::nTracks.get(taus.at(i)));
     m_histSvc->BookFillHist("FinalTauEta",50,-5,5, taus.at(i)->eta(), m_weight);
     m_histSvc->BookFillHist("FinalTauPhi",50,-5,5, taus.at(i)->phi(), m_weight);
     m_histSvc->BookFillHist("FinalTauPt",100,0,1000, taus.at(i)->pt()*1e-3, m_weight);
     m_histSvc->BookFillHist("FinalTauBDTScore",20,0.35,1, Props::BDTScore.get(taus.at(i)), m_weight);
     m_histNameSvc->set_nProng(-1);
     m_histSvc->BookFillHist("FinalTaunTracks",5,-0.5,4.5,Props::nTracks.get(taus.at(i)),m_weight);
  }

}

void AnalysisReader_hhbbtt::fill_boostedPlots(std::string description)
{
  int n_Tag = -1;
  if(m_FJNbtag == 0) n_Tag = 0;
  if(m_FJNbtag == 1) n_Tag = 1;
  if(m_FJNbtag == 2) n_Tag = 2;
  
  m_histNameSvc->set_description((description).c_str());
  m_histNameSvc->set_nTag(n_Tag);
  m_histNameSvc->set_nJet(-2); // TODO how to set 1Pjet ?!

  ResultHHbbtautau postSelectionResult = ((HHbbtautauBoostedJetSelection*)m_eventPostSelection)->result();

  m_histSvc->BookFillHist("SelFJPt",         200,  0, 2000, m_SelFJ->p4().Pt()/1e3, m_weight);
  m_histSvc->BookFillHist("SelFJM",          200,  0, 1000, m_SelFJ->p4().M()/1e3,  m_weight);
  m_histSvc->BookFillHist("SelFJEta",         60, -3, 3,    m_SelFJ->p4().Eta(),    m_weight);
  m_histSvc->BookFillHist("SelFJPhi",         80, -4, 4,    m_SelFJ->p4().Phi(),    m_weight);
    
  std::vector<const xAOD::Jet*> fatJets     = postSelectionResult.fatJets;
  m_histSvc->BookFillHist("LeadFJPt",         200,  0, 2000, fatJets.at(0)->p4().Pt()/1e3, m_weight);
  m_histSvc->BookFillHist("LeadFJM",          200,  0, 1000, fatJets.at(0)->p4().M()/1e3,  m_weight);


  int isTruthMatch = 0;
  if (m_isMC) isTruthMatch = Props::TruthMatch.get(m_SelDT);
  m_histSvc->BookFillHist("SelDTTruthMatch",   2, -0.5, 1.5, isTruthMatch, m_weight);
  
  m_histSvc->BookFillHist("MET",             100,  0, 1000, m_METVec.Pt()/1e3,                        m_weight);
  m_histSvc->BookFillHist("MET_phi",         100, -4,    4, m_METVec.Phi(),                       m_weight);
  
  TLorentzVector SelDTsubs = m_SelDTsubjets;
  m_histSvc->BookFillHist("SelDTsubsPt",      200,  0, 2000, SelDTsubs.Pt()/1e3,       m_weight);
  m_histSvc->BookFillHist("SelDTsubsM",       200,  0, 1000, SelDTsubs.M()/1e3,        m_weight);
  m_histSvc->BookFillHist("SelDTsubsEta",      60, -3, 3,    SelDTsubs.Eta(),          m_weight);
  m_histSvc->BookFillHist("SelDTsubsPhi",      80, -4, 4,    SelDTsubs.Phi(),          m_weight);
  m_histSvc->BookFillHist("deltaR_DTsubj12",   20,  0, 2,    m_SelDTsubj0.DeltaR(m_SelDTsubj1),    m_weight);
  m_histSvc->BookFillHist("DTsublPtOverleadPt",100, 0, 1,    m_SelDTsubj1.Pt()/m_SelDTsubj0.Pt(),  m_weight);
  m_histSvc->BookFillHist("DTsublPt",         100,  0, 500,  m_SelDTsubj1.Pt()/1e3,  m_weight);
  m_histSvc->BookFillHist("DTleadPt",         200,  0, 1000,  m_SelDTsubj0.Pt()/1e3,  m_weight);

  m_histSvc->BookFillHist("DTlead_ntracks",   5,  0, 5,    Props::subjet_lead_ntracks.get(m_SelDT),    m_weight);
  m_histSvc->BookFillHist("DTsubl_ntracks",   5,  0, 5,    Props::subjet_subl_ntracks.get(m_SelDT),    m_weight);
  m_histSvc->BookFillHist("DT_ntracks",   10,  0, 10,   Props::subjet_lead_ntracks.get(m_SelDT)+Props::subjet_subl_ntracks.get(m_SelDT),    m_weight);
  m_histSvc->BookFillHist("DiTauBDT", 100, 0, 1, Props::BDTScore.get(m_SelDT), m_weight);

  
  m_histSvc->BookFillHist("deltaR_FJ_DTsubs",  50,  0, 5,    SelDTsubs.DeltaR(m_SelFJ->p4()),      m_weight);  
  m_histSvc->BookFillHist("deltaPhi_FJ_DTsubs",  50,  0, 5,  SelDTsubs.DeltaPhi(m_SelFJ->p4()),      m_weight);  
  m_histSvc->BookFillHist("dPhi_DTsubs_MET",   64,-3.2,3.2,  SelDTsubs.DeltaPhi(m_METVec),         m_weight);
  m_histSvc->BookFillHist("subsmhh",          400,  0, 4000, (m_SelFJ->p4() + SelDTsubs).M()/1e3,  m_weight);
  m_histSvc->BookFillHist("subsbbttPt",       400,  0, 4000, (m_SelFJ->p4() + SelDTsubs).Pt()/1e3, m_weight);
    
  m_histSvc->BookFillHist("DTMassvsFJMass",     100, 0, 500, 100, 0, 500, SelDTsubs.M()/1e3, m_SelFJ->p4().M()/1e3, m_weight);
  m_histSvc->BookFillHist("CorrDTMassvsFJMass", 100, 0, 500, 100, 0, 500, m_CorrSelDT.M()*1e-3, m_SelFJ->p4().M()*1e-3, m_weight);

  m_histSvc->BookFillHist("effmHH",          400,  0, 4000, (m_SelFJ->p4() + m_CorrSelDT).M()/1e3,  m_weight);

  if(postSelectionResult.trigger == eFJT360)
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 0, m_weight);
  else if(postSelectionResult.trigger == eFJT400)
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 1, m_weight);
  else if(postSelectionResult.trigger == eFJT420)
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 2, m_weight);
  else if(postSelectionResult.trigger == eFJT440)
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 3, m_weight);
  else if(postSelectionResult.trigger == eFJT460)
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 4, m_weight);
  else
    m_histSvc->BookFillHist("TriggerAccepted", 6,  0, 6, 5, m_weight);

  return;
}

void AnalysisReader_hhbbtt::fill_boostedTree()
{
  m_tree->Reset();
  
  m_tree->sample = m_histNameSvc->getFullSample();

  m_tree->EventWeight = m_weight;
  m_tree->EventNumber = m_eventInfo->eventNumber();

  m_tree->m_FJNbtagJets = m_FJNbtag;
  m_tree->m_FJpt = m_SelFJ->p4().Pt()/1e3;
  m_tree->m_FJeta = m_SelFJ->p4().Eta();
  m_tree->m_FJphi = m_SelFJ->p4().Phi();
  m_tree->m_FJm = m_SelFJ->p4().M();

  m_tree->m_DTpt = m_SelDTsubjets.Pt()/1e3;
  m_tree->m_DTeta = m_SelDTsubjets.Eta();
  m_tree->m_DTphi = m_SelDTsubjets.Phi();
  m_tree->m_DTm = m_SelDTsubjets.M();

  m_tree->m_dPhiFJwDT = fabs(m_SelDTsubjets.DeltaPhi(m_SelFJ->p4()));
  m_tree->m_dRFJwDT = m_SelDTsubjets.DeltaR(m_SelFJ->p4());
  m_tree->m_dPhiDTwMET = m_SelDTsubjets.DeltaPhi(m_METVec);

  m_tree->m_MET = m_METVec.Pt()/1e3;
  m_tree->m_hhm = (m_SelFJ->p4() + m_SelDTsubjets).M()/1e3;
  m_tree->m_bbttpt = (m_SelFJ->p4() + m_SelDTsubjets).Pt()/1e3;
  m_tree->m_region = m_region;

  m_tree->Fill();
  return;
}

float AnalysisReader_hhbbtt::TauFakeCompWeight(std::vector<const xAOD::TauJet*> finalTaus) {
  float weight = 1.0;
  std::string systName = m_currentVar;
  if (systName.find("SysCompFakes") == std::string::npos) return weight;
  //bool doLQ3 = false; // TODO
  if (m_analysisType == "hadhad") {
    //if (doLQ3) {
    //   float mLQ = m_event->MLQx()/GeV;
    //   if      ( mLQ < 50.0  ) mLQ = 50.0;
    //   else if ( mLQ > 400.0 ) mLQ = 400.0;
    //   float sign = systName.Contains("SysCompFakes__1down") ? -1.0 : 1.0;
    //   weight += sign * fabs(0.05483 - 0.000303602 * mLQ);
    //}
    //else {
    float taupt = finalTaus.at(0)->p4().Pt()*1e-3;
    float tau1pt = finalTaus.at(1)->p4().Pt()*1e-3;
    if (taupt < tau1pt) {
      std::swap(taupt, tau1pt);
    }
    if (tau1pt > 100) tau1pt = 100;
    if (tau1pt < 20) tau1pt = 20;
    
    float ntag = m_histNameSvc->get_nTag();
    if (ntag < 2) return 1; 

    double a(0), b(0);
    if (systName.find("SysCompFakes__1up") != std::string::npos) {
      a = +0.28;
      b = -0.00773;
    } else if (systName.find("SysCompFakes__1down") != std::string::npos) {
      a = -0.28;
      b = +0.00773;
    } else {
      return 1;
    }

    weight += b * tau1pt + a;
  } else if (m_analysisType == "lephad") {
    //if (m_config->DoLQ3 || m_config->DoLQ) {
    //  float dR = m_event->dRLepJetLQ();
    //  int ntag = m_event->NBJet();
    //  if (ntag < 1) return 1; // Also need 1 tag for LQ
    //  double a(0), b(0);
    //  
    //  if (systName.Contains("SysCompFakes__1up")) {
    //    a = -0.142824;
    //    b = +0.0614033;
    //  } else if (systName.Contains("SysCompFakes__1down")) {
    //    a = +0.142824;
    //    b = -0.0614033;
    //  } else {
    //    return 1;
    //  }            
    //  weight += b * dR + a;
    //} else {
    ResultHHbbtautau postSelectionResult = ((HHbbtautauHadHadJetSelection*)m_eventPostSelection)->result();
    std::vector<const xAOD::Jet*> &jetvector = postSelectionResult.signalJets;
    std::vector<const xAOD::TauJet*> &tauvector = postSelectionResult.taus;
    const xAOD::Electron* el = postSelectionResult.el;
    const xAOD::Muon* mu = postSelectionResult.mu;

    TLorentzVector X = jetvector[0]->p4() + jetvector[1]->p4();
    if (el) X += el->p4();
    else if (mu) X += mu->p4();
    X += tauvector[0]->p4();
    float mhh = X.M()*1e-3;
    if (mhh > 1500) mhh = 1500;
    if (mhh < 200) mhh = 200;
    
    float ntag = m_histNameSvc->get_nTag();
    if (ntag < 2) return 1; 

    double a(0), b(0);
    if (systName.find("SysCompFakes__1up") != std::string::npos) {
      a = -0.145883;
      b = +0.000381682;
    } else if (systName.find("SysCompFakes__1down") != std::string::npos) {
      a = +0.145883;
      b = -0.000381682;
    } else {
      return 1;
    }
    weight += b * mhh + a;
  }
  return weight;
}


EL::StatusCode AnalysisReader_hhbbtt::applyFF(std::string region,std::vector<const xAOD::TauJet*> finalTaus, Trigger &trigger )
{
  // had-had updated to Adam's file
  if (m_analysisType == "hadhad") {
    float m_tauFakeFactor = 1;
    std::string sysStr = m_currentVar;

    std::string sysKey = m_currentVar;
    if (m_tauFFHadHadUnbin.find(sysKey) == m_tauFFHadHadUnbin.end())
      sysKey = "Nominal";

    float ntagReal = m_histNameSvc->get_nTag();

    float ntag = m_histNameSvc->get_nTag();
    if (ntag > 1) ntag = 1;

    float taupt = finalTaus.at(0)->p4().Pt()*1e-3;
    float tau1pt = finalTaus.at(1)->p4().Pt()*1e-3;
    int   ntrk   = Props::nTracks.get(finalTaus.at(0));
    int   ntrk1  = Props::nTracks.get(finalTaus.at(1));
    if (taupt < tau1pt) {
      std::swap(taupt, tau1pt);
      std::swap(ntrk, ntrk1);
    }
  
    if (taupt > 200.) taupt = 200.;
    if (taupt < 20.) taupt = 20.;
    if (tau1pt > 200.) tau1pt = 200.;
    if (tau1pt < 20.) tau1pt = 20.;
  
    int iKey(0);
    //int iNormKey(0);
    bool run2DFF(true);
    float fWeight(1.);
    float tauw(1.);

    if (run2DFF) {
      iKey = 200+(ntrk*10)+ntrk1;
      //iNormKey = (ntrk*10)+ntrk1;
      
      ///////////////////////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////////////////
      //                     2D DTT pT dependent FF
      ///////////////////////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////////////////
      int iBin = m_tauFFHadHad[sysKey][ntag][iKey]->FindBin(taupt,tau1pt);
      fWeight = m_tauFFHadHad[sysKey][ntag][iKey]->GetBinContent(iBin);
      
      /////////////////////////////////////////////////////////////////////////////
      //                    VARY DTT FF BY STAT UP AND DOWN (FAKE SYST)
      ////////////////////////////////////////////////////////////////////////////
      std::size_t foundSyst = sysStr.find("StatQCD");
      if (foundSyst!=std::string::npos) {
        float varFrac = sysStr=="FFStatQCD__1up" ? 1. : -1;
        fWeight += varFrac*m_tauFFHadHad[sysKey][ntag][iKey]->GetBinError(iBin);
      }
  
      ///////////////////////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////////////////
      //                    APPLY SEPARATE STT FF TO STT EVENTS
      //////////////////////////////////////////////////////////////////////////////
      ///////////////////////////////////////////////////////////////////////////////
      if (trigger == eSTT) {
        //MAYBE FIND A BETTER WAY TO FILL THESE WITH A MAP/ARRAY
        float i1P1P1tagSTT = m_tauFFHadHadUnbin[sysKey][1]["STT"]->GetBinContent(1);
        float i1P3P1tagSTT = m_tauFFHadHadUnbin[sysKey][1]["STT"]->GetBinContent(2);
        float i3P1P1tagSTT = m_tauFFHadHadUnbin[sysKey][1]["STT"]->GetBinContent(3);
        float i3P3P1tagSTT = m_tauFFHadHadUnbin[sysKey][1]["STT"]->GetBinContent(4);
  
        if (ntrk == 1) {
          if (ntrk1 == 1) {
            fWeight = i1P1P1tagSTT;
          } else if (ntrk1 == 3) {
            fWeight = i1P3P1tagSTT;
          }
        } else if ( ntrk == 3) {
          if (ntrk1 == 1) {
            fWeight = i3P1P1tagSTT; 
          } else if (ntrk1 == 3) {
            fWeight = i3P3P1tagSTT;
          }
        }
      }
     
      ////////////////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////////////////
      //                                    1TAG->2TAG FACTOR
      ////////////////////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////////////////////
      if (ntagReal == 2) {
        std::map<int,float> fTF;
        std::map<int,float> fTFError;
        for (const int& idx : {1,2,3,4}) {
          fTF[idx] = m_HadHadTF[sysKey]->GetBinContent(idx);
          fTFError[idx] = m_HadHadTF[sysKey]->GetBinError(idx);
        }
  
        ////////////////////////////////////////////////////////////////////////////////////////////
        //                          VARY 1TAG->2TAG TF FOR SYSTS
        //                          m-m-m-mixItUp stat errors for syst now
        ////////////////////////////////////////////////////////////////////////////////////////////
        if (ntrk == 1) {
          if (ntrk1 == 1) {
            // 1P1P
            if(sysStr == "Nominal") {
              fWeight *= fTF[1];
            } else if (sysStr.find("1tag2tagTF") != string::npos) {
              fWeight *= fTF[1] + (fTFError[1] * (sysStr.find("up") != string::npos ? 1 : -1)); 
            }
          } else if (ntrk1 == 3) {
            // 1P3P
            if(sysStr == "Nominal") {
              fWeight *= fTF[2];
            } else if (sysStr.find("1tag2tagTF") != string::npos) {
              fWeight *= fTF[2] + (fTFError[2] * (sysStr.find("up") != string::npos ? 1 : -1)); 
            }
          }
        } else if ( ntrk == 3) {
          if (ntrk1 == 1) {
            // 3P1P
            if(sysStr == "Nominal") {
              fWeight *= fTF[3];
            } else if (sysStr.find("1tag2tagTF") != string::npos) {
              fWeight *= fTF[3] + (fTFError[3] * (sysStr.find("up") != string::npos ? 1 : -1)); 
            }
          } else if (ntrk1 == 3) {
            // 3P3P
            if(sysStr == "Nominal") {
              fWeight *= fTF[4];
            } else if (sysStr.find("1tag2tagTF") != string::npos) {
              fWeight *= fTF[4] + (fTFError[4] * (sysStr.find("up") != string::npos ? 1 : -1)); 
            }
          }
        }
      }
  
  
      ////////////////////////////////////////////////////////////////////////////////////////////
      //                    OS-SS SYSTS
      //                    Values hard coded and taken from another file.
      //                    May Change.
      ////////////////////////////////////////////////////////////////////////////////////////////
      bool isLQ3 = false; // TODO
      if(ntrk == 1) {
        if(ntrk1 == 1) {
         float OSSSFactor = isLQ3 ? 0.863451338898 : 0.932473;
          if(sysStr == "OSSS__1up"){
            fWeight *= OSSSFactor;
          } else if ( sysStr == "OSSS__1down") {
            fWeight *= 1+fabs((1-OSSSFactor));
          }
        }else if (ntrk1 == 3) {
         float OSSSFactor = isLQ3 ? 0.709497892271 : 0.714655;
          if(sysStr == "OSSS__1up"){
            fWeight *= OSSSFactor;
          } else if ( sysStr == "OSSS__1down") {
            fWeight *= 1+fabs((1-OSSSFactor));
          }
        }
      } else if (ntrk == 3) {
        if(ntrk1 == 1) {
         float OSSSFactor = isLQ3 ? 1.13089765369 : 1.05198;
          if(sysStr == "OSSS__1up"){
            fWeight *= OSSSFactor;
          } else if ( sysStr == "OSSS__1down") {
            fWeight *= 1-fabs((1-OSSSFactor));
          }
        }else if (ntrk1 == 3) {
         float OSSSFactor = isLQ3 ? 0.583393098905 : 0.538265;
          if(sysStr == "OSSS__1up"){
            fWeight *= OSSSFactor;
          } else if ( sysStr == "OSSS__1down") {
            fWeight *= 1+fabs((1-OSSSFactor));
          }
        }
      }

      fWeight *= TauFakeCompWeight(finalTaus); 
      if (fWeight<0) fWeight = 0.;
      tauw *= fWeight;
    } 
    m_tauFakeFactor *= tauw;
    m_weight *= m_tauFakeFactor;

    m_histNameSvc->set_sample("fakes");
    if (m_region == "AA_OS" || m_region == "MA_OS" || m_region == "AM_OS") m_histNameSvc->set_description("MM_OS");
    else if (m_region == "AA_SS" || m_region == "MA_SS" || m_region == "AM_SS") m_histNameSvc->set_description("MM_SS");

  } else {
    //const int NBinspT1=8;
    const int NBinspT2=6;
    //float FF_pT1_Edges[NBinspT1+1]={40.0,50.0,60.0,70.0,80.0,100.0,140.0,240.0,1000.0};
    float FF_pT2_Edges[NBinspT2+1]={20.0,30.0,40.0,50.0,60.0,100.0,1000.0};

    bool useNTracksInfo=true;
    if(useNTracksInfo){
      m_2DFF = (TH2F*)m_FF_File->Get(("fakes_"+region).c_str());
      if(m_debug) std::cout << "Getting 2D hist fakes for region " << region << std::endl;
    }else{ m_2DFF = (TH2F*)m_FF_File->Get(("fakes_"+region).c_str());}

    bool norder=true;
    float x,y;

    float tauPt1=finalTaus.at(0)->p4().Pt()*1e-3;
    float tauPt2=finalTaus.at(1)->p4().Pt()*1e-3;

    if(tauPt1< tauPt2) norder=false;
    
    if(norder) {
       x=tauPt1;
       y=tauPt2;
    }else{
       x=tauPt2;
       y=tauPt1;
    }

    Int_t binx = 0;
    if(m_debug) std::cout << "x (Pt_1): " << x << std::endl;
    binx=m_2DFF->GetXaxis()->FindBin(x);
    if(m_debug) std::cout << "x Bin: " << binx << std::endl;

    float fakef=1.0;
    for(int j=0;j<NBinspT2+1;j++){
       if(y > FF_pT2_Edges[j] && y< FF_pT2_Edges[j+1]){
          fakef=m_2DFF->GetBinContent(binx,j+1);
          //m_subtract = m_mcBkgA->GetBinContent(binx,j+1);
          if(m_debug) std::cout << "Fake factor: " << fakef << std::endl;
       }
    }

    m_weight *= fakef;

    if(m_isMC) m_weight *= -1;

    m_histNameSvc->set_sample("fakes");
    if(m_region == "AA_OS") m_histNameSvc->set_description("MM_OS");
    else if(m_region == "AA_SS") m_histNameSvc->set_description("MM_SS");

  }

  return EL::StatusCode::SUCCESS;

}


bool AnalysisReader_hhbbtt::pass_DT_cuts(const xAOD::DiTauJet* ditau, double BDTDiTauIDCut)
{
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must have at least two sub-jets"<<std::endl;
  if(Props::n_subjets.get(ditau) < 2) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must have less than four sub-jets"<<std::endl;
  if(Props::n_subjets.get(ditau) > 3) return false;
  
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must pass ID cut ( BDTscore > " << BDTDiTauIDCut << " )" <<std::endl;
  if (Props::BDTScore.get(ditau) < BDTDiTauIDCut) return false;

  TLorentzVector subjet0;
  TLorentzVector subjet1;
  subjet0.SetPtEtaPhiE( Props::subjet_lead_pt.get(ditau),  Props::subjet_lead_eta.get(ditau), 
                        Props::subjet_lead_phi.get(ditau), Props::subjet_lead_e.get(ditau));
  subjet1.SetPtEtaPhiE( Props::subjet_subl_pt.get(ditau),  Props::subjet_subl_eta.get(ditau), 
                        Props::subjet_subl_phi.get(ditau), Props::subjet_subl_e.get(ditau));
  TLorentzVector DT_here = subjet0 + subjet1;                        

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must be in valid eta region" <<std::endl;
  if((fabs(DT_here.Eta()) >= 1.37 && fabs(DT_here.Eta()) <= 1.52) || fabs(DT_here.Eta()) >= 2.0) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate subjets must be contained in large-R jet area " << std::endl;
  if ( subjet0.DeltaR(subjet1) > 0.8 ) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must have two 1- or 3-prong subjets" <<std::endl;
  int subjet_lead_ntracks = Props::subjet_lead_ntracks.get(ditau);
  int subjet_subl_ntracks = Props::subjet_subl_ntracks.get(ditau);
  int subjet_lead_charge = Props::subjet_lead_charge.get(ditau);
  int subjet_subl_charge = Props::subjet_subl_charge.get(ditau);
  if (subjet_lead_ntracks != 1 && subjet_lead_ntracks != 3) return false;
  if (subjet_subl_ntracks != 1 && subjet_subl_ntracks != 3) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must be OS or SS" <<std::endl;
  if (fabs(subjet_lead_charge*subjet_subl_charge) != 1) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must have subjet pT > 50 GeV"<<std::endl;
  if (Props::subjet_subl_pt.get(ditau)/1e3 < 50) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> DiTau candidate must have substructure pT > 300 GeV"<<std::endl;
  if ((subjet0 + subjet1).Pt()/1e3 < 300) return false;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::pass_DT_cuts() INFO       --> Selecting DiTau candidate"<<std::endl;

  return true;
}


EL::StatusCode AnalysisReader_hhbbtt::select_DT(std::vector<const xAOD::DiTauJet*> diTaus, double BDTDiTauIDCut)
{ 
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_DT() INFO       Selecting m_SelDT..."<<std::endl;

  int nDiTaus = 0;
  for(unsigned int it = 0 ; it < diTaus.size(); ++it)
  {
  
    if ( !pass_DT_cuts(diTaus.at(it), BDTDiTauIDCut) ) continue;
    ++nDiTaus;
    m_SelDT = diTaus.at(it);
    m_SelDTChargeProd = Props::subjet_lead_charge.get(m_SelDT) * Props::subjet_subl_charge.get(m_SelDT);

    break; // avoid selecting another ditau
  }

  if(m_debug && m_SelDT != NULL) std::cout<<"AnalysisReader_hhbbtt::select_DT() INFO       Found good ditau with pt= " << m_SelDT->pt()/1e3 << " GeV, BDTScore= " << Props::BDTScore.get(m_SelDT) << ", q=" << m_SelDTChargeProd <<std::endl;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::select_fakeDT(std::vector<const xAOD::DiTauJet*> diTaus, double BDTDiTauIDCut)
{ 
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_fakeDT() INFO       Selecting m_SelDT..."<<std::endl;
  
  double maxBDTScore = -100.0;
  int DT_index = -1;
  for(unsigned int it = 0 ; it < diTaus.size(); ++it)
  {
    if ( !pass_DT_cuts(diTaus.at(it), BDTDiTauIDCut) ) continue;

    if(Props::BDTScore.get(diTaus.at(it)) > maxBDTScore)
    {
      maxBDTScore = Props::BDTScore.get(diTaus.at(it)); 
      DT_index = it;
    }
  }
  if(maxBDTScore > 0) 
  {
    m_SelDT = diTaus[DT_index];
    m_SelDTChargeProd = Props::subjet_lead_charge.get(m_SelDT) * Props::subjet_subl_charge.get(m_SelDT);
  }
  
  if(m_debug && m_SelDT != NULL) std::cout<<"AnalysisReader_hhbbtt::select_fakeDT() INFO       Found fake ditau with pt= " << m_SelDT->pt()/1e3 << " GeV, eta= " << m_SelDT->eta() << " and BDTScore= " << Props::BDTScore.get(m_SelDT) <<std::endl;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_hhbbtt::select_FJ(std::vector<const xAOD::Jet*> fatJets, std::string Xbb_wp)
{ 
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////// select 2tag FJ if any, only from the two leading fat-jets (always true for signal) //
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  
  int FJindex = -1;
  std::vector<const xAOD::Jet*> myFatJets; // will include only fat-jets with pT > 300 GeV and eta < 2.0

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> Create new list of FatJets, include only fat-jets pt>300 GeV and eta<2.0"<<std::endl;
  for(unsigned int ijet(0); ijet <fatJets.size(); ++ijet)
  {
    if(ijet == 0) m_histSvc -> BookFillHist("TriggerCheckLeadingFatJet_pT",  20, -0.5, 1000, fatJets[ijet]->p4().Pt()/1e3,  m_weight);
    if(fatJets[ijet]->p4().Pt()/1e3 < 300.0 && fabs(fatJets[ijet]->p4().Eta()) > 2.0) continue;
    myFatJets.push_back(fatJets.at(ijet));
  }
  if(myFatJets.size() < 1) return EL::StatusCode::SUCCESS;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> Leading FJ must have pt > 450 GeV"<<std::endl;
  if(myFatJets[0]->p4().Pt()/1e3 < 450.0) return EL::StatusCode::SUCCESS;
  
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> Is one of the leading two fat-jets 2tag? "<<std::endl;
  int FJ_multiplicity = 0;
  int FJ1tag_multiplicity = 0;
  int FJ2tag_multiplicity = 0;
  for(unsigned int ijet(0); ijet <myFatJets.size(); ++ijet)
  {
    // skip those FJs matched to the diTau
    if(m_SelDT->p4().DeltaR(myFatJets.at(ijet)->p4()) < 1.0) continue;

    ++FJ_multiplicity;
    if(Props::xbbResult_1tagNoMcut.get(myFatJets[ijet]) == 1) ++FJ1tag_multiplicity;
    if(Props::xbbResult_2tagNoMcut.get(myFatJets[ijet]) == 1) ++FJ2tag_multiplicity;

    if(ijet < 2){ // it has to be one of the two leading, the leading has the priority
      if(Props::xbbResult_2tagNoMcut.get(myFatJets[ijet]) == 1){ 
        m_SelFJ = myFatJets[ijet];
        m_FJNbtag = 2;
        FJindex = ijet;
        break;
      }
    }
  }

  if(m_debug &&  m_FJNbtag == 2) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 2tag found, fat-jet index: "<<FJindex<<std::endl;
  if(m_debug && !(m_FJNbtag == 2)) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 2tag not found"<<std::endl; 

  if (m_FJNbtag == 2)
    return EL::StatusCode::SUCCESS;
    
  //////////////////////////////////////////////////////////////////
  //////////////      if !2tag, select 1tag       //////////////////     
  //////////////////////////////////////////////////////////////////
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       Looking for 1b-tagged fat-jet, since there was no 2tag "<<std::endl;

  for(unsigned int ijet(0); ijet <myFatJets.size(); ++ijet)
  {
    // skip those FJs matched to the diTau
    if(m_SelDT->p4().DeltaR(myFatJets.at(ijet)->p4()) < 1.0) continue;

    if(ijet < 2){ // it has to be one of the two leading, the leading has the priority
      if(Props::xbbResult_1tagNoMcut.get(myFatJets[ijet]) == 1){ 
        m_SelFJ = myFatJets[ijet];
        m_FJNbtag = 1;
        FJindex = ijet;
        break;
      }
    }
  }

  if(m_debug &&  m_FJNbtag == 1) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 1tag found, fat-jet index: "<<FJindex<<std::endl;
  if(m_debug && !(m_FJNbtag == 1)) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 1tag not found"<<std::endl;

  if (m_FJNbtag == 1)  
    return EL::StatusCode::SUCCESS;


  ////////////////////////////////////////////////////////////////////////
  //////////////      select non b-tagged fat-jet       //////////////////     
  ////////////////////////////////////////////////////////////////////////
  
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       Selecting non b-tagged fat-jet"<<std::endl;

  for(unsigned int ijet(0); ijet <myFatJets.size(); ++ijet)
  {
    // skip those FJs matched to the diTau
    if(m_SelDT->p4().DeltaR(myFatJets.at(ijet)->p4()) < 1.0) continue;

    if(ijet < 2) // it has to be one of the two leading, the leading has the priority
    { 
      int xbbreturnval = Props::xbbResult_1tagNoMcut.get(myFatJets[ijet]);
      if(xbbreturnval == 0)
      {
        m_SelFJ = myFatJets[ijet];
        m_FJNbtag = 0;
        FJindex = ijet;
        break;
      }
    }
  }

  if(m_debug &&  m_FJNbtag == 0) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 0tag found, fat-jet index: "<<FJindex<<std::endl;
  if(m_debug && !m_FJNbtag == 0) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> 0tag not found"<<std::endl;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_hhbbtt::select_FJ_VRbtagged(std::vector<const xAOD::Jet*> fatJets)
{ 
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////// select b-tagged FJ if any                                                          //
  ////////////////////////////////////////////////////////////////////////////////////////////////////
    
  std::vector<const xAOD::Jet*> myFatJets; // will include only fat-jets with pT > 300 GeV and eta < 2.0 and dR(FJ,DT)>1 

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> Create new list of FatJets, include only fat-jets pt>300 GeV and eta<2.0"<<std::endl;
  for(unsigned int ijet(0); ijet <fatJets.size(); ++ijet)
  {
    if(m_SelDT->p4().DeltaR(fatJets.at(ijet)->p4()) < 1.0) continue;
    if(fatJets.at(ijet)->p4().Pt()/1e3 < 300.0) continue;
    if(fabs(fatJets.at(ijet)->p4().Eta()) > 2.0) continue;
    myFatJets.push_back(fatJets.at(ijet));
  }
  if(myFatJets.size() < 1) return EL::StatusCode::SUCCESS;

  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::select_FJ() INFO       --> Is the leading fat-jet 2tag/1tag/0tag? "<<std::endl;

  if(myFatJets.at(0)->p4().M()/1e3 < 50.0) return EL::StatusCode::SUCCESS;

  m_SelFJ = myFatJets.at(0);
  
  return EL::StatusCode::SUCCESS;
}


std::vector<const xAOD::Jet*> AnalysisReader_hhbbtt::select_FJs_4b(std::vector<const xAOD::Jet*> fatJets)
{
  // for 4b orthogonality
  std::vector<const xAOD::Jet*> myFatJets; // will include only fat-jets with pT > 300 GeV and eta < 2.0 and dR(FJ,SelFJ)>0.1 
  for(unsigned int ijet(0); ijet <fatJets.size(); ++ijet)
  {
    if(m_SelFJ->p4().DeltaR(fatJets.at(ijet)->p4()) < 0.1) continue;
    //TODO: angular
    if(fatJets.at(ijet)->p4().Pt()/1e3 < 300.0) continue;
    if(fabs(fatJets.at(ijet)->p4().Eta()) > 2.0) continue;
    myFatJets.push_back(fatJets.at(ijet));
    if(myFatJets.size()>1) break;
  }

  return myFatJets;
}


EL::StatusCode AnalysisReader_hhbbtt::fill_cutflow_boosted(std::string cutname)
{
  std::vector<string> cutname_vec;
  cutname_vec.push_back("Initial");
  cutname_vec.push_back("10GeVMETCut");
  cutname_vec.push_back("SelectDTandFJ");
  cutname_vec.push_back("*PreSel(0tag)");
  cutname_vec.push_back("*PreSel(1tag)");
  cutname_vec.push_back("*PreSel(2tag)");
  cutname_vec.push_back("*ZCR(0tag)");
  cutname_vec.push_back("*SR(1tag)");
  cutname_vec.push_back("*SR(2tag)");
  cutname_vec.push_back("*QCDCR(0tag)");
  cutname_vec.push_back("*QCDCR(1tag)");
  cutname_vec.push_back("*QCDCR(2tag)");

  
  int nbin_here = cutname_vec.size();
  double top_edge = (double)cutname_vec.size() + 0.5;
  
  for(unsigned int cf = 0; cf < cutname_vec.size(); cf++){
    double value_here = (double)cf + 1.0;
    if(cutname_vec[cf] == cutname) m_histSvc->BookFillHist("cutflow_sumofweights", nbin_here, 0.5, top_edge , value_here, m_weight);
    if(cutname_vec[cf] == cutname) m_histSvc->BookFillHist("cutflow_nevents", nbin_here, 0.5, top_edge , value_here, 1.0);
  }
  return EL::StatusCode::SUCCESS;
  
}

EL::StatusCode AnalysisReader_hhbbtt::calculateFF_boosted()
{ 
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::calculateFF_boosted() INFO Filling histos needed to derive the FFs"<<std::endl;
  
  // define diTau here
  double pT_here = m_SelDTsubjets.Pt()/1e3;
  double pT_subl = m_SelDTsubj1.Pt()/1e3;
  double eta_here = m_SelDTsubjets.Eta();
  if (pT_here <= 300) pT_here = 305;
  if (pT_here >= 1500) pT_here = 1495;
    
  float dPhiDTMET = fabs(m_SelDTsubjets.DeltaPhi(m_METVec));
  int ntracks = Props::subjet_lead_ntracks.get(m_SelDT)+Props::subjet_subl_ntracks.get(m_SelDT);
  
  std::string Ntag = "";
  std::string passORfail = "";
  std::string histname = "";
  std::string dPhiRegion = "";
  std::string EtaRegion = "";
  std::string Nsubjets = "";
  std::string LeadingJet = "";
  std::string DTSign = "";
  std::string DTNtracks = std::to_string(ntracks)+"tracks";
  std::string DTNtracks2 = "l"+std::to_string(Props::subjet_lead_ntracks.get(m_SelDT))+"s"+std::to_string(Props::subjet_subl_ntracks.get(m_SelDT))+"tracks";
  std::string SublTauPt = "";
  
  if(      m_FJNbtag == 0)   Ntag = "0tag";
  else if( m_FJNbtag == 1)   Ntag = "1tag";
  else if( m_FJNbtag == 2)   Ntag = "2tag";
  if(     !m_isFakes)      passORfail = "pass";
  else if( m_isFakes)      passORfail = "fail";
  if(      dPhiDTMET <= 1.0) dPhiRegion = "SR";
  else if( dPhiDTMET >= 2.1) dPhiRegion = "FFR";
  if(fabs(eta_here) < 1.37) EtaRegion = "EtaCentral";
  if(fabs(eta_here) > 1.52) EtaRegion = "EtaForward";
  if(Props::n_subjets.get(m_SelDT) == 2) Nsubjets = "2subjets";
  else if(Props::n_subjets.get(m_SelDT) == 3) Nsubjets = "3subjets";
  if (m_SelDT->p4().Pt() > m_SelFJ->p4().Pt()) LeadingJet = "LeadingDT";
  else LeadingJet = "LeadingFJ";
  if (m_SelDTChargeProd == -1) DTSign = "OS";
  else if (m_SelDTChargeProd == 1) DTSign = "SS";
  else std::cout << "ERROR found DT with q = " << m_SelDTChargeProd << std::endl;
  if (pT_subl < 100) SublTauPt = "LowSublPt";
  else if (pT_subl < 200) SublTauPt = "MediumSublPt";
  else SublTauPt = "HighSublPt";
  
  if(EtaRegion == "") return EL::StatusCode::SUCCESS;
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::calculateFF_boosted() INFO Region: "<<Ntag<<" "<<passORfail<<" "<<dPhiRegion<<" "<<EtaRegion<<std::endl;
  
  if(m_isMC) m_weight *= -1.0; // subtract MC
  m_histNameSvc->set_description("FFcalc");
  
  if(m_debug) std::cout<<"AnalysisReader_hhbbtt::calculateFF_boosted() INFO pt: " <<pT_here<<" weight: "<<m_weight<<std::endl;
  if(dPhiRegion != ""){
    m_histNameSvc->set_description(("FFcalc_dPhi" + dPhiRegion).c_str());
      
    histname = "Pt_" + Ntag + passORfail;
    m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
    
    histname = "Pt_" + EtaRegion + "_" + Ntag + passORfail;
    m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
    
    histname = "Eta_" + Ntag + passORfail;
    m_histSvc->BookFillHist((histname).c_str(), 60, -3, 3, eta_here, m_weight);
    
    histname = "dPhiDTMET_" + Ntag + passORfail;
    m_histSvc->BookFillHist((histname).c_str(), 11, 2.1, 3.2, dPhiDTMET, m_weight);
    
    histname = "PtEta_" + Ntag + passORfail;
    m_histSvc->BookFillHist((histname).c_str(), 120,300,1500,  60,-3,3, pT_here, eta_here, m_weight);
  }
  m_histNameSvc->set_description("FFcalc");
  
  histname = "Pt_" + EtaRegion + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + Nsubjets + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + LeadingJet + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + DTSign + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + DTSign + "_" + DTNtracks + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + DTSign + "_" + DTNtracks2 + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + EtaRegion + "_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + SublTauPt + "_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + DTSign + "_" + DTNtracks + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + DTSign + "_" + DTNtracks2 + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "Pt_" + DTSign + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 300, 1500, pT_here, m_weight);
  
  histname = "PtSubl_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 0, 600, m_SelDTsubj1.Pt()/1000, m_weight);
  
  histname = "PtLead_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 0, 600, m_SelDTsubj0.Pt()/1000, m_weight);
  
  histname = "PtRatio_" + DTSign + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 120, 0, 1.2, m_SelDTsubj1.Pt()/m_SelDTsubj0.Pt(), m_weight);
  
  histname = "dPhiDTMET_" + EtaRegion + "_" + Ntag + passORfail;
  m_histSvc->BookFillHist((histname).c_str(), 32, 0, 3.2, dPhiDTMET, m_weight);

  if(m_isMC) m_weight *= -1.0; // set MC m_weight to previous value
  m_histNameSvc->set_description("SR");

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::applyZhfSF_boosted()
{
  std::string name;
  name = m_histNameSvc->getFullSample();
  if (!(name=="Zbb"||name=="Zbc"||name=="Zbl"||name=="Zcc")) 
    return EL::StatusCode::SUCCESS;
  double ZhfSF_value(1.), ZhfSF_error(0.);
  m_config->getif<double>("ZhfSF_value", ZhfSF_value);
  m_config->getif<double>("ZhfSF_error", ZhfSF_error);
  if (m_currentVar == "ZhfSF__1up") ZhfSF_value += ZhfSF_error;
  else if (m_currentVar == "ZhfSF__1down") ZhfSF_value -= ZhfSF_error;
  if (m_debug) std::cout << "AnalysisReader_hhbbtt::applyZhfSF_boosted() INFO     Sample, ZhfSF ==> " 
                         << name << ", " << ZhfSF_value << std::endl;
                         
  m_weight *= ZhfSF_value;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::applyDiTauSF_boosted()
{
  if (! m_isMC) return EL::StatusCode::SUCCESS;
  if (m_isFakes) return EL::StatusCode::SUCCESS;
  if (! Props::TruthMatch.get(m_SelDT)) return EL::StatusCode::SUCCESS;

  double sf = 1.;
  double deltaSf_stat_up = 0.;
  double deltaSf_stat_down = 0.;
  double deltaSf_syst_up = 0.;
  double deltaSf_syst_down = 0.;

  m_config->getif<double>("DiTauSF", sf);
  m_config->getif<double>("DiTauSF_error_stat_up", deltaSf_stat_up);
  m_config->getif<double>("DiTauSF_error_stat_down", deltaSf_stat_down);
  m_config->getif<double>("DiTauSF_error_syst_up", deltaSf_syst_up);
  m_config->getif<double>("DiTauSF_error_syst_down", deltaSf_syst_down);

  if (m_currentVar == "DiTauSF_Stat__1up") sf += deltaSf_stat_up;
  if (m_currentVar == "DiTauSF_Stat__1down") sf -= deltaSf_stat_down;
  if (m_currentVar == "DiTauSF_Syst__1up") sf += deltaSf_syst_up;
  if (m_currentVar == "DiTauSF_Syst__1down") sf -= deltaSf_syst_down;

  m_weight *= sf;

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit()
{
    if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit() INFO     Applying FFs"<<std::endl;
    TLorentzVector DT_here = m_SelDTsubjets;
    // TLorentzVector DT_here = m_SelDT->p4();
    double pT_here = DT_here.Pt()/1e3;
    double ff_value = 0.0;
    if(pT_here <= 300.0) pT_here = 305.0;
    if(pT_here >= 1500.0) pT_here = 1495.0;         // still apply the FF from the last bin   

    if(m_FJNbtag == 0) return EL::StatusCode::SUCCESS;
    
    if(fabs(DT_here.Eta()) < 1.37){
      if(m_FJNbtag == 1){
        int binnumx = m_1DFF_0tagEtaCentral_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_0tagEtaCentral_boosted->GetBinContent(binnumx);
        //ff_value *= m_overallFFperRegion->GetBinContent(5)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 1tag SR
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      if(m_FJNbtag == 2){
        int binnumx = m_1DFF_0tagEtaCentral_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_0tagEtaCentral_boosted->GetBinContent(binnumx);
        //ff_value *= m_overallFFperRegion->GetBinContent(5)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 1tag SR   (FFR -> SR)
        //ff_value *= m_overallFFperRegion->GetBinContent(3)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 2tag FFR  (1tag -> 2tag)
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
    }
    if(fabs(DT_here.Eta()) > 1.52){
      if(m_FJNbtag == 1){
        int binnumx = m_1DFF_0tagEtaForward_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_0tagEtaForward_boosted->GetBinContent(binnumx);
        //ff_value *= m_overallFFperRegion->GetBinContent(5)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 1tag SR
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      if(m_FJNbtag == 2){
        int binnumx = m_1DFF_0tagEtaForward_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_0tagEtaForward_boosted->GetBinContent(binnumx);
        //ff_value *= m_overallFFperRegion->GetBinContent(5)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 1tag SR   (FFR -> SR)
        //ff_value *= m_overallFFperRegion->GetBinContent(3)/m_overallFFperRegion->GetBinContent(2); // 1tag FFR ----> 2tag FFR  (1tag -> 2tag)
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_NtagSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
    }
    
    m_weight *= ff_value;
    if(m_isMC) m_weight *= -1; // we need to subtract MC from data

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_hhbbtt::applyFF_boosted()
{
    if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted() INFO     Applying FFs"<<std::endl;
    double pT_here = m_SelDTsubjets.Pt()/1e3;
    double ff_value = 0.0;
    if(pT_here <= 300.0) pT_here = 305.0;
    if(pT_here >= 1400.0) pT_here = 1395.0;         // still apply the FF from the last bin   

    if(m_SelDTChargeProd != -1) return EL::StatusCode::SUCCESS;  // apply only to OS di-taus

    // std::string sHistName = "h_FF_Ptbinned_SS";
    std::string sHistName = "h_FF_Ptbinned_SS_0tag";
    if (m_currentVar != "Nominal")
    {
      if (m_BoostedFFMap.count(sHistName+m_currentVar) != 0)
      {
        sHistName = sHistName+m_currentVar;
      }
    }
    if(m_debug) std::cout << "AnalysisReader_hhbbtt::applyFF_boosted() variation: " << m_currentVar << ", get FF histogram: " << sHistName << std::endl;
    TH1* hFF = m_BoostedFFMap[sHistName];

    int iBin = hFF->GetXaxis()->FindBin(pT_here);
    ff_value = hFF->GetBinContent(iBin);

    if (m_currentVar == "FF_Stat__1up")
    {
      ff_value += hFF->GetBinErrorUp(iBin);
    }
    else if (m_currentVar == "FF_Stat__1down")
    {
      ff_value -= hFF->GetBinErrorLow(iBin);
    }
    else if (m_currentVar == "FF_Transition_Btag__1up")
    {
      TH1* hFF0tag = m_BoostedFFMap["h_FF_Ptbinned_SS_0tag_onebin"];
      TH1* hFF1tag = m_BoostedFFMap["h_FF_Ptbinned_SS_1tag_onebin"];
      ff_value += std::fabs(hFF0tag->GetBinContent(1) - hFF1tag->GetBinContent(1));
    }
    else if (m_currentVar == "FF_Transition_Btag__1down")
    {
      TH1* hFF0tag = m_BoostedFFMap["h_FF_Ptbinned_SS_0tag_onebin"];
      TH1* hFF1tag = m_BoostedFFMap["h_FF_Ptbinned_SS_1tag_onebin"];
      ff_value -= std::fabs(hFF0tag->GetBinContent(1) - hFF1tag->GetBinContent(1));
    }
    else if (m_currentVar == "FF_Transition_Sign__1up")
    {
      TH1* hFFOS = m_BoostedFFMap["h_FF_Ptbinned_OS"];
      ff_value += std::fabs(ff_value - hFFOS->GetBinContent(iBin));
    }
    else if (m_currentVar == "FF_Transition_Sign__1down")
    {
      // TH1* hFFOS = m_BoostedFFMap["h_FF_Ptbinned_OS"];
      TH1* hFFOS = m_BoostedFFMap["h_FF_Ptbinned_OS_0tag"];
      ff_value -= std::fabs(ff_value - hFFOS->GetBinContent(iBin));
    }

    if(m_debug) std::cout << "AnalysisReader_hhbbtt::applyFF_boosted() pT_here: " << pT_here << " var: " << m_currentVar << " ff: " << ff_value << std::endl;

    m_weight *= ff_value;
    if(m_isMC) m_weight *= -1; // we need to subtract MC from data

    return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit()
{
    if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs"<<std::endl;
    TLorentzVector DT_here = m_SelDTsubj0 + m_SelDTsubj1;
    // TLorentzVector DT_here = m_SelDT->p4();
    double pT_here = DT_here.Pt()/1e3;
    int ntracks = Props::subjet_lead_ntracks.get(m_SelDT)+Props::subjet_subl_ntracks.get(m_SelDT);

    double ff_value = 0.0;
    double ff_value_1up = 0.0;
    double ff_value_1down = 0.0;
    if(pT_here <= 300.0) pT_here = 305.0;
    if(pT_here >= 1500.0) pT_here = 1495.0;         // still apply the FF from the last bin   

    if(m_SelDTChargeProd != -1) return EL::StatusCode::SUCCESS;  // apply only to OS di-taus
    if (ntracks != 2 && ntracks != 4 && ntracks != 6) return EL::StatusCode::FAILURE; // only values that would make sense for SS/OS di-taus
    
    if(fabs(DT_here.Eta()) < 1.37)
    {
      if (ntracks == 2)
      {
        int binnumx = m_1DFF_SS_2tracks_EtaCentral_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_2tracks_EtaCentral_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_2tracks_EtaCentral_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_2tracks_EtaCentral_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      else if (ntracks == 4)
      {
        int binnumx = m_1DFF_SS_4tracks_EtaCentral_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_4tracks_EtaCentral_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_4tracks_EtaCentral_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_4tracks_EtaCentral_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      else if (ntracks == 6)
      {
        int binnumx = m_1DFF_SS_6tracks_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_6tracks_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_6tracks_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_6tracks_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
    }
    if(fabs(DT_here.Eta()) > 1.52)
    {
      if (ntracks == 2)
      {
        int binnumx = m_1DFF_SS_2tracks_EtaForward_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_2tracks_EtaForward_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_2tracks_EtaForward_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_2tracks_EtaForward_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      else if (ntracks == 4)
      {
        int binnumx = m_1DFF_SS_4tracks_EtaForward_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_4tracks_EtaForward_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_4tracks_EtaForward_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_4tracks_EtaForward_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
      else if (ntracks == 6)
      {
        int binnumx = m_1DFF_SS_6tracks_boosted->GetXaxis()->FindBin(pT_here);
        ff_value = m_1DFF_SS_6tracks_boosted->GetBinContent(binnumx);
        ff_value_1up = ff_value + m_1DFF_SS_6tracks_boosted->GetBinErrorUp(binnumx);
        ff_value_1down = ff_value - m_1DFF_SS_6tracks_boosted->GetBinErrorLow(binnumx);
        if(m_debug) std::cout<<"AnalysisReader_hhbbtt::applyFF_boosted_SignNtrackSplit() INFO     Applying FFs: diTaupT: "<<DT_here.Pt()*0.001<<", ntracks:"<<ntracks<<", |eta|: "<<fabs(DT_here.Eta())<<" FF: "<<ff_value<<std::endl;
      }
    }

    if (m_currentVar == "FF_DKIR__1up") ff_value = ff_value_1up;
    else if (m_currentVar == "FF_DKIR__1down") ff_value = ff_value_1down;
    
    m_weight *= ff_value;
    if(m_isMC) m_weight *= -1; // we need to subtract MC from data

    return EL::StatusCode::SUCCESS;
}


void AnalysisReader_hhbbtt::fill_triggerPlots()
{
    std::vector<float> L1_EMPhi  = Props::L1_EMPhi.get(m_eventInfo);
    std::vector<float> L1_EMEt   = Props::L1_EMEt.get(m_eventInfo);
    std::vector<float> L1_EMEta  = Props::L1_EMEta.get(m_eventInfo);
    std::vector<float> L1_JetEta = Props::L1_JetEta.get(m_eventInfo);
    std::vector<float> L1_JetPt  = Props::L1_JetPt.get(m_eventInfo);
    std::vector<float> L1_JetPhi = Props::L1_JetPhi.get(m_eventInfo);

    std::vector<float> tauPt;
    std::vector<float> tauPhi;
    std::vector<float> tauEta;

    std::vector<float> jetPt = L1_JetPt;

    std::vector<float> tauPtUnsorted;

    for (size_t j = 0; j < jetPt.size(); j++) {
      std::cout << "L1_JetPt: " << jetPt.at(j) << " L1_JetEta: " << L1_JetEta.at(j) << " L1_JetPhi: "<< L1_JetPhi.at(j)<< std::endl;
    }
    //std::cout << "here again" << std::endl;
    for (size_t j = 0; j < L1_EMEt.size(); j++) {
      if((L1_EMPhi.at(j)==0) && (L1_EMEt.at(j)==0) && (L1_EMEta.at(j)==0)) continue;
      tauPt.push_back(L1_EMEt.at(j));
      tauPhi.push_back(L1_EMPhi.at(j));
      tauEta.push_back(L1_EMEta.at(j));
      tauPtUnsorted.push_back(L1_EMEt.at(j));
      std::cout << "L1_TauPt: " << L1_EMEt.at(j) << " L1_TauEta: " << L1_EMEta.at(j) << " L1_TauPhi: "<< L1_EMPhi.at(j)<< std::endl;
    }

    if(tauPhi.size()<2 && L1_JetPhi.size()<2) return;

    float deltaR=9999.9;
    //int j=0;

    std::cout << "L1PtJet size before " << L1_JetPt.size() << std::endl;

    for (size_t k = 0; k < tauPt.size(); k++) {
      //std::cout << "tauPt: " << tauPt.at(k) << " tauPhi: " << tauPhi.at(k) << " tauEta: " << tauEta.at(k) << std::endl;
      //for(vector<float>::iterator it = L1_JetPt.begin(); it != L1_JetPt.end(); ) {
      for (size_t j = 0; j < L1_JetPt.size(); j++){
        //std::cout << "L1_JetPt: " << L1_JetPt.at(j) << " L1_JetPhi: " << L1_JetPhi.at(j) << " L1_JetEta: " << L1_JetEta.at(j) << std::endl;
        deltaR=sqrt(pow(L1_JetPhi.at(j)-tauPhi.at(k),2)+pow(L1_JetEta.at(j)-tauEta.at(k),2));
        //std::cout << "Delta R: " << deltaR << " (k,j) " << k << " , " << j << std::endl;
        if(deltaR<0.4) {
          std::cout << "Ovelap found with Delta R: " << deltaR << std::endl;
          std::cout << "Matched jet pT: " << L1_JetPt.at(j) << " jetPt size: " << jetPt.size() << std::endl;
          if(jetPt.size() ==0) break;
          for(vector<float>::iterator it = jetPt.begin(); it != jetPt.end(); ) {
             //std::cout << "just erasing " << std::endl;
             if(*it == L1_JetPt.at(j)) it = jetPt.erase(it);
             else ++it;
          }
	}
      }
      if(jetPt.size() ==0) break;
      //++it;
    }

    std::cout << "L1PtJet size after " << jetPt.size() << " TauPt size: " << tauPt.size() << std::endl;

    std::sort(tauPt.begin(),tauPt.end());

    std::vector<int> tau_index, jet_index;
    vector<float>::iterator it1;

    for(int j = tauPt.size()-1; j >= (int) tauPt.size()-2;j--) {
      if(j<0) {/*std::cout << "j: " << j << std::endl;*/ break;}
      std::cout << "tauPt: " << tauPt.at(j) << " j: " << j <<std::endl;
      it1=find(tauPtUnsorted.begin(),tauPtUnsorted.end(),tauPt.at(j));
      int pos = distance(tauPtUnsorted.begin(), it1);
      tau_index.push_back(pos);
      std::cout << "Position found at: " << pos << std::endl;
    }

    //std::cout << "After tau loop" << std::endl;
    int tauv_size=tauPt.size()-1;
    m_histSvc->BookFillHist("L1_lead_tau_pt",50,0,250,tauPt.at(tauv_size)*1e-3, 1.0);
    m_histSvc->BookFillHist("L1_sublead_tau_pt",50,0,250,tauPt.at(tauv_size-1)*1e-3, 1.0);

    TVector2 tau0(tauEta.at(tau_index.at(0)),tauPhi.at(tau_index.at(0)));
    TVector2 tau1(tauEta.at(tau_index.at(1)),tauPhi.at(tau_index.at(1)));
    float dPhi=tau0.DeltaPhi(tau1);
    float dR=(tau0-tau1).Mod();
    m_histSvc->BookFillHist("L1_dphitautau",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    m_histSvc->BookFillHist("L1_dRtautau",25,0,5,dR, 1.0);

    if(jetPt.size()>1){
      std::sort(jetPt.begin(),jetPt.end());
      for(int j = jetPt.size()-1; j >= (int) jetPt.size()-2; j--) {
         if(j<0) {/*std::cout << "j: " << j << std::endl;*/ break;}
         std::cout << "jetPt: " << jetPt.at(j) << std::endl;
         it1=find(L1_JetPt.begin(),L1_JetPt.end(),jetPt.at(j));
         int pos = distance(L1_JetPt.begin(), it1);
         jet_index.push_back(pos);
         std::cout << "jet Position found at: " << pos << std::endl;
      }
    }else if(jetPt.size()<2) return;

    int jetv_size=jetPt.size()-1;
    m_histSvc->BookFillHist("L1_lead_jet_pt",50,0,250,jetPt.at(jetv_size)*1e-3, 1.0);
    m_histSvc->BookFillHist("L1_sublead_jet_pt",50,0,250,jetPt.at(jetv_size-1)*1e-3, 1.0);

    TVector2 jet0(L1_JetEta.at(jet_index.at(0)),L1_JetPhi.at(jet_index.at(0)));
    TVector2 jet1(L1_JetEta.at(jet_index.at(1)),L1_JetPhi.at(jet_index.at(1)));

    dPhi=jet0.DeltaPhi(jet1);
    dR=(jet0-jet1).Mod();
    m_histSvc->BookFillHist("L1_dphijetjet",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    m_histSvc->BookFillHist("L1_dRjetjet",25,0,5,dR, 1.0);
    
    dPhi=jet0.DeltaPhi(tau0);
    m_histSvc->BookFillHist("L1_dphijet0tau0",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    dPhi=jet0.DeltaPhi(tau1);
    m_histSvc->BookFillHist("L1_dphijet0tau1",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    dPhi=jet1.DeltaPhi(tau0);
    m_histSvc->BookFillHist("L1_dphijet1tau0",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    dPhi=jet1.DeltaPhi(tau1);
    m_histSvc->BookFillHist("L1_dphijet1tau1",25,-TMath::Pi(),TMath::Pi(),dPhi, 1.0);
    dR=(jet0-tau0).Mod();
    m_histSvc->BookFillHist("L1_dRjet0tau0",25,0,5,dR, 1.0);
    dR=(jet0-tau1).Mod();
    m_histSvc->BookFillHist("L1_dRjet0tau1",25,0,5,dR, 1.0);
    dR=(jet1-tau0).Mod();
    m_histSvc->BookFillHist("L1_dRjet1tau0",25,0,5,dR, 1.0);
    dR=(jet1-tau1).Mod();
    m_histSvc->BookFillHist("L1_dRjet1tau1",25,0,5,dR, 1.0);

    //std::cout << "Here at last" << std::endl;

    return;

    //int jetv_size=jetPt.size()-1;
    //m_histSvc->BookFillHist("L1_lead_tau_pt",50,0,250,tauPt.at(jetv_size)*1e-3, 1.0);
    //m_histSvc->BookFillHist("L1_sublead_tau_pt",50,0,250,tauPt.at(jetv_size-1)*1e-3, 1.0);

    /*std::map<int,int> indexMap;

    for(int j=0;j<tauPhi.size();j++) {
       for(int k=0;k<tauPhi.size();k++){
          if(j==k) continue;
          
       }
    }

    for(int j=0;j<L1_JetPhi.size();j++) {
       for(int k=0;k<L1_JetPhi.size();k++){
          if(j==k) continue;
       }
    }

    
    for(int j=0;j<L1_JetPhi.size();j++) {
       for(int k=0;k<tauPhi.size();k++){
          
       }
    }*/

}


// float AnalysisReader_hhbbtt::getMT2(std::vector<TLorentzVector> bjets, TLorentzVector METv_new)
// {
//   mt2_bisect::mt2 mt2_event;
//   double pa[3] = {bjets.at(0).M(), bjets.at(0).Px(), bjets.at(0).Py()};  // visible particle #1: bjets.at(0)
//   double pb[3] = {bjets.at(1).M(), bjets.at(1).Px(), bjets.at(1).Py()};  // visible particle #2: bjets.at(1)

//   double pmiss[3] = {0.0, METv_new.Px(), METv_new.Py()};  // MET_new = MET + tau + tau
//   double mn = 80398.0;  // mass of W in MeV
//   mt2_event.set_momenta(pa, pb, pmiss);
//   mt2_event.set_mn(mn);

//   return mt2_event.get_mt2();

// }

float AnalysisReader_hhbbtt::mTsqr(TLorentzVector A, TLorentzVector B)
{
  float mTsqr = 2*A.Pt()*B.Pt()*(1-cos(A.Phi()-B.Phi()))*1e-6;
  return mTsqr;
}

