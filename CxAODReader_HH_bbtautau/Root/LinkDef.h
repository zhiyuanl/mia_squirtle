#include <CxAODReader/AnalysisReader.h>
#include <CxAODReader_HH_bbtautau/AnalysisReader_hhbbtt.h>
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class AnalysisReader+;
#pragma link C++ class AnalysisReader_hhbbtt+;
#endif
