#include "CxAODReader_HH_bbtautau/MVATree_hhbbtt.h"

MVATree_hhbbtt::MVATree_hhbbtt(bool persistent, bool readMVA, std::string analysisType,EL::IWorker* wk,std::vector<std::string> variations, bool nominalOnly) :
  MVATree(persistent, readMVA, wk, variations, nominalOnly),
  m_analysisType(analysisType)
{
  if(nominalOnly){variations = {"Nominal"};};
  for (std::string varName : variations) {
    std::cout << "varName" << std::endl;
    SetVariation(varName);
    SetBranches();
  }
  
}

void MVATree_hhbbtt::AddBranch(TString name, Int_t* address)
{
   m_treeMap[m_currentVar] -> Branch(name, address);
  if (m_currentVar == "Nominal") {
    m_reader.AddVariable(name, address);
  }
}
void MVATree_hhbbtt::AddBranch(TString name, ULong64_t* address)
{
  m_treeMap[m_currentVar] -> Branch(name, address);
  if (m_currentVar == "Nominal") {
    m_reader.AddVariable(name, address);
  }
}
void MVATree_hhbbtt::AddBranch(TString name, Float_t* address)
{
   m_treeMap[m_currentVar] -> Branch(name, address);
  if (m_currentVar == "Nominal") {
    m_reader.AddVariable(name, address);
  }
}

void MVATree_hhbbtt::AddBranch(TString name, std::string* address)
{
   m_treeMap[m_currentVar] -> Branch(name, address);
}

void MVATree_hhbbtt::SetBranches()
{

  if(m_currentVar == "Nominal"){
  // prepare MVA reader
    m_reader.SetSplitVar(&EventNumber);
    m_reader.AddReader("reader", 0);
  }
  
  if (m_analysisType != "boosted") {
    AddBranch("sample", &sample);

    AddBranch("EventWeightNoXSec", &EventWeightNoXSec);
    AddBranch("EventWeight", &EventWeight);
    AddBranch("EventNumber", &EventNumber);

    AddBranch("BDT", &BDT);

    //truth info

    AddBranch("NJets", &NJets);
    AddBranch("NJetsbtagged", &NJetsbtagged);

    AddBranch("Tau1Pt", &Tau1Pt);
    AddBranch("Tau1Eta", &Tau1Eta);
    AddBranch("Tau1Phi", &Tau1Phi);
    AddBranch("Tau1M", &Tau1M);

    AddBranch("Tau2Pt", &Tau2Pt);
    AddBranch("Tau2Eta", &Tau2Eta);
    AddBranch("Tau2Phi", &Tau2Phi);
    AddBranch("Tau2M", &Tau2M);

    AddBranch("diTauVisM", &diTauVisM);
    AddBranch("diTauVisPt", &diTauVisPt);
    AddBranch("diTauVisEta", &diTauVisEta);
    AddBranch("diTauVisPhi", &diTauVisPhi);

    AddBranch("diTauMMCM", &diTauMMCM);
    AddBranch("diTauMMCPt", &diTauMMCPt);
    AddBranch("diTauMMCEta", &diTauMMCEta);
    AddBranch("diTauMMCPhi", &diTauMMCPhi);

    AddBranch("diTauDR", &diTauDR);
    AddBranch("diTauDEta", &diTauDEta);
    AddBranch("diTauDPhi", &diTauDPhi);

    AddBranch("Jet1Pt", &Jet1Pt);
    AddBranch("Jet1Eta", &Jet1Eta);
    AddBranch("Jet1Phi", &Jet1Phi);
    AddBranch("Jet1M", &Jet1M);

    AddBranch("Jet2Pt", &Jet2Pt);
    AddBranch("Jet2Eta", &Jet2Eta);
    AddBranch("Jet2Phi", &Jet2Phi);
    AddBranch("Jet2M", &Jet2M);

    AddBranch("diJetM", &diJetM);
    AddBranch("diJetPt", &diJetPt);
    AddBranch("diJetEta", &diJetEta);
    AddBranch("diJetPhi", &diJetPhi);

    AddBranch("diJetDR", &diJetDR);
    AddBranch("diJetDEta", &diJetDEta);
    AddBranch("diJetDPhi", &diJetDPhi);

    AddBranch("diHiggsMScaled", &diHiggsMScaled);

    AddBranch("diHiggsM", &diHiggsM);
    AddBranch("diHiggsPt", &diHiggsPt);

    AddBranch("diJetdiTauDR", &diJetdiTauDR);
    AddBranch("diJetdiTauDPhi", &diJetdiTauDPhi);

    AddBranch("MTW_Max", &MTW_Max);
    AddBranch("MTW_Clos", &MTW_Clos);
    AddBranch("METCentrality", &METCentrality);
    AddBranch("MET", &MET);

    AddBranch("diJetVBFM", &diJetVBFM);
    AddBranch("diJetVBFPt", &diJetVBFPt);
    AddBranch("diJetVBFEta", &diJetVBFEta);
    AddBranch("diJetVBFPhi", &diJetVBFPhi);
    AddBranch("diJetVBFDR", &diJetVBFDR);
    AddBranch("diJetVBFDEta", &diJetVBFDEta);

  } else {
    // boosted
    
    AddBranch("sample", &sample);

    AddBranch("EventWeight", &EventWeight);
    AddBranch("EventNumber", &EventNumber);

    AddBranch("m_region", &m_region);

    AddBranch("m_FJNbtagJets", &m_FJNbtagJets);
    AddBranch("m_FJpt", &m_FJpt);
    AddBranch("m_FJeta", &m_FJeta);
    AddBranch("m_FJphi", &m_FJphi);
    AddBranch("m_FJm", &m_FJm);

    AddBranch("m_DTpt", &m_DTpt);
    AddBranch("m_DTeta", &m_DTeta);
    AddBranch("m_DTphi", &m_DTphi);
    AddBranch("m_DTm", &m_DTm);

    AddBranch("m_dPhiFTwDT", &m_dPhiFJwDT);
    AddBranch("m_dRFJwDT", &m_dRFJwDT);
    AddBranch("m_dPhiDTwMET", &m_dPhiDTwMET);

    AddBranch("m_MET", &m_MET);
    AddBranch("m_hhm", &m_hhm);
    AddBranch("m_bbttpt", &m_bbttpt);
    
  }
  
  // book MVA reader
  TString xmlFile = getenv("ROOTCOREBIN");
  // if (m_analysisType == 2 && m_readMVA) {
  //   // Run 1 files can be found in /eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run1Paper/MVATrainingFiles/
  //   // TODO this does not work out of the box, one has to remove the "> LIMIT ? ..." from "Expression" in the xml file
  //   xmlFile += "/data/CxAODReader/TMVAClassification_BDT_LiverpoolBmham_8TeV_llbb_2tag2jet_ptv0_120_ZllH125_0of2_root5.34.05_v3.0.xml";
  //   m_reader.BookReader("reader", xmlFile);
  // }

  if(m_analysisType=="hadhad" && m_readMVA){
    //  xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_Graviton_BDT_1_M300.weights.xml";
    //   xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_2HDM_BDT_1_M300.weights.xml";
    //  xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_Graviton_BDT_1_M400.weights.xml";
       xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_2HDM_BDT_1_M400.weights.xml";
    //  xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_Graviton_BDT_1_M600.weights.xml";
    //  xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_2HDM_BDT_1_M600.weights.xml";
    //  xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_Graviton_BDT_1_M900.weights.xml";
    //      xmlFile += "/data/CxAODReader_HH_bbtautau/TMVAClassification_BDTsomevar_2HDM_BDT_1_M900.weights.xml";
    m_reader.BookReader("reader", xmlFile);
  }

}

float MVATree_hhbbtt::getBinnedMV1c(float MV1c) {
  const int nbins = 5;
  float xbins[nbins+1] = {0, 0.4050, 0.7028, 0.8353, 0.9237, 1.0};

  if (MV1c >= 1) MV1c = 1-1e-5;
  if (MV1c <= 0) MV1c = 1e-5;

  for(int i=0; i<nbins; i++) {
    if ( MV1c >= xbins[i] && MV1c < xbins[i+1] ) {
      return (xbins[i] + xbins[i+1])/2;
    }
  }

  return 0;
}


void MVATree_hhbbtt::TransformVars() {
  MV1cB1 = getBinnedMV1c(MV1cB1);
  MV1cB2 = getBinnedMV1c(MV1cB2);
}

void MVATree_hhbbtt::Reset()
{
  
  if (m_analysisType != "boosted") {
    sample = "Unknown";
  
    EventWeightNoXSec = 0;
    EventWeight = 0;
    EventNumber = -1;

    MV1cB1 = -1;
    MV1cB2 = -1;
 
    BDT    = -1;

    Tau1Pt = -1;
    Tau1Eta = -1;
    Tau1Phi = -1;
    Tau1M = -1;

    Tau2Pt = -1;
    Tau2Eta = -1;
    Tau2Phi = -1;
    Tau2M = -1;

    diTauVisM = -1;
    diTauVisPt = -1;
    diTauVisEta = -1;
    diTauVisPhi = -1;

    diTauMMCM = -1;
    diTauMMCPt = -1;
    diTauMMCEta = -1;
    diTauMMCPhi = -1;

    diTauDR = -1;
    diTauDEta = -1;
    diTauDPhi = -1;

    Jet1Pt = -1;
    Jet1Eta = -1;
    Jet1Phi = -1;
    Jet1M = -1;

    Jet2Pt = -1;
    Jet2Eta = -1;
    Jet2Phi = -1;
    Jet2M = -1;

    diJetM = -1;
    diJetPt = -1;
    diJetEta = -1;
    diJetPhi = -1;

    diJetDR = -1;
    diJetDEta = -1;
    diJetDPhi = -1;

    diHiggsM =-1;
    diHiggsPt = -1;

    MTW_Max = -1;
    MTW_Clos = -1;
    METCentrality = -1;
    MET = -1;

    diJetVBFM = -1;
    diJetVBFPt = -1;
    diJetVBFPhi = -1;
    diJetVBFDR = -1;
    diJetVBFDEta = -1;
  } else {
    // boosted
    sample = "Unknown";
	
    EventWeight = 0;
    EventNumber = -1;

    m_region = "";

    m_FJpt = -999.;
    m_FJeta = -999.;
    m_FJphi = -999.;
    m_FJm = -999.;

    m_DTpt = -999.;
    m_DTeta = -999.;
    m_DTphi = -999.;
    m_DTm = -999.;

    m_dPhiFJwDT = -999.;
    m_dRFJwDT = -999.;
    m_dPhiDTwMET = -999.;
    m_MET = -999.;
    m_hhm = -999.;
    m_bbttpt = -999.;
  }

}

 void MVATree_hhbbtt::ReadMVA() {
   
   if (!m_readMVA) return;
   
   BDT = m_reader.EvaluateMVA("reader");
 }

