#ifndef CxAODReader_MVATree_hhbbtt_H
#define CxAODReader_MVATree_hhbbtt_H

#include "CxAODReader/MVATree.h"

class MVATree_hhbbtt : public MVATree {
  
protected:

  std::string m_analysisType;
  
  virtual void AddBranch(TString name, Float_t* address);
  virtual void AddBranch(TString name, Int_t* address);
  virtual void AddBranch(TString name, ULong64_t* address);
  virtual void AddBranch(TString name, std::string* address);
  virtual void SetBranches();
  virtual void TransformVars();
  float getBinnedMV1c(float MV1c);
  
public:
  
  MVATree_hhbbtt(bool persistent, bool readMVA, std::string analysisType, EL::IWorker* wk, std::vector<std::string> variations, bool nominalOnly);
  
  ~MVATree_hhbbtt() {}
  
  virtual void Reset();
  virtual void ReadMVA();
  
  std::string sample;
  
  float EventWeightNoXSec;
  float EventWeight;
  unsigned long long EventNumber;
 
  float MV1cB1;
  float MV1cB2;

  float BDT;

  int NJets;
  int NJetsbtagged;

  float Tau1Pt;
  float Tau1Eta;
  float Tau1Phi;
  float Tau1M;

  float Tau2Pt;
  float Tau2Eta;
  float Tau2Phi;
  float Tau2M;

  float diTauVisM;
  float diTauVisPt;
  float diTauVisEta;
  float diTauVisPhi;

  float diTauMMCM;
  float diTauMMCPt;
  float diTauMMCEta;
  float diTauMMCPhi;

  float diTauDR;
  float diTauDEta;
  float diTauDPhi;

  float Jet1Pt;
  float Jet1Eta;
  float Jet1Phi;
  float Jet1M;

  float Jet2Pt;
  float Jet2Eta;
  float Jet2Phi;
  float Jet2M;

  float diJetM;
  float diJetPt;
  float diJetEta;
  float diJetPhi;

  float diJetDR;
  float diJetDEta;
  float diJetDPhi;

  float diHiggsM;
  float diHiggsPt;

  float diHiggsMScaled;

  float diJetdiTauDR;
  float diJetdiTauDPhi;

  float MTW_Max;
  float MTW_Clos;
  float METCentrality;
  float MET;

  float diJetVBFM;
  float diJetVBFPt;
  float diJetVBFPhi;
  float diJetVBFEta;
  float diJetVBFDR;
  float diJetVBFDEta;

  float boostedDR;

  // boosted
  std::string m_region;

  int m_FJNbtagJets; //!

  float m_FJpt; //!
  float m_FJeta; //!
  float m_FJphi; //!
  float m_FJm; //!

  float m_DTpt; //!
  float m_DTeta; //!
  float m_DTphi; //!
  float m_DTm; //!

  float m_dPhiFJwDT; //!
  float m_dRFJwDT; //!
  float m_dPhiDTwMET; //!

  float m_MET; //!
  float m_hhm; //!
  float m_bbttpt; //!

};


#endif
