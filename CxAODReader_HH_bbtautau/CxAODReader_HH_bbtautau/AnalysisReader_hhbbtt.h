#ifndef CxAODReader_AnalysisReader_hhbbtt_H
#define CxAODReader_AnalysisReader_hhbbtt_H

#include "CxAODReader/AnalysisReader.h"
#include "CxAODReader_HH_bbtautau/MVATree_hhbbtt.h"
#include "CxAODTools_HH_bbtautau/OverlapRemoval_HH_bbtautau.h"

#include <CxAODTools_HH_bbtautau/HHbbtautauLepHadSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauHadHadSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauLepHadJetSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauHadHadJetSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauBoostedSelection.h>
#include <CxAODTools_HH_bbtautau/HHbbtautauBoostedJetSelection.h>

#include "CorrsAndSysts/CorrsAndSysts.h"

class AnalysisReader_hhbbtt : public AnalysisReader {
protected:

  enum Type {lephad,hadhad,boosted};

  virtual EL::StatusCode initializeSelection() override;
  virtual EL::StatusCode initializeCorrsAndSysts(); // no longer overriding, since moved out of the core reader
  virtual EL::StatusCode initializeTools() override;
  virtual EL::StatusCode initializeIsMC() override;
  virtual EL::StatusCode initializeChannel() override;
  virtual EL::StatusCode initializeVariations() override;

  virtual EL::StatusCode setObjectsForOR(const xAOD::ElectronContainer* electrons, 
					 const xAOD::PhotonContainer* photons,	
					 const xAOD::MuonContainer* muons,		
					 const xAOD::TauJetContainer* taus,
				    const xAOD::JetContainer* jets,
                const xAOD::JetContainer* fatjets,
                const xAOD::DiTauJetContainer* ditaus
					 );

  EL::StatusCode fill_bTaggerHists(const xAOD::Jet* jet);
  EL::StatusCode fill_nJetHistos(std::vector<const xAOD::Jet*> jets, string jetType);


  //EL::StatusCode fill_jetHistos(std::vector<const xAOD::Jet*> signalJets,
  //			std::vector<const xAOD::Jet*> forwardJets);
  EL::StatusCode setevent_flavour(std::vector<const xAOD::Jet*> selectedJets );
  EL::StatusCode setevent_flavour(const std::vector<int>& vFlavours );
  std::vector<const xAOD::Jet*> trackJetsForBTag(const xAOD::Jet* fatJet, const std::vector<const xAOD::Jet*>& vTrackJets);
  std::vector<const xAOD::Jet*> trackJetsForSF(const xAOD::Jet* fatJet_Sel, const std::vector<const xAOD::Jet*>& vFatJets_4b, const std::vector<const xAOD::Jet*>& vTrackJets);
  bool passVRJetOR(const std::vector<const xAOD::Jet*>& vTrackJetsForSF);

  EL::StatusCode applyAlternativeSelection(std::vector<const xAOD::DiTauJet*> my_ditauJets);
    
  void tagjet_selection(std::vector<const xAOD::Jet*> signalJets,
			std::vector<const xAOD::Jet*> forwardJets,
			std::vector<const xAOD::Jet*> &selectedJets,
			int &tagcatExcl,
			int &tagcatIncl);

  void compute_btagging(const std::vector<const xAOD::Jet_v1*>&);
  void compute_TRF_tagging (const std::vector<const xAOD::Jet*>& signalJets);
  void fatjet_selection ();

  void findNBTagTJ();

  void fillFatJetPlots();
  void fillDiTauJetPlots(std::vector<const xAOD::DiTauJet*> my_ditauJets);

  std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> fillDRMap(std::vector<const xAOD::DiTauJet*> my_ditauJets);
  void fillFullSystemVars(std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap);
  bool fill_cutFlow(ResultHHbbtautau &selectionResult, std::vector<const xAOD::Jet*> jets, std::vector<const xAOD::TauJet*> taus);
  void fill_SRPlots();
  void fill_eventLVars();
  void fill_jetHistos(std::vector<const xAOD::Jet*> jetvector);
  void fill_tauHistos(std::vector<const xAOD::TauJet*> tauvector);
  void fill_sjetHistos(std::vector<const xAOD::Jet*> sjets);
  void fill_tauFFHistos(std::string,std::vector<const xAOD::TauJet*> finalTaus);
  void fill_xHistos(std::vector<const xAOD::Jet*> jets, std::vector<const xAOD::TauJet*> taus);
  void fill_vbfHistos(std::vector<const xAOD::TauJet*> taus);
  void fill_triggerPlots();
  // float getMT2(std::vector<TLorentzVector> bjets, TLorentzVector met_tau);
  void fill_boostedPlots(std::string description);
  float mTsqr(TLorentzVector A, TLorentzVector B);
  EL::StatusCode applyFF(std::string,std::vector<const xAOD::TauJet*> finalTaus, Trigger &trigger);
  EL::StatusCode applyFF_boosted();
  EL::StatusCode applyFF_boosted_NtagSplit();
  EL::StatusCode applyFF_boosted_SignNtrackSplit();
  EL::StatusCode calculateFF_boosted();
  EL::StatusCode applyZhfSF_boosted();
  EL::StatusCode applyDiTauSF_boosted();
  bool pass_DT_cuts(const xAOD::DiTauJet* ditau, double BDTDiTauIDCut);
  EL::StatusCode select_DT(std::vector<const xAOD::DiTauJet*> diTaus, double BDTDiTauIDCut);
  EL::StatusCode select_fakeDT(std::vector<const xAOD::DiTauJet*> diTaus, double BDTDiTauIDCut);
  EL::StatusCode select_FJ(std::vector<const xAOD::Jet*> fatJets, std::string Xbb_wp);
  EL::StatusCode select_FJ_VRbtagged(std::vector<const xAOD::Jet*> fatJets);
  std::vector<const xAOD::Jet*> select_FJs_4b(std::vector<const xAOD::Jet*> fatJets);
  EL::StatusCode fill_cutflow_boosted(std::string cutname = "");

  bool applyPreSelection();
  bool applySRSelection();
  bool applyQCDSelection();
  bool applySideBandSelection();
  bool applyTTBarSelection();

  void fillSRPlots();
  void fillQCDCRPlots();
  void fillSideBandPlots();
  void fillTTBarCRPlots();
  void fillFullSystemPlots();


  void getSelDT(std::vector<const xAOD::DiTauJet*> my_ditauJets);
  void getSelFJ(std::map< std::pair <const xAOD::Jet*, const xAOD::DiTauJet*>, float> dRMap);

  std::string get2DProngRegion(std::vector<const xAOD::TauJet*> taus);

  //float computeBTagSFWeight (std::vector<const xAOD::Jet*> &signalJets);
  
  EL::StatusCode fill_lephad();
  EL::StatusCode fill_hadhad(); 
  EL::StatusCode fill_boosted();
  //EL::StatusCode initializeOR2();

  std::pair<int,int> HiggsTag(const xAOD::Jet *fatjet, std::string wp);

  // EL::StatusCode fill_2lepCutFlow(std::string label, bool isMu = false, bool isE = false);

  void fill_boostedTree();
  float TauFakeCompWeight(std::vector<const xAOD::TauJet*> finalTaus);


  const xAOD::Jet* m_SelFJ; //!
  std::vector<const xAOD::Jet*>       m_my_fatJets; //!
  std::vector<const xAOD::Jet*>       m_SelFJets; //!
  std::vector<const xAOD::Jet*>       m_MatchFJets; //!
  std::vector<int> m_have1BTagTJ; //!
  std::vector<int> m_have2BTagTJ; //!

  std::vector<const xAOD::Jet*> m_TaggedFJets; //!
  std::vector<const xAOD::Jet*> m_NonTaggedFJets; //!


  std::string m_analysisType; //!
  std::string m_analysisStrategy; //!
  std::string m_region; //!
  bool m_doTruthTagging; //!
  MVATree_hhbbtt* m_tree; //!
  double m_trkBTagLimit; //For HiggsTag
  bool m_use2DbTagCut; //!
  bool m_fillCR; //!
  bool m_getFFInputs; //!
  bool m_applyFF; //!
  bool m_calculateFF; //!
  bool m_combineTruthMatchedSamples; //!
  bool m_writeMVATree; //!
  bool m_readMVA; //!
  bool m_fillSR; //!
  bool m_fillSS; //!
  bool m_FillSRSel260_300; //!
  bool m_FillSRSel400; //!
  bool m_FillSRSel500_600_700; //!
  bool m_FillSRSel800_900_1000; //!
  bool m_useSideBandQCDMethod; //!

  int m_nBJets; //!

  bool m_PassTriggerSelection; //!
  bool m_PassTriggerJetSelection; //!

  bool m_RegA; //!
  bool m_RegB; //!
  bool m_RegC; //!
  bool m_RegD; //!
  int m_FJNbtag; //!
  double m_FFFatJetPtCut; //!
  bool m_applyBoostedFF; //!
  float m_weightFactor1; //!
  float m_weightFactor2; //!


  bool m_isSignal; //!
  bool m_isFakes; //!
  int m_counter; //!
  float m_deltaRJJ; //!
  float m_deltaPhiJJ; //!
  float m_deltaRTT; //!
  float m_deltaPhiTT; //!
  float m_deltaEtaJJ; //!
  float m_deltaEtaTT; //!
  float m_dRDiJetDiTau; //!
  float m_dPhiDiJetDiTau; //!
  float m_deltaRDiJetDiTau; //!
  float m_deltaPhiDiJetDiTau; //!
  float m_MTW_Max; //!
  float m_MTW_Clos; //!
  float m_METCentrality; //!
  // float m_mt2; //!
  float m_mTtot; //!
  TLorentzVector m_METVec; //!
  TLorentzVector m_MMCVec; //!
  TLorentzVector m_MMCVecScaled; //!
  TLorentzVector DiJetVec2; //!
  TLorentzVector m_bb; //!
  TLorentzVector m_bbScaled; //!
  TLorentzVector m_Xmmc; //!
  TLorentzVector m_XmmcScaled; //!
  TFile *m_FF_File; //!
  TFile *m_FF_File_boosted; //!
  std::map<std::string, TH1*> m_BoostedFFMap; //!
  TH2F *m_2DFF; //!
  TH2D *m_2DFF_0tag_dPhiVR_boosted; //!
  TH2D *m_2DFF_0tag_dPhiSR_boosted; //!
  TH2D *m_2DFF_0tag_dPhiNotSR_boosted; //!
  TH2D *m_2DFF_1tag_dPhiVR_boosted; //!
  TH2D *m_2DFF_1tag_dPhiSR_boosted; //!
  TH2D *m_2DFF_1tag_dPhiNotSR_boosted; //!
  TH1D *m_overallFFperRegion; //!
  TH1D *m_1DFF_0tagEtaCentral_dPhiFFR_boosted; //!
  TH1D *m_1DFF_1tagEtaCentral_dPhiFFR_boosted; //!
  TH1D *m_1DFF_0tagEtaForward_dPhiFFR_boosted; //!
  TH1D *m_1DFF_1tagEtaForward_dPhiFFR_boosted; //!
  TH1D *m_1DFF_0tagEtaCentral_boosted; //!
  TH1D *m_1DFF_0tagEtaForward_boosted; //!
  TH1D *m_1DFF_SS_EtaCentral_boosted; //!
  TH1D *m_1DFF_SS_EtaForward_boosted; //!
  TH1D *m_1DFF_SS_2tracks_EtaCentral_boosted; //!
  TH1D *m_1DFF_SS_2tracks_EtaForward_boosted; //!
  TH1D *m_1DFF_SS_4tracks_EtaCentral_boosted; //!
  TH1D *m_1DFF_SS_4tracks_EtaForward_boosted; //!
  TH1D *m_1DFF_SS_6tracks_boosted; //!
  TH1D *m_1DFF_SS_LowSublPt_EtaForward_boosted; //!
  TH1D *m_1DFF_SS_MediumSublPt_EtaForward_boosted; //!
  TH1D *m_1DFF_SS_HighSublPt_EtaForward_boosted; //!

  // for had-had fakes
  // first key is the syst. variation
  std::map<std::string, std::map<int, std::map<std::string, TH1F *> > > m_tauFFHadHadUnbin; //!
  std::map<std::string, std::map<int, std::map<int, TH2F*> > > m_tauFFHadHad; //!
  std::map<std::string, TH1F *> m_HadHadTF; //!
  

  const xAOD::DiTauJet* m_SelDT; //!
  TLorentzVector m_SelDTsubj0; //!
  TLorentzVector m_SelDTsubj1; //!
  TLorentzVector m_SelDTsubjets; //!
  TLorentzVector m_CorrSelDT; //!
  TLorentzVector m_SelDT_truth; //!
  TLorentzVector m_SelFJ_truth; //!
  TLorentzVector m_SelFJp; //!
  int m_SelDTChargeProd; //!
  bool m_fillDRPlot; //!
  float m_dR; //!
  int m_nTJ; //!
  int m_nBTagTJ; //!
  float m_fullMass; //!
  float m_Xhh; //!
  float m_Xhh2; //!
  float m_Xhh3; //!
  float m_Xhh4; //!
  TLorentzVector m_corrDTau; //!
  TLorentzVector m_corrDTau2; //!
  float m_dPhi; //!
  float m_dPhiFJ; //!
  float m_dEta; //!
  float m_MCut; //!
  float m_MCut2; //!
  float m_ElCutX; //!
  float m_ElCutY; //!
  const xAOD::Jet*                   m_SelFJ2; //!
  bool m_applyAlternativeSelection; //!

  //OverlapRemoval_HH_bbtautau   *m_overlapRemoval2;           // !

  TH1F* m_effHist; //!

  // VBF information for the cut
  bool m_hasVBF; //!
  TLorentzVector m_vbfj1; //!
  TLorentzVector m_vbfj2; //!

  bool m_hasVBF_nodeta; //!
  TLorentzVector m_vbfj1_nodeta; //!
  TLorentzVector m_vbfj2_nodeta; //!

  bool m_antiTau; //!

  CorrsAndSysts *m_corrsAndSysts; //!

  bool m_setEventFlavour; //!
  

public:
  AnalysisReader_hhbbtt();

  virtual ~AnalysisReader_hhbbtt();

protected:
#ifndef __MAKECINT__
  OverlapRemoval_HH_bbtautau   *m_overlapRemoval2;           // !
#endif // not __MAKECINT_
  
  // this is needed to distribute the algorithm to the workers
  ClassDefOverride(AnalysisReader_hhbbtt, 1);

};
#endif
