#!/usr/bin/env bash
set -euo pipefail

ptag="p3978"
deriv="HIGG4D3"

samples="LQ3.txt
VH.txt
Vjets.txt
diboson.txt
non_res_hh_ggF.txt
non_res_hh_vbf.txt
other_dihiggs.txt
res_hh_ggF.txt
res_hh_vbf.txt
single_higgs.txt
top.txt
ttH.txt"

for sample in ${samples}; do
    # Remove comments, empty lines and convert to DAOD format
    sed "s/#.*$// ; s/^\s*// ; s/\s*$// ; /^$/d" AOD/${sample} \
        | sed "s/recon/deriv/ ; s/AOD/DAOD_${deriv}/ ; s/$/_${ptag}/"
done
