#!/usr/bin/env bash
set -euo pipefail

deriv="HIGG4D3"
ptag="p3974"

prefix="/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists"

grl_list="data15_13TeV/20170619/physics_25ns_21.0.19.xml
data16_13TeV/20180129/physics_25ns_21.0.19.xml
data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml
data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml"

for grl in ${grl_list}; do
    run_list=$(grep "<Run>[0-9]\+</Run>" ${prefix}/${grl} | grep -o "[0-9]\+")
    scope=$(echo ${grl} | cut -d"/" -f 1)

    echo "GRL: ${grl}" 1>&2
    echo "Scope: ${scope}" 1>&2

    for run in ${run_list}; do
        rucio_result=$(rucio ls --short "${scope}.00${run}.*.DAOD_${deriv}.*_${ptag}" | cut -d":" -f 2)

        if (( $(echo ${rucio_result} | wc -l) != 1 )); then
            echo "Error: Run ${run}" 1>&2
            continue
        fi

        echo ${rucio_result}
    done
done
