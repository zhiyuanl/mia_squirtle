################################################################################
# Build configuration for FrameworkSub_HH_bbtautau
################################################################################

# Declare the name of the package:
atlas_subdir( FrameworkSub_HH_bbtautau )

# Declare the package's dependencies:
atlas_depends_on_subdirs()

# Build a shared library:
# atlas_add_library( FrameworkSub_HH_bbtautau
#    FrameworkSub_HH_bbtautau/*.h Root/*.h Root/*.cxx ${_dictionarySource}
#    PUBLIC_HEADERS FrameworkSub_HH_bbtautau
#    LINK_LIBRARIES )

atlas_install_generic( data/*
   DESTINATION data PKGNAME_SUBDIR )
