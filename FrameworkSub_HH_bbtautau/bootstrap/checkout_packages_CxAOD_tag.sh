#!/usr/bin/env bash
#git clone -b FrameworkSub ssh://git@gitlab.cern.ch:7999/CxAODFramework/FrameworkSub.git

# forbids an interactive shell from running this executable, in other words, do not source
if [[ $- == *i* ]] ; then
    echo "ERROR: I'm a script forcing you to execute. Don't source me!" >&2
    return 1
else
    # if I am OK to execute, force that the script stops if variables are not defined
    # this catches bugs in the code when you think a variable has value, but it is empy
    set -eu
fi

# check the number of parameters, if not stop
if [ $# -ne 2 ]; then
cat <<EOF
Usage: $0 packages_file                  FORCE_CHECKOUT
Usage: $0 packages_HH_CxAOD.txt 0
EOF
exit 1
fi

packages_list=$1
FORCE_CHECKOUT=$2

#path_prefix=ssh://git@gitlab.cern.ch:7999/agbet

# start loop over CxAODFramework packages
while read line
do
    #
    package=$(echo "$line" | awk '{print $1}')
    tag=$(echo "$line" | awk '{print $2}')
    #path=$path_prefix/$package.git
    path=$package.git
    echo "package=${package} tag=${tag} path=${path}"
    #
    if [[  -d $package ]] ; then
        if [[ $FORCE_CHECKOUT == "1" ]] ; then
            rm -rf $package
        else
            echo "$path already checked out"
            continue
        fi
    fi
    #
    # Jon suggests the entire package with "git clone $path"
    COMMAND1="git clone $path"
    echo "COMAND1=${COMMAND1}"
    ${COMMAND1}
    
    # if folder does not exist, continue
    if [ ! -d "$package" ]; then
	echo "WARNING!!! package ${package} does not have a folder! We'll skip the checkout of a tag for this package!"
	continue
    fi
    # and then if we want a particular tag we do to that folder and do "git checkout tag_version"
    COMMAND2="cd $package"
    echo "COMAND2=${COMMAND2}"
    ${COMMAND2}
    # ... and do "git checkout tag_version" and ...
    COMMAND3="git checkout $tag"
    echo "COMAND3=${COMMAND3}"
    ${COMMAND3}
    # ... and return to previous folder
    COMMAND4="cd .."
    echo "COMAND4=${COMMAND4}"
    ${COMMAND4}
    # done all for current package
done < $packages_list
# done loop over all the packages

echo "Done the checkout of CxAOD package from GitLab!"
