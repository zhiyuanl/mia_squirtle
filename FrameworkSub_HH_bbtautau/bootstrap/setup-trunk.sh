#!/usr/bin/env bash
# bootstrap script for VHbb
# Adrian Buzatu, adrian.buzatu@cern.ch, 12 Oct 2016
# with inspiration from Dan Guest for checkout_packages
# and from getTrunk.sh from previous personal bootstrap script 

# 1. setup ATLAS and load a newer version of git
# echo "STEP 1: Set upstream as your fork for FrameworkSub"
# cd FrameworkSub_HH_bbtautau
# echo "before:"
# git remote -v
# git remote add upstream ssh://git@gitlab.cern.ch:7999/${USER}/FrameworkSub_HH_bbtautau.git
# echo "after:"
# git remote -v
# cd ..

DATA_PREFIX="/afs/cern.ch/work/v/vhbbframework/public/data/dataForCxAODFramework_190225"
BOOTSTRAPDIR="FrameworkSub_HH_bbtautau/bootstrap"

echo "DATA_PREFIX=${DATA_PREFIX}"
echo "BOOTSTRAPDIR=${BOOTSTRAPDIR}"

# 1. setup ATLAS and load a newer version of git
echo "STEP 1: setup ATLAS and load a newer version of git"
setupATLAS
lsetup git

if [ -n ${CERN_USER} ]
then
    MY_USER=${CERN_USER}
    echo "You have defined CERN_USER, so using that for the scp."
else
    MY_USER=${USER}
    echo "You have not defined CERN_USER, so using USER. Define CERN_USER if your lxplus username is different than that of your local machine (for example at your home institute)"
fi
echo "MY_USER=${MY_USER}"

# 2. check out git packages                                                                                                                                                       
echo "STEP 2: checkout git packages"
#if ! $(bash ${BOOTSTRAPDIR}/checkout_packages_CxAOD.sh "${MY_USER}" "${BOOTSTRAPDIR}/packages_HH_CxAOD.txt" "0" "${DATA_PREFIX}") ; then                                         
#    echo "problem checking out git packages" >&2                                                                                                                                 
#    return                                                                                                                                                                       
#fi                                                                                                                                                                               

COMMAND="${BOOTSTRAPDIR}/checkout_packages_CxAOD.sh ${MY_USER} ${BOOTSTRAPDIR}/packages_HH_CxAOD.txt 0 ${DATA_PREFIX}"
echo ${COMMAND}
eval ${COMMAND}



# Danilo: removed checkout of the SVN packages
# do we really need them in rel. 21?

# to check svn packages, we need to use rc checkout
# at least for now, before they are also migrated to Git
# so we need to setup RootCore already

# from here the same order as in SVN approach
# 3. setup RootCore and AnalysisBase with version taken from the release-trunk
#echo "STEP 3: setup RootCore with AnalysisBase"
#release=`cat ${BOOTSTRAPDIR}/release-trunk`
#rcSetup -u
#rcSetup Base,$release

# 4. add the rest of the packages that are from CxAOD packages
# currently the file contains also the CxAOD packages
# but they will be ignored as already checked out
#packages = VHbb-packages-svn.txt
#rc checkout $packages
#echo "STEP 4: check out svn packages with rc checkout" 
#if ! ${BOOTSTRAPDIR}/checkout_packages_nonCxAOD.sh ${BOOTSTRAPDIR}/packages_HH_nonCxAOD.txt 0 ; then
#    echo "problem checking out svn packages with rc checkout" >&2
#    return
#fi

#echo "STEP 5: search for redundant packages" 
#${BOOTSTRAPDIR}/search_redundant_packages.sh ${BOOTSTRAPDIR}/packages_HH_*CxAOD.txt

# done
return # use instead of exit when sourcing the script
