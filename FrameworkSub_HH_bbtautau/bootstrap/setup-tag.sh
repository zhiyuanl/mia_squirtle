# 1. setup ATLAS and load a newer version of git
echo "STEP 1: setup ATLAS and load a newer version of git"
setupATLAS
lsetup git

BOOTSTRAPDIR=FrameworkSub_HH_bbtautau/bootstrap/

# 2. check out CxAOD packages
echo "STEP 2: checkout CxAOD packages with Git"
if ! ${BOOTSTRAPDIR}/checkout_packages_CxAOD_tag.sh ${BOOTSTRAPDIR}/packages_HH_CxAOD_tag.txt 0 ; then
    echo "problem checking out CxAOD packages with Git" >&2
    return
fi

# to check nonCxAOD packages, we need to use rc checkout from SVN
# at least for now, before they are also migrated to Git
# so we need to setup RootCore already

# from here the same order as in SVN approach
# 3. setup RootCore and AnalysisBase with version taken from the release-trunk
echo "STEP 3: setup RootCore with AnalysisBase"
release=`cat ${BOOTSTRAPDIR}/release`
rcSetup -u
rcSetup Base,$release

# 4. add the rest of the packages that are from CxAOD packages
# currently the file contains also the CxAOD packages
# but they will be ignored as already checked out
#packages = VHbb-packages-nonCxAOD.txt
#rc checkout $packages
echo "STEP 4: check out nonCxAOD packages from SVN with rc checkout"
if ! ${BOOTSTRAPDIR}/checkout_packages_nonCxAOD.sh ${BOOTSTRAPDIR}/packages_HH_nonCxAOD.txt 0 ; then
    echo "problem checking out nonCxAOD packages from SVN with rc checkout" >&2
    return
fi

# done
return # use instead of exit when sourcing the script
