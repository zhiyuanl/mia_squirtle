#!/usr/bin/env bash
# git clone -b FrameworkSub ssh://git@gitlab.cern.ch:7999/CxAODFramework/FrameworkSub.git

# forbids an interactive shell from running this executable, in other words, do not source
if [[ $- == *i* ]] ; then
    echo "ERROR: I'm a script forcing you to execute. Don't source me!" >&2
    return 1
else
    # if I am OK to execute, force that the script stops if variables are not defined
    # this catches bugs in the code when you think a variable has value, but it is empy
    set -eu
fi

# check the number of parameters, if not stop
if [ $# -ne 4 ]; then
cat <<EOF
Usage: $0 MY_USER PACKAGE_LIST                 FORCE_CHECKOUT DATA_PREFIX
Usage: $0 $USER   packages_VHbb_git_branch.txt 0              /afs/cern.ch/work/v/vhbbframework/public/data/dataForCxAODFramework
Usage: $0 $USER   packages_VHbb_git_tag.txt    0              /afs/cern.ch/work/v/vhbbframework/public/data/dataForCxAODFramework
Usage: WARNING: Use CERN_USER if your local machine has a different username USER than the one on lxplus (to scp correctly the .root files, not in GitLab any more!)
EOF
exit 1
fi

MY_USER=$1
PACKAGE_LIST=$2
FORCE_CHECKOUT=$3
DATA_PREFIX=$4
PATH_PREFIX="ssh://git@gitlab.cern.ch:7999"

echo "MY_USER=${MY_USER}"
echo "PACKAGE_LIST=${PACKAGE_LIST}"
echo "FORCE_CHECKOUT=${FORCE_CHECKOUT}"
echo "DATA_PREFIX=${DATA_PREFIX}"
echo "PATH_PREFIX=${PATH_PREFIX}"

# start loop over CxAODFramework packages
while read line
do

    # skip coments and blank lines
    QUOTE_RE="^#"
    EMPTY_RE="^$"
    if [[ $line =~ $QUOTE_RE || $line =~ $EMPTY_RE ]] ; then
        continue
    fi

    #
    package=$(echo "${line}" | awk '{print $1}')        # package name
    repo=$(echo "${line}" | awk '{print $2}')           # repo name
    tag_or_branch=$(echo "${line}" | awk '{print $3}')  # note it can be both a tag or a branch name
    copy_data_root=$(echo "${line}" | awk '{print $4}') # if we copy from /afs the data/*.root for this package 

    path=${PATH_PREFIX}/${repo}/${package}.git
    echo "repository=${repo} package=${package} tag_or_branch=${tag_or_branch} path=${path}"
    #
    if [[  -d ${package} ]] ; then
        if [[ ${FORCE_CHECKOUT} == "1" ]] ; then
            rm -rf ${package}
        else
            echo "$path already checked out"
            continue
        fi
    fi
    
    # clone the entire package
    COMMAND="git clone ${path}"
    echo "COMAND=${COMMAND}"
    ${COMMAND}
    
    # if folder does not exist, continue
    if [ ! -d "${package}" ]; then
	echo "WARNING!!! package ${package} does not have a folder! We'll skip the checkout of a tag_or_branch for this package!"
	continue
    fi

    # cd to that package
    COMMAND="cd ${package}"
    echo "COMMAND=${COMMAND}"
    ${COMMAND}

    # git checkout depending if it is a branch or a tag
    echo "Doing git checkout with -b, so that you have a branch to develop on directly."
    echo "So you can commit and merge directly. This for both branch and tag."
    branch_specific="origin/"
    if [[ "${tag_or_branch}" == "${branch_specific}"* ]]; then
	echo "You asked for a branch, as it starts with ${branch_specific}."
	branch_name=${tag_or_branch#${branch_specific}}
	echo "tag_or_branch=${tag_or_branch} branch_name=${branch_name}"
	COMMAND="git checkout ${tag_or_branch} -b ${branch_name}-${MY_USER}"
    else
	echo "You asked for a tag, as is it does not start with ${branch_specific}"
	tag_name=${tag_or_branch}
	echo "tag_or_branch=${tag_or_branch} tag_name=${tag_name}"
	COMMAND="git checkout ${tag_or_branch} -b master-${tag_name}-${MY_USER}"
    fi
    echo "COMMAND=${COMMAND}"
    ${COMMAND}

    # GitLab rules do not allow to store binaries
    # so *.root had been removed from data folder
    # and are copied now from /afs to data folder
    if [[ "${copy_data_root}" == "1" ]]; then
        COMMAND="pwd ; ls -lha ; du -sh *"
        echo "COMMAND=${COMMAND}"
	${COMMAND}
	COMMAND="scp -r ${MY_USER}@lxplus.cern.ch:${DATA_PREFIX}/${package}/* data/."
	echo "COMMAND=${COMMAND}"
        ${COMMAND}
        COMMAND="ls -lha data"
        echo "COMMAND=${COMMAND}"
        ${COMMAND}
    fi

    # return to previous folder
    COMMAND="cd .."
    echo "COMMAND=${COMMAND}"
    ${COMMAND}

    # done all for current package
done < ${PACKAGE_LIST}
# done loop over all the packages

echo "Done the checkout of git packages!"
