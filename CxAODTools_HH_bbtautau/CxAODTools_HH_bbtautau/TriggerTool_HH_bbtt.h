#ifndef CxAODTools_TriggerTool_HH_bbtt_H
#define CxAODTools_TriggerTool_HH_bbtt_H

#include <map>
#include <unordered_map>
#include <string>
#include <vector>

#ifndef __MAKECINT__
#include "EventLoop/StatusCode.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "MuonAnalysisInterfaces/IMuonTriggerScaleFactors.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "ElectronEfficiencyCorrection/IAsgElectronEfficiencyCorrectionTool.h"
#include "AsgTools/AnaToolHandle.h"
#include "TriggerAnalysisInterfaces/ITrigGlobalEfficiencyCorrectionTool.h"
#include "PATCore/PATCoreEnums.h"
#endif // not __MAKECINT__

#include "EventLoop/StatusCode.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"
#include "xAODMissingET/MissingET.h"
#include "xAODEgamma/Photon.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/DiTauJet.h"
#include "xAODTau/DiTauJetContainer.h"

#include "CxAODTools/ConfigStore.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauResult.h"

class TriggerTool_HH_bbtt {

protected:

  ConfigStore m_config; //!
  bool m_debug;
  std::string m_analysisType;
  bool m_nominalOnly;
  bool m_is50ns;
  std::string m_str_muTrigSF;
  bool antiTau;
  bool m_skipTriggerSelection;

  bool m_useRNNTaus = false; //!
  std::string m_tauIDWP = "medium"; //!
  PROP<int> *m_passIDProp = nullptr; //!

#ifndef __MAKECINT__
  // event info
  EventInfoHandler & m_eventInfoHandler;//!

  // CP tools
  CP::MuonTriggerScaleFactors* m_trig_sfmuon;                 // !

  // fields for TrigGlobalEfficiencyCorrectionTool
  asg::AnaToolHandle<ITrigGlobalEfficiencyCorrectionTool> m_leptrig_sf_Tool;                                  //!
  ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_leptrig_sf_electronEffToolsHandles;                //!
  ToolHandleArray<IAsgElectronEfficiencyCorrectionTool> m_leptrig_sf_electronSFToolsHandles;                 //!
  std::vector<asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> > m_leptrig_sf_electronToolsFactory;  //!
  ToolHandleArray<CP::IMuonTriggerScaleFactors> m_leptrig_sf_muonToolsHandles;                               //!
  std::vector<asg::AnaToolHandle<CP::IMuonTriggerScaleFactors> > m_leptrig_sf_muonToolsFactory;              //!
#endif // not __MAKECINT__
  std::string m_TriggerMenu;            //!
  std::vector<std::string> m_trig_Name; //!
  std::vector<std::string> m_trig_Sys;  //!

  // fields for TrigGlobalEfficiencyCorrectionTool
  std::map<std::string, std::string> m_leptrig_sf_triggerKeys; //!
  std::map<std::string,std::string> m_leptrig_sf_legsPerTool ; //!

public:

  TriggerTool_HH_bbtt (ConfigStore & config, EventInfoHandler & eventInfoHandler, std::string triggerType);
  virtual ~TriggerTool_HH_bbtt ();

  virtual EL::StatusCode initialize();
  //virtual EL::StatusCode initializeGlobalLeptonTrigTool();

  void getTriggerDecision(const xAOD::EventInfo* eventInfo, ResultHHbbtautau & result);
  double getTriggerSF(const xAOD::EventInfo* eventInfo, ResultHHbbtautau & result, const std::string & variation);
  //void getTriggerSFLepLep(unsigned runNumber, const std::string & variation, const std::vector<const xAOD::Electron*>& electrons, const std::vector<const xAOD::Muon*>& muons, double& efficiencyScaleFactor);

  std::vector<std::string> getTriggerSystList() {return m_trig_Sys;}

};

#endif // ifndef CxAODTools_TriggerTool_HH_bbtt_H
