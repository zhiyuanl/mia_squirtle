#ifndef CxAODTools__HHbbtautauSelection_H
#define CxAODTools__HHbbtautauSelection_H

#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"
#include "CxAODTools/EventSelection.h"
#include "CxAODMaker/EventInfoHandler.h"
#include <iostream>
#include "CxAODTools_HH_bbtautau/TriggerTool_HH_bbtt.h"
//#include "CxAODMaker_HH_bbtautau/DiTauMassHandler.h"
#include <TString.h>

namespace xAOD {
#ifndef XAODEGAMMA_ELECTRON_H
  class Electron;
#endif
#ifndef XAODEGAMMA_PHOTON_H
  class Photon;
#endif
#ifndef XAODMUON_MUON_H
  class Muon;
#endif
#ifndef XAODJET_JET_H
  class Jet;
#endif
#ifndef XAODTAUJET_TAU_H
  class Tau;
#endif

}

template <typename T>
class HHbbtautauSelection : public EventSelection
{
  public:

  protected:
    T m_result;
    bool m_debug;
    std::map<TString, T> m_sysResults;
    TriggerTool_HH_bbtt *m_triggerTool;
    std::string m_channel = "";
    //DiTauMassHandler& m_diTauMassHandler;

  public:
  HHbbtautauSelection(const TString cutFlowName = "PreselectionCutFlow") noexcept; 

    virtual ~HHbbtautauSelection() noexcept {}

    virtual T& result() { return m_result; }
    virtual void setResult(T& result) { m_result = result; m_sysResults[m_sysName] = result;}

    virtual std::map<TString, T>& results() { return m_sysResults; }
    virtual void setResults(std::map<TString, T>& results) { m_sysResults = results; }


    virtual bool passPreSelection(SelectionContainers & containers, bool isKinVar);

    virtual bool passSelection(SelectionContainers & containers, bool isKinVar);

    virtual void clearResults() {m_sysResults.clear();}
    
    virtual void setDebug(bool debug) {m_debug=debug;}

  protected:
    virtual bool passJetSelection(const xAOD::JetContainer* jets);
    virtual bool passFatJetSelection(const xAOD::JetContainer* jets);
    virtual bool passTrackJetSelection(const xAOD::JetContainer* jets);
    virtual bool passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo) = 0;
    virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus) = 0;
    virtual bool passLeptonSelection(const xAOD::ElectronContainer* electrons,
                                     const xAOD::MuonContainer* muons,
                                     const xAOD::MissingET* met) = 0;

    virtual bool passJetPreSelection(const xAOD::JetContainer* jets);
    virtual bool passFatJetPreSelection(const xAOD::JetContainer* jets);
    virtual bool passTrackJetPreSelection(const xAOD::JetContainer* jets);

    virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                                        const xAOD::MuonContainer* muons,
                                        const xAOD::MissingET* met) = 0;

    virtual bool passKinematics();

    virtual void clearResult();

    /// ensures consistency between channels
    int doHHLeptonSelection(const xAOD::ElectronContainer* electrons,
                            const xAOD::MuonContainer* muons,
                            const xAOD::Electron*& el1, const xAOD::Electron*& el2,
                            const xAOD::Muon*& mu1, const xAOD::Muon*& mu2);

    int doHHLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                               const xAOD::MuonContainer* muons,
                               const xAOD::Electron*& el1, const xAOD::Electron*& el2,
                               const xAOD::Muon*& mu1, const xAOD::Muon*& mu2);

    int doHHDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus);

    int doHHTauSelection(const xAOD::TauJetContainer* taus);

  virtual bool passTriggerSelection(const xAOD::EventInfo* /*evtinfo*/) {return true;}

  //void setDiTauMassHandler(DiTauMAssHandler& ditau) {m_diTauMassHandler = ditau;}

};

#include "CxAODTools_HH_bbtautau/HHbbtautauSelection.icc"

#endif
