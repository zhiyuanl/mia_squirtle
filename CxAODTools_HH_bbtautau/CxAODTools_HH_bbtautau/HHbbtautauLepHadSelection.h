#ifndef CxAOD__HHbbtautauLepHadSelection_H
#define CxAOD__HHbbtautauLepHadSelection_H

#include <vector>

#include "HHbbtautauSelection.h"

class HHbbtautauLepHadSelection : public HHbbtautauSelection<ResultHHbbtautau>
{
  public:
  HHbbtautauLepHadSelection(ConfigStore * config=nullptr, EventInfoHandler * eventInfoHandler=nullptr);
   virtual ~HHbbtautauLepHadSelection() noexcept {
    delete m_triggerTool;
    m_triggerTool = 0;
  }


  virtual bool passSelection(SelectionContainers & containers,
			     bool isKinVar)override;
  
  virtual bool passPreSelection(SelectionContainers & containers,
				bool isKinVar)override;
  
  virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
					     xAOD::EventInfo* eventInfoOut,
					     bool isKinVar,
					     bool isWeightVar,
					     std::string sysName,
					     int rdm_RunNumber,
					     CP::MuonTriggerScaleFactors* trig_sfmuon)override;
  
protected:
  bool m_antitau;
  bool m_useRNNTaus=false;
  std::string m_tauIDWP = "medium";
  PROP<int> *m_passIDProp = nullptr;

  virtual bool passLeptonSelection(const xAOD::ElectronContainer* electrons,
				   const xAOD::MuonContainer* muons,
				   const xAOD::MissingET* met)override;
  
  virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
				      const xAOD::MuonContainer* muons,
				      const xAOD::MissingET* met)override;
  
  virtual bool passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo) override;

  virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* /*ditaus*/) override {return true;};
  
  virtual bool passKinematics()override;
  
  //virtual bool passTriggerSelection(const xAOD::EventInfo* evtinfo) override;

};

#endif
