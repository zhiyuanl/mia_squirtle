#ifndef TAUHELPERS_H_
#define TAUHELPERS_H_

#include <string>

template <typename T> class PROP;

PROP<int> *getTauIDProp(const std::string &wp, bool rnn = false);

#endif
