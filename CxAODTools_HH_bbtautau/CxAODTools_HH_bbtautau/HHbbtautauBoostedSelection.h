#ifndef CxAOD__HHbbtautauBoostedSelection_H
#define CxAOD__HHbbtautauBoostedSelection_H

#include <vector>

#include "HHbbtautauSelection.h"

class HHbbtautauBoostedSelection : public HHbbtautauSelection<ResultHHbbtautau>
{
  public:
    HHbbtautauBoostedSelection(ConfigStore * config=nullptr, EventInfoHandler * eventInfoHandler=nullptr);
    virtual ~HHbbtautauBoostedSelection() noexcept {
      if(m_triggerTool){
	     delete m_triggerTool;
	     m_triggerTool = nullptr;
      }
    }


    virtual bool passSelection(SelectionContainers & containers,
			       bool isKinVar);

    virtual bool passPreSelection(SelectionContainers & containers,
				  bool isKinVar);

    virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                             xAOD::EventInfo* eventInfoOut,
                             bool isKinVar,
                             bool isWeightVar,
			     std::string sysName,
			     int rdm_RunNumber,
			     CP::MuonTriggerScaleFactors* trig_sfmuon)override;

  protected:
    bool m_antitau;
    bool m_debug;
    bool m_vetoResolvedTauEvents;

    virtual bool passLeptonSelection(const xAOD::ElectronContainer* electrons,
                                     const xAOD::MuonContainer* muons,
                                     const xAOD::MissingET* met);

    virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                                        const xAOD::MuonContainer* muons,
                                        const xAOD::MissingET* met);

    virtual bool passTauSelection(const xAOD::TauJetContainer* taus, const xAOD::EventInfo* evtinfo);

    virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus);
 
    virtual bool passKinematics();

    virtual void clearResult();

    virtual bool passTriggerSelection(const xAOD::EventInfo* evtinfo) override;
};

#endif
