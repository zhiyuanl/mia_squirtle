#ifndef CxAOD__HHbbtautauLepLepSelection_H
#define CxAOD__HHbbtautauLepLepSelection_H

#include <vector>

#include "HHbbtautauSelection.h"

class HHbbtautauLepLepSelection : public HHbbtautauSelection<ResultHHbbtautau>
{
    public:
        HHbbtautauLepLepSelection(ConfigStore * config=nullptr, EventInfoHandler * eventInfoHandler=nullptr);
        virtual ~HHbbtautauLepLepSelection() noexcept {
            delete m_triggerTool;
            m_triggerTool = 0;
        }


        virtual bool passSelection(SelectionContainers & containers,
                bool isKinVar) override;

        virtual bool passPreSelection(SelectionContainers & containers,
                bool isKinVar) override;

        virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                xAOD::EventInfo* eventInfoOut,
                bool isKinVar,
                bool isWeightVar,
                std::string sysName,
                int rdm_RunNumber,
                CP::MuonTriggerScaleFactors* trig_sfmuon) override;

    protected:
        virtual bool passLeptonSelection(const xAOD::ElectronContainer* electrons,
                const xAOD::MuonContainer* muons,
                const xAOD::MissingET* met) override;

        virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                const xAOD::MuonContainer* muons,
                const xAOD::MissingET* met) override;

        virtual bool passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo) override;

        virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus) override {return true;};

        virtual bool passKinematics() override;
};

#endif
