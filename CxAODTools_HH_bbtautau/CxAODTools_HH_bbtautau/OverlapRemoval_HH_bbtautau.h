#ifndef CxAODTools__OverlapRemoval_HH_bbtautau_H
#define CxAODTools__OverlapRemoval_HH_bbtautau_H

#include "CxAODTools/OverlapRemoval.h"
#include "AssociationUtils/DeltaRMatcher.h"

class OverlapRemoval_HH_bbtautau : public OverlapRemoval {
 public:
  
  OverlapRemoval_HH_bbtautau(ConfigStore & config);
    
    virtual void setORInputLabels(
				    const xAOD::ElectronContainer* electrons,
				                const xAOD::PhotonContainer* photons,
				                const xAOD::MuonContainer* muons,
						const xAOD::TauJetContainer* taus,
						const xAOD::JetContainer* jets,
				                const xAOD::JetContainer* fatjets,
                            const xAOD::DiTauJetContainer* ditaus) override;

    virtual EL::StatusCode removeOverlap(const xAOD::ElectronContainer* electrons,
                                         const xAOD::PhotonContainer* photons,
                                         const xAOD::MuonContainer* muons,
                                         const xAOD::TauJetContainer* taus,
                                         const xAOD::JetContainer* jets,
                                         const xAOD::JetContainer* fatjets,
                                         const xAOD::DiTauJetContainer* ditaus) override;

 private:
    ORUtils::DeltaRMatcher * m_dRMatcher;

};
#endif
