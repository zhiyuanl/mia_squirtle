#ifndef CxAODTools__HHbbtautauResult_H
#define CxAODTools__HHbbtautauResult_H

#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/DiTauJet.h"
#include "xAODJet/Jet.h"
#include "xAODMissingET/MissingET.h"

#include <iostream>
#include <string>

enum Trigger{eNone, eSET, eDET, eSMT, eDMT, eEMT, eETT, eMTT, eSTT, eDTT, eBTT, eSkip, eFJT,
  eFJT360, eFJT380, eFJT400, eFJT420, eFJT440, eFJT460};

// print out trigger enum value as text
// https://stackoverflow.com/a/3342891
std::ostream& operator<<(std::ostream& out, const Trigger value);

// structure holding kinematics of the result
struct ResultHHbbtautau
{
  bool pass;
  const xAOD::Electron* el;
  const xAOD::Muon* mu;
  const xAOD::MissingET* met;
  std::vector<const xAOD::Electron*> electrons;
  std::vector<const xAOD::Muon*> muons;
  std::vector<const xAOD::Electron*> signal_electrons;
  std::vector<const xAOD::Muon*> signal_muons;
  std::vector<const xAOD::TauJet*> taus;
  std::vector<const xAOD::Jet*> signalJets;
  std::vector<const xAOD::Jet*> forwardJets;
  std::vector<const xAOD::Jet*> fatJets;
  std::vector<const xAOD::Jet*> trackJets;
  std::vector<const xAOD::TauJet*> matchedTausSTT;
  std::vector<const xAOD::TauJet*> matchedTausDTT;
  std::vector<const xAOD::DiTauJet*> ditaus;
  Trigger trigger;
  float triggerSF;
  bool isSREvent;
  bool hasTightTau;
  bool isEl() { return el!=nullptr; }
  bool isMu() { return mu!=nullptr; }
  bool isLepHad() { return isEl() || isMu();}
  bool isHadHad() { return !isLepHad();}
  int nlep;
  //int nSignalTaus;
};

#endif
