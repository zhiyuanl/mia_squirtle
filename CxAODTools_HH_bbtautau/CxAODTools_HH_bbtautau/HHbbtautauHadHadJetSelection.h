#ifndef CxAOD__HHbbtautauHadHadJetSelection_H
#define CxAOD__HHbbtautauHadHadJetSelection_H

#include <vector>

#include "HHbbtautauSelection.h"

class HHbbtautauHadHadJetSelection : public HHbbtautauSelection<ResultHHbbtautau>
{
  public:
  HHbbtautauHadHadJetSelection(ConfigStore * config=nullptr, EventInfoHandler * eventInfoHandler=nullptr);
  virtual ~HHbbtautauHadHadJetSelection() noexcept {
    delete m_triggerTool;
    m_triggerTool = 0;
  }
  
  
  virtual bool passSelection(SelectionContainers & containers,
			     bool isKinVar)override;

  virtual bool passPreSelection(SelectionContainers & containers,
				bool isKinVar)override;

  virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
					     xAOD::EventInfo* eventInfoOut,
					     bool isKinVar,
					     bool isWeightVar,
					     std::string sysName,
					     int rdm_RunNumber,
					     CP::MuonTriggerScaleFactors* trig_sfmuon)override;

  protected:
  bool m_debug;
  int m_hasMoreThanTwoTau;
  int m_hasMoreThanOneJet;
  bool m_runFourTau;

  virtual bool passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo) override;

  virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* ) override {return true;};

  virtual bool passLeptonSelection(const xAOD::ElectronContainer* ,
				   const xAOD::MuonContainer* ,
				   const xAOD::MissingET* ) override {return true;}
  
  virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* ,
				      const xAOD::MuonContainer* ,
				      const xAOD::MissingET* ) override {return true;}
  
  virtual void clearResult() override;

};

#endif
