#ifndef CxAODTools_CommonProperties_HH_bbtautau_H
#define CxAODTools_CommonProperties_HH_bbtautau_H

#include "CxAODTools/CommonProperties.h"

#ifndef __MAKECINT__

//Truth tau info
PROPERTY_DECL( Props , double , pt_vis)
PROPERTY_DECL( Props , double , phi_vis)
PROPERTY_DECL( Props , double , eta_vis)
PROPERTY_DECL( Props , double , m_vis)
PROPERTY_DECL( Props , char , IsHadronicTau)
PROPERTY_DECL( Props , int , IsTauFromHiggs)
PROPERTY_DECL( Props , int , IsTauFromW)
PROPERTY_DECL( Props , int , IsFromLQ)
PROPERTY_DECL( Props , int , parentID)

// Props for truth-studies
PROPERTY_DECL( Props , int , MCTruthClfOrigin )
PROPERTY_DECL( Props , int , MCTruthClfType )
PROPERTY_DECL( Props , int , TruthJetPartonID )

// MMC
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu0_4vect_pt )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu0_4vect_eta )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu0_4vect_phi )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu0_4vect_m )

PROPERTY_DECL( Props , double , mmc_mlnu3p_nu1_4vect_pt )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu1_4vect_eta )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu1_4vect_phi )
PROPERTY_DECL( Props , double , mmc_mlnu3p_nu1_4vect_m )

// fat jet
PROPERTY_DECL( Props , int , xbbResult_1tagNoMcut )
PROPERTY_DECL( Props , int , xbbResult_1tag90Mcut )
PROPERTY_DECL( Props , int , xbbResult_1tag68Mcut )
PROPERTY_DECL( Props , int , xbbResult_2tagNoMcut )
PROPERTY_DECL( Props , int , xbbResult_2tag90Mcut )
PROPERTY_DECL( Props , int , xbbResult_2tag68Mcut )
PROPERTY_DECL( Props , int , n_subjets )
PROPERTY_DECL( Props , float , subjet_lead_pt )
PROPERTY_DECL( Props , float , subjet_lead_eta )
PROPERTY_DECL( Props , float , subjet_lead_phi )
PROPERTY_DECL( Props , float , subjet_lead_e )
PROPERTY_DECL( Props , int , subjet_lead_ntracks )
PROPERTY_DECL( Props , int , subjet_lead_charge )
PROPERTY_DECL( Props , float , subjet_subl_pt )
PROPERTY_DECL( Props , float , subjet_subl_eta )
PROPERTY_DECL( Props , float , subjet_subl_phi )
PROPERTY_DECL( Props , float , subjet_subl_e )
PROPERTY_DECL( Props , int , subjet_subl_ntracks )
PROPERTY_DECL( Props , int , subjet_subl_charge )
PROPERTY_DECL( Props , float , subjets_pt )
PROPERTY_DECL( Props , float , subjets_eta )
PROPERTY_DECL( Props , float , subjets_phi )
PROPERTY_DECL( Props , float , subjets_e )
PROPERTY_DECL( Props , int , Nbtags60 )
PROPERTY_DECL( Props , int , Nbtags70 )
PROPERTY_DECL( Props , int , Nbtags77 )
PROPERTY_DECL( Props , int , Nbtags85 )
PROPERTY_DECL( Props , std::vector<int> , vHadronConeExclTruthLabelID )

// fourtau
PROPERTY_DECL( Props , int , hasMoreThanTwoTau )

// FTTF
PROPERTY_DECL( Props , float , seedJetWidth )
PROPERTY_DECL( Props , float , seedJetJvt )

PROPERTY_DECL( Props , std::vector<float> , TrackWidthPt500 )
PROPERTY_DECL( Props , float , TrackWidthPt500PV0 )
PROPERTY_DECL( Props , float , TrackWidthPt500PVn )

PROPERTY_DECL( Props , std::vector<float> , TrackWidthPt1000 )
PROPERTY_DECL( Props , float , TrackWidthPt1000PV0 )
PROPERTY_DECL( Props , float , TrackWidthPt1000PVn )

// For had-had triggers
PROPERTY_DECL( Props , int , L1_match_J12 )
PROPERTY_DECL( Props , int , L1_match_J12_0ETA23 )
PROPERTY_DECL( Props , int , L1_match_J25 )

#endif

#endif

