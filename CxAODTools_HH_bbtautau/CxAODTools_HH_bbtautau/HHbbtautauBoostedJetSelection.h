#ifndef CxAOD__HHbbtautauBoostedJetSelection_H
#define CxAOD__HHbbtautauBoostedJetSelection_H

#include <vector>

#include "HHbbtautauSelection.h"

class HHbbtautauBoostedJetSelection : public HHbbtautauSelection<ResultHHbbtautau>
{
  public:
  HHbbtautauBoostedJetSelection(ConfigStore * config=nullptr, EventInfoHandler * eventInfoHandler=nullptr);
  virtual ~HHbbtautauBoostedJetSelection() noexcept {
    delete m_triggerTool;
    m_triggerTool = 0;
  }
  
  
  virtual bool passSelection(SelectionContainers & containers,
			     bool isKinVar)override;

  virtual bool passPreSelection(SelectionContainers & containers,
				bool isKinVar)override;

  virtual EL::StatusCode writeEventVariables(const xAOD::EventInfo* eventInfoIn,
					     xAOD::EventInfo* eventInfoOut,
					     bool isKinVar,
					     bool isWeightVar,
					     std::string sysName,
					     int rdm_RunNumber,
					     CP::MuonTriggerScaleFactors* trig_sfmuon)override;

  protected:
  bool m_debug;
  bool m_vetoResolvedTauEvents;

  virtual bool passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo) override;

  virtual bool passLeptonSelection(const xAOD::ElectronContainer* ,
				   const xAOD::MuonContainer* ,
				   const xAOD::MissingET* ) override {return true;}
  
  virtual bool passLeptonPreSelection(const xAOD::ElectronContainer* ,
				      const xAOD::MuonContainer* ,
				      const xAOD::MissingET* ) override {return true;}

  virtual bool passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus) override;
  
  virtual void clearResult() override;

};

#endif
