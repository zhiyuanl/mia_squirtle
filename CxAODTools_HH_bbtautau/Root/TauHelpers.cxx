#include "CxAODTools_HH_bbtautau/TauHelpers.h"
#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"

PROP<int> *getTauIDProp(const std::string &wp, bool rnn) {
  if (wp == "tight") {
    if (rnn) {
      return &Props::isRNNTight;
    } else {
      return &Props::isBDTTight;
    }
  } else if (wp == "medium") {
    if (rnn) {
      return &Props::isRNNMedium;
    } else {
      return &Props::isBDTMedium;
    }
  } else if (wp == "loose") {
    if (rnn) {
      return &Props::isRNNLoose;
    } else {
      return &Props::isBDTLoose;
    }
  }

  return nullptr;
}
