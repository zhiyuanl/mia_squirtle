#include "CxAODTools_HH_bbtautau/HHbbtautauHadHadJetSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

#include<TRandom.h>

HHbbtautauHadHadJetSelection::HHbbtautauHadHadJetSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
  HHbbtautauSelection("PreselectionJetCutFlow"),
  m_hasMoreThanTwoTau(0), 
  m_hasMoreThanOneJet(1), 
  m_runFourTau(false)
{
  m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "hadhad");
  m_triggerTool->initialize().ignore();
  config->getif<bool>("debug",m_debug);
  setDebug(m_debug);
  config->getif<bool>("runFourTau",m_runFourTau);
}

bool HHbbtautauHadHadJetSelection::passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* /*evtinfo*/)
{

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passTauSelection() vor variation " << m_sysName << std::endl;

  // TODO: clear previous tau list
  // Require exactly one tau passing "signal" cuts
  int res = doHHTauSelection(taus);

  if (m_debug) {
    std::cout << "HH:  " << res << " taus survived the doHHTauSelection for variation " << m_sysName << std::endl;
    std::vector<const xAOD::TauJet*> thesetaus = m_result.taus;
    for (auto itr = thesetaus.begin(); itr != thesetaus.end(); itr++) {
      const xAOD::TauJet* thistau = *itr;
      std::cout << "  tau "
		<< " pt=" << thistau->pt()/1000.;
    }
    std::cout << std::endl;
  }

  return (res == 2);
}

bool HHbbtautauHadHadJetSelection::passSelection(SelectionContainers & containers,
						 bool /*isKinVar*/) {
  

  //const xAOD::MissingET          * met       = containers.met;
  //const xAOD::ElectronContainer  * electrons = containers.electrons;
  //const xAOD::MuonContainer      * muons     = containers.muons;
  const xAOD::JetContainer       * jets      = containers.jets;
  const xAOD::JetContainer       * fatjets   = containers.fatjets;
  const xAOD::TauJetContainer    * taus      = containers.taus;
  const xAOD::EventInfo          * evtinfo   = containers.evtinfo;

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passSelection" << std::endl;

  // assume that T has a field bool pass
  clearResult();

  if (! passTauSelection(taus, evtinfo)){
    if (m_runFourTau) m_hasMoreThanTwoTau = 1;
    m_result.pass = false;
    if (!m_runFourTau) return false;
  }
  else
    if (m_runFourTau) m_hasMoreThanTwoTau = 0;

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetSelection(jets);
  bool passFatJetSel = false;

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetSelection(fatjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    if (m_runFourTau) m_hasMoreThanOneJet = 0;
    m_result.pass = false;
    if (!m_runFourTau) return false;
  }
  else
    if (m_runFourTau) m_hasMoreThanOneJet = 1;

  if (m_runFourTau) {
    Props::hasMoreThanTwoTau.set(evtinfo, m_hasMoreThanTwoTau);
    if (m_hasMoreThanTwoTau == 1 || m_hasMoreThanOneJet == 0) return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true; 
  return true;
}

// preselection versions
bool HHbbtautauHadHadJetSelection::passPreSelection(SelectionContainers & containers,
						 bool isKinVar) {
  const xAOD::EventInfo         * evtinfo   = containers.evtinfo;
  //const xAOD::MissingET         * met       = containers.met;
  //const xAOD::ElectronContainer * electrons = containers.electrons;
  //const xAOD::MuonContainer     * muons     = containers.muons;
  const xAOD::JetContainer      * jets      = containers.jets;
  const xAOD::JetContainer      * fatjets   = containers.fatjets;
  const xAOD::TauJetContainer   * taus      = containers.taus;  

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passPreSelection() for variation " << m_sysName << std::endl;


  clearResult();

  if (! passTauSelection(taus, evtinfo)) {
    if (m_runFourTau) m_hasMoreThanTwoTau = 1;
    m_result.pass = false;
    if (m_debug) std::cout << "HH: bbtautauHadHad*Jet*Selection passPreSelection: failed tau selection for variation " << m_sysName << std::endl;
    if (!m_runFourTau) return false;
  }
  else
    if (m_runFourTau) m_hasMoreThanTwoTau = 0;

  if(!isKinVar) m_cutFlow.count("Preselection taus");
  
  // Reset trigger flag from first selector
  //m_result.trigger = (Trigger)Props::HHbbtautauTrigger.get(evtinfo);

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetPreSelection(jets);
  bool passFatJetSel = false;  // HF set false, otherwise jet selection never fails

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetPreSelection(fatjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    if (m_runFourTau) m_hasMoreThanOneJet = 0;
    m_result.pass = false;
    if (m_debug) std::cout << "HH: FAILED jet selection for variation " << m_sysName << std::endl;
    if (!m_runFourTau) return false;
  }
  else
    if (m_runFourTau) m_hasMoreThanOneJet = 1;

  if (m_runFourTau) {
    Props::hasMoreThanTwoTau.set(evtinfo, m_hasMoreThanTwoTau);
    if (m_hasMoreThanTwoTau == 1 || m_hasMoreThanOneJet == 0) return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true;  

  if(!isKinVar) m_cutFlow.count("Preselection jet");

  if (m_debug) std::cout << "HH: Selection result for variation" << m_sysName << ": e " 
	    << m_result.el << " m "
	    << m_result.mu << " taus "
    	    << m_result.taus.size() << " " <<  (m_result.taus.size() ? m_result.taus.at(0) : 0) << " jets "
	    << m_result.signalJets.size() << " trig " 
	    << m_result.trigger << std::endl;

  // Store result (per-syst) to be picked up by MMC
  m_sysResults[m_sysName] = m_result;

  return true;
}

EL::StatusCode HHbbtautauHadHadJetSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                             xAOD::EventInfo* eventInfoOut,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string sysName,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

  // TODO: write out correctly fule lepton SF + trigger SF for correct trigger based on enum and 
  //       chosen random tau
  //
  Props::trigSF.set(eventInfoOut, 1);

  if (!m_result.pass) return EL::StatusCode::SUCCESS;

   if( m_debug ){
    std::cout << "HH: " << m_result.matchedTausSTT.size() << " Trigger: " << m_result.trigger << std::endl;
    std::cout << "HH: " << m_result.matchedTausDTT.size() << " Trigger: " << m_result.trigger << std::endl;
    std::cout << "HH: bbtautauHadHad*Jet*Selection::writeEventVariables: calling getTriggerSF()" << std::endl;
  }

  m_triggerTool->getTriggerSF(eventInfoIn,m_result,sysName);
  
  if ( m_debug ) std::cout << "HH: trigSF: " << m_result.triggerSF << std::endl;

  Props::trigSF.set(eventInfoOut, m_result.triggerSF); 

  return EL::StatusCode::SUCCESS;
}

void HHbbtautauHadHadJetSelection::clearResult() {
  m_result = m_sysResults[m_sysName];
  // Clear taus as need to redo them after random picking
  m_result.taus.clear();
}
