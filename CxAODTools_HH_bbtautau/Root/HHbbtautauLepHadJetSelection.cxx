#include "CxAODTools_HH_bbtautau/HHbbtautauLepHadJetSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include <iostream>
#include<TRandom.h>

HHbbtautauLepHadJetSelection::HHbbtautauLepHadJetSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
  HHbbtautauSelection("PreselectionJetCutFlow"),
  m_antitau(false),
  fake(false)
{
  config->getif<bool>("AntiTau",m_antitau);
  config->getif<bool>("Fake",fake);
  config->getif<bool>("debug",m_debug);
 if (fake)m_antitau=true;
  m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "lephad");
  m_triggerTool->initialize().ignore();
}

bool HHbbtautauLepHadJetSelection::passTauSelection(const xAOD::TauJetContainer* /*taus*/,  const xAOD::EventInfo* /*evtinfo*/)
{
  // Require exactly one tau passing "signal" cuts
  
  if (m_antitau && m_result.taus.size()>0) {
    for (auto itr = m_result.taus.begin(); itr != m_result.taus.end(); ) {
      const xAOD::TauJet * tau = *itr;
      if (!Props::isHHRandomTau.get(tau)) {
	m_result.taus.erase(itr);
      } else {
	++itr;
      }
    }
  }
  if(fake){
    if (m_result.taus.size() == 1 || m_result.taus.size() == 0) return true;
    else return false;
  }

  else return (m_result.taus.size() == 1);

//   int res = doHHTauSelection(taus);
//   return (res == 1);
}

bool HHbbtautauLepHadJetSelection::passSelection(SelectionContainers & containers,
						 bool /*isKinVar*/) {
  

  //const xAOD::MissingET          * met       = containers.met;
  //const xAOD::ElectronContainer  * electrons = containers.electrons;
  //const xAOD::MuonContainer      * muons     = containers.muons;
  const xAOD::JetContainer       * jets      = containers.jets;
  const xAOD::JetContainer       * fatjets   = containers.fatjets;
  const xAOD::TauJetContainer    * taus      = containers.taus;
  const xAOD::EventInfo          * evtinfo   = containers.evtinfo;

  // assume that T has a field bool pass
  clearResult();

  if (! passTauSelection(taus, evtinfo)){
    m_result.pass = false;
    return false;
  }

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetSelection(jets);
  bool passFatJetSel = false;

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetSelection(fatjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    m_result.pass = false;
    return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true; 
  return true;
}

// preselection versions
bool HHbbtautauLepHadJetSelection::passPreSelection(SelectionContainers & containers,
						 bool isKinVar) {
  const xAOD::EventInfo         * evtinfo   = containers.evtinfo;
  //const xAOD::MissingET         * met       = containers.met;
  //const xAOD::ElectronContainer * electrons = containers.electrons;
  //const xAOD::MuonContainer     * muons     = containers.muons;
  const xAOD::JetContainer      * jets      = containers.jets;
  const xAOD::JetContainer      * fatjets   = containers.fatjets;
  const xAOD::TauJetContainer   * taus      = containers.taus;  

  clearResult();
  
  if (! passTauSelection(taus, evtinfo)) {
    m_result.pass = false;
    return false;
  }
  if(!isKinVar) m_cutFlow.count("Preselection taus");
  
  // Reset trigger flag from first selector
  //m_result.trigger = (Trigger)Props::HHbbtautauTrigger.get(evtinfo);

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetPreSelection(jets);
  bool passFatJetSel = false;

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetPreSelection(fatjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    m_result.pass = false;
    return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true;  

  if(!isKinVar) m_cutFlow.count("Preselection jet");

  if (m_debug)
    std::cout << "Selection result: e " 
			 << m_result.el << " " << (m_result.el ? m_result.el->pt() : 0) << " m " 
			 << m_result.mu << " " << (m_result.mu ? m_result.mu->pt() : 0) << " taus "
			 << m_result.taus.size() << " " 
			 <<  (m_result.taus.size() ? m_result.taus.at(0) : 0) << " " << (m_result.taus.size() ? m_result.taus.at(0)->pt() : 0) << " jets "
			 << m_result.signalJets.size() << " trig " 
			 << m_result.trigger << std::endl;

  // Store result (per-syst) to be picked up by MMC
  // TODO: is this really getting stored per-syst as is on input or is being overwritten?
  m_sysResults[m_sysName] = m_result;

  //Props::HHbbtautauResult.set(evtinfo, m_result);
  //ResultHHbbtautauLepHad result = Props::HHbbtautauResult.get(evtinfo);
  return true;
}

EL::StatusCode HHbbtautauLepHadJetSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                             xAOD::EventInfo* eventInfoOut,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string sysName,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

  // TODO: write out correctly fule lepton SF + trigger SF for correct trigger based on enum and 
  //       chosen random tau

  // ensure to write all variables for all events
  
  Props::leptonSF.set(eventInfoOut, 1);
  Props::tauSF.set(eventInfoOut,1);
  Props::trigSF.set(eventInfoOut, 1);
  
  if (!m_result.pass) return EL::StatusCode::SUCCESS;
   if(m_result.taus.size()>0) Props::tauSF.set(eventInfoOut,Props::effSF.get(m_result.taus.at(0)));
 
  m_triggerTool->getTriggerSF(eventInfoIn,m_result,sysName);
  if (m_debug) std::cout << "trigSF: " << m_result.triggerSF << std::endl;
  Props::trigSF.set(eventInfoOut, m_result.triggerSF);

  // lepton SF
  if (m_result.isEl()) {
    float leptonSF = 1;
    if (m_result.el)leptonSF *= Props::effSFtightLH.get(m_result.el) * Props::effSFReco.get(m_result.el) * Props::effSFIsoFixedCutLooseTightLH.get(m_result.el);
    Props::leptonSF.set(eventInfoOut, leptonSF);
  } else {
    float leptonSF = 1;
    if (m_result.mu)leptonSF *= Props::mediumEffSF.get(m_result.mu) * Props::TTVAEffSF.get(m_result.mu) * Props::fixedCutLooseIsoSF.get(m_result.mu);//Props::effSF.get(m_result.mu)
    Props::leptonSF.set(eventInfoOut, leptonSF);
  }

  return EL::StatusCode::SUCCESS;
}

void HHbbtautauLepHadJetSelection::clearResult() {
  m_result = m_sysResults[m_sysName];
  // Clear taus as need to redo them after random picking
  ////m_result.taus.clear();
}
