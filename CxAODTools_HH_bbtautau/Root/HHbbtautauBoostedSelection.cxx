#include "CxAODTools_HH_bbtautau/HHbbtautauBoostedSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODTau/DiTauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

HHbbtautauBoostedSelection::HHbbtautauBoostedSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) : 
 HHbbtautauSelection("PreselectionLepCutFlow"),
 m_antitau(false),
 m_vetoResolvedTauEvents(false)
{
  
 config->getif<bool>("AntiTau",m_antitau);
 config->getif<bool>("debug",m_debug);
 config->getif<bool>("vetoResolvedTauEvents", m_vetoResolvedTauEvents);
 setDebug(m_debug);
 m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "boosted");
 m_triggerTool->initialize();
}

void HHbbtautauBoostedSelection::clearResult() {
  m_result.pass = false;
  m_result.taus.clear();
  m_result.matchedTausSTT.clear();
  m_result.matchedTausDTT.clear();
  m_result.signalJets.clear();
  m_result.forwardJets.clear();
  m_result.fatJets.clear();
  m_result.trackJets.clear();
  m_result.met = nullptr;
  m_result.mu=nullptr; 
  m_result.el=nullptr;
  m_result.trigger=eNone;
  m_result.triggerSF=1.;
  m_result.isSREvent=false;
  m_result.hasTightTau=false;
  m_result.ditaus.clear();
  //m_result.nSignalTaus=0;
  m_result.nlep=0;
}

bool HHbbtautauBoostedSelection::passSelection(SelectionContainers & containers,
					      bool isKinVar) {

  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
 
  return HHbbtautauSelection<ResultHHbbtautau>::passSelection(containers, isKinVar);
}

bool HHbbtautauBoostedSelection::passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus)
{

  if (m_debug) std::cout << "HH:bbtautauBoostedSelection::passDiTauJetSelection" << std::endl;
  int res = doHHDiTauJetSelection(ditaus);
  if(res >= 1) {
    if (m_debug) std::cout << "HH: found DiTauJet. Good event!" << std::endl;
    return true;
  }
  return false;

}

bool HHbbtautauBoostedSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons,
                                               const xAOD::MuonContainer* muons,
                                               const xAOD::MissingET* met) {
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  if (m_debug) std::cout << "HH:bbtautauBoostedSelection::passLeptonSelection" << std::endl;
  int res = doHHLeptonSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 0) {
    if (m_debug) std::cout << "HH: found Lepton. Failing event!" << std::endl;
    return false;
  }
  m_result.met = met;
  return true;

}

bool HHbbtautauBoostedSelection::passTauSelection(const xAOD::TauJetContainer* taus, const xAOD::EventInfo* /*evtinfo*/)
{
  if (m_debug) std::cout << "HH: in HHbbtautauBoostedSelection::passTauSelection() for variation" << m_sysName << std::endl;
  int res = doHHTauSelection(taus);
  //Ensure othogonality to resolved hadhad and lephad channels
  //if(m_result.nSignalTaus >= 2 || (m_result.nSignalTaus == 1 && m_result.nlep == 1) ) {
  if(m_vetoResolvedTauEvents && res != 0) {
    if (m_debug) std::cout << "HH: found Resolved Tau. Failing event!" << std::endl;
    return false;
  }
  return true;

}

// preselection versions
bool HHbbtautauBoostedSelection::passPreSelection(SelectionContainers & containers,
						 bool isKinVar) {

  if (m_debug) std::cout << "HH: bbtautauBoostedSelection::passPreSelection for variation " << m_sysName << std::endl;
  return HHbbtautauSelection<ResultHHbbtautau>::passPreSelection(containers, isKinVar);
}

bool HHbbtautauBoostedSelection::passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                                                       const xAOD::MuonContainer* muons,
                                                       const xAOD::MissingET* /*met*/)
{
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  if (m_debug) std::cout << "HH: bbtautauBoostedSelection::passLeptonPreSelection for variation" << m_sysName << std::endl;
  int res = doHHLeptonPreSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 0) {
    if (m_debug) std::cout << "HH: found Lepton. Failed event for variation" << m_sysName << std::endl;
    return false;
  }
  return true;

}

bool HHbbtautauBoostedSelection::passKinematics() {
  // MJ cuts, like MET / MPT etc...
  // my advice is to add in passKinematics() prototype all the stuff that
  // doesn't need to be put in the Result struct, like MPT
  if (m_debug) std::cout << "HH: bbtautauBoostedSelection::passKinematics" << std::endl;

  return true;
}

bool HHbbtautauBoostedSelection::passTriggerSelection(const xAOD::EventInfo* /*evtinfo*/) {
  // TODO: fill in this
  if (m_debug) std::cout << "HH: bbtautauBoostedSelection::passTriggerSelection" << std::endl;
  return true;
}


EL::StatusCode HHbbtautauBoostedSelection::writeEventVariables(const xAOD::EventInfo* /*eventInfoIn*/,
                             xAOD::EventInfo* /*eventInfoOut*/,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string /*sysName*/,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

  return EL::StatusCode::SUCCESS;
}

