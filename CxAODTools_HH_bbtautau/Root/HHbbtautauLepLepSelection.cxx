#include "CxAODTools_HH_bbtautau/HHbbtautauLepLepSelection.h"
#include "CxAODTools_HH_bbtautau/TriggerTool_HH_bbtt.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include <iostream>
#include<TRandom.h>

HHbbtautauLepLepSelection::HHbbtautauLepLepSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
    HHbbtautauSelection("PreselectionLepCutFlow")
{
    config->getif<bool>("debug", m_debug);
    m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "leplep");
    m_triggerTool->initialize();
    m_channel = "leplep";
}

bool HHbbtautauLepLepSelection::passSelection(SelectionContainers & containers,
        bool isKinVar) {

    // here just as an example:
    // if a new passKinematics() function is defined with some variables in the prototype,
    // one needs to reimplement passSelection here
    // otherwise, don't need to put any code

    return HHbbtautauSelection<ResultHHbbtautau>::passSelection(containers, isKinVar);
}

bool HHbbtautauLepLepSelection::passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo)
{
    int res = doHHTauSelection(taus);

    // we want no hadronic taus
    if (res == 0) {
        return true;
    }
    return false;
}

bool HHbbtautauLepLepSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons,
        const xAOD::MuonContainer* muons,
        const xAOD::MissingET* met)
{
    const xAOD::Electron* el_junk1 = nullptr;
    const xAOD::Electron* el_junk2 = nullptr;
    const xAOD::Muon* mu_junk1 = nullptr;
    const xAOD::Muon* mu_junk2 = nullptr;

    int res = doHHLeptonSelection(electrons, muons, el_junk1, el_junk2, mu_junk1, mu_junk1);
    // electrons and muons are stored in the doHHLeptonPreSelection function directly in
    // the m_result.electrons and m_result.muons vectors.
    // TODO: one could in principle remove the electrons and muons from the arguments like it is done with taus,
    // however, this requires a major rewrite of lephad and hadhad code ...

    // set isSREvent if we have at least two electrons,
    m_result.isSREvent = m_result.electrons.size() + m_result.muons.size() >= 2;

    // we want at least 2 leptons
    // res is 2 if there are at least 2 loose leptons from which at least one is a signal lepton
    if(res == 2)
    {
        m_result.met = met;
        return true;
    }
    return false;
}

// preselection versions
bool HHbbtautauLepLepSelection::passPreSelection(SelectionContainers & containers,
        bool isKinVar) {

    // here just as an example:
    // if a new passKinematics() function is defined with some variables in the prototype,
    // one needs to reimplement passSelection here
    // otherwise, don't need to put any code
    return HHbbtautauSelection<ResultHHbbtautau>::passPreSelection(containers, isKinVar);
}

bool HHbbtautauLepLepSelection::passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
        const xAOD::MuonContainer* muons,
        const xAOD::MissingET* met) {
    return passLeptonSelection(electrons, muons, met);
}

bool HHbbtautauLepLepSelection::passKinematics() {
    // MJ cuts, like MET / MPT etc...
    // my advice is to add in passKinematics() prototype all the stuff that
    // doesn't need to be put in the Result struct, like MPT

    return true;
}

EL::StatusCode HHbbtautauLepLepSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
        xAOD::EventInfo* eventInfoOut,
        bool isKinVar,
        bool isWeightVar,
        std::string sysName,
        int rdm_RunNumber,
        CP::MuonTriggerScaleFactors* trig_sfmuon) {

    // Here vent variables can be written out

    return EL::StatusCode::SUCCESS;
}
