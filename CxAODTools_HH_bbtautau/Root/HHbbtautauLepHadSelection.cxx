#include "CxAODTools_HH_bbtautau/HHbbtautauLepHadSelection.h"
#include "CxAODTools_HH_bbtautau/TriggerTool_HH_bbtt.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include <iostream>
#include<TRandom.h>

#include "CxAODTools_HH_bbtautau/TauHelpers.h"


HHbbtautauLepHadSelection::HHbbtautauLepHadSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
  HHbbtautauSelection("PreselectionLepCutFlow"),
  m_antitau(false)
{
  config->getif<bool>("debug",m_debug);
  config->getif<bool>("AntiTau",m_antitau);
  config->getif<bool>("useRNNTaus",m_useRNNTaus);
  config->getif<std::string>("TauIDWP", m_tauIDWP);

  m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "lephad");
  m_triggerTool->initialize().ignore();

  m_passIDProp = getTauIDProp(m_tauIDWP, m_useRNNTaus);
  if (!m_passIDProp) {
    throw std::runtime_error("HHbbtautauLepHadSelection: Invalid Tau-ID working point.");
  }
}

bool HHbbtautauLepHadSelection::passSelection(SelectionContainers & containers,
					      bool isKinVar) {
  
  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
 
  return HHbbtautauSelection<ResultHHbbtautau>::passSelection(containers, isKinVar);
}

bool HHbbtautauLepHadSelection::passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* /*evtinfo*/)
{
  int res=doHHTauSelection(taus);
 
  if (m_antitau) {
    for (const auto& tau : m_result.taus) {
      // For anti-tau CR, veto events with any medium taus selected as will overlap with SR
      if (m_passIDProp->get(tau)) return false;
    }
  }

  // Restrict to == 1 in second selector (after random tau) for antitau
  // For signal taus require == 1 now since later on we remove taus not matched to trigger 
  // from results list (for input to random selection) but this may still veto a signal event
  // even if not passing the LTT trigger (as long as the other one does)
  if ((m_antitau && res >= 1) || (!m_antitau && res == 1)) {
    return true;
  }
  return false;
}

bool HHbbtautauLepHadSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons,
                                                    const xAOD::MuonContainer* muons,
                                                    const xAOD::MissingET* met)
{
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  int res = doHHLeptonSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 1)//(res<2) //no DLV
    {
      return false;
    }
  m_result.met = met;
  return true;
}

// preselection versions
bool HHbbtautauLepHadSelection::passPreSelection(SelectionContainers & containers,
						bool isKinVar) {
  
  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
  return HHbbtautauSelection<ResultHHbbtautau>::passPreSelection(containers, isKinVar);
}

bool HHbbtautauLepHadSelection::passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                                               const xAOD::MuonContainer* muons,
                                               const xAOD::MissingET* /*met*/) {
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  int res = doHHLeptonPreSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 1)//(res!=2) //inverse DLV. DLV is performed for loose leptons as well.
  {
     return false;
  }
  return true;
  
}

bool HHbbtautauLepHadSelection::passKinematics() {
  // MJ cuts, like MET / MPT etc...
  // my advice is to add in passKinematics() prototype all the stuff that
  // doesn't need to be put in the Result struct, like MPT

  return true;
}

EL::StatusCode HHbbtautauLepHadSelection::writeEventVariables(const xAOD::EventInfo* /*eventInfoIn*/,
                             xAOD::EventInfo* /*eventInfoOut*/,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string /*sysName*/,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

//   // ensure to write all variables for all events
//   Props::leptonSF.set(eventInfoOut, 1);
//   
//   if (!m_result.pass) return EL::StatusCode::SUCCESS;
//   
//   // lepton SF
//   if (m_result.isEl()) {
//     float leptonSF = 1;
//     leptonSF *= Props::effSFmediumLH.get(m_result.el);
//     Props::leptonSF.set(eventInfoOut, leptonSF);
//   } else {
//     float leptonSF = 1;
//     leptonSF *= Props::effSF.get(m_result.mu);
//     Props::leptonSF.set(eventInfoOut, leptonSF);
//   }

  return EL::StatusCode::SUCCESS;
}


/*bool HHbbtautauLepHadSelection::passTriggerSelection(const xAOD::EventInfo* evtinfo) {

  m_result.trigger = eNone;
  float GeV = 1000.;
  
  // Does it pass+match single electron trigger?
  const xAOD::Electron* e = m_result.el;
  if (e && e->pt() > 25 * GeV && 	
      Props::passHLT_e24_lhmedium_iloose_L1EM20VH.get(evtinfo) &&
      Props::matchHLT_e24_lhmedium_iloose_L1EM20VH.get(e)) {
    m_result.trigger = eSET;
    return true;
  }
  
  // Does it pass+match single muon trigger?
  const xAOD::Muon* m = m_result.mu;
  if (m && m->pt() > 21 * GeV &&
      Props::passHLT_mu20_iloose_L1MU15.get(evtinfo) && 
      Props::matchHLT_mu20_iloose_L1MU15.get(m)) {	
    m_result.trigger = eSMT;	
    return true;
  }
  
  // Does it pass+match e/mu+tau trigger.  Only keep those taus that do
  // Picking the random tau is dealt with in main analysis loop and 
  // resetting other properties by the tau handler

  for (auto itr = m_result.taus.begin(); itr != m_result.taus.end(); ) {
    const xAOD::TauJet* tau = *itr;
    if (tau->pt() > 30 * GeV) {
      if (e && e->pt() > 18 * GeV && 		
	  Props::passHLT_e17_lhmedium_tau25_medium1_tracktwo.get(evtinfo) &&
	  Props::matchHLT_e17_lhmedium_tau25_medium1_tracktwo.get(tau)) {  
	m_result.trigger = eETT;
	++itr;
      } else if (m && m->pt() > 15 * GeV &&
		 Props::passHLT_mu14_tau35_medium1_tracktwo.get(evtinfo) &&
		 Props::matchHLT_mu14_tau35_medium1_tracktwo.get(tau)) {
	m_result.trigger = eMTT;	
	++itr;
      } else {
	itr = m_result.taus.erase(itr);
      }
    } else {
      ++itr;
    }
  }
  return (m_result.trigger != eNone);
  }*/
