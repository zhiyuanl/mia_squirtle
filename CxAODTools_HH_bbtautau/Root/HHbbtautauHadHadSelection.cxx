#include "CxAODTools_HH_bbtautau/HHbbtautauHadHadSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

#include "CxAODTools_HH_bbtautau/TauHelpers.h"


HHbbtautauHadHadSelection::HHbbtautauHadHadSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) : 
 HHbbtautauSelection("PreselectionLepCutFlow"),
 m_antitau(false)
{
 config->getif<bool>("AntiTau",m_antitau);
 config->getif<bool>("debug",m_debug);
 config->getif<bool>("useRNNTaus",m_useRNNTaus);
 config->getif<std::string>("TauIDWP", m_tauIDWP);
 setDebug(m_debug);
 m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "hadhad");
 m_triggerTool->initialize().ignore(); //FIXME

 m_passIDProp = getTauIDProp(m_tauIDWP, m_useRNNTaus);
 if (!m_passIDProp) {
   throw std::runtime_error("HHbbtautauHadHadSelection: Invalid Tau-ID working point.");
 }
}

void HHbbtautauHadHadSelection::clearResult() {
  m_result.pass = false;
  m_result.taus.clear();
  m_result.matchedTausSTT.clear();
  m_result.matchedTausDTT.clear();
  m_result.signalJets.clear();
  m_result.forwardJets.clear();
  m_result.fatJets.clear();
  m_result.met = nullptr;
  m_result.mu=nullptr; 
  m_result.el=nullptr;
  m_result.trigger=eNone;
  m_result.triggerSF=1.;
  m_result.isSREvent=false;
  m_result.hasTightTau=false;
}

bool HHbbtautauHadHadSelection::passSelection(SelectionContainers & containers,
					      bool isKinVar) {

  // here just as an example:
  // if a new passKinematics() function is defined with some variables in the prototype,
  // one needs to reimplement passSelection here
  // otherwise, don't need to put any code
 
  return HHbbtautauSelection<ResultHHbbtautau>::passSelection(containers, isKinVar);
}

bool HHbbtautauHadHadSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons,
                                               const xAOD::MuonContainer* muons,
                                               const xAOD::MissingET* met) {
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  if (m_debug) std::cout << "HH:bbtautauHadHadSelection::passLeptonSelection" << std::endl;
  int res = doHHLeptonSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 0) {
    if (m_debug) std::cout << "HH: found Lepton. Failing event!" << std::endl;
    return false;
  }
  m_result.met = met;
  return true;

}

bool HHbbtautauHadHadSelection::passTauSelection(const xAOD::TauJetContainer* taus, const xAOD::EventInfo* /*evtinfo*/)
{
  if (m_debug) std::cout << "HH: in HHbbtautauHadHadSelection::passTauSelection() for variation" << m_sysName << std::endl;
  int res = doHHTauSelection(taus);

  if (m_debug) std::cout << "HH:  " << res << " taus were selected for variation" << m_sysName << std::endl;

  if (res<2) return false;
  // double triggerSF_nominal=1.;
  // int rdm_RunNumber=1;
  // m_triggerTool->setMatchedTaus(evtinfo, triggerSF_nominal, m_result.el, m_result.mu, m_result.taus, rdm_RunNumber, "NOMINAL" );
  // if (!m_triggerTool->getTriggerDecision()) {
  //   std::cout << "selected taus were not triggered" << std::endl;
  //   return false;
  // }

  // // Only do if antitau config (not separate slector as also needs to be done for DLV reversed and antiisol)
  // // m_result also contains medium taus for the m_antitau CR
  // // so we need a "selection" also if there are ==2 taus
  // if (m_antitau) res=doRandomTauSelection(evtinfo);

  // std::cout << "HF:  " << res << " taus survived the random selection" << std::endl;

  if (m_debug) std::cout << "HH: ";

  int nSignalTaus = 0; // i.e. taus passing the required tau WP
  std::vector<const xAOD::TauJet*> thesetaus = m_result.taus;
  for (auto itr = thesetaus.begin(); itr != thesetaus.end(); itr++) {
    const xAOD::TauJet* thistau = *itr;
    if (m_passIDProp->get(thistau)) nSignalTaus++;
    if (m_debug) std::cout << "  tau "
			   << " pt=" << thistau->pt()/1000.;
  }

  if (nSignalTaus==2)
    m_result.isSREvent = true;
  else
    m_result.isSREvent = false;

  if (m_debug) std::cout << std::endl;

  return true;

}

// preselection versions
bool HHbbtautauHadHadSelection::passPreSelection(SelectionContainers & containers,
						 bool isKinVar) {

  if (m_debug) std::cout << "HH: bbtautauHadHadSelection::passPreSelection for variation " << m_sysName << std::endl;
  return HHbbtautauSelection<ResultHHbbtautau>::passPreSelection(containers, isKinVar);
}

bool HHbbtautauHadHadSelection::passLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                                                       const xAOD::MuonContainer* muons,
                                                       const xAOD::MissingET* /*met*/)
{
  const xAOD::Electron* el_junk=nullptr;
  const xAOD::Muon* mu_junk=nullptr;
  if (m_debug) std::cout << "HH: bbtautauHadHadSelection::passLeptonPreSelection for variation" << m_sysName << std::endl;
  int res = doHHLeptonPreSelection(electrons, muons, m_result.el, el_junk, m_result.mu, mu_junk);
  if(res != 0) {
    if (m_debug) std::cout << "HH: found Lepton. Failed event for variation" << m_sysName << std::endl;
    return false;
  }
  return true;

}

bool HHbbtautauHadHadSelection::passKinematics() {
  // MJ cuts, like MET / MPT etc...
  // my advice is to add in passKinematics() prototype all the stuff that
  // doesn't need to be put in the Result struct, like MPT
  if (m_debug) std::cout << "HH: bbtautauHadHadSelection::passKinematics" << std::endl;

  return true;
}

bool HHbbtautauHadHadSelection::passTriggerSelection(const xAOD::EventInfo* /*evtinfo*/) {
  // TODO: fill in this
  if (m_debug) std::cout << "HH: bbtautauHadHadSelection::passTriggerSelection" << std::endl;
  return true;
}


EL::StatusCode HHbbtautauHadHadSelection::writeEventVariables(const xAOD::EventInfo* /*eventInfoIn*/,
                             xAOD::EventInfo* /*eventInfoOut*/,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string /*sysName*/,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

  return EL::StatusCode::SUCCESS;
}
