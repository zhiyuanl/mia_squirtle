#include "CxAODTools_HH_bbtautau/HHbbtautauResult.h"

// print out trigger enum value as text
// https://stackoverflow.com/a/3342891
std::ostream& operator<<(std::ostream& out, const Trigger value){
  const char* s = 0;
#define PROCESS_VAL(p) case(p): s = #p; break;
  switch(value){
    PROCESS_VAL(eNone);
    PROCESS_VAL(eSET);
    PROCESS_VAL(eDET);
    PROCESS_VAL(eSMT);
    PROCESS_VAL(eDMT);
    PROCESS_VAL(eEMT);
    PROCESS_VAL(eETT);
    PROCESS_VAL(eMTT);
    PROCESS_VAL(eSTT);
    PROCESS_VAL(eDTT);
    PROCESS_VAL(eBTT);
    PROCESS_VAL(eSkip);
    PROCESS_VAL(eFJT);
  }
#undef PROCESS_VAL
  return out << s;
}

