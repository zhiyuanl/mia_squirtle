#include "CxAODTools_HH_bbtautau/HHbbtautauBoostedJetSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

#include<TRandom.h>

HHbbtautauBoostedJetSelection::HHbbtautauBoostedJetSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
  HHbbtautauSelection("PreselectionJetCutFlow"),
  m_vetoResolvedTauEvents(false)
{
  m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "boosted");
  m_triggerTool->initialize();
  config->getif<bool>("debug",m_debug);
  config->getif<bool>("vetoResolvedTauEvents", m_vetoResolvedTauEvents);
  setDebug(m_debug);
}

bool HHbbtautauBoostedJetSelection::passDiTauJetSelection(const xAOD::DiTauJetContainer* ditaus)
{

  if (m_debug) std::cout << "HH:bbtautauBoostedSelection::passLeptonSelection" << std::endl;
  int res = doHHDiTauJetSelection(ditaus);
  if(res >= 1) {
    if (m_debug) std::cout << "HH: found DiTauJet. Good event!" << std::endl;
    return false;
  }
  return true;

}

bool HHbbtautauBoostedJetSelection::passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* /*evtinfo*/)
{

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passTauSelection() vor variation " << m_sysName << std::endl;
  int res = doHHTauSelection(taus);
  if (m_vetoResolvedTauEvents)
    return (res == 0);

  return true;
}

bool HHbbtautauBoostedJetSelection::passSelection(SelectionContainers & containers,
						 bool /*isKinVar*/) {
  

  //const xAOD::MissingET          * met       = containers.met;
  //const xAOD::ElectronContainer  * electrons = containers.electrons;
  //const xAOD::MuonContainer      * muons     = containers.muons;
  const xAOD::JetContainer       * jets      = containers.jets;
  const xAOD::JetContainer       * fatjets   = containers.fatjets;
  const xAOD::JetContainer       * trackjets   = containers.trackjets;
  const xAOD::TauJetContainer    * taus      = containers.taus;
  const xAOD::EventInfo          * evtinfo   = containers.evtinfo;

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passSelection" << std::endl;

  // assume that T has a field bool pass
  clearResult();

  if (! passTauSelection(taus, evtinfo)){
    m_result.pass = false;
    return false;
  }

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetSelection(jets);
  bool passFatJetSel = false;
  bool passTrackJetSel = false;

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetSelection(fatjets);
  }

  if (trackjets) {
    passTrackJetSel = HHbbtautauSelection<ResultHHbbtautau>::passTrackJetSelection(trackjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    m_result.pass = false;
    return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true; 
  return true;
}

// preselection versions
bool HHbbtautauBoostedJetSelection::passPreSelection(SelectionContainers & containers,
						 bool isKinVar) {
  const xAOD::EventInfo         * evtinfo   = containers.evtinfo;
  //const xAOD::MissingET         * met       = containers.met;
  //const xAOD::ElectronContainer * electrons = containers.electrons;
  //const xAOD::MuonContainer     * muons     = containers.muons;
  const xAOD::JetContainer      * jets      = containers.jets;
  const xAOD::JetContainer      * fatjets   = containers.fatjets;
  const xAOD::JetContainer      * trackjets   = containers.trackjets;
  const xAOD::TauJetContainer   * taus      = containers.taus;  

  if (m_debug) std::cout << "HH: in HHbbtautauHadHad*Jet*Selection::passPreSelection() for variation " << m_sysName << std::endl;


  clearResult();

  if (! passTauSelection(taus, evtinfo)) {
    m_result.pass = false;
    if (m_debug) std::cout << "HH: bbtautauHadHad*Jet*Selection passPreSelection: failed tau selection for variation " << m_sysName << std::endl;
    return false;
  }
  if(!isKinVar) m_cutFlow.count("Preselection taus");
  
  // Reset trigger flag from first selector
  //m_result.trigger = (Trigger)Props::HHbbtautauTrigger.get(evtinfo);

  bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetPreSelection(jets);
  bool passFatJetSel = false;  // HF set false, otherwise jet selection never fails
  bool passTrackJetSel = false; // true after HHbbtautauSelection

  if (fatjets) {
    passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetPreSelection(fatjets);
  }

  if (trackjets) {
    passTrackJetSel = HHbbtautauSelection<ResultHHbbtautau>::passTrackJetPreSelection(trackjets);
  }

  if (!passJetSel && !passFatJetSel ) {
    m_result.pass = false;
    if (m_debug) std::cout << "HH: FAILED jet selection for variation " << m_sysName << std::endl;
    return false;
  }

  if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
    m_result.pass = false;
    return false;
  }

  m_result.pass = true;  

  if(!isKinVar) m_cutFlow.count("Preselection jet");

  if (m_debug) std::cout << "HH: Selection result for variation" << m_sysName << ": e " 
	    << m_result.el << " m "
	    << m_result.mu << " taus "
    	    << m_result.taus.size() << " " <<  (m_result.taus.size() ? m_result.taus.at(0) : 0) << " jets "
	    << m_result.signalJets.size() << " trig " 
	    << m_result.trigger << std::endl;

  // Store result (per-syst) to be picked up by MMC
  m_sysResults[m_sysName] = m_result;

  return true;
}

EL::StatusCode HHbbtautauBoostedJetSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
                             xAOD::EventInfo* eventInfoOut,
                             bool /*isKinVar*/,
                             bool /*isWeightVar*/,
			     std::string sysName,
			     int /*rdm_RunNumber*/,
			     CP::MuonTriggerScaleFactors* /*trig_sfmuon*/) {

  // TODO: write out correctly fule lepton SF + trigger SF for correct trigger based on enum and 
  //       chosen random tau
  //

  if (!m_result.pass) return EL::StatusCode::SUCCESS;

   if( m_debug ){
    std::cout << "HH: " << m_result.matchedTausSTT.size() << " Trigger: " << m_result.trigger << std::endl;
    std::cout << "HH: " << m_result.matchedTausDTT.size() << " Trigger: " << m_result.trigger << std::endl;
    std::cout << "HH: bbtautauHadHad*Jet*Selection::writeEventVariables: calling getTriggerSF()" << std::endl;
  }

  m_triggerTool->getTriggerSF(eventInfoIn,m_result,sysName);
  
  if ( m_debug ) std::cout << "HH: trigSF: " << m_result.triggerSF << std::endl;

  Props::trigSF.set(eventInfoOut, m_result.triggerSF); 

  return EL::StatusCode::SUCCESS;
}

void HHbbtautauBoostedJetSelection::clearResult() {
  m_result = m_sysResults[m_sysName];
  // Clear taus as need to redo them after random picking
  m_result.taus.clear();
}
