// CxAODTools includes
#include "CxAODTools_HH_bbtautau/TriggerTool_HH_bbtt.h"
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools/ReturnCheck.h"
#include "xAODEventInfo/EventInfo.h"
#include "CxAODTools_HH_bbtautau/HHbbtautauResult.h"
#include "CxAODTools_HH_bbtautau/TauHelpers.h"
#include "iostream"


// Shorthand for pass / match props for trigger
using TriggerTuple = std::tuple<PROP<int>*,PROP<int>*>;

// Forward declarations
bool pass_and_match_dtt(const TriggerTuple &prop_tuple,
                        const xAOD::EventInfo * eventInfo,
                        const xAOD::TauJet *lead_tau,
                        const xAOD::TauJet *sublead_tau);


TriggerTool_HH_bbtt::TriggerTool_HH_bbtt(ConfigStore & config, EventInfoHandler & eventInfoHandler, std::string triggerType) :
  m_config(config),
  m_nominalOnly(true),
  m_is50ns(false),
  m_skipTriggerSelection(false),
  m_eventInfoHandler(eventInfoHandler),
  m_trig_sfmuon(nullptr),
  m_TriggerMenu("CxAODtag16")  // BR: do we still need this?
{
  m_config.getif<bool>("debug", m_debug);
  m_analysisType = triggerType;
  m_config.getif<bool>("nominalOnly", m_nominalOnly);
  // explicit flag for 50ns -> needed for electron triggers
  m_config.getif<bool>("is50ns", m_is50ns);
  m_config.getif<bool>("AntiTau", antiTau);
  m_config.getif<std::string>("TriggerMenu",m_TriggerMenu);
  if (m_debug) std::cout << "TriggerMenu = " << m_TriggerMenu << std::endl;
  m_config.getif<bool>("skipTriggerSelection", m_skipTriggerSelection);
}

TriggerTool_HH_bbtt::~TriggerTool_HH_bbtt()
{
  delete m_trig_sfmuon;
}

EL::StatusCode TriggerTool_HH_bbtt::initialize()
{
  Info("TriggerTool_HH_bbtt::initialize()", "Initialize trigger.");

  // Setup offline Tau-ID working point
  m_config.getif<bool>("useRNNTaus", m_useRNNTaus);
  m_config.getif<std::string>("TauIDWP", m_tauIDWP);
  m_passIDProp = getTauIDProp(m_tauIDWP, m_useRNNTaus);
  if (!m_passIDProp) {
    Error("TriggerTool_HH_bbtt::initialize()", "Invalid Tau-ID working point.");
    return EL::StatusCode::FAILURE;
  }

  // BR: what are these lines doing? good_config is always 1 ...
  // wrong configs
  bool good_config = 1;
  if(!good_config) Fatal("TriggerTool::initialize()","Trigger configuration %s is not valid.",m_TriggerMenu.c_str());

  // muon sf tool
  if (m_analysisType == "lephad") {
    m_trig_sfmuon = new CP::MuonTriggerScaleFactors("TrigSFClass_HHBBTT");
    TOOL_CHECK("TriggerTool::initialize muon trig SF", m_trig_sfmuon->setProperty("MuonQuality","Medium"));
    TOOL_CHECK("TriggerTool::initialize muon trig SF", m_trig_sfmuon->initialize());
  }

  // trigger systematics
  if (!m_nominalOnly) {
    m_trig_Sys.push_back("MUON_EFF_TrigSystUncertainty__1up");
    m_trig_Sys.push_back("MUON_EFF_TrigSystUncertainty__1down");
    m_trig_Sys.push_back("MUON_EFF_TrigStatUncertainty__1up");
    m_trig_Sys.push_back("MUON_EFF_TrigStatUncertainty__1down");
  }

  // print of systs defined at reader level
  for (size_t i = 0; i < m_trig_Sys.size(); i++) {
    Info("TriggerTool::initialize", "%s analysis will use %s systematics.", m_analysisType.c_str(), m_trig_Sys.at(i).c_str());
  }

  //-------------------------------------------------------
  return EL::StatusCode::SUCCESS;
}


void TriggerTool_HH_bbtt::getTriggerDecision(const xAOD::EventInfo * eventInfo, ResultHHbbtautau & result){

  if (m_debug) std::cout << "HH: TriggerTool_HH_bbtt::getTriggerDecision()" << std::endl;
  float GeV = 1000.;

  if (m_skipTriggerSelection) {
    result.matchedTausSTT.clear();
    result.matchedTausDTT.clear();
    result.trigger = eSkip;
    return;
  }
  bool isMC = Props::isMC.get(eventInfo);
  double rdmNumber=0;
  if (isMC) rdmNumber = Props::RandomRunNumber.get(eventInfo);
  else rdmNumber=eventInfo->runNumber();
  
  //std::cout << "Run number "<< rdmNumber << std::endl;
  //efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
  // need to add them in the trigger list ??
  if(m_analysisType=="lephad") {
    const xAOD::Electron * e = result.el;
    const xAOD::Muon * m = result.mu;
    std::vector<const xAOD::TauJet*>& taus = result.taus;
    std::vector<const xAOD::TauJet*> tausToErase;

    if(rdmNumber<=284484){
      //std::cout << "2015" << std::endl;
      // Does it pass+match single electron trigger?
      if (e && e->pt() > 25 * GeV && 	
	  ((Props::passHLT_e24_lhmedium_iloose_L1EM20VH.get(eventInfo) &&
	   Props::matchHLT_e24_lhmedium_iloose_L1EM20VH.get(e))||
	  (Props::passHLT_e60_lhmedium.get(eventInfo) &&
	  Props::matchHLT_e60_lhmedium.get(e))||
	  (Props::passHLT_e120_lhloose.get(eventInfo) &&
	   Props::matchHLT_e120_lhloose.get(e)))
	  ) {
	if (m_debug) std::cout << "TRIG: SET" << std::endl;
	result.trigger = eSET;
	
	return;
      }

      // Does it pass+match single muon trigger?
      if (m && m->pt() > 21 * GeV &&     
	  ((Props::passHLT_mu20_iloose_L1MU15.get(eventInfo) && 
	  Props::matchHLT_mu20_iloose_L1MU15.get(m))||
	  (Props::passHLT_mu50.get(eventInfo) && 
	   Props::matchHLT_mu50.get(m))) ){
	  
	if (m_debug) std::cout << "TRIG: SMT" << std::endl;
	result.trigger = eSMT;
	
	return;
      }

    }

    else if(rdmNumber>284484 && rdmNumber<325558){ //all of 2016 (and 2017 A but no data in there)
      
      
	// Does it pass+match single electron trigger?
	if (e && e->pt() > 27 * GeV && 	
	    ((Props::passHLT_e26_lhtight_nod0_ivarloose.get(eventInfo) &&
	      Props::matchHLT_e26_lhtight_nod0_ivarloose.get(e))||
	     (Props::passHLT_e60_lhmedium_nod0.get(eventInfo) &&
	      Props::matchHLT_e60_lhmedium_nod0.get(e))||
	     (Props::passHLT_e140_lhloose_nod0.get(eventInfo) &&
	      Props::matchHLT_e140_lhloose_nod0.get(e))) )
	  {
	  if (m_debug)std::cout << "TRIG: SET" << std::endl;
	  result.trigger = eSET;
	  
	  return;
	}
	
	
      // Does it pass+match single muon trigger? 
      if (m && m->pt() > 27 * GeV &&
	  ((Props::passHLT_mu26_ivarmedium.get(eventInfo) && 
	   Props::matchHLT_mu26_ivarmedium.get(m))||
	  (Props::passHLT_mu50.get(eventInfo) &&
	   Props::matchHLT_mu50.get(m))) ){
	if (m_debug)std::cout << "TRIG: SMT" << std::endl;
	result.trigger = eSMT;
	
	return;
      }
    }

    else if (rdmNumber>=325713 && rdmNumber<=341649){ // B onwards (all) 2017
      
      
      //if(!isMC){
	// Does it pass+match single electron trigger?
	if (e && e->pt() > 27 * GeV && 	
	    ((Props::passHLT_e26_lhtight_nod0_ivarloose.get(eventInfo) &&
	      Props::matchHLT_e26_lhtight_nod0_ivarloose.get(e))||
	     (Props::passHLT_e60_lhmedium_nod0.get(eventInfo) &&
	      Props::matchHLT_e60_lhmedium_nod0.get(e))||
	     (Props::passHLT_e140_lhloose_nod0.get(eventInfo) &&
	      Props::matchHLT_e140_lhloose_nod0.get(e))||
	     (Props::passHLT_e300_etcut.get(eventInfo) &&
	      Props::matchHLT_e300_etcut.get(e))) )
	  {
	  if (m_debug)std::cout << "TRIG: SET" << std::endl;
	  result.trigger = eSET;
	  //std::cout << "TRIG: SET " <<result.trigger << std::endl;
	  return;
	}

	
      // Does it pass+match single muon trigger? 
      if (m && m->pt() > 27 * GeV &&
	  ((Props::passHLT_mu26_ivarmedium.get(eventInfo) && 
	   Props::matchHLT_mu26_ivarmedium.get(m))||
	  (Props::passHLT_mu50.get(eventInfo) &&
	   Props::matchHLT_mu50.get(m))||
	   (Props::passHLT_mu60_0eta105_msonly.get(eventInfo) &&
	   Props::matchHLT_mu60_0eta105_msonly.get(m))) )
	{
	if (m_debug)std::cout << "TRIG: SMT" << std::endl;
	result.trigger = eSMT;
	// std::cout << "TRIG: SMT " << result.trigger << std::endl;
	// if (eSMT==0) std::cout << "TRIG: SMT fail" << std::endl;
	return;
      }
    }
    else if (rdmNumber>=348885){ // B1 onwards 2018
      
      
      //if(!isMC){
	// Does it pass+match single electron trigger?
	if (e && e->pt() > 27 * GeV && 	
	    ((Props::passHLT_e26_lhtight_nod0_ivarloose.get(eventInfo) &&
	      Props::matchHLT_e26_lhtight_nod0_ivarloose.get(e))||
	     //(Props::passHLT_e26_lhtight_nod0.get(eventInfo) &&
	     //Props::matchHLT_e26_lhtight_nod0.get(e))||
	     (Props::passHLT_e60_lhmedium_nod0.get(eventInfo) &&
	      Props::matchHLT_e60_lhmedium_nod0.get(e))||
	     (Props::passHLT_e140_lhloose_nod0.get(eventInfo) &&
	      Props::matchHLT_e140_lhloose_nod0.get(e))||
	     (Props::passHLT_e300_etcut.get(eventInfo) &&
	      Props::matchHLT_e300_etcut.get(e)))) 
	  {
	  if (m_debug)std::cout << "TRIG: SET" << std::endl;
	  result.trigger = eSET;
	  //std::cout << "TRIG: SET " <<result.trigger << std::endl;
	  return;
	}
	
	
      // Does it pass+match single muon trigger? 
      if (m && m->pt() > 27 * GeV &&
	  ((Props::passHLT_mu26_ivarmedium.get(eventInfo) && 
	   Props::matchHLT_mu26_ivarmedium.get(m))||
	  (Props::passHLT_mu50.get(eventInfo) &&
	   Props::matchHLT_mu50.get(m))||
	   (Props::passHLT_mu60_0eta105_msonly.get(eventInfo) &&
	   Props::matchHLT_mu60_0eta105_msonly.get(m))) )
	{
	if (m_debug)std::cout << "TRIG: SMT" << std::endl;
	result.trigger = eSMT;
	// std::cout << "TRIG: SMT " << result.trigger << std::endl;
	// if (eSMT==0) std::cout << "TRIG: SMT fail" << std::endl;
	return;
      }
    }        
  

    // Does it pass+match e/mu+tau trigger.  Only keep those taus that do
    // Picking the random tau is dealt with in main analysis loop and
    // resetting other properties by the tau handler

    //bool runTauLoop = true;
    if (taus.size()>0){      //switch to turn off the tau loop and the lepton/tau triggers 
    
    for (auto itr = taus.begin(); itr != taus.end(); ) {
      const xAOD::TauJet* tau = *itr;
      
      if (tau->pt() > 30 * GeV) {
	
	if (rdmNumber<=300287){// 2015 and period A
	  if (e && e->pt() > 18 * GeV &&
	      Props::passHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo.get(eventInfo) &&
	      Props::matchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo.get(tau)) {  
	    result.trigger = eETT;
	    
	    if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	    ++itr;
	    continue;
	  }
	  else if (m && m->pt() > 15 * GeV &&
		   Props::passHLT_mu14_tau25_medium1_tracktwo.get(eventInfo) && 
		   Props::matchHLT_mu14_tau25_medium1_tracktwo.get(tau)) {
	    result.trigger = eMTT;
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr;
	    continue;
	  }
	  else {
	    tausToErase.push_back(tau);
	    ++itr; continue;
	  }
	}
	else if(rdmNumber>300287 && rdmNumber<325558){ //Period B till the end of 2016 and 2017 A
	 
	  if (e && e->pt() > 18 * GeV &&
	      Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo.get(eventInfo) &&
	      Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo.get(tau)) {  
	    result.trigger = eETT;
	    
	    if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	    ++itr; continue;
	  }
		
	
	  else if (m && !isMC && m->pt() > 15 * GeV &&
	  	   Props::passHLT_mu14_ivarloose_tau25_medium1_tracktwo.get(eventInfo) && 
	  	   Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwo.get(tau)) {
	    result.trigger = eMTT;
	    
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr; continue;
	  }

	  else if (m && isMC && m->pt() > 15 * GeV &&
	  	   Props::passHLT_mu14_iloose_tau25_medium1_tracktwo.get(eventInfo) && 
	  	   Props::matchHLT_mu14_iloose_tau25_medium1_tracktwo.get(tau)) {
	    result.trigger = eMTT;
	    
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr; continue;
	  }
	  
	  else {
	    tausToErase.push_back(tau);
	    ++itr; continue;
	  }
	}
	else if(rdmNumber>=325713 && rdmNumber<=326695){ //B1-B4 2017 
	  std::cout<<"early 2017"<<std::endl;
	  if (e && e->pt() > 18 * GeV &&
	      ((Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo.get(eventInfo) &&
	      Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo.get(tau))||
	       (Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12.get(eventInfo) &&
		Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12.get(tau) ))) {  
	    result.trigger = eETT;
	    
	    if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	    ++itr; continue;
	  }
		
	
	  else if (m && m->pt() > 15 * GeV &&
	  	   (Props::passHLT_mu14_ivarloose_tau25_medium1_tracktwo.get(eventInfo) && 
	  	   Props::matchHLT_mu14_ivarloose_tau25_medium1_tracktwo.get(tau))) {
	    result.trigger = eMTT;
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr; continue;
	  }
	  
	  else {
	    tausToErase.push_back(tau);
	    ++itr; continue;
	  }
	}
	else if (rdmNumber>=326834  && rdmNumber<=341649){ //B5-end 2017 
	  
	  if (e && e->pt() > 18 * GeV &&
	      ((Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR_EM15TAU12I_J25.get(eventInfo) &&
	       Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1DR_EM15TAU12I_J25.get(tau))||
	       (Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12.get(eventInfo) &&
		Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo_L1EM15VHI_2TAU12IM_4J12.get(tau) ))) {  
	    result.trigger = eETT;
	    if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	    ++itr; continue;
	  }
	  
	  else if (m && m->pt() > 15 * GeV && // both MC and data
		   (tau->pt() > 40 * GeV) &&
	  	   ((Props::passHLT_mu14_ivarloose_tau35_medium1_tracktwo.get(eventInfo) && // are the tau25 or tau35?
		     Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwo.get(tau)))) { 
	    
	    result.trigger = eMTT;
	    
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr; continue;
	  }
	  
	  
	  else {
	    tausToErase.push_back(tau);
	    ++itr; continue;
	  }
	}
	else if (rdmNumber>=348885 && rdmNumber<=364485){ // B-J 2018 rdmNumber<=355468
	  
	  if (e && e->pt() > 18 * GeV &&
	      ((Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1DR_EM15TAU12I_J25.get(eventInfo) &&
		Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1DR_EM15TAU12I_J25.get(tau))||
	       (Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1EM15VHI_2TAU12IM_4J12.get(eventInfo) &&
		Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF_L1EM15VHI_2TAU12IM_4J12.get(tau) ))) {  
	    result.trigger = eETT;
	    if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	    ++itr; continue;
	  }
	  
	  else if (m && m->pt() > 15 * GeV && 
		   (tau->pt() > 40 * GeV) &&
	  	   (Props::passHLT_mu14_ivarloose_tau35_medium1_tracktwoEF.get(eventInfo) && 
		    Props::matchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF.get(tau))) { 
	    result.trigger = eMTT;
	    
	    if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	    ++itr; continue;
	  }
	  
	  
	  else {
	    tausToErase.push_back(tau);
	    ++itr; continue;
	  }
	}

	// else if (rdmNumber>=355529 && rdmNumber<=364485){ // K-  2018
	  
	//   if (e && e->pt() > 18 * GeV &&
	//       ((Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR_EM15TAU12I_J25.get(eventInfo) &&
	// 	Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1DR_EM15TAU12I_J25.get(tau))||
	//        (Props::passHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1EM15VHI_2TAU12IM_4J12.get(eventInfo) &&
	// 	Props::matchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA_L1EM15VHI_2TAU12IM_4J12.get(tau) ))) {  
	//     result.trigger = eETT;
	//     if (m_debug) std::cout << "TRIG: ETT" << std::endl;
	//     ++itr; continue;
	//   }
	  
	//   else if (m && m->pt() > 15 * GeV &&
	//   	   (Props::passHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA.get(eventInfo) && 
	// 	     Props::matchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA.get(tau))) { 
	    
	//     result.trigger = eMTT;
	    
	//     if (m_debug) std::cout << "TRIG: MTT" << std::endl;
	//     ++itr; continue;
	//   }
	  
	  
	//   else {
	//     tausToErase.push_back(tau);
	//     ++itr; continue;
	//   }
	// }
	
      }
      else {
	++itr;
      }
    }
    }//if loop over taus
  }
  
  else if(m_analysisType=="hadhad"){
    // References:
    // https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/rBR_Period_Report.php
    // https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled
    //
    // 2015 AllYear 266904:284484
    // 2016 AllYear 296939:311481
    // 2017 AllYear 324320:341649
    // 2018 AllYear 348197:364485
    //
    // 2016 Period A 296939:300287
    // 2016 Period B-D3 300345:302872
    // 2016 Period D4- 302919:311481
    // 2017 Period B1-B4 325713:326695
    // 2017 Period B5-B7 326834:327490
    // 2017 Period B8- 327582:341649
    // 2018 Period K- 355529:364485

    const bool is15 = 266904 <= rdmNumber && rdmNumber <= 284484;
    //const bool is16 = 296939 <= rdmNumber && rdmNumber <= 311481;
    const bool is17 = 324320 <= rdmNumber && rdmNumber <= 341649;
    const bool is18 = 348197 <= rdmNumber && rdmNumber <= 364485;

    const bool is16PeriodA = 296939 <= rdmNumber && rdmNumber <= 300287;
    const bool is16PeriodB_D3 = 300345 <= rdmNumber && rdmNumber <= 302872;
    const bool is16PeriodD4_end = 302919 <= rdmNumber && rdmNumber <= 311481;
    const bool is17PeriodB1_B4 = 325713 <= rdmNumber && rdmNumber <= 326695;
    const bool is17PeriodB5_B7 = 326834 <= rdmNumber && rdmNumber <= 327490;
    const bool is17PeriodB8_end = 327582 <= rdmNumber && rdmNumber <= 341649;
    const bool is18PeriodK_end = 355529 <= rdmNumber && rdmNumber <= 364485;

    std::vector<const xAOD::TauJet *> taus = result.taus;
    result.taus.clear();
    result.matchedTausSTT.clear();
    result.matchedTausDTT.clear();

    // Check if rdmNumber falls into configured periods
    if (!(is15 || is16PeriodA || is16PeriodB_D3 || is16PeriodD4_end
          || is17PeriodB1_B4 || is17PeriodB5_B7 || is17PeriodB8_end
          || is18) && rdmNumber != 0) {
      Warning("TriggerTool::getTriggerDecision", "rdmNumber = %lu doesn't fit in configured periods. Update TriggerTool!",
              static_cast<unsigned long>(rdmNumber));
    }

    // Check STT
    bool passSTT = false;
    for (auto iter = taus.begin(); iter != taus.end(); iter++) {
      auto tau = *iter;

      if ((is15 || is16PeriodA)
          && tau->pt() > 100 * GeV
          && Props::passHLT_tau80_medium1_tracktwo_L1TAU60.get(eventInfo)
          && Props::matchHLT_tau80_medium1_tracktwo_L1TAU60.get(tau)) {
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      } else if (is16PeriodB_D3
                 && tau->pt() > 140 * GeV
                 && Props::passHLT_tau125_medium1_tracktwo.get(eventInfo)
                 && Props::matchHLT_tau125_medium1_tracktwo.get(tau)) {
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      } else if ((is16PeriodD4_end || is17PeriodB1_B4)
                 && tau->pt() > 180 * GeV
                 && Props::passHLT_tau160_medium1_tracktwo.get(eventInfo)
                 && Props::matchHLT_tau160_medium1_tracktwo.get(tau)) {
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      } else if ((is17PeriodB5_B7 || is17PeriodB8_end)
                 && tau->pt() > 180 * GeV
                 && Props::passHLT_tau160_medium1_tracktwo_L1TAU100.get(eventInfo)
                 && Props::matchHLT_tau160_medium1_tracktwo_L1TAU100.get(tau)) {
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      } else if (is18 && tau->pt() > 180 * GeV
                 && Props::passHLT_tau160_medium1_tracktwoEF_L1TAU100.get(eventInfo)
                 && Props::matchHLT_tau160_medium1_tracktwoEF_L1TAU100.get(tau)) {
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      } else if (is18PeriodK_end && tau->pt() > 180 * GeV
                 && Props::passHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100.get(eventInfo)
                 && Props::matchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100.get(tau)) {
        // In 2018 starting from period K the recommendation is to use a
        // logical OR between BDT (medium1_tracktwoEF) and RNN triggers (mediumRNN_tracktwoMVA)
        passSTT = true;
        result.matchedTausSTT.push_back(tau);
      }
    }

    if (passSTT) {
      result.taus = taus;
      result.trigger = eSTT;
      return;
    }

    // Check DTT
    bool passDTT = false;
    PROP<int> *matchDTT = nullptr;

    const TriggerTuple dtt_15_no_j25 = {
        &Props::passHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
        &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM};

    const TriggerTuple dtt_16_j25 = {
        &Props::passHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
        &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo};

    const TriggerTuple dtt_17_4j12 = {
        &Props::passHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM_4J12,
        &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM_4J12};

    const TriggerTuple dtt_17_dR = {
        &Props::passHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR_TAU20ITAU12I_J25,
        &Props::matchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1DR_TAU20ITAU12I_J25};

    const TriggerTuple dtt_18_4j12_bdt = {
        &Props::passHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1TAU20IM_2TAU12IM_4J12_0ETA23,
        &Props::matchHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1TAU20IM_2TAU12IM_4J12_0ETA23};

    const TriggerTuple dtt_18_4j12_rnn = {
        &Props::passHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1TAU20IM_2TAU12IM_4J12_0ETA23,
        &Props::matchHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1TAU20IM_2TAU12IM_4J12_0ETA23};

    const TriggerTuple dtt_18_dR_bdt = {
        &Props::passHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1DR_TAU20ITAU12I_J25,
        &Props::matchHLT_tau35_medium1_tracktwoEF_tau25_medium1_tracktwoEF_L1DR_TAU20ITAU12I_J25};

    const TriggerTuple dtt_18_dR_rnn = {
        &Props::passHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1DR_TAU20ITAU12I_J25,
        &Props::matchHLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_L1DR_TAU20ITAU12I_J25};

    for (auto iterA = taus.begin(); iterA != taus.end(); iterA++) {
      for (auto iterB = iterA + 1; iterB != taus.end(); iterB++) {
        const auto tauA = *iterA; // Higher pt tau
        const auto tauB = *iterB; // Lower pt tau

        // Factoring out tau thresholds since they are the same for all DTTs
        if (!(tauA->pt() > 40. * GeV && tauB->pt() > 30. * GeV)) { continue; }

        if (is15
            && pass_and_match_dtt(dtt_15_no_j25, eventInfo, tauA, tauB)) {
          passDTT = true;
          matchDTT = std::get<1>(dtt_15_no_j25);
        } else if ((is16PeriodA || is16PeriodB_D3 || is16PeriodD4_end || is17PeriodB1_B4)
                   && pass_and_match_dtt(dtt_16_j25, eventInfo, tauA, tauB)) {
          // Warning: There is temporal overlap with 4J12 which ran all year in
          // 2017 This is because we are running an OR between 4J12 and L1Topo
          // in 17 and we need a backup for periods where L1Topo was not yet
          // available
          passDTT = true;
          matchDTT = std::get<1>(dtt_16_j25);
        } else if (is17
                   && pass_and_match_dtt(dtt_17_4j12, eventInfo, tauA, tauB)) {
          passDTT = true;
          matchDTT = std::get<1>(dtt_17_4j12);
        } else if ((is17PeriodB5_B7 || is17PeriodB8_end)
                   && pass_and_match_dtt(dtt_17_dR, eventInfo, tauA, tauB)) {
          passDTT = true;
          matchDTT = std::get<1>(dtt_17_dR);
        }
        // In 2018 we use an OR of L1Topo and 4J12 trigger. Additionally, it is
        // recommended to use a logical OR of BDT (medium1_tracktwoEF) and RNN
        // triggers (mediumRNN_tracktwoMVA)
        else if (is18
                 && pass_and_match_dtt(dtt_18_4j12_bdt, eventInfo, tauA, tauB)) {
          // BDT 4J12 (2018)
          passDTT = true;
          matchDTT = std::get<1>(dtt_18_4j12_bdt);
        } else if (is18PeriodK_end
                   && pass_and_match_dtt(dtt_18_4j12_rnn, eventInfo, tauA, tauB)) {
          // RNN 4J12 (2018 Period K-)
          passDTT = true;
          matchDTT = std::get<1>(dtt_18_4j12_rnn);
        } else if (is18
                   && pass_and_match_dtt(dtt_18_dR_bdt, eventInfo, tauA, tauB)) {
          // BDT L1Topo (2018)
          passDTT = true;
          matchDTT = std::get<1>(dtt_18_dR_bdt);
        } else if (is18PeriodK_end
                   && pass_and_match_dtt(dtt_18_dR_rnn, eventInfo, tauA, tauB)) {
          // RNN L1Topo (2018 Period K-)
          passDTT = true;
          matchDTT = std::get<1>(dtt_18_dR_rnn);
        }
      }
    }

    // Add all trigger-matched taus passing the lower threshold (can be more than 2)
    if (passDTT) {
      for (auto tau : taus) {
        if (matchDTT->get(tau) && tau->pt() > 30 * GeV) {
          result.taus.push_back(tau);
          result.matchedTausDTT.push_back(tau);
        }
      }
      result.trigger = eDTT;
      return;
    }
  }
  
  else if(m_analysisType=="boosted"){
    bool passFJT(false);
    // references:
    // https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/runBrowserReport.php?fnt=data16_13TeV&pn=AllYear&runs=&gfile=&smk=&cn=HLT_j*_a10r*&stt=&stm=
    // https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/runBrowserReport.php?fnt=data17_13TeV&pn=B&runs=&gfile=&smk=&cn=HLT_j*_a10r*&stt=&stm=

    const bool passFJT360 = Props::passHLT_j360_a10r_L1J100.get(eventInfo);
    const bool passFJT400 = Props::passHLT_j400_a10r_L1J100.get(eventInfo);
    const bool passFJT420 = Props::passHLT_j420_a10r_L1J100.get(eventInfo);
    const bool passFJT440 = Props::passHLT_j440_a10r_L1J100.get(eventInfo);
    const bool passFJT460 = Props::passHLT_j460_a10r_L1J100.get(eventInfo);

    const bool is15 = 266904 <= rdmNumber && rdmNumber <= 284484;
    const bool is16PeriodA = 296939 <= rdmNumber && rdmNumber <= 300287;
    const bool is16PeriodB = 300345 <= rdmNumber && rdmNumber <= 300908;
    const bool is16PeriodC = 301912 <= rdmNumber  &&  rdmNumber <= 302393;
    const bool is16PeriodD = 302737 <= rdmNumber && rdmNumber <= 303560;
    const bool is16PeriodE = 303638 <= rdmNumber && rdmNumber <= 303892;
    const bool is16PeriodF_end = 303943 <= rdmNumber && rdmNumber <= 311481;
    const bool is17PeriodB = 325713 <= rdmNumber && rdmNumber <= 328393;
    const bool is17PeriodC = 329385 <= rdmNumber && rdmNumber <= 330470;
    const bool is17PeriodD = 330857 <= rdmNumber && rdmNumber <= 332304;
    const bool is17PeriodE = 332720 <= rdmNumber && rdmNumber <= 334779;
    const bool is17PeriodF = 334842 <= rdmNumber && rdmNumber <= 335290;
    const bool is17PeriodH = 336497 <= rdmNumber && rdmNumber <= 336782;
    const bool is17PeriodI = 336832 <= rdmNumber && rdmNumber <= 337833;
    const bool is17PeriodK = 338183 <= rdmNumber && rdmNumber <= 340453;
    const bool is18 = 348197 <= rdmNumber && rdmNumber <= 364485;

    if (!( is15 || is16PeriodA || is16PeriodB 
          || is16PeriodC || is16PeriodD || is16PeriodE
          || is16PeriodF_end || is17PeriodB || is17PeriodC
          || is17PeriodD || is17PeriodE || is17PeriodF
          || is17PeriodH || is17PeriodI || is17PeriodK || is18 )
        && rdmNumber != 0)
    {
      Warning("TriggerTool::getTriggerDecision", "rdmNumber = %lu doesn't fit in configured periods. Update TriggerTool!",
        static_cast<unsigned long>(rdmNumber));
    }


    if (is15)
    {
      passFJT = passFJT360;
      result.trigger = eFJT360;
    }
    else if (is16PeriodA)
    {
      if (rdmNumber == 299584)
      {
          passFJT = passFJT400;
          result.trigger = eFJT400;
      }
      else
      {
          passFJT = passFJT360;
          result.trigger = eFJT360;
      }
    }
    else if (is16PeriodB)
    {
      if (rdmNumber == 300345)
      {
          passFJT = passFJT400;
          result.trigger = eFJT400;
      }
      else 
      {
          passFJT = passFJT400;
          result.trigger = eFJT400;
      }
    }
    else if (is16PeriodC)
    {
      if (rdmNumber == 301912 ||
          rdmNumber == 301915 ||
          rdmNumber == 301918)
      {
          passFJT = passFJT360;
          result.trigger = eFJT360;
      }
      else 
      {
          passFJT = passFJT400;
          result.trigger = eFJT400;
      }
    }
    else if (is16PeriodD)
    {
      if (rdmNumber == 302831)
      {
          passFJT = passFJT360;
          result.trigger = eFJT360;
      }
      else if (rdmNumber == 303007 ||
               rdmNumber == 303079 ||
               rdmNumber == 303201)
      {
          passFJT = passFJT420;
          result.trigger = eFJT420;
      }
      else 
      {
          passFJT = passFJT400;
          result.trigger = eFJT400;
      }
    }
    else if (is16PeriodE)
    {
      passFJT = passFJT400;
      result.trigger = eFJT400;
    }
    else if (is16PeriodF_end)
    {
      if (rdmNumber == 309390)
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
      else
      {
          passFJT = passFJT420;
          result.trigger = eFJT420;
      }
    }
    else if (is17PeriodB)
    {
      if (rdmNumber == 327490)
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
      else if (327582 <= rdmNumber && rdmNumber <= 327860)
      {
          passFJT = passFJT460;
          result.trigger = eFJT460;
      }
      else if (328221 <= rdmNumber && rdmNumber <= 328393)
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
      else 
      {
          passFJT = passFJT420;
          result.trigger = eFJT420;
      }
    }
    else if (is17PeriodC)
    {
      if (rdmNumber == 330160 || 
          rdmNumber == 330470)
      {
          passFJT = passFJT460;
          result.trigger = eFJT460;
      }
      else 
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
    }
    else if (is17PeriodD)
    {
      if (rdmNumber == 331082 || 
          rdmNumber == 331772 || 
          rdmNumber == 331951 || 
          rdmNumber == 332303)
      {
          passFJT = passFJT460;
          result.trigger = eFJT460;
      }
      else 
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
    }
    else if (is17PeriodE)
    {
      if (rdmNumber == 333828)
      {
          passFJT = false; // no unprescaled FJT in this run
          result.trigger = eNone;
      }
      else
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
    }
    else if (is17PeriodF)
    {
      passFJT = passFJT440;
      result.trigger = eFJT440;
    }
    else if (is17PeriodH)
    {
      passFJT = passFJT440;
      result.trigger = eFJT440;
    }
    else if (is17PeriodI)
    {
      if (rdmNumber == 337005 || 
          rdmNumber == 337156)
      {
          passFJT = passFJT460;
          result.trigger = eFJT460;
      }
      else 
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
    }
    else if (is17PeriodK)
    {
      if (rdmNumber == 338377 ||
          rdmNumber == 338498 ||
          rdmNumber == 338675 ||
          rdmNumber == 338712 ||
          rdmNumber == 338834 ||
          rdmNumber == 339205 ||
          rdmNumber == 339346 ||
          rdmNumber == 339396 ||
          rdmNumber == 339535 ||
          rdmNumber == 339758 ||
          rdmNumber == 339849 ||
          rdmNumber == 340072 ||
          rdmNumber == 340453)
      {
          passFJT = passFJT460;
          result.trigger = eFJT460;
      }
      else
      {
          passFJT = passFJT440;
          result.trigger = eFJT440;
      }
    }
    else if (is18)
    {
      passFJT = passFJT460;
      result.trigger = eFJT460;
    }

    if (!passFJT)
      result.trigger = eNone;

  }
  else {
    Warning("TriggerTool::getTriggerDecision", "analysisType %s not supported.", m_analysisType.c_str());
  }

  return;

}

double TriggerTool_HH_bbtt::getTriggerSF(const xAOD::EventInfo* eventInfo, ResultHHbbtautau & result, const std::string & variation){

  
  bool isMC = Props::isMC.get(eventInfo);
  double runNumber = 0;
  if (isMC) {
    
    runNumber = Props::RandomRunNumber.get(eventInfo);
   
  } else {
    Warning("TriggerTool_HH_bbtt::getTriggerSF()", "You are calling getTriggerSF() for data, this may lead to errors!");
    runNumber = eventInfo->runNumber();
  }

  if (runNumber == 0) {
    Warning("TriggerTool_HH_bbtt::getTriggerSF()", "runNumber is 0. This should not happen!");
  }

  float GeV = 1000.;

  Trigger trig = result.trigger;
  double triggerSF = 1.;


  if (trig == eSET) {
    //////////////////////////
    // single electron trigger
    //////////////////////////

    // TWiki: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/XAODElectronEfficiencyCorrectionTool

    // the scale factors for the single electron triggers are already computed in the ElectronHandler in CxAODMaker
    // so we can just retrieve the property

      const xAOD::Electron * el = result.el;
      if (!el) {
        Error("TriggerTool_HH_bbtt::getTriggerSF()", "Triggered electron is null-pointer. This should not happen!");
        exit(1);
      }
      if(runNumber <= 284484) {  // different WP for 2015
        
        triggerSF = Props::trigSFmediumLH_offTightIsoFixedCutLoose.get(el); 
      } else { // same for 2016-2018
       
        triggerSF = Props::trigSFtightLHIsoFixedCutLoose.get(el);
      }
    

  }

  else if (trig == eETT) {
    
    // use the SFs of the single electron/tau trigger with equivalent threshold and multiply them
    
    const xAOD::Electron * el = result.el;
    const xAOD::TauJet * tau = 0;
    if (result.taus.size() > 0)
      tau = result.taus.at(0);
    
      // electron trigger
    if (!el) {
      Error("TriggerTool_HH_bbtt::getTriggerSF()", "Triggered electron is null-pointer. This should not happen!");
      exit(1);
    }
    triggerSF = Props::e17trigSFmediumLHIsoFixedCutLoose.get(el);
    
      // tau trigger
    if (!tau) {
        Error("TriggerTool_HH_bbtt::getTriggerSF()", "Triggered tau is null-pointer. This should not happen!");
        exit(1);
    }
    
      triggerSF *= Props::effSFtrigger25.get(tau);

  }
  
  else if (trig == eSMT || trig == eMTT) {   // single muon, muon plus tau 

    // TWiki: https://twiki.cern.ch/twiki/bin/viewauth/Atlas/MuonTriggerPhysicsRecommendationsRel212017
    //
    // for the muon-tau trigger use the SFs of the single muon/tau trigger with equivalent threshold and multiply them

    // put the triggered muons in a container and define the trigger string
    ConstDataVector<xAOD::MuonContainer> selectedMuons(SG::VIEW_ELEMENTS);

     // check if all muons are not null pointers
    for (const xAOD::Muon * mu : selectedMuons) {
      if (!mu) {
        Error("TriggerTool_HH_bbtt::getTriggerSF()", "Triggered muon is null-pointer. This should not happen!");
        exit(1);
      }
    }

    if (trig == eSMT) {
      selectedMuons.push_back(result.mu);
      if(runNumber <= 284484) {  // 2015
        m_str_muTrigSF= "HLT_mu20_iloose_L1MU15_OR_HLT_mu50";
      } else { // 2016, 2017 and 2018
        m_str_muTrigSF="HLT_mu26_ivarmedium_OR_HLT_mu50";
      }
      //else if(m_analysisType == "lephad" && trig == eMTT) m_str_muTrigSF="HLT_mu14"; //not avaialble for rel21

      // setup the requested systematic variation
      CP::SystematicVariation currentSys(variation);
      CP::SystematicSet currentSysSet(currentSys.name());
      if (m_trig_sfmuon->applySystematicVariation(currentSysSet) != CP::SystematicCode::Ok) {
        Warning("TriggerTool_HH_bbtt::getTriggerSF()", "There was an error setting the systmatic variation '%s' in the muon trigger scale factor tool!", variation.c_str());
        exit(1);
      }

      // retrieve muon SF trigger scale factor (SLT)
      if (m_trig_sfmuon->getTriggerScaleFactor(*selectedMuons.asDataVector(), triggerSF, m_str_muTrigSF) == CP::CorrectionCode::Error) {
        Warning("TriggerTool_HH_bbtt::getTriggerSF()", "MuonTriggerScaleFactors returned CP::CorrectionCode::Error. Setting trigger SF to 1.");
      }
    }

    else{ //(trig == eMTT) use the SFs of the single muon/tau trigger with equivalent threshold and multiply them
      //***********
      triggerSF=1; //setting the Mu14 branch to 1
      //**********
      const xAOD::TauJet * tau = result.taus.at(0);
      if (!tau) {
        Error("TriggerTool_HH_bbtt::getTriggerSF()", "Triggered muon is null-pointer. This should not happen!");
        exit(1);
      }

      if (runNumber<326834) triggerSF *= Props::effSFtrigger25.get(tau); // 2015-2016
      else triggerSF *= Props::effSFtrigger35.get(tau);

    }
  }
  else if (trig == eSTT) {
    // ===============================
    // Trigger SFs: Single Tau Trigger
    // ===============================
    if (!result.isSREvent) {
      // We don't have VeryLoose SFs for the fake CR
      result.triggerSF = triggerSF;
      return triggerSF;
    }

    bool appliedSF = false;
    for (const auto tau : result.taus) {
      // Check if tau is trigger matched
      const auto &matchedTaus = result.matchedTausSTT;
      const auto matched = std::find(matchedTaus.begin(), matchedTaus.end(), tau) != matchedTaus.end();
      // TODO: Why can we have more than 1 trigger-matched tau for STT?
      if (matched) {
        // Copied from getTriggerDecision
        // TODO: Avoid this copy
        const bool is15 = 266904 <= runNumber && runNumber <= 284484;
        const bool is17 = 324320 <= runNumber && runNumber <= 341649;
        const bool is18 = 348197 <= runNumber && runNumber <= 364485;

        const bool is16PeriodA = 296939 <= runNumber && runNumber <= 300287;
        const bool is16PeriodB_D3 = 300345 <= runNumber && runNumber <= 302872;
        const bool is16PeriodD4_end = 302919 <= runNumber && runNumber <= 311481;

        // No recommendations for VeryLoose (offline) taus exist so only apply
        // trigger SFs for medium (offline) taus
        if (is15 || is16PeriodA) {
          // tau80 trigger
          triggerSF *= Props::effSFtrigger80.get(tau);
        } else if (is16PeriodB_D3) {
          // tau125 trigger
          triggerSF *= Props::effSFtrigger125.get(tau);
        } else if (is16PeriodD4_end || is17) {
          // tau160 trigger
          triggerSF *= Props::effSFtrigger160.get(tau);
        } else if (is18) {
          // FIXME: SFs Not yet available
          triggerSF *= Props::effSFtrigger160.get(tau);
        } else {
          Error("TriggerTool_HH_bbtt::getTriggerSF", "Invalid run number");
        }
        appliedSF = true;
        break;
      }
    }

    if (!appliedSF) {
      Error("TriggerTool_HH_bbtt::getTriggerSF", "STT trigger scale factor incorrectly applied!");
    }

  } else if (trig == eDTT) {
    // ===========================
    // Trigger SFs: Di-tau trigger
    // ===========================
    if (!result.isSREvent) {
      // We don't have VeryLoose SFs for the fake CR
      result.triggerSF = triggerSF;
      return triggerSF;
    }

    auto appliedSF = false;
    auto pt40TauSF = false;
    int cnt = 0;
    for (const auto tau : result.taus) {
      // Check if tau is trigger matched
      const auto &matchedTaus = result.matchedTausDTT;
      const auto matched = std::find(matchedTaus.begin(), matchedTaus.end(), tau) != matchedTaus.end();

      // Apply scale factor if tau is matched
      if (matched) {
        PROP<float> *SFprop = nullptr;
        if (!pt40TauSF && tau->pt() > 40 * GeV) {
          // Apply 40 GeV SF
          SFprop = &Props::effSFtrigger35;
          pt40TauSF = true;
        } else {
          // Apply 30 GeV SF
          SFprop = &Props::effSFtrigger25;
        }
        if (cnt < 2) { triggerSF *= SFprop->get(tau); }
        cnt++;
      }
    }

    if (cnt == 2 && pt40TauSF) {
      appliedSF = true;
    }

    if (!appliedSF) {
      Error("TriggerTool_HH_bbtt::getTriggerSF", "DTT trigger scale factor incorrectly applied!");
    }

  } else if (trig == eSkip) {
      Warning("TriggerTool_HH_bbtt::getTriggerSF()", "Unexpected combination of trigger and channel: eSkip and %s, setting trigger SF to 1", m_analysisType.c_str());
      result.triggerSF = 1;
      return 1;
  } else if ( trig == eFJT360 || trig == eFJT380 || trig == eFJT400 || 
	      trig == eFJT420 || trig == eFJT440 || trig == eFJT460 ) {
    // there are no trigger scale factors for LRJ triggers so far
    result.triggerSF = 1; return 1;
  } else {
    std::cout << "HH: Encountered unexpected trigger " << trig << std::endl;
  }

  result.triggerSF = triggerSF;
  //std::cout<<"SF "<<triggerSF<<std::endl;
  return triggerSF;
}


// Helper functions for hadhad
// Checks whether the event passed the trigger and both taus are trigger matched
bool pass_and_match_dtt(const TriggerTuple &prop_tuple,
                        const xAOD::EventInfo * eventInfo,
                        const xAOD::TauJet *lead_tau,
                        const xAOD::TauJet *sublead_tau) {
  bool pass = true;

  const auto pass_event = std::get<0>(prop_tuple);
  const auto pass_matching = std::get<1>(prop_tuple);

  // Event passed trigger
  pass = pass && pass_event->get(eventInfo);

  // Taus pass trigger-matching
  pass = pass && pass_matching->get(lead_tau);
  pass = pass && pass_matching->get(sublead_tau);

  return pass;
}

