#include "CxAODTools_HH_bbtautau/HHbbtautauLepLepJetSelection.h"
#include "xAODTau/TauJet.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/Muon.h"
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"
#include <iostream>
#include<TRandom.h>

HHbbtautauLepLepJetSelection::HHbbtautauLepLepJetSelection(ConfigStore * config, EventInfoHandler * eventInfoHandler) :
    HHbbtautauSelection("PreselectionJetCutFlow")
{
    config->getif<bool>("debug", m_debug);
    m_triggerTool = new TriggerTool_HH_bbtt(*config, *eventInfoHandler, "leplep");
    m_triggerTool->initialize();
    m_channel = "leplep";
}

bool HHbbtautauLepLepJetSelection::passTauSelection(const xAOD::TauJetContainer* taus,  const xAOD::EventInfo* evtinfo)
{
    // we want no hadronic taus
    return m_result.taus.size() == 0;
}

bool HHbbtautauLepLepJetSelection::passLeptonSelection(const xAOD::ElectronContainer* electrons,
        const xAOD::MuonContainer* muons,
        const xAOD::MissingET* met)
{
    // require at least two leptons
    return m_result.electrons.size() + m_result.muons.size() >= 2;
}


// this is only used in the reader
bool HHbbtautauLepLepJetSelection::passSelection(SelectionContainers & containers,
        bool isKinVar)
{
    const xAOD::MissingET          * met       = containers.met;
    const xAOD::ElectronContainer  * electrons = containers.electrons;
    const xAOD::MuonContainer      * muons     = containers.muons;
    const xAOD::JetContainer       * jets      = containers.jets;
    const xAOD::JetContainer       * fatjets   = containers.fatjets;
    const xAOD::TauJetContainer    * taus      = containers.taus;
    const xAOD::EventInfo          * evtinfo   = containers.evtinfo;

    clearResult();

    if (!passTauSelection(taus, evtinfo)){
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passSelection() - fail tau selection" << std::endl;
        return false;
    }

    if (!passLeptonSelection(electrons, muons, met)) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passSelection() - fail lepton selection" << std::endl;
        return false;
    }

    bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetSelection(jets);
    bool passFatJetSel = false;

    if (fatjets) {
        passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetSelection(fatjets);
    }

    if (!passJetSel && !passFatJetSel ) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passSelection() - fail jet selection" << std::endl;
        return false;
    }

    if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passSelection() - fail passKinematics()" << std::endl;
        return false;
    }

    m_triggerTool->getTriggerDecision(evtinfo, m_result);
    if (m_result.trigger == eNone && m_result.trigger != eSkip){
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passSelection() - fail trigger selection" << std::endl;
        return false;
    }

    m_result.pass = true;
    return true;
}

// preselection version - this is used in the Maker
bool HHbbtautauLepLepJetSelection::passPreSelection(SelectionContainers & containers,
        bool isKinVar) {
    const xAOD::EventInfo         * evtinfo   = containers.evtinfo;
    const xAOD::MissingET         * met       = containers.met;
    const xAOD::ElectronContainer * electrons = containers.electrons;
    const xAOD::MuonContainer     * muons     = containers.muons;
    const xAOD::JetContainer      * jets      = containers.jets;
    const xAOD::JetContainer      * fatjets   = containers.fatjets;
    const xAOD::TauJetContainer   * taus      = containers.taus;

    clearResult();

    if (!passTauSelection(taus, evtinfo)){
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passPreSelection() - fail tau selection" << std::endl;
        return false;
    }

    if (!isKinVar) m_cutFlow.count("Preselection taus");

    if (!passLeptonSelection(electrons, muons, met)) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passPreSelection() - fail lepton selection" << std::endl;
        return false;
    }

    if (!isKinVar) m_cutFlow.count("Preselection leptons");

    bool passJetSel = HHbbtautauSelection<ResultHHbbtautau>::passJetSelection(jets);
    bool passFatJetSel = false;

    if (fatjets) {
        passFatJetSel = HHbbtautauSelection<ResultHHbbtautau>::passFatJetSelection(fatjets);
    }

    if (!passJetSel && !passFatJetSel ) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passPreSelection() - fail jet selection" << std::endl;
        return false;
    }

    if (!HHbbtautauSelection<ResultHHbbtautau>::passKinematics()) {
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passPreSelection() - fail passKinematics()" << std::endl;
        return false;
    }

    m_triggerTool->getTriggerDecision(evtinfo, m_result);
    if (m_result.trigger == eNone && m_result.trigger != eSkip){
        m_result.pass = false;
        if (m_debug) std::cout << "HH: HHbbtautauLepLepJetSelection::passPreSelection() - fail trigger selection" << std::endl;
        return false;
    }

    m_result.pass = true;

    if(!isKinVar) m_cutFlow.count("Preselection jet");

    if (m_debug) std::cout << "Selection result: electrons " << m_result.electrons.size() << " "
        << (m_result.electrons.size() > 0 ? m_result.electrons.at(0) : 0) << " " << (m_result.electrons.size() > 0 ? m_result.electrons.at(0)->pt() : 0) << " "
        << (m_result.electrons.size() > 1 ? m_result.electrons.at(1) : 0) << " " << (m_result.electrons.size() > 1 ? m_result.electrons.at(1)->pt() : 0)
        << " muons " << m_result.muons.size() << " "
        << (m_result.muons.size() > 0 ? m_result.muons.at(0) : 0) << " " << (m_result.muons.size() > 0 ? m_result.muons.at(0)->pt() : 0) << " "
        << (m_result.muons.size() > 1 ? m_result.muons.at(1) : 0) << " " << (m_result.muons.size() > 1 ? m_result.muons.at(1)->pt() : 0)
        << " taus " << m_result.taus.size()
        << " jets " << m_result.signalJets.size()
        << " trig " << m_result.trigger
        << std::endl;

    m_sysResults[m_sysName] = m_result;

    return true;
}

EL::StatusCode HHbbtautauLepLepJetSelection::writeEventVariables(const xAOD::EventInfo* eventInfoIn,
        xAOD::EventInfo* eventInfoOut,
        bool isKinVar,
        bool isWeightVar,
        std::string sysName,
        int rdm_RunNumber,
        CP::MuonTriggerScaleFactors* trig_sfmuon)
{
    if (!m_result.pass) {
        return EL::StatusCode::SUCCESS;
    }

    if( m_debug  ){
        std::cout << "HH: e " << m_result.electrons.size() << " m " << m_result.muons.size() << " Trigger: " << m_result.trigger << std::endl;
        std::cout << "HH: bbtautauLepLep*Jet*Selection::writeEventVariables: calling getTriggerSF()" << std::endl;
    }

    // scale factors
    bool isMC = Props::isMC.get(eventInfoIn);
    if (isMC) {  // scale factors are only needed for MC, not data
        // trigger scale factor
        m_triggerTool->getTriggerSF(eventInfoIn, m_result, sysName);
        if (m_debug) std::cout << "HH: trigSF: " << m_result.triggerSF << std::endl;
        Props::trigSF.set(eventInfoOut, m_result.triggerSF);

        // lepton scale factor
        float leptonSF = 1.0;
        // electrons: medium id, loose iso
        for (const xAOD::Electron* el : m_result.electrons) {
            leptonSF *= Props::effSFmediumLH.get(el) * Props::effSFReco.get(el) * Props::effSFIsoFixedCutLooseMediumLH.get(el);
        }
        // muons: medium id, loose iso
        for (const xAOD::Muon* mu : m_result.muons) {
            leptonSF *= Props::mediumEffSF.get(mu) * Props::TTVAEffSF.get(mu) * Props::fixedCutLooseIsoSF.get(mu);
        }
        if (m_debug) std::cout << "HH: leptonSF: " << leptonSF << std::endl;
        Props::leptonSF.set(eventInfoOut, leptonSF);
    } else {  // for consistency set SF decorators to 1 for data
        Props::trigSF.set(eventInfoOut, 1.0);
        Props::leptonSF.set(eventInfoOut, 1.0);
    }

    return EL::StatusCode::SUCCESS;
}

void HHbbtautauLepLepJetSelection::clearResult()
{
    m_result = m_sysResults[m_sysName];
}
