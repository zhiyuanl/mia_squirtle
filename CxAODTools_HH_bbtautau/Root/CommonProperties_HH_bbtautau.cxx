#include "CxAODTools_HH_bbtautau/CommonProperties_HH_bbtautau.h"

//Truth tau info
PROPERTY_INST( Props , double , pt_vis)
PROPERTY_INST( Props , double , phi_vis)
PROPERTY_INST( Props , double , eta_vis)
PROPERTY_INST( Props , double , m_vis)
PROPERTY_INST( Props , char , IsHadronicTau)
PROPERTY_INST( Props , int , IsTauFromHiggs)
PROPERTY_INST( Props , int , IsTauFromW)
PROPERTY_INST( Props , int , IsFromLQ)
PROPERTY_INST( Props , int , parentID)

// Props for truth-studies
PROPERTY_INST( Props , int , MCTruthClfOrigin )
PROPERTY_INST( Props , int , MCTruthClfType )
PROPERTY_INST( Props , int , TruthJetPartonID )

// MMC
PROPERTY_INST( Props , double , mmc_mlnu3p_nu0_4vect_pt )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu0_4vect_eta )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu0_4vect_phi )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu0_4vect_m )

PROPERTY_INST( Props , double , mmc_mlnu3p_nu1_4vect_pt )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu1_4vect_eta )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu1_4vect_phi )
PROPERTY_INST( Props , double , mmc_mlnu3p_nu1_4vect_m )

// fat jet
PROPERTY_INST( Props , int , xbbResult_1tagNoMcut )
PROPERTY_INST( Props , int , xbbResult_1tag90Mcut )
PROPERTY_INST( Props , int , xbbResult_1tag68Mcut )
PROPERTY_INST( Props , int , xbbResult_2tagNoMcut )
PROPERTY_INST( Props , int , xbbResult_2tag90Mcut )
PROPERTY_INST( Props , int , xbbResult_2tag68Mcut )
PROPERTY_INST( Props , int , n_subjets )
PROPERTY_INST( Props , float , subjet_lead_pt )
PROPERTY_INST( Props , float , subjet_lead_eta )
PROPERTY_INST( Props , float , subjet_lead_phi )
PROPERTY_INST( Props , float , subjet_lead_e )
PROPERTY_INST( Props , int , subjet_lead_ntracks )
PROPERTY_INST( Props , int , subjet_lead_charge )
PROPERTY_INST( Props , float , subjet_subl_pt )
PROPERTY_INST( Props , float , subjet_subl_eta )
PROPERTY_INST( Props , float , subjet_subl_phi )
PROPERTY_INST( Props , float , subjet_subl_e )
PROPERTY_INST( Props , int , subjet_subl_ntracks )
PROPERTY_INST( Props , int , subjet_subl_charge )
PROPERTY_INST( Props , float , subjets_pt )
PROPERTY_INST( Props , float , subjets_eta )
PROPERTY_INST( Props , float , subjets_phi )
PROPERTY_INST( Props , float , subjets_e )
PROPERTY_INST( Props , int , Nbtags60 )
PROPERTY_INST( Props , int , Nbtags70 )
PROPERTY_INST( Props , int , Nbtags77 )
PROPERTY_INST( Props , int , Nbtags85 )
PROPERTY_INST( Props , std::vector<int> , vHadronConeExclTruthLabelID )

// fourtau
PROPERTY_INST( Props , int , hasMoreThanTwoTau )

// FTTF
PROPERTY_INST( Props , float , seedJetWidth )
PROPERTY_INST( Props , float , seedJetJvt )

PROPERTY_INST( Props , std::vector<float> , TrackWidthPt500 )
PROPERTY_INST( Props , float , TrackWidthPt500PV0 )
PROPERTY_INST( Props , float , TrackWidthPt500PVn )

PROPERTY_INST( Props , std::vector<float> , TrackWidthPt1000 )
PROPERTY_INST( Props , float , TrackWidthPt1000PV0 )
PROPERTY_INST( Props , float , TrackWidthPt1000PVn )

PROPERTY_INST( Props , int , L1_match_J12 )
PROPERTY_INST( Props , int , L1_match_J12_0ETA23 )
PROPERTY_INST( Props , int , L1_match_J25 )
