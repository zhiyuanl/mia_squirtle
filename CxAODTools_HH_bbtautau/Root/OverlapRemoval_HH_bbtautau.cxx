#include "CxAODTools_HH_bbtautau/OverlapRemoval_HH_bbtautau.h"
#include "CxAODTools/CommonProperties.h"
#include "AssociationUtils/DeltaRMatcher.h"

OverlapRemoval_HH_bbtautau::OverlapRemoval_HH_bbtautau(ConfigStore & config) :
  OverlapRemoval(config) {
  double dR = 0.2;
  bool useRapidity = true;
  m_dRMatcher = new ORUtils::DeltaRMatcher(dR,useRapidity);
}


void OverlapRemoval_HH_bbtautau::setORInputLabels(
						  const xAOD::ElectronContainer* /*electrons*/,
						  const xAOD::PhotonContainer* /*photons*/,
						  const xAOD::MuonContainer* /*muons*/,
						  const xAOD::TauJetContainer* taus,
						  const xAOD::JetContainer* jets,
						  const xAOD::JetContainer* /*fatjets*/,
                    const xAOD::DiTauJetContainer* ditaus) {
  
  if (m_debug) Info("OverlapRemoval_HH_bbtautau::setORInputLabels",
		    "Setting OR input label for DB analysis.");
  
  // Set input flag for the OR tool (IntProp::ORInputLabel) 

  // TJS - only set input labels for jets and taus
  if (jets) {
    for (const xAOD::Jet* jet : *jets) {
      Props::ORInputLabel.set(jet, Props::isVetoJet.get(jet));
    }
  }

  if (taus) {
    for (const xAOD::TauJet* tau : *taus) {
      //Props::ORInputLabel.set(tau, Props::passTauSelector.get(tau));
      Props::ORInputLabel.set(tau, Props::passPreSel.get(tau));
    }
  }

  if (ditaus) {
    for (const xAOD::DiTauJet* ditau : *ditaus) {
      //Props::ORInputLabel.set(tau, Props::passTauSelector.get(tau));
      Props::ORInputLabel.set(ditau, Props::passPreSel.get(ditau));
    }
  }
}

EL::StatusCode OverlapRemoval_HH_bbtautau::removeOverlap(const xAOD::ElectronContainer* electrons,
                                             const xAOD::PhotonContainer* photons,
					     const xAOD::MuonContainer* muons,
					     const xAOD::TauJetContainer* taus,
                                             const xAOD::JetContainer* jets,
                                             const xAOD::JetContainer* fatjets,
                                             const xAOD::DiTauJetContainer* ditaus)
{
  if (m_debug) std::cout << "OverlapRemoval_HH_bbtautau::removeOverlap" << std::endl;

  setORInputLabels(electrons, photons, muons, taus, jets, fatjets, ditaus); // These are probably already set from the previous OR but will leave it here for now

   for(const auto jet: *jets){
     if(!Props::ORInputLabel.get(jet) || Props::OROutputLabel.get(jet)) continue;
     Props::OROutputLabel.set(jet,false);
   }
   for(const auto tau: *taus){
     if(!Props::ORInputLabel.get(tau) || Props::OROutputLabel.get(tau)) continue;
     Props::OROutputLabel.set(tau,false);
     /*Props::TauORJetDR.set(tau, 1e3);
     Props::TauORJetPt.set(tau, -999);
     Props::TauORJetEta.set(tau, -999);
     Props::TauORJetPhi.set(tau, -999);
     Props::TauORJetE.set(tau, -999);*/
   }

   for(const auto jet : * jets){
     if(!Props::ORInputLabel.get(jet) || Props::OROutputLabel.get(jet)) continue;
     bool isBTagged = Props::isBJet.get(jet);
     
     for(const auto tau : * taus){
       
       if(!Props::ORInputLabel.get(tau) || Props::OROutputLabel.get(tau)) continue;
       bool isAntiTau = Props::isAntiTau.get(tau);
     
       bool isOverlap = false;
       isOverlap = m_dRMatcher->objectsMatch(*jet,*tau);
       
       // HF: not sure an anti-flag for a tau is enough. in had-had we can have a medium anti-tau
       if(isBTagged  && !isAntiTau && isOverlap){ 
	 Props::OROutputLabel.set(jet, true); 
       }// b-jet and tau overlapping then remove b-jet
       else if(!isBTagged               && isOverlap){ 
	 Props::OROutputLabel.set(jet, true); 
       }// jet and (anti-)tau overlapping then remove light-jet
       
       if(isOverlap){// debug info so can be removed later
	 /*if(Props::OROutputLabel.get(jet)){
	   TLorentzVector tempTau = tau->p4();
	   TLorentzVector tempJet = jet->p4();
	   float tempDR = tempTau.DeltaR(tempJet);
	   if(tempDR < Props::TauORJetDR.get(tau)){
	     Props::TauORJetDR.set(tau, tempDR);
	     Props::TauORJetPt.set(tau, jet->pt());
	     Props::TauORJetEta.set(tau, jet->eta());
	     Props::TauORJetPhi.set(tau, jet->phi());
	     Props::TauORJetE.set(tau, jet->e());
	   }
	   }*/

	 bool tempTauOR = Props::OROutputLabel.get(tau);
	 bool tempJetOR = Props::OROutputLabel.get(jet);
	 if (m_debug) std::cout << "HH: bbttOR, " 
		   << "jpt= " << jet->pt()/1000.
		   << " tpt= " << tau->pt()/1000.
		   << " eta= " << tau->eta()
		   << " phi= " << tau->phi()
		   << " Overlapping: " << isOverlap << ", BTagJet: " << isBTagged <<  ", AntiTau: " << isAntiTau << ", TauOR: " << tempTauOR << ", JetOR: " << tempJetOR << std::endl;
       }

     }
    
   }
   if (ditaus) {
     for( const xAOD::DiTauJet*  ditau : *ditaus) {
        Props::OROutputLabel.set(ditau,false);
     }
   }
   OverlapRemoval::getOROutputLabels(electrons,  photons, muons,  taus, jets, fatjets, ditaus);

   return EL::StatusCode::SUCCESS;
}
