#!/bin/bash
# Adrian Buzatu, adrian.buzatu@glasgow.ac.uk, 11 June 2016
# script aimed to keep only the good runs from a certain good run list in a data file list the CxAOD is run on
# motivation is to avoid wasting CPU power and book keeping efforts, as no event would be saved in the end anyway
# to be run from FrameworkSub/In with ../../FrameworkExe/scripts/runKeepOnlyGoodRuns.sh

#GRLFileName="../data/GRL/data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml"
GRLFileName="../data/GRL/data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns_HadHad_Modified.xml"
SampleFilePath="./"
SampleFilePrefix="list_sample_grid.data16_13TeV"
SampleFileSuffix=".AllYear"
#Derivations="HIGG5D1,HIGG5D2,HIGG2D4"
Derivations="HIGG4D3"

for Derivation in `echo "${Derivations}" | awk -v RS=, '{print}'`
do
    Command="../../FrameworkExe_HH_bbtautau/scripts/keepOnlyGoodRuns.py ${GRLFileName} ${SampleFilePath}${SampleFilePrefix}${Derivation}${SampleFileSuffix}"
    echo "Running ${Command}"
    `echo "${Command}"`
    echo "Done for ${Derivation}."
done
