import sys

def dump(n):
    d = {}
    with open(n) as f:
        for l in f:
            t =  l.strip('\n').split()
            d[t[0]] = (float(t[1]), t[4]) 
    return d

dold, dnew = dump(sys.argv[1]), dump(sys.argv[2])        
with open("missing.txt", "w") as fmiss, open("incomplete.txt", "w") as fincomp:    
    for i, (n, ds) in dold.iteritems():
        if not i in dnew:
            fmiss.write("%s\t%s\n" %(i,ds))
            continue
        
        n2 = dnew[i][0]            
        if abs((n2 - n)/float(n)) > 0.05:
            fincomp.write("%s\t%s\t(%s\t->\t%s)\n" %(i, ds, n, n2))
