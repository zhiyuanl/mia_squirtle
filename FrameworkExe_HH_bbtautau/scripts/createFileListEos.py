import os
os.environ['EOS_MGM_URL']="root://eosatlas.cern.ch"
os.environ['PATH']+=":/afs/cern.ch/project/eos/installation/pro/bin"

dir='eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/HIGG4D3_13TeV/CxAOD_Paris1b/data/'

for root, directories, filenames in os.walk(dir):
     for directory in directories:
             print os.path.join(root, directory) 
     for filename in filenames: 
             print os.path.join(root,filename) 
