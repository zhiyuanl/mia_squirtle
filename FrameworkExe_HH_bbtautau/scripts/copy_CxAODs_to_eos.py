#!/usr/bin/python

import os, math, sys

#############################
#
# Script for copying grid-job output-files (CxAODs) to EOS.
#
# Sub-directories are created on EOS to group the files into samples.
#
# VERY IMPORTANT :
# Execute in the directory where you have dq2-get your jobs.
#
#############################

#############################
# parameters
#############################

# only print commands instead of executing them
test_only = False

# verbose mode (otherwise print warnings only)
verbose = True

# skip existing files (also if being corrupt, e.g. size 0)
skip_existing = True

# to copy the samples to an eos directory
#main_path = "/eos/atlas/user/c/carquin/HHbbtautau/Run2/"
main_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG6/HH/bbtautau/"
#main_path = "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/"
# to copy the samples to a local directory
#main_path = "/disk/lustre/atlas/pdt/DAOD/"
main_path = "/eos/user/d/dferreir/hh_CxAOD_split"

# determines full output path (full_path)
energy_tag     = "13TeV"
derivation_tag = "HIGG4D3"
production_tag = "TestVBFv1"

#############################
# sample definition
#############################

our_samples = [
# sample		1st match		 2nd match
["data",		"physics_Main",	         ""],

# ttbar related samples
# note: changed ttbar name tag to have PhPy in the name instead of PwPy
["ttbar",		"PhPyEG_P2012_ttbar",	 "allhad."],
["ttbar_PwPy8EG",       "PhPy8EG_A14_ttbar",     ""],
["ttbar_Syst",		"ttbar",		 "PhPyEG_P2012rad"],
["ttbar_Syst",		"aMcAtNloHerwigppEG_ttbar", ""],
["ttbar_Syst",          "PwHerwigppEG_UEEE5_ttbar", ""],
["ttbarH", "aMcAtNloHerwigppEG_UEEE5_CTEQ6L1_CT10ME_ttH125_dil", ""],
["ttbarH", "aMcAtNloHerwigppEG_UEEE5_CTEQ6L1_CT10ME_ttH125_allhad", ""],
["ttbarH", "aMcAtNloHerwigppEG_UEEE5_CTEQ6L1_CT10ME_ttH125_semilep", ""],
["ttbarH_tautau", "aMcAtNloHppEG_UE5_C6L1_CT10ME_ttH125_H2tau_dilep",""],
["ttbarH_tautau", "aMcAtNloHppEG_UE5_C6L1_CT10ME_ttH125_H2tau_allhad", ""],
["ttbarH_tautau", "aMcAtNloHppEG_UE5_C6L1_CT10ME_ttH125_H2tau_semilep", ""],
["ttbarWW", "MGPy8EG_A14NNPDF23_ttbarWW", ""],
["ttbarW", "MGPy8EG_A14NNPDF23LO_ttW", ""],
["ttZ", "MGPy8EG_A14NNPDF23LO_ttZllonshell", ""],
["ttZ", "MGPy8EG_A14NNPDF23LO_ttZnnqq", ""],
["ttbar_tautau", "MGPy8EG_A14NNPDF23LO_tttautau", ""],
["4topSM", "MGPy8EG_A14NNPDF23_4topSM", ""],

# dijet background
["dijet",		"jetjet_JZ",		 ""],

# electroweak V+jets backgrounds
["ZeeB",		"Sh_Zee",       	 "BFilter"],
["ZeeC",		"Sh_Zee",	         "CFilterBVeto"],
["ZeeL",		"Sh_Zee",	         "CVetoBVeto"],
["ZmumuB",		"Sh_Zmumu",	         "BFilter"],
["ZmumuC",		"Sh_Zmumu",	         "CFilterBVeto"],
["ZmumuL",		"Sh_Zmumu",	         "CVetoBVeto"],
["ZtautauB",		"Sh_CT10_Ztautau",	         "BFilter"],
["ZtautauC",		"Sh_CT10_Ztautau",	         "CFilterBVeto"],
["ZtautauL",		"Sh_CT10_Ztautau",	         "CVetoBVeto"],
["ZtautauB_NNLOPDF",      "Sh_221_NNPDF30NNLO_Ztautau",            "BFilter"],
["ZtautauC_NNLOPDF",      "Sh_221_NNPDF30NNLO_Ztautau",            "CFilterBVeto"],
["ZtautauL_NNLOPDF",      "Sh_221_NNPDF30NNLO_Ztautau",            "CVetoBVeto"],
["ZnunuB",		"Sh_Znunu",	         "BFilter"],
["ZnunuC",		"Sh_Znunu",	         "CFilterBVeto"],
["ZnunuL",		"Sh_Znunu",	         "CVetoBVeto"],
["WenuB",		"Sh_Wenu",	         "BFilter"],
["WenuC",		"Sh_Wenu",	         "CFilterBVeto"],
["WenuL",		"Sh_Wenu",	         "CVetoBVeto"],
["WmunuB",		"Sh_Wmunu",              "BFilter"],
["WmunuC",		"Sh_Wmunu",              "CFilterBVeto"],
["WmunuL",		"Sh_Wmunu",              "CVetoBVeto"],
["WtaunuB",		"Sh_CT10_Wtaunu",             "BFilter"],
["WtaunuC",		"Sh_CT10_Wtaunu",             "CFilterBVeto"],
["WtaunuL",		"Sh_CT10_Wtaunu",             "CVetoBVeto"],
["WtaunuB_NNLOPDF",     "Sh_221_NNPDF30NNLO_Wtaunu",             "BFilter"],
["WtaunuC_NNLOPDF",     "Sh_221_NNPDF30NNLO_Wtaunu",             "CFilterBVeto"],
["WtaunuL_NNLOPDF",     "Sh_221_NNPDF30NNLO_Wtaunu",             "CVetoBVeto"],

# Higgs backgrounds
["VBFH125_tautauhh", "PwPy8EG_CT10_AZNLOCTEQ6L1_VBFH125_tautauhh", ""],
["ggH125_tautauhh", "PwPy8EG_CT10_AZNLOCTEQ6L1_ggH125_tautauhh_unpol", ""],
["ggH125_tautauhh", "PwPy8EG_CT10_AZNLOCTEQ6L1_ggH125_tautauhh", ""],

# other
["DYtautauLO", "PwPy8EG_AZNLOCTEQ6L1_DYtautau", ""],
["DYtautauNLO", "Py8EG_A14NNPDF23LO_DYtautau", ""],
["DYee", "PwPy8EG_AZNLOCTEQ6L1_DYee", ""],
["DYmumu", "PwPy8EG_AZNLOCTEQ6L1_DYmumu", ""],

# ggF signals
["LQ3LQ3250", "Py8EG_A14NNPDF23LO_LQ3LQ3_250", ""],
["LQ3LQ3500", "Py8EG_A14NNPDF23LO_LQ3LQ3_500", ""],
["LQ3LQ31000", "Py8EG_A14NNPDF23LO_LQ3LQ3_1000", ""],
["RSG_C20_M300", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M300", ""],
["RSG_C20_M700", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M700", ""],
["RSG_C10_M260", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M260", ""],	
["RSG_C10_M300", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M300", ""],
["RSG_C10_M400", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M400", ""],
["RSG_C10_M500", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M500", ""],
["RSG_C10_M600", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M600", ""],
["RSG_C10_M700", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M700", ""],
["RSG_C10_M800", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M800", ""],
["RSG_C10_M900", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M900", ""],
["RSG_C10_M1000", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M1000", ""],
["RS_G_hh_bbtt", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20", ""],
["RS_G_hh_bbtt", "MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10", ""],
["NonResHH_bbtt", "aMcAtNloHerwigppEG_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh", ""],
["NonResHH_yytt", "aMcAtNloHerwigppEG_UEEE5_CTEQ6L1_CT10ME_hh_yytt_hh",""],
["Xtohh260", "X260tohh_bbtautau_hadhad", ""],
["Xtohh300", "X300tohh_bbtautau_hadhad", ""],
["Xtohh400", "X400tohh_bbtautau_hadhad", ""],
["Xtohh500", "X500tohh_bbtautau_hadhad", ""],
["Xtohh600", "X600tohh_bbtautau_hadhad", ""],
["Xtohh700", "X700tohh_bbtautau_hadhad", ""],
["Xtohh800", "X800tohh_bbtautau_hadhad", ""],
["Xtohh1000", "X1000tohh_bbtautau_hadhad", ""],
["2HDM_Xtohh_bbtautau_hadhad", "tohh_bbtautau_hadhad", ""],

# VBF signals
["vbf_hh_l0cvv0cv1", "vbf_hh_l0cvv0cv1", ""],
["vbf_hh_l1cvv0cv1", "vbf_hh_l1cvv0cv1", ""],
["vbf_hh_l1cvv0p5cv1", "vbf_hh_l1cvv0p5cv1", ""],
["vbf_hh_l1cvv1cv1", "vbf_hh_l1cvv1cv1", ""],
["vbf_hh_l1cvv1p5cv1", "vbf_hh_l1cvv1p5cv1", ""],
["vbf_hh_l1cvv2cv1", "vbf_hh_l1cvv2cv1", ""],
["vbf_hh_l0cvv1cv1", "vbf_hh_l0cvv1cv1", ""],
["vbf_hh_l1cvv0cv0p5", "vbf_hh_l1cvv0cv0p5", ""],
["vbf_hh_l1cvv0cv0", "vbf_hh_l1cvv0cv0", ""],
["vbf_hh_l1cvv1cv0p5", "vbf_hh_l1cvv1cv0p5", ""],
["vbf_hh_l2cvv1cv1", "vbf_hh_l2cvv1cv1", ""],

# single top
["singletop_t",		"P2012_singletop_tchan", ""],
["singletop_t_Syst",    "_singletop_tchan",      "rad"],
["singletop_s",		"Schan",		 ""],
["singletop_s_Syst",	"Schan",		 "rad"],
["singletop_Wt",	"P2012_Wt_inclusive",    ""],
["singletop_Wt_dilep",	"P2012_Wt_dilepton",     ""],
["singletop_Wt_Syst",   "P2012_Wt_DS_",          ""],

# VH
["ZHvv125",		"ZH125_nunubb",		 ""], ##
["ZHvv125",             "ZvvH125_bb",            ""],
["ZHvv125",             "ggZH125",               "vevebb"],
["ZHvv125",             "ggZH125",               "vmuvmubb"],
["ZHvv125",             "ggZH125",               "vtauvtaubb"],
["ZHll125",             "ZH125_llbb",            ""], ##
["ZHll125",             "ggZH125",               "eebb"],
["ZHll125",             "ggZH125",               "mumubb"],
["ZHll125",             "ggZH125",               "tautaubb"],
["ZHll125",             "ZllH125_bb",            ""],
["WH125",		"WH125_lnubb",		 ""], ##
["WH125",               "WlvH125_bb",            ""],
["WH125_inc",  "Py8EG_A14NNPDF23LO_WH125_inc", ""],
["ZHvv125J_MINLO",      "ZH125J_MINLO_v",        ""], ##
["ZHvv125J_MINLO",	"ggZH125_vevebb",  	 "NLO"],
["ZHvv125J_MINLO",      "ggZH125_vmuvmubb",      "NLO"], 
["ZHvv125J_MINLO",      "ggZH125_vtauvtaubb",    "NLO"],   
["WHlv125J_MINLO",	"WpH125J_MINLO",	 ""],
["WHlv125J_MINLO",	"WmH125J_MINLO",	 ""],
["ZHll125J_MINLO",	"ZH125J_MINLO_ee",	 ""],
["ZHll125J_MINLO",	"ZH125J_MINLO_mumu",	 ""],
["ZHll125J_MINLO",	"ZH125J_MINLO_tautau",	 ""],
["ZHll125J_MINLO",      "ggZH125_eebb",          "NLO"],
["ZHll125J_MINLO",      "ggZH125_mumubb",        "NLO"],
["ZHll125J_MINLO",      "ggZH125_tautaubb",      "NLO"],
["ZH125_inc",           "Py8EG_A14NNPDF23LO_ZH125_inc", ""],

# VV
["WW",			"WplvWmqq",		 ""],
["WW",			"WmlvWpqq",		 ""],
["WW",                  "WpqqWmlv",              ""],
["WW",                  "WmqqWplv",              ""],
["WW_Pw",               "WWlvqq",                "PwPy8EG"],
["WW_Pw",               "WWlvlv",                "PwPy8EG"],
["WZ",                  "WlvZqq",                ""],
["WZ",                  "WqqZll",                ""],
["WZ",                  "WqqZvv",                ""],
["WZ_Pw",               "WZqqll",                "PwPy8EG"],
["WZ_Pw",               "WZqqvv",                "PwPy8EG"],
["WZ_Pw",               "WZlvqq",                "PwPy8EG"],
["WZ_Pw",               "WZlvll",                "PwPy8EG"],
["WZ_Pw",               "WZlvvv",                "PwPy8EG"],
["ZZ",                  "ZqqZll",                ""],
["ZZ",                  "ZqqZvv",                ""],
["ZZ_Pw",               "ZZqqll",                "PwPy8EG"],
["ZZ_Pw",               "ZZllll",                "PwPy8EG"],
["ZZ_Pw",               "ZZvvll",                "PwPy8EG"],
["ZZ_Pw",               "ZZvvvv",                "PwPy8EG"],
["ggA",			"_ggA",			 ""],
["HVT",			"_HVT",			 ""],
["monoHbb",		"_monoHbb",		 ""],
["monoWjj",		"_monoWjj",		 ""],
["monoZjj",		"_monoZjj",		 ""],
["ExcitedQ",		"_ExcitedQ",		 ""],
["HZZllqq",		"ZZllqq",		 ""],
["HZZvvqq",             "ZZvvqq",                ""],

# other V+jets
["Zee_MG",		"MGPy8EG_A14NNPDF23LO_Zee", ""],
["Zmumu_MG",		"MGPy8EG_A14NNPDF23LO_Zmumu", ""],
["Ztautau_MG",		"MGPy8EG_A14NNPDF23LO_Ztautau", ""],
["Znunu_MG",		"MGPy8EG_A14NNPDF23LO_Znunu", ""],
["Wenu_MG",		"MGPy8EG_A14NNPDF23LO_Wenu", ""],
["Wmunu_MG",		"MGPy8EG_A14NNPDF23LO_Wmunu", ""],
["Wtaunu_MG",		"MGPy8EG_A14NNPDF23LO_Wtaunu", ""],
["Wenu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wplusenu", ""],
["Wenu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wminusenu", ""],
["Wmunu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wplusmunu", ""],
["Wmunu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wminusmunu", ""],
["Wtaunu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wplustaunu", ""],
["Wtaunu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Wmintaunu", ""],
["Wtaunu_Pw",     "PwPy8EG_AZNLOCTEQ6L1_Wminustaunu", ""],
["Zee_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Zee", ""],
["Zmumu_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Zmumu", ""],
["Ztautau_Pw",		"PwPy8EG_AZNLOCTEQ6L1_Ztautau", ""],
]


#############################
# code
#############################

is_eos = ("/eos/" in main_path)
eostype = ""
if "/eos/atlas" in main_path: eostype = "eosatlas.cern.ch"
elif "/eos/user" in main_path: eostype = "eosuser.cern.ch"
ana_dir = derivation_tag + "_" + energy_tag + "/"
prod_dir = "CxAOD_" + production_tag + "/"
full_path = main_path + ana_dir + prod_dir

def makeDir(path):
  if (is_eos) : command = "xrd eosuser.cern.ch mkdir " + path
  else :     command = "mkdir " + path
  if verbose : print command
  if not test_only : os.system(command)


if __name__ == "__main__":

  print "Going to copy files from current directory to", full_path

  # make sample dirs

  makeDir(main_path + ana_dir)
  makeDir(full_path)
  for our_sample in our_samples :
    makeDir(full_path + our_sample[0]+"/")

  # loop over subdirs in current dir

  for subdir, dirs, files in os.walk(".") :

    if verbose : print "Directory:", subdir

    # determine sample

    sample = "Unknown"
    for our_sample in our_samples :
      if not our_sample[1] in subdir : continue
      if not our_sample[2] in subdir : continue
      sample = our_sample[0]
    if sample is "Unknown":
      print "Warning: could not determine sample from directory'" + subdir + "'. Skipping."
      continue
    if verbose : print "Sample:", sample

    final_path = full_path + sample + "/" + subdir + "/"
    makeDir(final_path)

    # loop over files in current subdir and copy

    for file in files :
      file_path = subdir + "/" + file
      if verbose : print "File:", file
      if (is_eos) :
        command = "xrdcp "
        if not skip_existing : command += "--force "
        command += file_path + " root://"+eostype+"/" + final_path + file
      else :
        command = "cp "
        if skip_existing : command += "-u "
        command += file_path + " "+ final_path
      if test_only :
        if verbose : print command
      else :         os.system(command)
      
  print "Copied files from current directory to", full_path
  print "Done!"

