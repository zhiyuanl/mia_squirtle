print "Start Python"

####################################################
##### Start                                 ########
####################################################

total = len(sys.argv)
# number of arguments plus 1
if total!=3:
    print "You need some arguments, will ABORT!"
    print "Ex: ./keepOnlyGoodRuns.py GRLFileName sampleFileName"
    print "Ex: ./keepOnlyGoodRuns.py data15_13TeV.periodAllYear_DetStatus-v75-repro20-01_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml list_sample_grid.data15_13TeV_25ns_HIGG2D4_20.7_13May16.txt"
    assert(False)
# done if

GRLFileName=sys.argv[1]
sampleFileName=sys.argv[2]
debug=False
warning=True

inputFileName=sampleFileName
suffix="_OnlyGoodRuns"
outputFileName=sampleFileName+suffix

# Typical in our GRL .xml file
# <Metadata Name="RQTSVNVersion">DQDefects-00-02-02</Metadata>
# <Metadata Name="RunList">276262,276329,276336,276416,276511,276689,276778,276790,276952,276954,278880,278912,278968,279169,279259,279279,279284,279345,279515,279598,279685,279813,279867,279928,279932,279984,280231,280273,280319,280368,280423,280464,280500,280520,280614,280673,280753,280853,280862,280950,280977,281070,281074,281075,281317,281385,281411,282625,282631,282712,282784,282992,283074,283155,283270,283429,283608,283780,284006,284154,284213,284285,284420,284427,284484</Metadata>

root = ET.parse(GRLFileName).getroot()
root=root[0]
for a in root.findall('Metadata'):
    if "RunList" in a.attrib.values():
        stringGRL=a.text
if debug:
    print "stringGRL",stringGRL
listGRL=stringGRL.split(",")
if debug:
    print "listGRL",listGRL
#
outputFile = open(outputFileName, 'w')
inputFile=open(inputFileName,"r")
if debug:
    print "**** Start looping over each line ****"
for dataset in inputFile:
    if debug:
        print "dataset initial",dataset
    # first remove all the comments already from the file and start with a clean uncommented list
    dataset=dataset.replace("#","")
    if debug:
        print "dataset after remove comments",dataset
    list_element=dataset.split(".")
    runNumber=list_element[1]
    # ignore the first two elements, which are "00"
    runNumber=runNumber[2:]
    if debug:
        print "runNumber",runNumber
    if runNumber in listGRL:
        outputFile.write(dataset)
    else:
        if warning:
            print "runNumber",runNumber,"is not in list of good runs and is commented out in the .txt"
        outputFile.write("#"+dataset)
    # done if
# done for loop
        
#os.rename(inputFileName,inputFileName+".bk")
os.rename(outputFileName,inputFileName)

# done for

