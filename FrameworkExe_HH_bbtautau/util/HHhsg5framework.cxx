#include "xAODRootAccess/Init.h"
#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/ToolsDiscovery.h>
#include <SampleHandler/DiskListLocal.h>
#include <SampleHandler/DiskListXRD.h>
#include "SampleHandler/ScanDir.h"
#include <SampleHandler/Sample.h>
#include <SampleHandler/ToolsJoin.h>
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"

#include <iostream>
#include <fstream>
#include <TSystem.h> 
#include <stdlib.h> 

#include "CxAODMaker_HH_bbtautau/AnalysisBase_HH_bbtautau.h"
#include "CxAODTools/ConfigStore.h"
//#include "TupleMaker/TupleMaker.h"

#include "AsgTools/StatusCode.h"
#include "EventLoop/StatusCode.h"
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/SystematicCode.h"

#include <getopt.h>

void printUsage(std::ostream & os) {
  os << "Usage: " << std::endl;
  os << "HHhsg5framework [ options ]" << std::endl;
  os << "   Options: " << std::endl;
  os << "    " << std::setw(24) << "-h | --help : print this text" << std::endl;
  os << "    " << std::setw(24) << "-s | --sampleIn <path> : override the sampleIn value from the config file"  << std::endl;
  os << "    " << std::setw(24) << "-d | --submitDir <path> : override the submitDir value from the config file" << std::endl;
  os << "    " << std::setw(24) << "-o | --sampleOut <path> : override the sample_out value from the config file" << std::endl;
}

std::vector<std::string> split_string(const std::string &s, char delim) {
  std::stringstream ss(s);
  std::string item;
  std::vector<std::string> tokens;
  while (getline(ss, item, delim)) {
    tokens.push_back(item);
  }
  return tokens;

}

void replaceAll( std::string &s, const std::string &search, const std::string &replace ) {
    for( size_t pos = 0; ; pos += replace.length() ) {
        pos = s.find( search, pos );
        if( pos == std::string::npos ) break;
        s.erase( pos, search.length() );
        s.insert( pos, replace );
    }
}

int main(int argc, char* argv[]) {
  // flags for command line overrides
  bool overrideConfig = false;
  bool overrideInput = false;
  bool overrideSubmitDir = false;
  bool overrideSampleOut = false;
  // command line override values
  std::string configFile_override="";
  std::string sampleIn_override = "";
  std::string submitDir_override = "";
  std::string sample_out_override = "";
  static struct option long_options[] = {
    {"help",       no_argument,       0, 'h'},
    {"config",     required_argument, 0, 'c'},
    {"sampleIn",   required_argument, 0, 's'},
    {"submitDir",  required_argument, 0, 'd'},
    {"sampleOut",  required_argument, 0, 'o'},
    {0,0,0,0}
  };

  int option_index =0;
  int c = 0;
  while ( (c = getopt_long( argc, argv, "c:s:d:o:", long_options, &option_index))!=-1) {
    switch(c)
    {
      case 'h':
        printUsage(std::cout);
        return 0;
      case 'c':
        configFile_override = std::string(optarg);
        overrideConfig = true;
        break;
      case 's':
        sampleIn_override = std::string(optarg);
        overrideInput = true;
        break;
      case 'd':
        submitDir_override = std::string(optarg);
        overrideSubmitDir = true;
        break;
      case 'o':
        sample_out_override = std::string(optarg);
        overrideSampleOut = true;
        break;
      default:
        printUsage(std::cout);
        return EXIT_FAILURE;
    }
    //std::cout << "End of option loop..."  << optarg << std::endl;
  }

  // steer file has to put here: FrameworkExe/data/framework-run.cfg
  // RootCore will copy it to $ROOTCOREBIN/data/FrameworkExe/framework-run.cfg
  // ConfigStore::createStore() will prepend "$ROOTCOREBIN/" to the path given
  std::string configfile = "framework-run";
  std::string configPath = "data/FrameworkExe_HH_bbtautau/"+configfile+".cfg";
  if (overrideConfig) {
    configfile = configFile_override;
    configPath = "data/FrameworkExe_HH_bbtautau/"+configfile+".cfg";
    Info("HHhsg5framework","Reading config file from command line : %s", configPath.c_str());
  }
  
  ConfigStore* config = ConfigStore::createStore( configPath );
  if (!config) {
    Error("hsg5framework","Couldn't instantiate ConfigStore");
    return 0;
  }

  // enable failure on unchecked status codes
  bool enableFailure = false;
  config->getif<bool>("failUncheckedStatusCodes", enableFailure);
  if (enableFailure) {
    xAOD::TReturnCode::enableFailure();
    StatusCode::enableFailure();
    CP::CorrectionCode::enableFailure();
    CP::SystematicCode::enableFailure();
  }

  bool runTupleMaker = false;
  config->getif<bool>("runTupleMaker",runTupleMaker);
 
  bool officialProduction = true;
  config->getif<bool>("officialProduction",officialProduction);
  // output directory and file names (if not specified in steer file, default names are used)
  //  - output directory (submitDir)
  //  - output file name (sample_out) - in grid mode this is overridden below
  std::string submitDir  = "submitDir";
  std::string sample_out = "CxAOD"; 
  bool overWriteSubmitDir = true;
  config->getif<std::string>("submitDir",submitDir);
  config->getif<std::string>("sample_out",sample_out);
  config->getif<bool>("overWriteSubmitDir",overWriteSubmitDir);

  // overrides from command line
  if (overrideSubmitDir) {
    submitDir = submitDir_override;
    Info("hsg5framework", "Overriding submitDir : %s", submitDir.c_str());
  }

  if (overrideSampleOut) {
    sample_out = sample_out_override;
    Info("HHhsg5framework", "Overriding sample_out: %s", sample_out.c_str());
  }

  if(overWriteSubmitDir) gSystem->Exec(("rm -rf "+submitDir).c_str());

  // check that output directory does not exist (otherwise EventLoop will crash)
  if (!gSystem->AccessPathName(submitDir.c_str())) {
    Error("HHhsg5framework","Output directory already exists, please change the value of submitDir in the configuration file");
    return 0;
  }

  // input sample name
  // - local running : the path to the directory required by SampleHandler
  // - grid running  : the path to a text file containing sample names (sample_in must contain the word "grid" to activate grid running) 
  std::string sample_in  = ""; 
  sample_in = config->get<std::string>("sample_in");
  if (overrideInput) {
    sample_in = sampleIn_override;
    Info("HHhsg5framework","Overriding sampleIn : %s",sample_in.c_str());
  }

  // for grid running - contains sample names from text file
  std::vector<std::string> sample_name_in;

  // set up the job for xAOD access:
  xAOD::Init().ignore();
  
  // define if interactive or grid job, taken from 'grid' existing in name of submission text file
  // see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CxAODFramework#Grid_running
  bool grid = sample_in.find("grid") != std::string::npos;
  Info("hsg5framework","Run on grid = %s", grid ? "true" : "false");

  // instantiate SampleHandler
  SH::SampleHandler sampleHandler;
 
  // set input samples
  if (!grid) {
    std::vector<std::string> samples = split_string(sample_in, ',');  // a comma-separated list of samples can be given
    int sample_count = 0;
    for (std::string sample : samples) {  // detect the files for each sample and add them to the sample handler
      sample_count += 1;
      if (sample.find("root://") == 0) {  // check if path starts with 'root://'
        // if this is the case the sample needs to be accessed via xrootd
        // we need to pass two strings to the DiskListXRD constructor: the server and the pnfs path (directory only)
        // the xrootd path has the following form: 'root://server.tld:port/pnfs/some/path/rucio/etc/myfile.root'
        // trying to list the file directly fails, so we need to get the directory and the file name separatly
        // and restricht the sample handler to only pick up this one file and not other files which may be in this directory
        int pnfs_pos = sample.find("/pnfs");
        int last_slash_pos = sample.rfind("/");
        int len_protocol = std::string("root://").length();  // we know that the sample name starts with ''root://'
        std::string server_path = sample.substr(len_protocol, pnfs_pos - len_protocol);
        std::string pnfs_path = sample.substr(pnfs_pos, last_slash_pos - pnfs_pos);  // from /pnfs until the last slash
        std::string file_name = sample.substr(last_slash_pos + 1);  // +1 => don't include the slash in the file name
        SH::DiskListXRD list(server_path, pnfs_path);
        SH::ScanDir()                                  // the sample handler doesn't like if there are two files added which are in the same directory
            .sampleName(std::to_string(sample_count))  // because then there are two samples with the same name.
                                                       //We an rename the samples, because in the end when merging they get a new name anyways
            .filePattern(file_name)  // only pick up the requested file
            .scan(sampleHandler,list);
      } else {  // file is accessible via the local file system
        SH::DiskListLocal list(sample);
        SH::ScanDir().scan(sampleHandler,list);
      }
    }
    SH::mergeSamples(sampleHandler, sample_out, ".*");  // merge all input files to one output sample
  }
  else {
    // read input file
    
    std::ifstream infile;
    
    infile.open(sample_in);
    std::string line;
    while (!infile.eof()) {
     
      getline(infile, line);
      // don't run over commented lines
      if (line.find("#") != std::string::npos) continue;
      // ignore empty lines
      if (line.empty()) continue;
      // add sample name to vector
      if(!infile.eof()) 
	sample_name_in.push_back(line);
    }
    infile.close();
    // check if text file contains sample names
    if ( sample_name_in.size() == 0) {
      Info("hsg5framework","No samples specified in text file!");
      return 0;
    }
    // declare the jobs
    const int n_sample_name_in = static_cast<int>(sample_name_in.size());
    for (int isam = 0; isam < n_sample_name_in; ++isam) {
      SH::scanRucio (sampleHandler, sample_name_in[isam]);
      std::cout << "sample -> " << sample_name_in[isam] << std::endl;
    }
  }
  // Set the name of the input TTree. It's always "CollectionTree" for xAOD files.
  sampleHandler.setMetaString("nc_tree", "CollectionTree");

  // print what we found:
  sampleHandler.print();

  // create an EventLoop job:
  EL::Job job;
  job.sampleHandler(sampleHandler);

  // limit number of events to maxEvents - set in config
  int eventMax = -1;
  config->getif<int>("maxEvents",eventMax);
  job.options()->setDouble (EL::Job::optMaxEvents, eventMax);
  //job.options()->setDouble (EL::Job::optSkipEvents, 40400);
  // Set the xAOD access mode of the job:
  job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class );///changed from branch

  // setup CxAODMaker
  if(!runTupleMaker){
   AnalysisBase_HH_bbtautau* algorithm = new AnalysisBase_HH_bbtautau();
   //algorithm->m_maxEvent = static_cast<int>(job.options()->getDouble(EL::Job::optMaxEvents));
   algorithm->m_maxEvent = static_cast<int>(job.options()->castDouble(EL::Job::optMaxEvents));
   algorithm->setConfig(config);
 
   job.algsAdd(algorithm);
  }

  // setup TupleMaker
  bool addBDTvars = false;
  config->getif<bool>("addBDTvars",addBDTvars);
  if (runTupleMaker) {
    // job.options()->setString( EL::Job::optXaodAccessMode, EL::Job::optXaodAccessMode_class ); // TJS remove
    // TupleMaker* tupleMaker = new TupleMaker();
    // tupleMaker->configure(config);
    // job.algsAdd(tupleMaker);
  }
  
  // run the job using the direct driver (local) or the prun driver (grid)
  if (!grid) {
    EL::Driver* eldriver = 0;
    std::string driver = "direct";
    config->getif<std::string>("driver", driver);
    if (driver == "direct")
      eldriver = new EL::DirectDriver;
    else {
      Error("hsg5framework", "Unknown driver '%s'", driver.c_str());
      return 0;
    }
    eldriver->submit(job, submitDir);
  }
  else { 
    EL::PrunDriver driver;
    // determine ana_tag name
    std::string ana_tag = "ana_tag"; // default value if it cannot be determined below
    bool ana_tag_flag = false;
    config->getif<bool>("autoDetermineSelection",ana_tag_flag);
    if (!ana_tag_flag) {
      std::string selectionName = "";
      config->getif<std::string>("selectionName",selectionName);
      if      (selectionName == "lephad") ana_tag = "HIGG4D2";
      else if (selectionName == "hadhad") ana_tag = "HIGG4D3";
      else if (selectionName == "leplep") ana_tag = "HIGG4D1";
    }
    else {
      if      (sample_name_in[0].find("HIGG4D2") != std::string::npos) ana_tag = "HIGG4D2";
      else if (sample_name_in[0].find("HIGG4D3") != std::string::npos) ana_tag = "HIGG4D3";
      else if (sample_name_in[0].find("HIGG4D1") != std::string::npos) ana_tag = "HIGG4D1";
    }
    // form needed names
    std::string vtag = "vtag"; // default in case it is not specified in steer file
    config->getif<std::string>("vtag",vtag);
    
    bool haveOfficialSample = false;
    bool haveGroupSample = false;
    for (std::string name : sample_name_in ) {
      if (name.find("group.") != std::string::npos ||
          name.find("user.") != std::string::npos) {
        haveGroupSample = true;
      } else {
        haveOfficialSample = true;
      }
    }
    
    if (haveGroupSample && haveOfficialSample) {
      Error("hsg5framework","Found official and group production samples in input files! Cannot determine output sample names.");
      return EXIT_FAILURE;
    }
    
    // position of tag "mc14_13TeV" or similar in input sample
    // 1 = beginning (for official production)
    // 3 = for group/user production
    int tag_offset = 1;
    if (haveGroupSample) {
      tag_offset = 3;
    }

    std::ostringstream ostr_sample_out_begin,fullname;
    fullname << "%in:name%";
    
    if(officialProduction) { // use group name for officialProduction
      Info("HHhsg5framework","Configuring for official production");
      ostr_sample_out_begin << "group.phys-hdbs.%in:name[" << tag_offset + 0 << "]%.%in:name[" << tag_offset + 1 << "]%.";
    } else { // use user name for non-official production
      ostr_sample_out_begin << "user.%nickname%.%in:name[" << tag_offset + 0 << "]%.%in:name[" << tag_offset + 1 << "]%.";
    }

    std::string sample_out_begin = ostr_sample_out_begin.str();
    std::ostringstream ostr_sample_out_end;
    ostr_sample_out_end << "." << ana_tag << "." << vtag;

    std::string sample_out_end = ostr_sample_out_end.str();
  
    for (unsigned int isam = 0; isam < sample_name_in.size(); ++isam)
    {
       // find the proper part of the string to prune
       std::string sample_out_name2 = "";
       std::string str = sample_name_in[isam];
       int count_dots = 0;
       for(unsigned int i = 0 ; i < str.length(); i++) {
         if ( str[i]=='.' ) count_dots++;
         else if (count_dots==(tag_offset+1) ) sample_out_name2 += str[i];
       }
       // prune generators
       std::string sample_out_prune = sample_out_name2;
       replaceAll(sample_out_prune,"ParticleGenerator","PG");
       replaceAll(sample_out_prune,"Pythia","Py");
       replaceAll(sample_out_prune,"Powheg","Pw");
       replaceAll(sample_out_prune,"MadGraph","MG");
       replaceAll(sample_out_prune,"EvtGen","EG");
       replaceAll(sample_out_prune,"Sherpa","Sh");
      
       std::string fullName = sample_out_begin+sample_out_prune+sample_out_end;
            
       sampleHandler.setMetaString (".*"+sample_out_name2+".*", "nc_outputSampleName", fullName);
      
       
    }

    // Overwrite naming with short naming scheme that include all AMI-tags
    // No need to define different vtags for different MC campaigns or fullsim / fastsim
    bool gridNameWithTags = false;
    config->getif<bool>("gridNameWithTags", gridNameWithTags);
    if (gridNameWithTags) {
      std::ostringstream ostr_name;

      if (officialProduction) {
        ostr_name << "group.phys-hdbs";
      } else {
        ostr_name << "user.%nickname%";
      }

      ostr_name << ".%in:name[" << tag_offset + 0 << "]%";  // mc16_13TeV
      ostr_name << ".%in:name[" << tag_offset + 1 << "]%";  // DSID
      ostr_name << "." << ana_tag;                          // HIGG4Dx depending on channel
      ostr_name << ".%in:name[" << tag_offset + 5 << "]%";  // AMI-tags
      ostr_name << "." << vtag;                             // Tag specified in config

      sampleHandler.setMetaString("nc_outputSampleName", ostr_name.str());
    }

    if( officialProduction) { // for official production - set the official command line option
      driver.options()->setString(EL::Job::optSubmitFlags, "--official --voms=atlas:/atlas/phys-hdbs/Role=production"); //skipScout is temporary
      //	driver.options()->setString(EL::Job::optSubmitFlags, "--official --voms=atlas:/atlas/phys-higgs/Role=production");
      }

      //driver.options()->setString(EL::Job::optSubmitFlags, "--skipScout");
      // other options
      double nFilesPerJob = -1.;
      config->getif<double>("nFilesPerJob",nFilesPerJob);
      if (nFilesPerJob > 0) driver.options()->setDouble("nc_nFilesPerJob", nFilesPerJob);
      //
      double nGBPerJob = -1.;
      config->getif<double>("nGBPerJob",nGBPerJob);
      if (nGBPerJob > 0) {
        if (nGBPerJob == 1000.) driver.options()->setString("nc_nGBPerJob", "MAX");
        else driver.options()->setDouble("nc_nGBPerJob", nGBPerJob);
      }
      //
      std::string excludedSite = "none";
      config->getif<std::string>("excludedSite",excludedSite);
      if (excludedSite != "none") driver.options()->setString("nc_excludedSite", excludedSite);
      //
      bool submit = false;
      config->getif<bool>("submit",submit);
      if (!submit) {
        driver.options()->setDouble("nc_noSubmit", 1);
        driver.options()->setDouble("nc_showCmd", 1);
      }
      driver.options()->setString("nc_mergeOutput", "true");

      std::string destSE = "none";
      config->getif<std::string>("destSE", destSE);
      if (destSE != "none") {
        driver.options()->setString("nc_destSE", destSE);
      }

      // run
      //driver.submit(job, submitDir); // with monitoring
      driver.submitOnly(job, submitDir); // no monitoring
  }

  return 0;

}

   
