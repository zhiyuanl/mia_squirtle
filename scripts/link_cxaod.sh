#!/usr/bin/env bash
set -euo pipefail
#set -x

# Avoid iteration when no matches are found
shopt -s nullglob

function usage {
    echo "link_cxaod.sh INDIR OUTDIR"
    echo "    INDIR: Directory containing CxAODs"
    echo "    OUTDIR: Directory with CxAODReader / MIA structure"
}

if [[ "$#" -ne 2 ]];
then
    usage
    exit 1
fi

function get_cxaod_dir {
    DIRNAME="$1"
    DSID="$(basename ${DIRNAME} | cut -d'.' -f 4)"
    CXAOD_DIR=$(cxaod_dir.py ${DSID})
    echo ${CXAOD_DIR}
}

INDIR="$1"
OUTDIR="$2"

# data 15/16
mkdir -p "${OUTDIR}/15_16/data"
for CXAOD in ${INDIR}/*.data1[56]_13TeV.*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    LINK="${OUTDIR}/15_16/data/$(basename ${CXAOD})"
    ln -s "${TARGET}" "${LINK}"
done

# data 17
mkdir -p "${OUTDIR}/17/data"
for CXAOD in ${INDIR}/*.data17_13TeV.*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    LINK="${OUTDIR}/17/data/$(basename ${CXAOD})"
    ln -s "${TARGET}" "${LINK}"
done

# data 18
mkdir -p "${OUTDIR}/18/data"
for CXAOD in ${INDIR}/*.data18_13TeV.*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    LINK="${OUTDIR}/18/data/$(basename ${CXAOD})"
    ln -s "${TARGET}" "${LINK}"
done

# mc16a
for CXAOD in ${INDIR}/*.mc16_13TeV.*.bbtt_h[lh]_A*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    CXAOD_DIR=$(get_cxaod_dir "${CXAOD}")
    LINK="${OUTDIR}/15_16/${CXAOD_DIR}/$(basename ${CXAOD})"

    mkdir -p "${OUTDIR}/15_16/${CXAOD_DIR}"
    ln -s "${TARGET}" "${LINK}"
done

# mc16d
mkdir -p "${OUTDIR}/17"
for CXAOD in ${INDIR}/*.mc16_13TeV.*.bbtt_h[lh]_D*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    CXAOD_DIR=$(get_cxaod_dir "${CXAOD}")
    LINK="${OUTDIR}/17/${CXAOD_DIR}/$(basename ${CXAOD})"

    mkdir -p "${OUTDIR}/17/${CXAOD_DIR}"
    ln -s "${TARGET}" "${LINK}"
done

# mc16e
mkdir -p "${OUTDIR}/18"
for CXAOD in ${INDIR}/*.mc16_13TeV.*.bbtt_h[lh]_E*_CxAOD.root;
do
    TARGET="$(readlink -e ${CXAOD})"
    CXAOD_DIR=$(get_cxaod_dir "${CXAOD}")
    LINK="${OUTDIR}/18/${CXAOD_DIR}/$(basename ${CXAOD})"

    mkdir -p "${OUTDIR}/18/${CXAOD_DIR}"
    ln -s "${TARGET}" "${LINK}"
done
