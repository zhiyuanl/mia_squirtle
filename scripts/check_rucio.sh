#!/usr/bin/env bash
set -euo pipefail

while read line; do
    rucio_result=$(rucio ls --short "${line}" | cut -d":" -f 2)

    # Check if exists on rucio
    if [[ -z "${rucio_result}" ]]; then
        echo "Not found: ${line}" 1>&2
        continue
    fi

    # Check if not empty
    nfiles=$(rucio list-files "${rucio_result}" | grep "Total files" | grep -o "[0-9]\+")
    if (( ${nfiles} <= 0 )); then
        echo "Empty: ${line}" 1>&2
        continue
    fi

    echo "${rucio_result}"
done
