#!/usr/bin/env python
import argparse
import logging
import re
import sys


logging.basicConfig(
    format="%(name)-25s %(levelname)-10s %(message)s",
    level=logging.INFO)
log = logging.getLogger(__name__)


def main(args):
    log.debug("Args: " + repr(args))

    try:
        from rucio.client import Client
    except ImportError:
        log.error("Cannot import rucio. Do 'lsetup rucio' and 'voms-proxy-init'"
                  " first!");
        sys.exit(1)

    rucio = Client()
    rucio.ping()


    scope = "mc16_13TeV"

    # AMI-Tag pattern (exactly 3 AMI tags)
    pattern_amitag = re.compile(r"(e\d*)_((?:a|s)\d*)_(r\d*)")
    allowed_rtags = {"r9364", "r10201", "r10724"}


    with open(args.infile, "r") as infile, open(args.outfile, "w") as outfile:
        for dsid in infile:
            # Remove comments & leading / trailing whitespace
            dsid = re.sub(r"#.*$", "", dsid)
            dsid = dsid.strip()

            # Skip empty lines
            if not dsid:
                continue

            log.debug("Processing DSID: " + dsid)
            search_string = scope + ".{dsid}.*.recon.AOD.*".format(dsid=dsid)
            search_result = rucio.list_dids(
                scope, {"name": search_string}, type="container")

            search_result = list(search_result)

            aods = []
            for dataset in search_result:
                ami_tag = dataset.split(".")[-1]

                m = pattern_amitag.match(ami_tag)
                if m:
                    etag, stag, rtag = m.groups()
                    if rtag not in allowed_rtags:
                        continue

                    nfiles = len(list(rucio.list_files(scope, dataset)))

                    log.debug("Found: " + dataset)
                    log.debug("Number of files: " + str(nfiles))

                    if not nfiles:
                        log.warn("No files for dataset: " + dataset)
                        continue

                    aods.append(dataset)

            if not len(aods):
                log.warn("DSID {} not found".format(dsid))

            for aod in aods:
                log.debug("Writing out: " + aod)
                outfile.write(aod + "\n")

            outfile.write("\n")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Accepts a file containing MC DSIDs and retrieves available AODs "
                    "for MC16A/D/E campaigns")
    parser.add_argument("infile",
                        help="File containing MC DSIDs")
    parser.add_argument("outfile",
                        help="Output file with logical dataset names")
    args = parser.parse_args()

    main(args)
