#!/usr/bin/env python
import argparse
import logging
import re
import sys


logging.basicConfig(
    format="%(name)-25s %(levelname)-10s %(message)s",
    level=logging.INFO)
log = logging.getLogger(__name__)


def main(args):
    log.debug("Args: " + repr(args))

    try:
        from rucio.client import Client
    except ImportError:
        log.error("Cannot import rucio. Do 'lsetup rucio' and 'voms-proxy-init'"
                  " first!");
        sys.exit(1)

    rucio = Client()
    rucio.ping()

    # AMI-Tag pattern (exactly 4 AMI tags)
    pattern_amitag = re.compile(r"(e\d*)_((?:a|s)\d*)_(r\d*)_(p\d*)")

    # Allowed AMI tags
    allowed_ptags = set(args.ptags) if args.ptags is not None else None

    with open(args.infile, "r") as infile, open(args.outfile, "w") as outfile:
        for aod in infile:
            # Remove comments & leading / trailing whitespace
            aod = re.sub(r"#.*$", "", aod)
            aod = aod.strip()

            # Skip empty lines
            if not aod:
                continue

            log.debug("Processing AOD: " + aod)

            # Get DSID and AMI-tags
            scope, dsid, phys_short, step, datatype, ami_tag = aod.split(".")

            search_string = ".".join([scope, dsid, phys_short, "deriv", "DAOD_{deriv}", ami_tag + "_p*"])
            search_string = search_string.format(deriv=args.derivation)
            search_result = rucio.list_dids(scope, {"name": search_string},
                                            type="container")

            # Filter search results
            derivations = []
            for dataset in search_result:
                m = pattern_amitag.match(dataset.split(".")[-1])
                if not m:
                    continue

                etag, stag, rtag, ptag = m.groups()
                if allowed_ptags is not None and ptag not in allowed_ptags:
                    continue

                # Tuple for easy sorting by latest ptag
                ptag_int = int(ptag[1:])
                derivations.append((ptag_int, dataset))

                log.debug("Got match " + dataset)

            if not len(derivations):
                log.warn("No results for AOD: " + aod)
                continue

            # Sort by latest ptag first
            derivations = sorted(derivations, reverse=True)
            log.debug("Candidates: " + repr(derivations))

            latest = derivations[0][1]
            log.debug("Latest: " + repr(latest))

            outfile.write(latest + "\n")



if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Write out latest p-tag for a given derivation / AOD.")
    parser.add_argument("infile",
                        help="File containing AOD names.")
    parser.add_argument("outfile",
                        help="Output file with logical dataset names.")
    parser.add_argument("--derivation", default="HIGG4D3",
                        help="Name of derivation to search for.")
    parser.add_argument("--ptags", nargs="+", default=None,
                        help="P-tag filter. If not given all p-tags are allowed.")
    args = parser.parse_args()

    main(args)
